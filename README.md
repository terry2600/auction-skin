This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Бэкенд - папка 'server', Фронтенд - папка 'client'

## Available Scripts

### React

### `npm run start`

Запускает приложение в режиме разработчика.<br>
Откройте [http://localhost:3006](http://localhost:3006), чтобы посмотреть в браузере.<br>
Чтобы поменять порт, неообходимо зайти в `package.json` и у "scripts"."start" установить значение порта на необходиомое.

Страница будет перезагружена, если внести изменения в коде.<br>
В консоли отобразятся все возникшие ошибки.

### `npm run build`

Создает версию приложения для продакшена в папке `build`.<br>


Посмотрите секцию [deployment](https://facebook.github.io/create-react-app/docs/deployment) для получения более точной информации.

### Express app
### `npm run server`

Поднимает экспрессовский сервер.<br>
Создает таблицы в бд и заполняет их необходимыми данными

### `npm run restart`

Перезапускает экспрессовский сервер.<br>

## Переменные среды
В `server/config/constants.js` определены стандартные значения переменных среды<br>
В `client/config/constants.js` определены значения переменных сред для клиента<br>
В случае если установлена переменная среды с таким же названием, то приоритет будет у неё<br><br>

# Сервер
SERVER_PORT: 9004, - порт сервера<br>
SERVER_URL: 'http://auction.ad.gtbrain.ru:9004', - внешний адрес сервера<br>
REDIS_URL: 'localhost:6379', - адрес сервера Redis<br>
PASSPORT_API_KEY: 'BC3B7D8E8F336D486512DB77A66BDC03', - апи ключ авторизации в стим<br>
SOCKET_SECRET_KEY: 'tmnttmnt', - секретный ключ для сокетов<br>
DB_HOST: 'localhost', - хост бд<br>
DB_PORT: '5432', - порт бд<br>
DB_NAME: 'auction', - название продакшн базы данных<br>
DB_NAME_TEST: 'auction_test', - название базы данных для прогонки тестов<br>
DB_USERNAME: 'auc_admin', - пользователь бд<br>
DB_PASSWORD: 'tmnt', - пароль пользователя бд<br>
CLIENT_HOME_PAGE_URL: 'http://auction.ad.gtbrain.ru:3007', - задает юрл, на который пользователеь будет переброшен после успешной аутентификации<br>
FAILURE_REDIRECT_URL: 'https://gaycity.love/', - задает значение URL, на который пользователь будет переброшен после неуспешной аутентификации<br>
STEAM_IMAGE_URL: 'http://cdn.steamcommunity.com/economy/image/', - начало юрл изображнеия стим-предмета<br>
TIME_SERVICE_URL: 'http://auction.ad.gtbrain.ru:5004', - урл таймсервиса<br>
TIME_SERVICE_SECRET: 'Fuck you anonymous bitch!', - секрет при общении с таймсервисом<br>
TRADE_SERVICE_URL: 'http://auction.ad.gtbrain.ru:6004', - урл трейдсервиса<br>
TRADE_SERVICE_SECRET: 'Fuck you anonymous bitch! (2)', - секрет при общении с трейдсервисом<br>
PAYMENT_SERVICE_URL: 'http://auction.ad.gtbrain.ru:4004', - урл пейментсервиса<br>
PAYMENT_SERVICE_SECRET: 'Fuck you anonymous bitch! (3)', - секрет при общении с пейментсервисом<br>
BID_TIME: 10000 - время, отведенное для ставки<br>
<br>
# Клиент
EXTENSION_ID: 'fmadghckfohbhafcdmigiddopfbpcope', - ID расширения<br>
BACKEND_URL: 'http://auction.ad.gtbrain.ru:9004/' - URL бэкэнда (для socket.io)<br>

Переменные для ботов находятся в `server/config/bots-config.json`,
а сам класс ботов описан в `server/config/bots.js`


## Схема БД (16.01.2020)
https://drive.google.com/file/d/1FnOg2CTGJ5BfmCr6WOsZR2aBpSZBUDAn/view?usp=sharing
