import React from 'react';
import Button from './Button';

export default ({id, heading, value, buttonText, click, change}) => (
    <div className='profile-info'>
        <div className="profile-info-block">
            <div className='profile-info__heading'>{heading}</div>
            <div id={id} className='profile-info__value'>{value}</div>
        </div>
        {buttonText ? (
            <Button text={buttonText}
                    type='button_small button_lightblue button_transparent'
                    onClick={click}
                    style={{marginLeft: 'auto'}}
            />
        ) : null
        }
    </div>
)
