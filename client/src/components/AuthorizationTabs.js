import React from "react";
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import Button from './Button';
import CheckBox from './CheckBox';
import SocialsIcon from './SocialsIcon';
import {Link} from 'react-router-dom';

export default ({openSteam}) => (
    <Tabs>
        <TabList className='authorization-tab-list'>
            <Tab className='authorization-tab-list__item'>Войти</Tab>
            <Tab className='authorization-tab-list__item'>Регистрация</Tab>
        </TabList>
        <div className='authorization-tab-panels'>
            <TabPanel className='authorization-tab-panel'>
                <Button text='Вход через Steam'
                        type='button_steam button_transparent'
                        onClick={openSteam}
                />
                <div className='authorization-tab-panel__or'>или по логину в системе</div>
                <form>
                    <input type='email'
                           className='authorization-tab__email'
                           placeholder='Почта'/>
                    <input type='password'
                           className='authorization-tab__password'
                           placeholder='Пароль'/>
                    <div className='authorization-tab-panel__login'>
                        <CheckBox label='Оставаться в системе'/>
                        <button className='authorization__button'>Войти</button>
                    </div>
                </form>
                <Link to='https://avatanplus.com/files/resources/mid/5cc69df8beb8316a67d913cd.png' className='authorization__forgot'>Забыли пароль?</Link>
                <div className="authorization__dash"/>
                <div className='authorization-socials'>
                    <div className='authorization-socials__text'>Вход с помощью:</div>
                    <div className='authorization-socials__icons'>
                        <SocialsIcon name='odnoklassniki'/>
                        <SocialsIcon name='vk'/>
                        <SocialsIcon name='facebook'/>
                        <SocialsIcon name='twitter'/>
                        <SocialsIcon name='google'/>
                    </div>
                </div>
                <div className="authorization-tab-panel__mark">
                    Если вы входите через Steam и не зарегистрированны на сайте,
                    то вы будете зарегистрированны и принимаете
                    <Link to='/profile' className='authorization-tab-panel__mark-link'>Условия пользовательского соглашения</Link> и
                    <Link to='/profile' className='authorization-tab-panel__mark-link'>Политику конфиденциальности</Link>
                </div>

            </TabPanel>
            <TabPanel className='authorization-tab-panel'>
                <Button text='Вход через Steam'
                        type='button_steam button_transparent'/>
                <div className='authorization-tab-panel__or'>или по логину в системе</div>
                <form>
                    <input type='email'
                           className='authorization-tab__email'
                           placeholder='Почта'/>
                    <input type='password'
                           className='authorization-tab__password'
                           placeholder='Пароль'/>
                    <input type='password'
                           className='authorization-tab__password'
                           placeholder='Повторите пароль'/>
                    <div className='authorization-tab-panel__login'>
                        <CheckBox label='Оставаться в системе'/>
                        <button className='authorization__button'>Регистрация</button>
                    </div>
                </form>
                <div className="authorization__dash"/>
                <div className='authorization-socials'>
                    <div className='authorization-socials__text'>Регистрация с помощью:</div>
                    <div className='authorization-socials__icons'>
                        <SocialsIcon name='odnoklassniki'/>
                        <SocialsIcon name='vk'/>
                        <SocialsIcon name='facebook'/>
                        <SocialsIcon name='twitter'/>
                        <SocialsIcon name='google'/>
                    </div>
                </div>
                <div className="authorization-tab-panel__mark">
                    Если вы входите через Steam и не зарегистрированны на сайте,
                    то вы будете зарегистрированны и принимаете
                    <Link to='/profile' className='authorization-tab-panel__mark-link'>Условия пользовательского соглашения</Link> и
                    <Link to='/profile' className='authorization-tab-panel__mark-link'>Политику конфиденциальности</Link>
                </div>
            </TabPanel>
        </div>
    </Tabs>
)