import React from "react";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import FAQAccordeons from './FAQAccordeons'
import '../App.css';

export default ({tabs}) => (
    <Tabs className='faq-tabs' selectedIndex={0} onSelect={() => null}>
        <TabList className='faq-tab-list'>
            {tabs.map(tab => (
              <Tab className='tab-pill' key={tab.id} >{tab.title}</Tab>
            ))}
        </TabList>
        <div className="faq-tab-panels">
            {tabs.map(tab => (
                <TabPanel className='faq-tab-panel' key={tab.id}>
                    <FAQAccordeons faqs={tab.Faqs} />
                </TabPanel>
            ))}
        </div>
    </Tabs>
)
