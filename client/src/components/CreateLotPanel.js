import React from 'react';
import {getRarity} from '../functions/getRarityColor';
import InventoryItemCs from './InventoryItemCs';
import InventoryItemDota from "./InventoryItemDota";
import DribbleButton from "./DribbleButton";
import ScrollContainer from 'react-indiana-drag-scroll';
import Loader from "./Loader";
import CreationWarning from "./CreationWarning";

export default ({
                    tabIndex, inventory, activeId, searchText, priceText, participantsText, priceTextError,
                    participantsTextError, selectedItemError, isFetching, isSending, creationNotificationItem, creationNotification,
                    selectItem, handleSearchChange, handlePriceChange, handleParticipantsChange, submitLot, resetAfterCreation
                }) => (
    <form onSubmit={submitLot} className='form_create-lot'>
        <div className='inventory-filter'>
            <div className='filter-search-container'>
                <input
                    value={searchText}
                    onChange={handleSearchChange}
                    placeholder='Начните вводить название'
                    type='text'
                    className='filter__search filter__search_create'
                />
            </div>
            <ScrollContainer className='scrollbar-container_custom scrollbar-container_inventory'>
                <div className='inventory-list'>
                    {isFetching || isSending ? (
                        <Loader/>
                    ) : (
                        tabIndex !== 1 ? (
                            inventory.map(item => (
                                <InventoryItemCs
                                    key={item.id}
                                    id={item.id}
                                    img={item.icon_url}
                                    activeId={activeId}
                                    onClick={selectItem}
                                    item={item}
                                    wear={item.tags[4] !== null ? getRarity(item.tags[4]) : "gray"}
                                    name={item.market_name}/>
                            ))
                        ) : (
                            inventory.map(item => (
                                <InventoryItemDota
                                    key={item.id}
                                    item={item}
                                    activeId={activeId}
                                    onClick={selectItem}
                                />
                            ))
                        )
                    )}
                </div>
            </ScrollContainer>
            {selectedItemError && <span className='text__error'>* Выберите один предмет интвентаря</span>}
        </div>
        {creationNotification.active ? (
            <div className='create-lot-notification'>
                <CreationWarning
                    itemIcon={creationNotificationItem.icon_url}
                    color={creationNotification.type === 'error' ? '#ff0059' : '#5db936'}
                    title={creationNotification.title}
                    text={creationNotification.text}
                    icon={creationNotification.type === 'error' ?
                        require('../assets/img/red-warn.png') :
                        require('../assets/img/green-warn.png')}
                    buttonText={creationNotification.type === 'error' ? 'Установить расширение' : 'Создать другой лот'}
                    onButtonClick={resetAfterCreation}
                />
            </div>
        ) : (
            <div className='auction-filter'>
                <div className='auction-filter-range-pickers'>
                    <label className='input_create-lot filter__subheading'>
                        Стоимость лота
                        <input
                            value={priceText}
                            onChange={handlePriceChange}
                            placeholder='0'
                            type='text'
                            className={priceTextError ? 'range__input input__error' : 'range__input'}
                        />
                    </label>
                    <label className='input_create-lot filter__subheading'>
                        Количество участников
                        <input
                            value={participantsText}
                            onChange={handleParticipantsChange}
                            placeholder='0'
                            type='text'
                            className={participantsTextError ? 'range__input input__error' : 'range__input'}
                        />
                    </label>
                </div>
                <DribbleButton type='submit' text='Разместить лот'/>
            </div>
        )}
    </form>
)

