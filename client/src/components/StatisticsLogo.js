import React from 'react';
import '../App.css';

export default ({img, color}) => (

    <div className={color === 'violet' ?
                        'statistics-item__circle statistics-item__circle_violet' :
                    color === 'orange' ?
                        'statistics-item__circle statistics-item__circle_orange':
                        'statistics-item__circle statistics-item__circle_red'}>
        <img className='statistics-item__img' src={img} alt='Фото'/>
    </div>
)