import React from "react";
import {Scrollbars} from "react-custom-scrollbars";
import CircleImg from "./CircleImg";
import ListItem from "./LastBidItem";

export default ({participants}) => (
    <React.Fragment>
        <div className="popup__heading">Список участников
            <span
                className='popup-participants__val'>{participants.length}</span>
        </div>
        <div className="popup-participant-list">
            <Scrollbars renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}>
                {participants.map(user => (
                    <div key={user.id} className="popup-participants-list__item">
                        <CircleImg
                            width='36px'
                            height='36px'
                            avatar={user.avatar}
                        />
                        <ListItem
                            user={user.steam_nick}
                            bid='324 Р'
                            paddingLeft='24px'/>
                    </div>
                ))}
            </Scrollbars>
        </div>
    </React.Fragment>
)