import React from "react";
import NotificationItem from "./NotificationItem";

export default class NotificationList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            notification: []
        }
    }

    componentDidMount() {
        this.props.socket.on('notification', ({id, type_id, user_id, body}) => {
            this.setState(prevState => {
                let text = '';
                let title = '';
                let type = '';

                switch (type_id) {
                    case 1:
                        if (user_id === this.props.userId) {
                            title = 'Ваш лот разыгран';
                        } else {
                            title = 'Лот разыгран';
                        }
                        text = `Предмет '${body.item_name}' был выкуплен за ${body.purchase_price}руб`;
                        type = 'success';
                        break;
                    case 2:
                        title = 'Лот был отменен';
                        text = body.item_name;
                        type = 'warning';
                        break;
                    case 3:
                        title = 'Баланс пополнен';
                        text = `${body.amount} ${body.currency}`;
                        type = 'success';
                        break;
                    case 4:
                        title = 'Баланс не пополнен';
                        text = `${body.amount} ${body.currency}`;
                        type = 'warning';
                        break;
                    case 5:
                        title = 'Заявка на вывод средств';
                        text = 'Исполнено';
                        type = 'success';
                        break;
                    case 6:
                        title = 'Заявка на вывод средств';
                        text = 'Не исполнено';
                        type = 'warning';
                        break;
                    case 7:
                        title = 'Заявка на вывод средств';
                        text = 'Не исполнено';
                        type = 'warning';
                        break;
                }

                return {
                    notification: [...prevState.notification, {id, title, text, type}]
                }
            }, () => {
                setTimeout(() => {
                    this.setState(prevState => {
                        const notification = prevState.notification.filter(notification => notification.id !== id);

                        return {notification}
                    })
                }, 5000)
            })

        })
    }

    closeNotification = (id) => {
        this.setState(prevState => {
            const notification = prevState.notification.filter(notification => notification.id !== id);

            return {notification}
        })
    };

    render() {
        let {notification} = this.state;
        return (
            <React.Fragment>
                <div className='notification-list'>
                    {notification.map(item => (
                        <NotificationItem
                            key={item.id}
                            title={item.title}
                            text={item.text}
                            type={item.type}
                            onClick={this.closeNotification}
                        />
                    ))}
                </div>
            </React.Fragment>
        )
    }
}