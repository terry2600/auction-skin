import React from "react";
import Gear from "./Gear";
import ReactTooltip from "react-tooltip";

export default ({ item, activeId, onClick }) => (
    <div
        className={activeId === item.id ? 'item-img-container selected' : 'item-img-container'}
        onClick={() => onClick(item, item.id)}>
        <Gear/>
        <ReactTooltip id={item.market_name} type='light' effect='float' place="top">
            <span>{item.market_name}</span>
        </ReactTooltip>
        <img data-tip data-for={item.market_name}
             src={item.icon_url}
             className='item-img'
             alt='Изображение предмета'
        />
    </div>
)