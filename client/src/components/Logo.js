import React from 'react';
import {Link} from 'react-router-dom';
import {UserContext} from '../App';
import PopupCreateLot from "../containters/PopupCreateLot";
import logo from '../assets/img//logo.png';
import '../App.css';
import CurrentGames from "./HeaderTabs/CurrentGames";
import Games from "./HeaderTabs/Games";
import Can from './Can';

export default ({games, selectedGame, toggleGame, checkExtension, checkTradeBan}) => (
    <UserContext.Consumer>
        {({steamId, role}) => (
            <div className='logo-container'>
                <div>
                    <Link to='/' className='logo__link'>
                        <img src={logo} style={{height: '18px'}} alt='Лого'/>
                    </Link>
                </div>
                <div style={{display: "flex", justifyContent: "space-between", width: "100%", margin: '15px'}}>
                    {window.innerWidth >= 580? <CurrentGames games={games} selectedGame={selectedGame} toggleGame={toggleGame}/>:''}
                    <Can
                        role={role}
                        resource='lots'
                        action='create:own'
                        yes={() => (
                            <React.Fragment>
                                <div className='logo-items'>
                                    <Link to={'/userlots'}>
                                        <Games games={"48"}/>
                                    </Link>
                                </div>
                                <div className='logo-items'>
                                    <PopupCreateLot
                                        steamId={steamId}
                                        checkExtension={checkExtension}
                                        checkTradeBan={checkTradeBan}/>
                                </div>
                            </React.Fragment>
                        )}
                        no={() => null}
                    />
                </div>
            </div>
        )}
    </UserContext.Consumer>
)