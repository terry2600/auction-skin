import React from 'react';
import CurveTriangle from "./CurveTriangle";
import '../App.css';

const Tooltip = ({options, activeOption, sort = false, navigation = false, isOpened, triangleWidth, triangleHeight, change}) => (
    <div className={isOpened ? `tooltip-container open` : `tooltip-container`}>
        <CurveTriangle width={triangleWidth} height={triangleHeight} strokeColor='#243754' fillColor='#243754'/>
        <div className='modal-wrapper'>
            <div className="modal-content">
                {navigation ? (
                    options.map((item, index) => (
                        <a key={index} href={item.url} className='profile-option'>{item.label}</a>
                    ))
                ) : (options.map((item, index) => (
                        <div className={activeOption === index ? 'filter__item active' : 'filter__item'} key={index}
                             onClick={() => change(item)}>
                            {item.label}
                            {sort ? <span className={`stairs stairs_${item.sortValue}`}/> : null}
                        </div>
                    ))
                )}
            </div>
        </div>
    </div>
);

export default Tooltip;