import React from "react";
import Date from "./Date/Date";
import {Scrollbars} from "react-custom-scrollbars";

export default ({transactions})=>(
    <div className="transactions-table">
        <Scrollbars style={{height:400}}>
        <table className=" popup-table table transactions">
            <thead>
            <tr className='table__row'>
                <th className='table__heading'><div className="trns">Дата</div></th>
                <th className='table__heading'><div className="trns">Лот</div></th>
                <th className='table__heading'><div className="trns">Получил/Отдал</div></th>
                <th className='table__heading left'><div className="trns">Статус</div></th>
            </tr>
            </thead>
            <tbody>
            {transactions.map((item, index) => (
                <tr className='table__row' key={index}>
                    <td className='table__item '><div className="trns" style={{color:'white'}}><Date date={item.date} type={'full'}/></div></td>
                    <td className='table__item '><div className="trns">{item.name}</div></td>
                    <td className='table__item '><div className="trns">{item.got ? 'Получил' : 'Отдал'}</div></td>
                    <td className='table__item '><div className="trns">{item.status === 5 ? 'Отмена' : 'Успех'}</div></td>
                </tr>
            ))}
            </tbody>
        </table>
    </Scrollbars>
    </div>
)
