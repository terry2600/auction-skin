import React from "react";

export default ({img, text}) => (
    <div className='tab-pill_create-content'>
        <img
            className='tab-pill-create__img'
            src={img}
            alt='Изображение игры'
        />
        <div className='tab-pill_create__text'>{text}</div>
    </div>
)

