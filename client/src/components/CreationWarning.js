import React from "react";
import Button from "./Button";

export default ({ itemIcon, color, title, text, icon, buttonText, onButtonClick }) => (
    <div className='lot-warning-item' style={{borderColor: color, display: "flex", color: "#49566d"}}>
        <div className='lot-warning-info'>
            <div className='item-img-container'>
                <img
                    src={itemIcon}
                    className='item-img'
                    alt='Изображение предмета'
                />
            </div>
            <div className="lot-warning-content">
                <div className='lot-warning_icon'>
                    <img src={icon} alt="" style={{width: '40px'}}/>
                </div>
                <div className='lot-warning-text'>
                    <h3 style={{color: "white"}}>{title}</h3>
                    <div style={{fontSize: '14px'}}>{text}</div>
                </div>
            </div>
        </div>
        <Button
            text={buttonText}
            type='button_big button_transparent'
            onClick={onButtonClick}
        />
    </div>
)