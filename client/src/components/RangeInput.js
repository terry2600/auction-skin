import React from 'react';
import {Range, getTrackBackground} from 'react-range';
import Dropdown from 'react-dropdown';
import '../App.css';

const optionsPlay = [
    {value: 'До победы', label: 'До победы'},
    {value: 'До чего-то', label: 'До чего-то'}
];

class RangeInput extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            values: [this.props.current],
            selectedPlay: optionsPlay[0].value,
        }
    }

    handleRangeInput = (e) => {
        let value = e.target.value;
        if (/^[\d\b]+$/.test(value) && value >= this.props.MIN && value <= this.props.MAX) {
             this.setState({values: [value]})
        } else if (value >= this.props.MAX){
            this.setState({values: [this.props.MAX]})
        } else {
            this.setState({values: [this.props.MIN]})
        }
    };

    render(){
        const {selectedPlay} = this.state;
        const {MIN, MAX, STEP, createLot, label } = this.props;
        return(
            <div className='range-container'>
                {createLot ?
                    <div className='make-lot-input'>
                        <div className='filter__subheading'>{label}</div>
                        <input placeholder='200'
                               type='text'
                               className='range__input'
                               value={this.state.values[0]}
                               ref={input => this.search = input}
                               onChange={this.handleRangeInput}
                        />
                    </div> :
                    <div className='make-bid-input'>
                        <div className="make-bid__play">
                            <div className='filter__subheading'>Играть</div>
                            <Dropdown options={optionsPlay}
                                      value={selectedPlay}
                                      controlClassName='filter__dropdown dropdown'
                                      menuClassName='dropdown_menu'
                                      arrowClassName='dropdown__arrow'
                            />
                        </div>
                        <div className="make-bid__bid">
                            <div className='filter__subheading'>Ставка</div>
                            <input placeholder='200'
                                   type='text'
                                   className='range__input'
                                   value={this.state.values[0]}
                                   ref={input => this.search = input}
                                   onChange={this.handleRangeInput}
                            />
                        </div>
                        <button className='selected-lot__make-bid'>ОК</button>
                    </div>
                }
                <div className='make-bid__range'>
                    <Range
                        values={this.state.values}
                        step={STEP}
                        min={MIN}
                        max={MAX}
                        onChange={values => this.setState({values})}
                        renderTrack={({props, children})=>(
                            <div
                                onMouseDown={props.onMouseDown}
                                onTouchStart={props.onTouchStart}
                                style={{
                                    ...props.style,
                                    height: '36px',
                                    display: 'flex',
                                    width: '100%'
                                }}
                            >
                                <div
                                    ref={props.ref}
                                    style={{
                                        height: '4px',
                                        width: '100%',
                                        borderRadius: '4px',
                                        background: getTrackBackground({
                                            values: this.state.values,
                                            colors: ['#2fb8ff', '#2d4263'],
                                            min: MIN,
                                            max: MAX
                                        }),
                                        alignSelf: 'center'
                                    }}
                                >
                                    {children}
                                </div>
                            </div>
                        )}
                        renderThumb={({ props, isDragged }) => (
                            <div
                                {...props}
                            >
                                <div
                                    style={{
                                        height: '10px',
                                        width: '10px',
                                        display: 'flex',
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        borderRadius: '50%',
                                        backgroundColor: '#2fb8ff'
                                    }}
                                />
                            </div>
                        )}
                    />
                </div>
            </div>


        )
    }
}
export  default RangeInput;