import React from "react";
import 'react-perfect-scrollbar/dist/css/styles.css';
import {Scrollbars} from "react-custom-scrollbars";
import Date from "./Date/Date"


const dealsStatus = (status)=>{
    switch (status) {
        case 1:
            return "Ожидает";
        case 2:
            return "Отклонён";
        case 3:
            return "Исполнен";
        default:
            return "Некорректно"
    }
};

export default ({deals}) => (
    <div className='profile-tab-wrapper'>
        <Scrollbars style={{  height: 400}} className='scrollbar-container_custom'>
        <table className='table profile-table'>
            <thead>
            <tr className="table__row">
                <th className="table__heading">Номер операции</th>
                <th className="table__heading">Дата</th>
                <th className="table__heading">Тип</th>
                <th className="table__heading">Сумма</th>
                <th className="table__heading">Система</th>
                <th className="table__heading">Статус</th>
            </tr>
            </thead>
        </table>

            <table className='table profile-table'>
                <thead>
                <tr>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                </tr>
                </thead>
                <tbody>
                {deals.map((item, index) => (
                    <tr className='table__row' key={index}>
                        <td className='table__item'>
                            {item.id}
                        </td>
                        <td className='table__item'><Date date={item.createdAt ? item.createdAt : ''} type='full'/></td>
                        <td className='table__item'>{item.sign === -1?'Вывод':'Депозит'}</td>
                        <td className='table__item'>{item.amount}</td>
                        <td className='table__item'>{item.system}</td>
                        <td className='table__item'>{dealsStatus(item.status)}</td>
                    </tr>
                ))}
                </tbody>
            </table>
        </Scrollbars>

    </div>

)