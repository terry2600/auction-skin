import React from 'react';
import '../App.css';

export default ({onClick, open}) => (

    <div className={open ? 'burger-menu open' : 'burger-menu'} onClick={onClick}>
        <div className="burger-box">
            <div className="burger-inner"/>
        </div>
    </div>
)