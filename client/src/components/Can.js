import rules from '../config/rules';

const check = (rules, role, resource, action) => {
    const permissions = rules[role].permissions[resource];
    if (!permissions) return false;

    if (permissions.includes(action)) {
        return true;
        // const permissionCondition = permissions[action];
        // if (!permissionCondition) return false;
        //
        // return permissionCondition(data);
    }

    return false;
};

const Can = ({role, resource, action, yes, no}) => check(rules, role, resource, action) ? yes(no) : no();

export default Can;
