import React from 'react';
import {Link} from "react-router-dom";
import PopupBots from "../containters/PopupBots";
import '../App.css';

export default () => (
    <div className='header-tab-container'>
        <Link to='/faq'>
            <div className='icons-header-faq'>
                <div className='icons-header_icon-faq'/>
                <span className='tab-text'>FAQ</span>
            </div>
        </Link>
        <PopupBots/>
        <Link to='/exchange'>
            <div className='icons-header-exchange'>
                <div className='icons-header_icon-exchange'/>
                <span className='tab-text'>Обмены</span>
            </div>
        </Link>
        {/*<Tab text='FAQ'*/}
        {/*     name='faq'*/}
        {/*     activeName =''*/}
        {/*/>*/}
        {/*<Tab text='Боты'*/}
        {/*     name='bots'*/}
        {/*     modal={true}*/}
        {/*/>*/}
        {/*<Tab text='Обмены'*/}
        {/*     name='exchange'/>*/}
    </div>
)