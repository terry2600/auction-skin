import React from 'react';
import {Range, getTrackBackground} from 'react-range';
import '../App.css';

class RangeInputMultiple extends React.Component {

    handleRangeInputLeft = (e) => {
        let value = e.target.value;
        if (/^[\d\b]+$/.test(value) && value >= this.props.min) {
            if (value >= this.props.rangePrice[1]) {
               this.props.changeRangePrice([this.props.rangePrice[1], this.props.rangePrice[1]])
            } else {
                this.props.changeRangePrice([e.target.value, this.props.rangePrice[1]])
            }
        } else if (value <= this.props.min) {
            this.props.changeRangePrice( [this.props.min, this.props.rangePrice[1]])
        }
    };

    handleRangeInputRight = (e) => {
        let value = e.target.value;
        if (/^[\d\b]+$/.test(value) && value <= this.props.max) {
            if (value <= this.props.rangePrice[0]) {
                this.props.changeRangePrice([this.props.rangePrice[0], this.props.rangePrice[0]])
            } else {
                this.props.changeRangePrice([this.props.rangePrice[0], e.target.value])
            }
        } else if (value <= this.props.max) {
            this.props.changeRangePrice([this.props.rangePrice[0], this.props.max])
        }
    };

    render() {
        return (
            <div style={{
                display: 'flex',
                justifyContent: 'center',
                flexWrap: 'wrap'
            }}>
                <div className='multiple-range-pickers'>
                    <input type='text'
                           value={this.props.rangePrice[0]}
                           onChange={this.handleRangeInputLeft}
                           className='filter__range'/>
                    <span style={{color: '#617088', fontSize: '14px', padding: '0 4px'}}>-</span>
                    <input type='text'
                           value={this.props.rangePrice[1]}
                           onChange={this.handleRangeInputRight}
                           className='filter__range'/>
                </div>
                <Range values={this.props.rangePrice}
                       step={this.props.step}
                       min={this.props.min}
                       max={this.props.max}
                       onChange={this.props.changeRangePrice}
                       renderTrack={({props, children}) => (
                           <div
                               onMouseDown={props.onMouseDown}
                               onTouchStart={props.onTouchStart}
                               style={{
                                   ...props.style,
                                   height: '36px',
                                   display: 'flex',
                                   width: '100%'
                               }}>
                               <div ref={props.ref}
                                    style={{
                                        height: '4px',
                                        width: '100%',
                                        borderRadius: '4px',
                                        background: getTrackBackground({
                                            values: this.props.rangePrice,
                                            colors: ['#2d4263', '#2fb8ff', '#2d4263'],
                                            min: this.props.min,
                                            max: this.props.max
                                        }),
                                        alignSelf: 'center'
                                    }}
                               >
                                   {children}
                               </div>
                           </div>
                       )}
                       renderThumb={({props, isDragged}) => (
                           <div
                               {...props}
                           >
                               <div
                                   style={{
                                       height: '10px',
                                       width: '10px',
                                       borderRadius: '50%',
                                       backgroundColor: '#2fb8ff'
                                   }}
                               />
                           </div>
                       )}
                />
            </div>
        )
    }
}

export default RangeInputMultiple;