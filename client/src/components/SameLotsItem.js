import React from "react";
import "../App.css";
import Label from "./Label";

export default ({user,price,date})=>(
    <div className='last-bid-item'>
        <div className='last-bid-item__user'>{date}</div>
        <div className='last-bid-item__user'>{user}</div>
        <div className='last-bid-item__line'/>
        <Label color='green' text={price}/>
    </div>
)