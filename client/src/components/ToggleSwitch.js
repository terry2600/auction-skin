import React from 'react';
import '../App.css';

class ToggleSwitch extends React.Component {
    state = {
        toggle: false
    };

    toggle = () => {
        this.setState({toggle: !this.state.toggle})
    };

    render() {
        const className = `toggle-switch ${this.state.toggle ? 'active' : ''}`;
        return (
            <div className={className}
                 onClick={this.toggle}>
                <div className='toggle-button'/>
            </div>
        )
    }
}

export default ToggleSwitch;