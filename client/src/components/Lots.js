import React from 'react';
import ReactPaginate from 'react-paginate';
import Tooltip from './Tooltip';
import {Scrollbars} from "react-custom-scrollbars";
import Can from '../components/Can';
import '../App.css';
import Loader from "./Loader";
import {ReactComponent as Reload} from '../assets/img/reload.svg';

export default ({
                    isFetching, isNewLotsAvailable, filtersOpened, lots, viewed, total, role,
                    lotFilters, lotFiltersIndex, tabs, currentTab, tabIndex, page, perPages, perPageIndex,
                    currentPerPage, selectedGame, selectedLotId, toggleLotFilter, changeLotFilter, refreshLots,
                    changePage, getSelectedLot, changeTab, changePerPage
                }) => (
    <div className='lots'>
        <div className="lots-header">
            <ul className='filter-pill-list pill-list'>
                <li className='pill-list__item pill-item'>
                    <div className='pill-item__text'>Показывать:</div>
                    <div className='pill-item__tooltip'>
                        <button className='tooltip__trigger tooltip__trigger_type'
                                onClick={() => toggleLotFilter('isTypeFilterOpened')}>
                            {currentTab.label}
                        </button>
                        <Can
                            role={role}
                            resource='lots'
                            action='read:own'
                            yes={() => (
                                <Tooltip
                                    options={tabs}
                                    activeOption={tabIndex}
                                    isOpened={filtersOpened.isTypeFilterOpened}
                                    triangleWidth={80}
                                    triangleHeight={40}
                                    change={changeTab}
                                />
                            )}
                            no={() => (
                                <Tooltip
                                    options={tabs.slice(0, 3)}
                                    activeOption={tabIndex}
                                    isOpened={filtersOpened.isTypeFilterOpened}
                                    triangleWidth={80}
                                    triangleHeight={40}
                                    change={changeTab}
                                />
                            )}
                        />
                    </div>
                </li>
                <li className='pill-list__item pill-item'>
                    <div className='pill-item__text'>Выводить:</div>
                    <div className='pill-item__tooltip'>
                        <button className='tooltip__trigger'
                                onClick={() => toggleLotFilter('isNumberFilterOpened')}>
                            {currentPerPage.label}
                        </button>
                        <Tooltip
                            options={perPages}
                            activeOption={perPageIndex}
                            isOpened={filtersOpened.isNumberFilterOpened}
                            triangleWidth={80}
                            triangleHeight={40}
                            change={changePerPage}
                        />
                    </div>
                    {/*<Dropdown*/}
                    {/*    options={lotsNumber.values}*/}
                    {/*    value={lotsNumber.values[lotsNumber.activeIndex]}*/}
                    {/*    controlClassName='pill-item__dropdown dropdown'*/}
                    {/*    menuClassName='dropdown_menu'*/}
                    {/*    arrowClassName='dropdown__arrow'*/}
                    {/*/>*/}
                </li>
            </ul>
            <div className='filter-refresh-container'>
                <div className='pill-item__tooltip'>
                    <button className='tooltip__trigger' onClick={() => toggleLotFilter('isSortFilterOpened')}>
                        Фильтры
                        <span className='stairs stairs_desc'/>
                    </button>
                    <Tooltip
                        options={lotFilters}
                        activeOption={lotFiltersIndex}
                        sort={true}
                        isOpened={filtersOpened.isSortFilterOpened}
                        triangleWidth={100}
                        triangleHeight={40}
                        change={changeLotFilter}
                    />
                </div>
                <button className={isNewLotsAvailable ? 'filter-refresh active' : 'filter-refresh'}
                        onClick={refreshLots}>
                    <Reload className={isFetching ? 'refresh-image active' : 'refresh-image'}
                            viewBox="2 2 28 28"/>
                </button>
            </div>
        </div>
        <div className="lots-content">
            <div className='total-lots-table flex-table table'>
                <div className='table-header'>
                    <div className='table__row'>
                        <div className='table__heading'>Название лота</div>
                        <div className='table__heading'>Пользовательская цена</div>
                        <div className='table__heading'>Стоимость участия</div>
                    </div>
                </div>
                <div className='table-body'>
                    <Scrollbars
                        hideTracksWhenNotNeeded={true}
                        style={{height: '616px', width: '100%', position: 'static'}}
                        className='scrollbar-container_custom'
                        renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                        renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                        renderView={props => <div style={{...props.style, marginRight: 0}} className="view"/>}
                    >
                        {isFetching ? (
                            <Loader
                                type='BallTriangle'
                                color='#00BFFF'
                                height={300}
                                width={300}
                            />
                        ) : (
                            lots?lots.map(item => (
                                <div className={selectedLotId === item.id ? 'table__row selected' : 'table__row'}
                                     key={item.id}
                                     onClick={() => getSelectedLot(item.id, item.current_participants)}>
                                    <div className={`table__item table__item_${selectedGame}`}>
                                        <div
                                            className='lot-img-container'
                                            style={selectedGame === 'dota' ? {background: 'none'} : {background: `linear-gradient(135deg, ${item.color} 0%, ${item.color}00 100%)`}}>
                                            <img src={item.icon_url}
                                                 className='lot-img'
                                                 alt='Изображение предмета'
                                            />
                                        </div>
                                        <div className='lot-item-main-info'>
                                            <div className='lot-table-value'>{item.name}</div>
                                            <div className='lot-table-text'>{item.game_name}</div>
                                        </div>
                                    </div>
                                    <div className='table__item'>{item.price}</div>
                                    <div className='table__item'>{item.cost_of_participation}</div>
                                </div>
                            )):''
                        )}
                    </Scrollbars>
                </div>
            </div>
            <div className='total-lots-pagination lots-pagination'>
                <div className='lots-pagination__results'>
                    Результаты:
                    <span className='lots-pagination__value'>{viewed}</span>
                    <span className='lots-pagination__text'>из {total}</span>
                </div>
                <ReactPaginate
                    pageCount={Math.ceil(total / currentPerPage.value)}
                    forcePage={page}
                    pageRangeDisplayed={4}
                    marginPagesDisplayed={1}
                    pageClassName={'lots-pagination__page'}
                    containerClassName={'pagination_total-lots'}
                    nextClassName={'next-label_disabled'}
                    previousClassName={'previous-label_disabled'}
                    pageLinkClassName={'lots-pagination__link'}
                    activeLinkClassName={'lots-pagination__link_active'}
                    breakLinkClassName={'lots-pagination__break-link'}
                    breakClassName={'lots-pagination__break'}
                    onPageChange={page => changePage(page)}
                />
            </div>
        </div>
    </div>
)
