import React from "react";
import ParticipationWarning from "./ParticipationWarning";

export default ({warnings })=>(
    <div className={warnings.length !== 0?'lot-warning':'none'}>
        {warnings ? warnings.map((item,key)=>(
            <ParticipationWarning key = {key} title={item.title} text={item.text} color={item.color} icon={item.icon}/>
        )) : ''}
    </div>
)