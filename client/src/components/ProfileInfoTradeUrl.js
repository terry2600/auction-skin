import React from 'react';
import Button from './Button';

export default ({id, heading, tradeUrl, inChangeMode, newTradeUrl, error, setTradeUrl, changeTradeUrl, change}) => {
    let inputProps = {};

    if (inChangeMode) {
        inputProps.value = newTradeUrl
    } else {
        inputProps.placeholder = 'Пожалуйста, введите свой trade url'
    }

    return (
        <div className='profile-info'>
            <div className="profile-info-block">
                <div className='profile-info__heading'>{heading}</div>
                {tradeUrl && !inChangeMode ? (
                    <React.Fragment>
                        <div id={id} style={{flexGrow: 1, paddingLeft: '20px'}} className='profile-info__value'>{tradeUrl}</div>
                        <Button
                            text='Изменить'
                            type='button_small button_transparent'
                            onClick={changeTradeUrl}
                            style={{marginLeft: '12px'}}
                        />
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <input
                            {...inputProps}
                            className='profile-info__input' type="text"
                            onChange={(e) => change(e.target.value)}/>
                        <Button
                            text='Применить'
                            type='button_small button_lightblue button_transparent'
                            onClick={setTradeUrl}
                            style={{marginLeft: 'auto'}}
                        />
                    </React.Fragment>
                )}
            </div>
            {error && (
                <div className='profile-info__error'>{error.message}</div>
            )}
        </div>
    )
}
