import React from 'react';

export default class CurveTriangle extends React.Component {
    constructor(props) {
        super(props);
        this.triangleRef = React.createRef();
    }

    componentDidMount() {
        this.updateCanvas();
    }

    updateCanvas() {
        const ctx = this.triangleRef.current.getContext('2d');
        ctx.beginPath();
        ctx.bezierCurveTo(8, 24, 40, 20, 44, 6);
        ctx.bezierCurveTo(44, 6, 46, 20, 80, 24);
        ctx.strokeStyle=this.props.strokeColor;
        ctx.fillStyle=this.props.fillColor;
        ctx.fill();
        ctx.stroke();
    }

    render() {
        return(
            <div className='curve-triangle'>
                <canvas id='triangle' ref={this.triangleRef} width={this.props.width} height={this.props.height}/>
            </div>
        )
    }
}