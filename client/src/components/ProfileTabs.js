import React from "react";
import ProfileTable from './ProfileTable';
import ProfileAffiliate from './ProfileAffiliate';
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import SelectedLotImg from './InventoryItemCs';
import TransactionsTable from "./TransactionsTable";
import Scrollbars from "react-custom-scrollbars";
import {getRarity} from '../functions/getRarityColor';
import DealsTable from "./DealsTable";
import ProfileNotifications from "./ProfileNotifications";
const communityImageLink = "https://steamcommunity-a.akamaihd.net/economy/image/";

export default ({deals,lots, ownLots, affiliate, inventory, activeIndex, onClick, transactions, loadInventory, notifications, setActiveNotifications, steamId}) => (
    <Tabs className='profile-tabs'>
        <TabList className='profile-tab-list'>
            <Tab className='profile-tab-list__item'>История аукционов</Tab>
            <Tab className='profile-tab-list__item'><span className='profile-item__text'>Транзакции</span></Tab>
            <Tab className='profile-tab-list__item'>Партнерка</Tab>
            <Tab className='profile-tab-list__item'>Мои лоты</Tab>
            <Tab className='profile-tab-list__item'>Мой инвентарь</Tab>
            <Tab className='profile-tab-list__item'>Внутренние транзакции</Tab>
            <Tab className='profile-tab-list__item'>Уведомления</Tab>
        </TabList>
        <div className="profile-tab-panels">
            <TabPanel className='profile-tab-panel'>
                <ProfileTable lots={lots}/>
            </TabPanel>
            <TabPanel className='profile-tab-panel'>
                <DealsTable deals={deals}/>
            </TabPanel>
            <TabPanel className='profile-tab-panel'>
                <ProfileAffiliate affiliate={affiliate} steamId={steamId}/>
            </TabPanel>
            <TabPanel className='profile-tab-panel'>
                <ProfileTable lots={ownLots}/>
            </TabPanel>
            <TabPanel className='profile-tab-panel'>
                <Tabs className='faq-tabs'>
                    <TabList className='inventory-tab-list'>
                        <Tab className='tab-pill' onClick={() => loadInventory(730)}>CS:GO</Tab>
                        <Tab className='tab-pill' onClick={() => loadInventory(570)}>DOTA2</Tab>
                    </TabList>
                    <TabPanel>
                        <div className='inventory-tab'>
                            <div className='inv-acc' data-header="CS:GO">
                                <div className="profile-tab-wrapper">
                                    <Scrollbars style={{height: 500}} className='scrollbar-container_custom'>
                                        <div className='inventory-list profile_inventory-list'>
                                            {inventory.map((item, index) => (
                                                item.type !== "Контейнер, базового класса"?
                                                <SelectedLotImg
                                                    key={item.id}
                                                    index={index}
                                                    img={communityImageLink + item.icon_url}
                                                    activeIndex={activeIndex}
                                                    onClick={onClick}
                                                    wear={item.tags[4] !== null ? getRarity(item.tags[4]) : "gray"}
                                                    name={item.market_name}
                                                />:''
                                            ))}
                                        </div>
                                    </Scrollbars>
                                </div>
                            </div>
                        </div>
                    </TabPanel>
                    <TabPanel>
                        <div className='inventory-tab'>
                            <div className='inv-acc' data-header="CS:GO">
                                <div className="profile-tab-wrapper">
                                    <Scrollbars style={{height: 500}} className='scrollbar-container_custom'>
                                        <div className='inventory-list profile_inventory-list'>
                                            {inventory.map((item, index) => (
                                                <SelectedLotImg
                                                    key={item.id}
                                                    index={index}
                                                    img={communityImageLink + item.icon_url}
                                                    activeIndex={activeIndex}
                                                    onClick={onClick}
                                                    wear={item.tags[4] !== null ? getRarity(item.tags[4]) : "gray"}
                                                    name={item.market_name}
                                                />
                                            ))}
                                        </div>
                                    </Scrollbars>
                                </div>
                            </div>
                        </div>
                    </TabPanel>
                </Tabs>
                }
            </TabPanel>
            <TabPanel className='profile-tab-panel'>
                <TransactionsTable transactions={transactions}/>
            </TabPanel>
            <TabPanel><ProfileNotifications setActiveNotifications={setActiveNotifications} notifications={notifications}/></TabPanel>
        </div>
    </Tabs>
)
