import React from 'react';
import '../App.css';

export default ({height, width, avatar}) => (
    <div style={{display: 'inline-block', position: 'relative'}}>
        <div style={{height: height, width: width, borderRadius: '50%', overflow: 'hidden'}}>
            <img
                src={avatar}
                style={{height: 'inherit', width: 'inherit', objectFit: 'cover'}}
                alt='Аватарка'
            />
        </div>
    </div>
)