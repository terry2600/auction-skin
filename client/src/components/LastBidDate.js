import React from 'react';
import '../App.css';

export default ({time})=> (
    <div className='last-bid-time'>{time.toLocaleDateString()}</div>
)