import React from "react";

export default ({icon,color,link})=>(
    <div className='lot-link_container' style={{borderColor:color}} onClick={()=>{window.open(link)}}>
        <img src={icon} alt=''/>
    </div>
)