import React from 'react';
import Button from "./Button";

const TradeUrlInput = ({error, change, setTradeUrl}) => (
    <div className='trade-url-container'>
        <div className='trade-url-wrapper'>
            <div className='trade-url'>
                <div className='trade-url__heading'>Trade URL</div>
                <div className='trade-url-content'>
                    <input
                        type='text'
                        className='trade-url__input'
                        onChange={(e) => change(e.target.value)}
                    />
                    {error && (
                        <div className='trade-url__error'>{error.message}</div>
                    )}
                </div>
            </div>
            <div className="trade-url-buttons">
                <Button text='Применить'
                        type='button_big button_lightblue button_transparent button_apply-trade-url'
                        onClick={setTradeUrl}
                        style={{marginLeft: '18px'}}
                />
                <a href='https://steamcommunity.com/id/me/tradeoffers/privacy#trade_offer_access_url'
                   className='button button_big button_transparent button_trade-url' target='_blank'>Получите ссылку на обмен
                </a>
            </div>
            </div>
        <div className='trade-url-overlay'/>
    </div>
);

export default TradeUrlInput;