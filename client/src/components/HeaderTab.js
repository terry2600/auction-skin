import React from 'react';
import {Link} from 'react-router-dom';
import PopupBots from "../containters/PopupBots";
import '../App.css';
import icons from '../assets/img/header-sheet.svg';
import faqIcon from '../assets/img/header-icons/faq.svg';
export default ({text, name, modal}) => (
    <div className={modal ? 'header-tab header-tab-popup' : 'header-tab'}>
        {modal ?
            <PopupBots text={text} name={name}/>
            :
            <Link to={`/${name}`} style = {{    display:'flex'}}>
                <svg className='tab-icon'>
                    <use href={`${icons}#${name}`}/>
                </svg>
                <span className='tab-text'>{text}</span>
            </Link>
        }
    </div>
)