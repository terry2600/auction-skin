import React from 'react';
import Slide from './Slide';

export default ({slides, isScrollable, getSelectedLot, selectedGame}) => (
    <div className='slider' style={{overflow: 'hidden', padding: '20px 0 46px', margin: '0 -20px'}}>
        <div className='slide-track'>
            {slides.map(item => (
                <Slide
                    key={item.id}
                    color={item.color}
                    img={item.icon_url}
                    name={item.name.split('|')[0]}
                    type={item.name.split('|')[1]}
                    id={item.id}
                    current_participants={item.current_participants}
                    getSelectedLot={getSelectedLot}
                    selectedGame={selectedGame}
                />
            ))}
            {isScrollable ?
                slides.map(item => (
                    <Slide
                        key={item.id}
                        color={item.color}
                        img={item.icon_url}
                        name={item.name.split('|')[0]}
                        type={item.name.split('|')[1]}
                    />
                ))
                : null
            }
        </div>
    </div>
)
