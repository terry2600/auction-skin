import React from 'react';
import {Scrollbars} from "react-custom-scrollbars";
import Date from "./Date/Date";

export default ({notifications, setActiveNotifications}) => (
    <div className='profile-tab-wrapper'>
        <Scrollbars style={{height: 400}} className='scrollbar-container_custom'>
            <table className='table profile-table'>
                <thead>
                <tr className="table__row">
                    <th className="table__heading">Код</th>
                    <th className="heading_notification">Сообщение</th>
                    <th className="table__heading">Дата и время</th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {notifications.map((item, index) => {
                    switch (item.type_id) {
                        case 1:
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'flex-start'}}>
                                            <div style={{marginLeft: '15px'}}>Ваш лот: <div
                                                style={{color: 'green'}}>{item.body.item_name}</div> завершён успешно!
                                            </div>
                                            <div style={{marginLeft: '35px'}}>Победитель лота: <div
                                                style={{color: 'green'}}>{item.body.winner_nick}</div></div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            );
                        case 2:
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>Ваш лот: <div style={{color: 'green'}}>{item.body.item_name}</div> был
                                                отменён!
                                            </div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            );
                        case 3: {
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>Пополение счёта на : <div
                                                style={{color: 'green'}}>{item.body.amount}</div> завершёно успешно!
                                            </div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            )
                        }
                        case 5: {
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>Вывод с вашего счёта на : <div
                                                style={{color: 'green'}}>{item.body.amount}</div> завершён успешно!
                                            </div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            )
                        }
                        case 4: {
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>К сожалению,Пополнение вашего счёта на : <div
                                                style={{color: 'red'}}>{item.body.amount}</div> не было завершено!
                                            </div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            )
                        }
                        case 6: {
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>К сожалению,вывод с вашего счёта на : <div
                                                style={{color: 'red'}}>{item.body.amount}</div> не был завершено!
                                            </div>
                                        </td>
                                        <td className={'table__item'}><Date type={'full'} date={item.createdAt}/></td>
                                    </tr>
                                </React.Fragment>
                            )
                        }
                        case 8: {
                            return (
                                <React.Fragment key={index}>
                                    <tr className={item.active ? 'table__row notification-table' : 'table__row'}
                                        style={{cursor: 'pointer'}} key={index} onClick={() => {
                                        setActiveNotifications(item.id)
                                    }}>

                                        <td className='table__item'>
                                            {item.id}
                                        </td>
                                        <td className='table__item'>
                                            <img
                                                src={item.active ? require('../assets/img/green-warn.png') : require('../assets/img/gray-warn.png')}
                                                alt="" style={{width: '35px'}}/> У вас новое сообщение
                                        </td>
                                        <td className='table__item'>{item.date}</td>

                                    </tr>
                                    <tr style={item.active ? {} : {display: 'none'}}
                                        className='table__row notification-block'>
                                        <td className={'table__item'}></td>
                                        <td className={'table__item'}
                                            style={{display: 'flex', justifyContent: 'space-between'}}>
                                            <div>Обмен лота номер : <div
                                                style={{color: 'green'}}>{item.body.lot_id}</div> завершен успешно!
                                            </div>
                                        </td>
                                        <td className={'table__item'}>
                                            <div><Date type={'full'} date={item.createdAt}/></div>
                                        </td>
                                    </tr>
                                </React.Fragment>
                            )
                        }
                        default:
                            return ""
                    }

                })}
                </tbody>
            </table>
        </Scrollbars>
    </div>
)