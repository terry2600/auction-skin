import React from "react";

export default ({date,type})=>(
    <span>
        {type === 'year'?date.split('T')[0]:''}
        {type === 'time'?date.split('T')[1].split('.')[0]:''}
        {type === 'full'?date.split('T')[0]+" ,"+date.split('T')[1].split('.')[0]:''}
    </span>
)