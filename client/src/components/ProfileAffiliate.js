import React from "react";
import ProfileInfo from '../components/ProfileInfo';
import Date from "./Date/Date";
import {CopyContext} from "../containters/Profile";

export default ({affiliate, steamId}) => (
    <div className='profile-tab-wrapper'>
        <div className="profile-affiliate-header affiliate-header">
            <div className='affiliate-header__heading'>
                Привлекай друзей и зарабатывая <span className='affiliate-header__highlight'>5%</span> от их пополнений!
            </div>
            <div className='affiliate-header__subheading'>
                Деньги, заработанные в партнерской программе, сразу доступны для вывода
            </div>
            <CopyContext.Consumer>
                {({copyToClipboard}) => (
                    <ProfileInfo
                        id='referal-link'
                        heading='Ваша реферальная ссылка'
                        value={`auction-skin.com?invite=${steamId}`}
                        buttonText='Скопировать'
                        click={copyToClipboard}
                    />
            )}
            </CopyContext.Consumer>
        </div>
        <div className="profile-affiliate-body">
            <table className='table affiliate-table'>
                <thead>
                    <tr className="table__row">
                        <th className='table__heading'>Реферал</th>
                        <th className='table__heading'>Дата регистрации</th>
                        <th className='table__heading'>Ваш бонус</th>
                    </tr>

                </thead>
                <tbody>
                    {affiliate.map((item, index) => (
                        <tr className="table__row" key={index}>
                            <td className='table__item'>{item.steam_nick}</td>
                            <td className='table__item'><Date type={'year'} date={item.registration_date}/></td>
                            <td className='table__item'>0{item.bonus}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>

    </div>
)
