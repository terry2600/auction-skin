import React from "react";
import ListItem from "./LastBidItem";
import LastBidTime from "./LastBidTime";
import PopupSimilarLot from "../containters/PopupSimilarLot";

export default ({title, col1, col2, col3, list, lotId}) => (
    <React.Fragment>
        <div className="popup__heading">{title}</div>
        <table className=" popup-table table">
            <thead>
            <tr className='table__row'>
                <th className='table__heading'>{col1}</th>
                <th className='table__heading'>{col2}</th>
                <th className='table__heading'>{col3}</th>
            </tr>
            </thead>
            {list !== undefined &&
            <tbody>{list.map((item, index) => (
                <tr className='table__row' key={index}>
                    <td className='table__item'>
                        <LastBidTime time={new Date(item.end_date)}/>
                    </td>
                    <td colSpan='2' className='table__item'>
                        <ListItem user={item.LotResult.User.steam_nick} bid={`${item.LotResult.purchase_price} Р`}/>
                    </td>
                </tr>
            ))}
            </tbody>
            }
        </table>
        <PopupSimilarLot lotId={lotId}/>
    </React.Fragment>
)
