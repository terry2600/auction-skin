import React from 'react';
import '../App.css';

export default ({text, value}) => (

    <div className='statistics-item-text'>
        <div className='statistics-item__heading'>{text}</div>
        <div className='statistics-item__value'>{value}</div>
    </div>
)