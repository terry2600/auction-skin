import React from "react";
import ListItem from "./LastBidItem";
import PopupHistoryBid from "../containters/PopupHistoryBid";

export default ({lastBids, lotId,name}) => {
    return (
        <div className="popup-history-bid">
            <div className='table__row'>
                <span className='table__heading'>Никнейм участника</span>
                <span className='table__heading'>Ставка</span>
            </div>
            {lastBids.map(bid => (
                <div className='table__row' key={bid.id}>
                    <div className='table__item'>
                        <ListItem
                            key={bid.id}
                            user={bid.steam_nick}
                            bid={`${bid.bet_size} Р`}/>
                    </div>
                </div>
            ))}
           <PopupHistoryBid name = {name} lotId={lotId}/>
        </div>
    )
}
