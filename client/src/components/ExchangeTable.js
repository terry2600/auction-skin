import React from "react";
import ExchangeItem from "../components/ExchangeItem"
import {Scrollbars} from "react-custom-scrollbars";
export default ({exchanges, got, sent})=>(
    <div className="exchange-table">
        <Scrollbars style={{height: 500}}>
            {exchanges.map((item,key)=>(
                <ExchangeItem id={item.id}
                image={item.image} name={item.name} game={item.game}
                tradeLink={item.tradeLink} winner={item.winner}
                key={key}
                got={got} sent={sent}
                />
            ))}
        </Scrollbars>
    </div>
)
