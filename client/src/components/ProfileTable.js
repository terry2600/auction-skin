import React from "react";
import Label from './Label';
import 'react-perfect-scrollbar/dist/css/styles.css';
import {Scrollbars} from "react-custom-scrollbars";
import Date from "./Date/Date"

export default ({lots}) => (
    <div className='profile-tab-wrapper'>
        <Scrollbars style={{  height: 400}} className='scrollbar-container_custom'>
        <table className='table profile-table'>
            <thead>
            <tr className="table__row">
                <th className="table__heading">Номер лота</th>
                <th className="table__heading">Лот</th>
                <th className="table__heading">Цена</th>
                <th className="table__heading">Дата проведения</th>
                <th className="table__heading">Участников</th>
                <th className="table__heading">Статус</th>
                <th className="table__heading">Победитель</th>
                <th className="table__heading">Цена выкупа</th>
            </tr>
            </thead>
        </table>

            <table className='table profile-table'>
                <thead>
                <tr>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                    <th className="table__heading"></th>
                </tr>
                </thead>
                <tbody>
                {lots.map((item, index) => (
                    <tr className='table__row' key={index}>
                        <td className='table__item'>
                            {item.id}
                        </td>
                        <td className='table__item'>
                            {item.GameItem.name}
                            <Label text={item.GameItem.Game.name} style={{marginLeft: '16px'}}/>
                        </td>
                        <td className='table__item'>{item.price}</td>
                        <td className='table__item'><Date date={item.end_date ? item.end_date : item.create_date} type='full'/></td>
                        <td className='table__item'>{item.participants}</td>
                        <td className='table__item'>{item.LotStatus.id === 1?<div className='button_more'>Отменить</div>:item.LotStatus.status_description}</td>
                        <td className='table__item'>{item.LotResult ? item.LotResult.User.steam_nick : '-'}</td>
                        <td className='table__item'>
                            <Label color='green' text={item.LotResult ? item.LotResult.purchase_price : '-'}/>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </Scrollbars>

    </div>

)
