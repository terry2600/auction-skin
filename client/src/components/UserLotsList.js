import React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import {Scrollbars} from "react-custom-scrollbars";
import Date from "./Date/Date";
import '../App.css';

export default ({list, setSelectedLot}) => {
    return (
        <Scrollbars style={{height: 800 }} className='scrollbar-container_custom'>
            <table className='user-lot-table table'>
                <tbody>
                {list.map((item, index) =>(
                    <tr className='table__row' key={index}
                        onClick={() => setSelectedLot(item.id)}
                    >
                        <td className='table__item'>
                            <div className={item.color === 'blue' ? 'bid-img-container bid-img-container_blue' : 'bid-img-container bid-img-container_red'}>
                                <img src={item.icon_url}
                                     style={{width:'100px'}}
                                    className='bid-img'
                                    alt='Изображение предмета'
                                />
                            </div>
                            <div className='bid-item-main-info'>
                                <div className='bid-table-value' style={{color:'white',marginLeft:'10px'}}>{item.name}</div>
                                <div className='bid-table-text' style={{color:'white',marginLeft:'10px'}}>{item.game_name}</div>
                            </div>
                        </td>
                        <td className='table__item'>
                            <span className='bid-table-text'>Участников:
                                <span className='bid-table-value bid-table-value_padding'>{item.current_participants}</span>
                                /{item.participants}
                            </span>
                        </td>
                        <td className='table__item'>
                            <span className='bid-table-text'>Стоимость шага:</span>
                            <span className='bid-table-value bid-table-value_padding'>{item.lot_step}</span>
                        </td>
                        <td className='table__item'>
                            <span className='bid-table-text'>Дата начала:</span>
                            <span className='bid-table-value bid-table-value_padding'><Date date={item.start_date} type={'year'}/></span>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
        </Scrollbars>
    )
}
