import React from 'react';
import ReactTooltip from "react-tooltip";
import Gear from "./Gear";
import '../App.css';


export default ({id, img, activeId, onClick, item, wear, name}) => (
    <div
        className={(activeId === id && typeof id !== 'undefined') ? 'selected-lot__img-container selected' : 'selected-lot__img-container'}
        onClick={typeof onClick !== 'undefined' ? () => {
            onClick(item, id)
        } : null}>
        {typeof id !== 'undefined' ? <Gear/> : null}
        <ReactTooltip id={name} type='light' effect='float' place="top">
            <span>{name}</span>
        </ReactTooltip>
        <a data-tip data-for={name}>
            <div className='selected-lot__transparency' style={{backgroundColor: wear}}/>
            <img
                src={img}
                className='selected-lot__img'
                alt='Изображение предмета'
            />
        </a>
    </div>
)