import React from "react";

export default ({ width, height, top }) => (
    <div className='loader' style={{width, height, top}}/>
)