import React from 'react';


class CountDown extends React.Component {
    render() {
        const time = this.props.time;
        return (
            <React.Fragment>
                {('0' + Math.floor((time % 86400000) / 3600000)).slice(-2)}:
                {('0' + Math.floor((time % 3600000) / 60000)).slice(-2)}:
                {('0' + Math.floor((time % 60000) / 1000)).slice(-2)}
            </React.Fragment>
        )
    }
}

export default CountDown;
