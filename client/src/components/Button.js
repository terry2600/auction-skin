import React from 'react';
import '../App.css';

export default ({ text, type, onClick, style }) => (
    <div onClick={onClick} style={style}
         className = {`button ${type}`}>
        {text}
    </div>
)