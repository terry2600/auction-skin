import React from 'react';
import '../App.css';

export default ({color, text, value, style, link}) => (
    <div style={style} className={color === 'green' ?
        'label label_green' :
        color === 'dark-green' ?
            'label label_dark-green' :
            'label label_dark-blue'}
    >
        {(typeof value !== 'undefined') ?
            <div>
                {text}
                <span style={{color: '#ffffff', marginLeft: '3px'}}>{value}</span>
            </div>
            :
            typeof link !== 'undefined' ? <a href={link} className='label__link'>{text}</a> : <div>{text}</div>
        }
    </div>
)