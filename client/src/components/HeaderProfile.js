import React from 'react';
import CircleImg from '../components/CircleImg';
import Label from '../components/Label';
import Can from '../components/Can';
import Tooltip from "./Tooltip";
import {Link} from 'react-router-dom';
import {UserContext, BalanceContext} from '../App';
import '../App.css';

export default ({isTooltipOpened, options, toggleTooltip}) => (
    <UserContext.Consumer>
        {({avatar, userName, role}) => (
            <BalanceContext.Consumer>
                {({balance}) => (
                    <Can
                        role={role}
                        resource='profile'
                        action='read:own'
                        yes={() => (
                            <div className='header-profile-container'>
                                <div className='header-profile-user'>
                                    <Link to='/profile'>
                                        <CircleImg
                                            height='48px'
                                            width='48px'
                                            avatar={avatar}
                                        />
                                    </Link>
                                    <span className='header-profile-nickname' onClick={toggleTooltip}>
                                        {userName}
                                        <div className={"arrowDown"}>
                                            <img src={require("../assets/img/dropdown-arrow.png")} alt=""
                                                 color={'white'}/>
                                        </div>
                                        <Tooltip
                                            navigation={true}
                                            options={options}
                                            isOpened={isTooltipOpened}
                                            triangleWidth={60}
                                            triangleHeight={30}
                                        />
                                    </span>
                                </div>
                                <Label
                                    color='green'
                                    text={`${balance} Р`}
                                />
                            </div>
                        )}
                        no={() => null}
                    />
                )}
            </BalanceContext.Consumer>
        )}
    </UserContext.Consumer>
)