import React from 'react';
import '../App.css';

const Slide = {
    position: 'relative',
    borderRadius: '18px',
    overflow: 'hidden',
    margin: 'auto',
    width: '155px',
    height: '120px',
    boxShadow: `inset 0px 30px 40px 0px rgba(245, 255, 227, 0)`,
    cursor: 'pointer'
};

export default ({color, img, name, type, id, current_participants, selectedGame, getSelectedLot}) => (
    <div className={`slide slide_${selectedGame}`} style={{...Slide, backgroundColor: `${color}99`}} onClick={() => getSelectedLot(id, current_participants)}>
        <img
            className='slide__img'
            src={img}
            alt='logo'
            draggable={false}
        />
        <div style={{ position: 'absolute', left: '10px', bottom: '12px', zIndex: '2'}}>
            <div style={{fontSize: '14px', color: '#ffffff', paddingBottom: '2px'}}>{name}</div>
            <div style={{fontSize: '12px', color: '#ffffff', opacity: '.3'}}>{type}</div>
        </div>
    </div>
)