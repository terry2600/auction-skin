import React from "react";
import '../App.css';
import HeroesBlock from "./Heroes/HeroesBlock";
import {Scrollbars} from "react-custom-scrollbars";

export default ({strHeroes, agilityHeroes, smartHeroes, setActive, submit, reset}) => (
    <div className={"heroes-popup-container"}>
        <div className='heroes-popup_wrapper'>
            <div className={"heroes-popup-button-container"}>
                <div className='button button_big button_gradient' onClick={reset}
                     style={{marginRight: '14px'}}>Сбросить героев
                </div>
                <div className='button button_big button_gradient' onClick={submit}>Применить</div>
            </div>
            <div className='heroes-popup_block'>
                <Scrollbars
                    autoHide={false}
                    hideTracksWhenNotNeeded={false}
                    style={{width: 'auto', height: 500}}
                    className='scrollbar-container_custom'
                    renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                    renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                    renderView={props => <div {...props} className="view"/>}
                >
                    <HeroesBlock title={'Сила'} heroes={strHeroes} setActive={setActive}/>
                    <HeroesBlock title={'Ловкость'} heroes={agilityHeroes} setActive={setActive}/>
                    <HeroesBlock title={'Интеллект'} heroes={smartHeroes} setActive={setActive}/>
                </Scrollbars>
            </div>
        </div>
    </div>
)