import React from 'react';
import SelectedLotItem from './SelectedLotItem';
import LastBidTime from './LastBidTime';
import LastBidItem from './LastBidItem';
import DribbleButton from "./DribbleButton";
import PopupSimilarLot from "../containters/PopupSimilarLot";
import Warnings from "./Warnings";
import ParticipationWarning from "./ParticipationWarning";
import {UserContext, BalanceContext} from '../App';
import '../App.css';

export default ({lot, itemParams, similarLots, isSelected, isParticipated, applyNotification, participate, warnings, steamId}) => (
    isSelected ? (
        <UserContext.Consumer>
            {({steamId}) => (
                <BalanceContext.Consumer>
                    {({balance}) => (
                        <div className='selected-lot'>
                            {warnings !== [] ? <Warnings warnings={warnings}/> : ''}
                            <div className="selected-lot-wrapper">
                                <SelectedLotItem
                                    stickers={lot.GameItem.stickers}
                                    steamId={steamId}
                                    img={lot.GameItem.icon_url}
                                    name={lot.GameItem.name}
                                    appid={lot.app_id}
                                    color={lot.color}
                                    marketHN={lot.GameItem.market_hash_name}
                                    type={itemParams.CsType ? itemParams.CsType.name_ru : itemParams.DotaType.name_ru}
                                    exterior={itemParams.CsExterior ? itemParams.CsExterior.name_ru : itemParams.DotaRarity.name_ru}
                                />
                                <div className='selected-lot__params lot-params'>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading slot-params_heading'>Стоимость лота</div>
                                        <div
                                            className='text-column__value lot-params__value'>{Math.ceil(lot.price)}</div>
                                    </div>
                                    <span className='dash selected-bid__dash'/>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading lot-params__heading'>Конец приема заявок
                                        </div>
                                        <div
                                            className='text-column__value lot-params__value'>{lot.expired_date.toLocaleString('ru')}</div>
                                    </div>
                                    <span className='dash selected-bid__dash'/>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading lot-params__heading'>Дата проведения</div>
                                        <div
                                            className='text-column__value lot-params__value'>{lot.start_date.toLocaleString('ru')}</div>
                                    </div>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading slot-params_heading'>Стоимость шага</div>
                                        <div
                                            className='text-column__value lot-params__value'>{Math.ceil(lot.lot_step)}</div>
                                    </div>
                                    <span className='dash selected-bid__dash'/>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading lot-params__heading'>Стоимость участия
                                        </div>
                                        <div
                                            className='text-column__value lot-params__value'>{Math.ceil(lot.cost_of_participation)}</div>
                                    </div>
                                    <span className='dash selected-bid__dash'/>
                                    <div className='text-column lot-params__item'>
                                        <div className='text-column__heading lot-params__heading'>Кол-во участников
                                        </div>
                                        <div
                                            className='text-column__value lot-params__value'>{lot.current_participants}/{lot.participants}</div>
                                    </div>
                                </div>
                                {steamId ? (
                                    !isParticipated ? (
                                        <div className='selected-lot-bottom-container'>
                                            <DribbleButton
                                                type='button'
                                                text='Принять участие'
                                                onClick={() => participate(lot.id, Math.ceil(lot.cost_of_participation), balance)}
                                            />
                                            {similarLots.length !== 0 ? (
                                                <div className='selected-lot__history'>
                                                    <div className='selected-lot__history-text'>
                                                        <div className="selected-lot__history-heading">
                                                            История лотов
                                                        </div>
                                                        <div className='selected-lot__history-more'>
                                                            <PopupSimilarLot lotId={lot.id}/>
                                                        </div>
                                                    </div>
                                                    {similarLots.map((lot, index) => (
                                                        <div key={index} className='selected-lot__last-bid'>
                                                            <LastBidTime time={new Date(lot.end_date)}/>
                                                            <LastBidItem
                                                                user={lot.LotResult.User.steam_nick}
                                                                bid={lot.LotResult.purchase_price}
                                                            />
                                                        </div>
                                                    ))}
                                                </div>
                                            ) : (
                                                <div className="selected-lot__history-heading">Такой предмет еще не
                                                    разыгрывался!</div>
                                            )}
                                        </div>
                                    ) : (
                                        isParticipated && (
                                            <ParticipationWarning
                                                color={applyNotification.type === 'error' ? '#ff0059' : '#5db936'}
                                                title={applyNotification.title}
                                                text={applyNotification.text}
                                                icon={applyNotification.type === 'error' ?
                                                    require('../assets/img/red-warn.png') :
                                                    require('../assets/img/green-warn.png')}
                                            />
                                        )
                                    )
                                ) : (
                                    <div className='authorization-warning'>Аторизуйтесь, чтобы принять участие в аукционе!</div>
                                )}
                            </div>
                        </div>
                    )}
                </BalanceContext.Consumer>
            )}
        </UserContext.Consumer>
    ) : (
        <div className='auction-info'>
            <div className="auction-info-wrapper">
                <div className='auction-info-container'>
                    <div className='auction-info__text'>
                        <h2>Как играть?</h2>
                        <div>Просматривай открытые лоты в левой части сайте</div>
                        <div>Вся информация по лоту в левой части сайта, активируется после выбора лота</div>
                        <div>Участие в лоте стоит не более 1% от стоимости лота, которую установил продавец. Плата за
                            вход помогает продавцу получить полную стоимость за продаваемый скин.
                        </div>
                        <a style={{color: "#2ba7e8", textDecoration: "none"}}>Подробнее</a>
                    </div>
                    <div className='auction-info__content'>
                        <h2>Посмотри короткое видео о сервисе</h2>
                        <div className='auction-info__video'/>
                    </div>
                </div>
            </div>
        </div>
    )
)
