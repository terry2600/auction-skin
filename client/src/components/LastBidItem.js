import React from 'react';
import Label from './Label';
import '../App.css';

export default ({user, bid, paddingLeft}) => (
    <div style={{paddingLeft: paddingLeft}} className='last-bid-item'>
        <div className='last-bid-item__user'>{user}</div>
        <div className='last-bid-item__line'/>
        <Label color='green'
               text={bid}
        />
    </div>
)