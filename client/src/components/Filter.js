import React from 'react';
import Dropdown from 'react-dropdown';
import RangeInputMultiple from './RangeInputMultiple';
import '../App.css';
import {Switch} from 'antd';
import 'antd/dist/antd.css';
import HeroesPopup from "./HeroesPopup";
import Popup from "reactjs-popup";
import Loader from "./Loader";

class Filters extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            proOpen: false,
            isHeroesOpen: false,
            isMobile: true
        };
    };

    componentDidMount() {
        window.addEventListener('resize', this.onResize);
    }

    openPro = (checked) => {
        this.setState({
            proOpen: checked
        })
    };

    openHeroes = () => {
        this.setState({isHeroesOpen: true}, () => document.body.classList.add('modal-open'));
    };

    closeHeroes = () => {
        this.setState({isHeroesOpen: false}, () => document.body.classList.remove('modal-open'));
    };

    bindSubmitHeroes = () => {
        this.props.submitHeroes(this.closeHeroes);
    };

    onResize = () => {
        if (window.innerWidth >= 1000) {
            this.setState({isMobile: false})
        }
        if (window.innerWidth < 1000) {
            this.setState({isMobile: true})
        }
    };

    render() {
        const {
            paramsToSend, filters, games, selectedGame, rangePrice, stepPrice, minPrice, maxPrice, heroes, isFetching,
            changeRangePrice, setActiveHeroes, resetHeroes, sendFilters, onFilterSelect, changeFilterSearch
        } = this.props;
        const {isMobile} = this.state;
        return (
            <div className='filter'>
                {isFetching ? (
                    <Loader/>
                ) : (
                    <div className='filter-wrapper'>
                        <form className='form-filter'>
                            <React.Fragment>
                                <div className='form-filter__item'>
                                    <input
                                        placeholder='Поиск лотов'
                                        type='text'
                                        className='filter__search'
                                        onChange={changeFilterSearch}
                                    />
                                </div>
                                {isMobile ?
                                    <div className='form-filter__item form-filter__item_games'>
                                        <div className='filter__subheading'>Игра</div>
                                        <Dropdown
                                            options={games}
                                            value={selectedGame}
                                            controlClassName='filter__dropdown dropdown'
                                            menuClassName='dropdown_menu'
                                            arrowClassName='dropdown__arrow'
                                            onChange={res => this.props.toggleGame(res)}
                                        />
                                    </div> : ''}
                                {filters.filter(filter => !filter.pro).map(filter => (
                                        <div className='form-filter__item' key={filter.title}>
                                            <div className='filter__subheading'>{filter.title}</div>
                                            {filter.title === "Герой" ? (
                                                <div className='hero_filter filter__dropdown dropdown'
                                                     onClick={this.openHeroes}>Выбрать героя
                                                </div>
                                            ) : (
                                                <Dropdown
                                                    options={filter.values}
                                                    value={{
                                                        value: paramsToSend[filter.type].id,
                                                        label: paramsToSend[filter.type].label
                                                    }}
                                                    onChange={onFilterSelect}
                                                    controlClassName='filter__dropdown dropdown'
                                                    menuClassName='dropdown_menu'
                                                    arrowClassName='dropdown__arrow'
                                                />)}
                                        </div>
                                    )
                                )}
                                <Popup open={this.state.isHeroesOpen}
                                       onClose={this.closeHeroes}
                                       className='hero-popup'
                                       contentStyle={{
                                           borderRadius: '15px', overflow: 'auto',
                                           background: 'rgb(16, 26, 43)', border: 'none',
                                           color: '#3c4b63', fontSize: '12px', padding: '0',
                                           width: '95%', height: '800px', maxHeight: '95%'
                                       }}>
                                    <HeroesPopup
                                        submit={this.bindSubmitHeroes}
                                        reset={resetHeroes}
                                        strHeroes={heroes.strength}
                                        agilityHeroes={heroes.agility}
                                        smartHeroes={heroes.intelligence}
                                        setActive={setActiveHeroes}/>
                                </Popup>
                                {(minPrice < maxPrice) && (
                                    <div className='form-filter__item'>
                                        <div className='filter__subheading'>Цена лота</div>
                                        <RangeInputMultiple
                                            rangePrice={rangePrice}
                                            step={stepPrice}
                                            min={minPrice}
                                            max={maxPrice}
                                            changeRangePrice={changeRangePrice}/>
                                    </div>
                                )}
                                <div className="pro-button">
                                    <div className='filter-switch'>
                                        <div className='filter__subheading' style={{padding: 16}}>
                                            Pro-фильтры
                                        </div>
                                        <Switch onChange={this.openPro} style={{margin: 10}}/>
                                    </div>
                                    {this.state.proOpen ?
                                        filters.filter(filter => filter.pro).map(filter =>
                                            <div className='form-filter__item' key={filter.title}>
                                                <div className='filter__subheading'>{filter.title}</div>
                                                <Dropdown
                                                    options={filter.values}
                                                    value={{
                                                        value: paramsToSend[filter.type].id,
                                                        label: paramsToSend[filter.type].label
                                                    }}
                                                    onChange={onFilterSelect}
                                                    controlClassName='filter__dropdown dropdown'
                                                    menuClassName='dropdown_menu'
                                                />
                                            </div>
                                        ) : ''}
                                </div>
                                <div className='filter-button'>
                                    <div className='button_place-lot' onClick={sendFilters}>Подобрать</div>
                                </div>
                            </React.Fragment>
                        </form>
                    </div>
                )}
            </div>
        )
    };
}

export default Filters;
