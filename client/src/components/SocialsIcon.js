import React from 'react';
import '../App.css';
import icons from '../assets/img/socials-sheet.svg';
import {Link} from 'react-router-dom';

export default ({name}) => (

    <Link className={name === 'odnoklassniki' ?
                    'social-icon social-icon_odnoklassniki' :
                    name === 'vk' ?
                        'social-icon social-icon_vk' :
                        name === 'facebook' ?
                            'social-icon social-icon_facebook' :
                            name === 'twitter' ?
                                'social-icon social-icon_twitter' :
                                'social-icon social-icon_google'

    }>
        <svg className='icon'>
            <use href={`${icons}#${name}`}/>
        </svg>
    </Link>
)