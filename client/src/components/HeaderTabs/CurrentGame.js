import React from "react";
import '../../App.css';

export default ({game, image, color, selectedGame, toggle})=>(
    <div
        className={`current-game_container ${selectedGame.value === game.value ? "active-game" : ''}`}
        onClick = {() => {toggle(game)}}
    >
        <div className='current-game_image' style={{backgroundColor: color}}>
            <img src={image} alt='' style={{width:"15px",height:"15px"}}/>
        </div>
        <div className='current-game_title' style={{marginLeft:"10px"}}>{game.label}</div>
    </div>
)

