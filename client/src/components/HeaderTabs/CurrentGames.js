import React from "react";
import CurrentGame from "./CurrentGame";

export default ({games, selectedGame, toggleGame}) => (
    <div className='logo-items'>
        {games.map(item =>
            <CurrentGame
                key={item.value}
                image={`../${item.img}`}
                game={item}
                color={item.color}
                selectedGame={selectedGame}
                toggle={toggleGame}
            />
        )}
    </div>
)