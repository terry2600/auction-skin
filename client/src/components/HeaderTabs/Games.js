import React from "react";
import '../../App.css';

export default ({games}) =>(
    <div className='current-games'>
        <div >Игры</div>
        <div className={'blue-text'}>{games}</div>
        <div>
            <img src={require("../../assets/img/fire.png")} alt="" style={{width:'15px',height:'20px',marginLeft:'5px'}}/>
        </div>
    </div>
)