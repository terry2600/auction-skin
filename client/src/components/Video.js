import React from "react";
import YouTube from 'react-youtube';
import Popup from "reactjs-popup";

const opts = {
    height: '100%',
    width: '100%',
    playerVars: {
        autoplay: 0
    }
};

export default ({title, videoId, onPlay, onClose, open, index, activeIndex}) => (
    <div className='video__item'>
        <div className='video-item__heading'>{title}</div>
        <div className='you-tube-container' onClick={() => onPlay(index)}>
            <YouTube
                containerClassName='video-container video-container_opacity'
                videoId={videoId}
                opts={opts}
            />
        </div>
        <Popup open={activeIndex === index && open ? open : false}
               onClose={onClose}
               overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
               contentStyle={{
                   overflow: 'hidden',
                   background: 'none', border: 'none',
                   color: '#3c4b63', fontSize: '12px', padding: '0',
                   width: '80%', height: '80%'
               }}
        >
            <YouTube
                containerClassName='video-container'
                videoId={videoId}
                opts={opts}
            />
        </Popup>
    </div>
)