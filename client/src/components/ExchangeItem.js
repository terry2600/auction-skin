import React from "react";

export default ({id, image, name, game, tradeLink, winner, got, sent})=>(
    <div className="exchange-item">
        <div className="exchange-item_item">
            <div className={'bid-img-container bid-img-container_blue'}>
                <img src={image}
                     className='bid-img'
                     alt='Изображение предмета'
                />
            </div>
            <div className={"exchange-item_names"}>
                <div className={"exchange-item_name"}>{name}</div>
                <div className={"exchange-item_game"}>{game}</div>
            </div>

        </div>
            {winner ? (
                <div className="exchange-item_item">
                    <button className="selected-lot__make-bid ex_button" onClick={() => got(id)}>Получил</button>
                </div>
            ) : (
                <div className="exchange-item_item">
                    <button className="selected-lot__make-bid ex_button" onClick={() => window.open(tradeLink, "_blank")}>Передать</button>
                    <button className="selected-lot__make-bid ex_button" onClick={() => sent(id)}>Передал</button>
                </div>
            )}

    </div>
)
