import React from 'react';
import '../App.css';

export default ({text, openModal})=> (
    <span className='button_more' onClick={openModal}>{text}</span>
)