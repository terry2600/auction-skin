import React from 'react';

export default ({label}) => (
    <div className='checkbox'>
        <label className='checkbox-container'>{label}
            <input type='checkbox'
                   value={label}
                   className='checkbox-input_hidden'
            />
            <div className='checkbox__checkmark'/>
        </label>
    </div>
)
