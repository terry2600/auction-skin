import React from 'react';
import UserLotsList from './UserLotsList'
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import '../App.css';

const Text = {
    fontSize: '23px',
    color: '#ffffff',
    fontWeight: '700'
};

export default ({isFetching, lots, total, setSelectedLot}) => (
    isFetching ? (
        <Loader
            type='BallTriangle'
            color='#00BFFF'
            height={300}
            width={300}
        />
    ) : (
        <div className='user-lot-container'>
            <div className="user-lot-wrapper">
                <div className='user-lot-top'>
                    <div style={Text}>Ваши лоты
                        <span style={{color: '#2fb8ff', paddingLeft: '10px'}}>{total}</span>
                    </div>
                </div>
                <UserLotsList
                    list={lots}
                    setSelectedLot={setSelectedLot}
                />
            </div>
        </div>
    )
)

