import React from "react";
import '../../App.css';
import Hero from './Hero'

export default ({title,heroes,setActive})=>(
    <div className='heroes-block'>
        <div className='heroes-block_title_wrapper'>
             <div className='heroes-block_title'>{title}</div>
            <div className='divider'/>
        </div>
        <div className='heroes-block_container'>
            {heroes?heroes.map((item,key)=>(
                <Hero key = {key} heroName={item.name} active = {item.active} image={item.img} setActive={setActive} id = {item.id} type = {item.type}/>
            )):''}
        </div>
    </div>
)