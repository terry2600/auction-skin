import React from "react";
import '../../App.css';


export default ({heroName,image,setActive,id,type,active})=>(
    <div className={`hero${active?'-active':''}`} onClick={()=>{setActive(id,type)}}>
        <div className='hero-image'><img className="hero-image" src={image} alt=""/></div>
        <div className='hero-name'>{heroName}</div>
    </div>
)