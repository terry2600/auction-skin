import React from "react";
import CountDown from "./CountDown";

export default ({lastBid, type, time, distance, makeBid, bidStep}) => (
    <React.Fragment>
        <div className="current-bid__info">
            <div className="current-bid__cost">{typeof lastBid === 'undefined' ? 0 : lastBid.bet_size} &#8381;</div>
            <div className="dash selected-bid__dash"/>
            <div className='current-bid-time'>
                <div className="current-bid-time__text">Время</div>
                <div className="current-bid-time__val">
                    <CountDown type={type} time={time} distance={distance}/>
                    <div className='current-bid-time__icon'>
                        <svg className='icon' viewBox="0 0 27 35">
                            <path d="M26.296,20.967 C25.899,15.793 23.492,12.551 21.369,9.690 C19.402,7.042
                                    17.704,4.755 17.704,1.381 C17.704,1.109 17.552,0.862 17.312,0.738 C17.070,0.613
                                    16.780,0.633 16.561,0.793 C13.372,3.076 10.711,6.925 9.781,10.598 C9.136,13.155
                                    9.051,16.029 9.039,17.927 C6.093,17.298 5.426,12.889 5.419,12.841 C5.386,12.613
                                    5.246,12.4135.043,12.305 C4.838,12.198 4.597,12.190 4.389,12.293 C4.234,12.368
                                    0.598,14.217 0.386,21.599 C0.371,21.845 0.370,22.091 0.370,22.337 C0.370,29.509
                                    6.203,35.345 13.371,35.345 C13.380,35.346 13.391,35.347 13.399,35.345 C13.402,35.345
                                    13.405,35.345 13.408,35.345 C20.559,35.324 26.370,29.497 26.370,22.337 C26.370,21.977
                                    26.296,20.967 26.296,20.967 ZM13.371,33.900 C10.981,33.900 9.037,31.828 9.037,29.281
                                    C9.037,29.194 9.036,29.107 9.043,28.999 C9.072,27.925 9.276,27.192 9.499,26.704
                                    C9.918,27.605 10.667,28.433 11.884,28.433 C12.283,28.433 12.606,28.109 12.606,27.710
                                    C12.606,26.681 12.627,25.494 12.883,24.423 C13.111,23.473 13.655,22.462 14.345,21.652
                                    C14.652,22.704 15.250,23.555 15.834,24.385 C16.670,25.574 17.534,26.802 17.686,28.898
                                    C17.695,29.022 17.704,29.147 17.704,29.281 C17.704,31.828 15.760,33.900 13.371,33.900 Z"/>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div className='outbid-container'>
            <div className="current-bid-step">
                <div className="current-bid-step__text">Шаг</div>
                <div className="current-bid-step__val">{bidStep} &#8381;</div>
            </div>
            <button className='selected-lot__make-bid' onClick={makeBid}>Перебить ставку</button>
        </div>
    </React.Fragment>
)