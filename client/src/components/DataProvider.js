import {stringify} from 'querystring'
import {GET_LIST, GET_ONE, CREATE, DELETE, DELETE_MANY, UPDATE} from 'react-admin'

export default (type, resource, params) =>{
    let url = '/users';
    let query;
    const options = {
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        })
    };
    switch (type) {
        case CREATE:
            options.method = 'POST';
            options.body = JSON.stringify(params.data);
            break;
        case GET_ONE:
            url = `/users/user/${Number(params.id)}`;
            break;
        case UPDATE:
            url = `/users/${params.id}`;
            options.method = 'PUT';
            options.body = JSON.stringify(params.data);
            break;
        case DELETE:
            url = `users/${params.id}`;
            options.method = 'DELETE';
            break;
        case DELETE_MANY:
            query = JSON.stringify({ids: params.ids});
            url = `/users/${query}`;
            options.method = 'DELETE';
            break;
        case GET_LIST:
            const {page, perPage} = params.pagination;
            const { field, order } = params.sort;
            query = JSON.stringify({
                sort: [field, order],
                range: [(page - 1) * perPage, page * perPage - 1],
                filter: params.filter
            });
            url = `/users/${query}`;
            break

    }
    return fetch(url, options)
        .then(res => {
            return res.json()
        })
        .then(json => {
            switch(type){
                case GET_ONE:
                    return {
                        data: json
                    };
                case GET_LIST:
                    return {
                        data: json.users,
                        total: json.total
                    };
                case CREATE:
                    return {
                        data: {...params.data, id: json[0].id},
                    };
                case UPDATE:
                    return {
                        data: {...params.data},
                    };
                default:
                    return { data: json };
            }

        })
}