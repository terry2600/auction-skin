import React from "react";
import SameLotsItem from "./SameLotsItem";
import ButtonMore from "./ButtonMore";

export default ({SameLots})=>{
    return(
        <div>
            <div className='table__row'>
                <span className='table__heading'>Дата</span>
                <span className='table__heading'>Победитель</span>
                <span className='table__heading'>Цена</span>
            </div>
            {SameLots.map(item =>(
                <div className='table__row'>
                  <div className='table__item'>
                      <SameLotsItem
                          date={item.end_date}
                          user={item.LotResult.User.steam_nick}
                          price={item.LotResult.purchase_price}
                      />
                  </div>
                </div>
                )
            )}
            <div className='popup__more'>
                <ButtonMore text='Показать всех'/>
            </div>
        </div>
    )
}