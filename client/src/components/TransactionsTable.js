import React from "react";
import Label from './Label';
import 'react-perfect-scrollbar/dist/css/styles.css';
import {Scrollbars} from "react-custom-scrollbars";
import Date from "./Date/Date";

export default ({transactions}) => (
    <div className='profile-tab-wrapper'>
        {/*<table className='table profile-table'>*/}
        {/*    <thead>*/}
        {/*    <tr className="table__row">*/}
        {/*        <th className="table__heading">Дата</th>*/}
        {/*        <th className="table__heading">Номер лота</th>*/}
        {/*        <th className="table__heading">Наименование</th>*/}
        {/*        <th className="table__heading">Тип операции</th>*/}
        {/*        <th className="table__heading">Сумма</th>*/}
        {/*    </tr>*/}
        {/*    </thead>*/}
        {/*</table>*/}
        <Scrollbars style={{height: 400}} className='scrollbar-container_custom'>
            <table className='table profile-table'>
                <thead>
                     <tr className="table__row">
                        <th className="table__heading">Дата</th>
                        <th className="table__heading">Номер лота</th>
                        <th className="table__heading">Наименование</th>
                        <th className="table__heading">Тип операции</th>
                        <th className="table__heading">Сумма</th>
                    </tr>
                </thead>
                <tbody>
                {transactions.map((item, index) => (
                    <tr className='table__row' key={index}>
                        <td className='table__item'><Date date={item.createdAt} type='full'/></td>
                        <td className='table__item'>{item.lot_id}</td>
                        <td className='table__item'>{item.lot_name}</td>
                        <td className='table__item'>{item.TransactionType.description}</td>
                        <td className='table__item'><Label text={item.amount} color={'green'}/></td>
                    </tr>
                ))}
                </tbody>
            </table>
        </Scrollbars>
    </div>

)
