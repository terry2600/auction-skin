import React from "react";

export default ({ color, title, text, icon }) => (
        <div className='lot-warning-item' style={{borderColor: color, display: "flex", color: "#49566d"}}>
            <div className='lot-warning_icon'>
                <img src={icon} alt="" style={{width: '40px'}}/>
            </div>
            <div className='lot-warning-content'>
                <h3 style={{color: "white"}}>{title}</h3>
                <div>{text}</div>
            </div>
        </div>
)