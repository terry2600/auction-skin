import React from 'react';
import '../App.css';

export default ({type, text, onClick}) => {
    const chars = [];

    for (let index in text) {
        chars.push(<span key={index}>{text[index]}</span>)
    }

    return (
        <button type={type} className='dribble_button' onClick={onClick}>
            <div>
                {chars}
            </div>
        </button>
    )
}

