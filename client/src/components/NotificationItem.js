import React from "react";

const NotificationItem = ({title, text, type, onClick}) => (
    <div className='notification-item' onClick={() => onClick(title)}>
        <div className='notification-item-wrapper'>
            <div className='notification-item__icon'>
                <img src={type === 'success' ? require('../assets/img/green-warn.png') : require('../assets/img/red-warn.png')} alt="" style={{width: '40px'}}/>
            </div>
            <div className='notification-item-content'>
                <h3 className='notification-item__heading'>{title}</h3>
                <div className='notification-item__text'>{text}</div>
            </div>
        </div>
    </div>
);

export default NotificationItem;