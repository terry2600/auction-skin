import React from 'react';
import Label from './Label';
import ButtonMore from './ButtonMore';
import LotLink from './LotLink';
import '../App.css';

export default ({img, name, type, exterior, steamId, color, marketHN, appid,stickers}) => (
    <div>
        <div className='selected-lot__main-info'>
            <div
                className='lot-img-container'
                style={appid === 570 ? {background: 'none'} : {background: `linear-gradient(135deg, ${color} 0%, ${color}00 100%)`, border: 'none'}}>
                <img src={img}
                     className='lot-img'
                     alt='Изображение предмета'
                />
            </div>
            <div className='selected-lot__info'>
                <div className='selected-lot__name'>
                    <div>{name}</div>
                    <div className='selected-lot__type'>
                        <Label text={type} style={{marginRight: '10px'}}/>
                        <Label color='dark-green' text={exterior}/>
                    </div>
                </div>
                <div className='selected-lot__game-logo'>
                    <img
                        className='selected-lot__game-logo-img'
                        src={require('../assets/img/cs-go-logo.png')}
                        alt='Изображение игры'
                    />
                </div>
            </div>
        </div>
        <div className='selected-lot-info_container'>
            <div className='selected-lot__links'>
                <LotLink color={'#2fb7ff'} link={`https://steamcommunity.com/market/listings/${appid}/${marketHN}`}
                         icon={require('../assets/img/icon-1.png')}/>
                <LotLink color={'#5f89cb'} link={`https://steamcommunity.com/profiles/${steamId}/inventory/`}
                         icon={require('../assets/img/icon-2.png')}/>
            </div>
            <div className='selected-lot__description'>Наиболее примечательной особенностью ножа-потрошителя является
                крюк
                для потрошения
                на тыльной стороне лезвия. Первоначально применявшийся как вспомогательное средство
                для свежевания животных
                {stickers?`Наклейки на этом предмете:${JSON.parse(stickers)}`:``}
                <ButtonMore text='подробнее'/></div>
        </div>
    </div>
)