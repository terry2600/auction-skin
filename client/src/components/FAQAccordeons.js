import React from "react";
import Accordion from 'react-tiny-accordion';
import '../App.css';

export default ({faqs}) => (
    faqs.map(faq => (
        <Accordion className='accordion' key={faq.id}>
            <div className='accordion-item' data-header={faq.title}>
                <div className='accordion__text' dangerouslySetInnerHTML={{__html: faq.text}} />
            </div>
        </Accordion>
    ))
)
