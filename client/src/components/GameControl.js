import React from 'react';
import Bids from '../components/Bids';
import CircleImg from '../components/CircleImg';
import CountDown from "../components/CountDown";

class GameControl extends React.Component {
    render() {
        const {lot, lastBid, time, timer, makeBid, endedLot} = this.props;
        return (
            timer === 'lot' ? (
                <div className='timer-countdown'>
                    <CountDown type='lot' time={time} stopCountDown={this.stopCountDown}/>
                </div>
            ) : (
                <div className="popup-current-bid current-bid">
                    {!endedLot ? (
                        <React.Fragment>
                            <div className='current-bid__last-info'>
                                <div className="current-bid__heading">Текущая ставка</div>
                                {lastBid ? (
                                    <React.Fragment>
                                        <CircleImg
                                            width='22px'
                                            height='22px'
                                            avatar={lastBid.avatar}/>
                                        <div className="current-bid__nickname">{lastBid.steam_nick}</div>
                                    </React.Fragment>
                                ) : (
                                    <div className="current-bid__nickname">Ставок еще не было</div>
                                )}
                            </div>
                            <Bids
                                lastBid={lastBid}
                                type='lot'
                                time={time}
                                makeBid={makeBid}
                                bidStep={lot.lot_step}
                                stopCountDown={this.stopCountDown}
                            />
                        </React.Fragment>
                    ) : (
                        <div className='winner-info'>
                            <div className='winner-info__heading'>Победитель аукциона</div>
                            <div className='winner'>
                                <div className='winner__name'>{endedLot.winner_nick}</div>
                                <CircleImg
                                    height='80px'
                                    width='80px'
                                    avatar={endedLot.winner_avatar}/>
                            </div>
                            <div className='winner-info__subheading'>Он получил</div>
                            <div className='winner-item'>
                                <div className='winner-item__name'>{endedLot.item_name}</div>
                                <div className='winner-item__icon'>
                                    <img className='winner-item__icon-image' src={endedLot.item_icon}
                                         alt='Выигранный предмет'/>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            )
        )
    }
}

export default GameControl;
