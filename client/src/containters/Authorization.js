import React from "react";
import Can from '../components/Can';
import {UserContext} from '../App'

class Authorization extends React.Component {
    render() {
        return (
            <UserContext.Consumer>
                {({role}) => (
                    <Can
                        role={role}
                        resource='auth'
                        action='read:own'
                        yes={() => (
                            <div className='authorization_container'>
                                <a href={'/api/auth/steam'} className='button button_steam button_big button_transparent'>Войти</a>
                            </div>
                        )}
                        no={() => null}
                    />
                )}
            </UserContext.Consumer>
        )
    }
}

export default Authorization;
