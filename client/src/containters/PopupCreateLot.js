import React from "react";
import Popup from "reactjs-popup";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import TabPillCreate from '../components/TabPillCreate';
import CreateLotPanel from '../components/CreateLotPanel';
import Button from "../components/Button";
import {notification } from 'antd'

const openNotificationWithIcon = (type,title,text) => {
    notification[type]({
        message: title,
        description: text,
    });
};

const defaultNotification = {
    active: false,
    type: null,
    title: null,
    text: null
};

const initialState = {
    tabIndex: 0,
    activeId: null,
    isOpen: false,
    inventory: [],
    filteredInventory: [],
    searchText: '',
    priceText: '',
    participantsText: '',
    priceTextError: false,
    participantsTextError: false,
    selectedItem: null,
    selectedItemError: false,
    isFetching: true,
    isSending: false,
    creationNotificationItem: null,
    creationNotification: defaultNotification
};

class PopupCreateLot extends React.Component {
    constructor(props) {
        super(props);
        this.state = initialState;
        this.controller = new AbortController()
    }

    openModal = () => {
        this.setState({isOpen: true}, () => document.body.classList.add('modal-open'));
        this.getCsItems();
    };

    closeModal = () => {
        this.setState(initialState, () => document.body.classList.remove('modal-open'));
    };

    getInventory = (index, lastIndex) => {
        try {
            if (index !== lastIndex) {
                switch (index) {
                    case 0:
                        this.getCsItems();
                        break;
                    case 1:
                        this.getDotaItems();
                        break;
                    default:
                        break;
                }
            }
        }
        catch (e) {
            console.log(e)
        }
    };

    getCsItems = () => {

            if (this.state.isOpen) {
                this.setState({
                    activeId: null,
                    selectedItem: null,
                    isFetching: true
                })
            }
            fetch(`/api/users/${this.props.steamId}/inventory/730`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => {
                    if(!response.ok) throw new Error('err');
                    return response.json()})
                .then(inventory => {
                    console.log(inventory);
                    inventory.forEach(item => item.icon_url = 'http://cdn.steamcommunity.com/economy/image/' + item.icon_url);
                    this.setState({inventory, isFetching: false});
                })
                .catch(err=>{
                    openNotificationWithIcon('error','Не удалось получить предметы','К сожалению, в данный момент невозможно получить предметы из этой игры, повторите запрос позже!')

                })
    };

    getDotaItems = () => {
            this.setState({
                activeId: null,
                selectedItem: null,
                isFetching: true
            });
            fetch(`/api/users/${this.props.steamId}/inventory/570`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => {
                    if(!response.ok) throw new Error('err');
                    return response.json()})
                .then(inventory => {
                    console.log(inventory);
                    inventory.forEach(item => item.icon_url = 'http://cdn.steamcommunity.com/economy/image/' + item.icon_url);
                    this.setState({inventory, isFetching: false});
                })
                .catch(err=>{
                    openNotificationWithIcon('error','Не удалось получить предметы','К сожалению, в данный момент невозможно получить предметы из этой игры, повторите запрос позже!')

                })

    };

    selectItem = (item, id) => {
        try{
            this.setState(prevState => {
                let activeId = prevState.activeId === id ? null : id;

                return {
                    selectedItem: item,
                    activeId: activeId
                }
            });
        }
        catch (e) {
            console.log(e)
        }
    };

    handleSearchChange = (e) => {
        try{
            let filteredInventory = this.state.inventory.filter(item => {
                return item.market_name.toUpperCase().includes(e.target.value.toUpperCase())
            });
            this.setState({searchText: e.target.value, filteredInventory: filteredInventory})
        }
        catch (e) {
            console.log(e)
        }
    };

    handlePriceChange = (e) => {
        let price = e.target.value;
        if (price === '' || /^(?!0+$)[0-9]{1,10}$/.test(price)) {
            this.setState({priceText: price})
        }
    };

    handleParticipantsChange = (e) => {
        let participants = e.target.value;
        if (participants === '' || /^(?!0+$)[0-9]{1,10}$/.test(participants)) {
            this.setState({participantsText: participants})
        }
    };

    validate = () => {
        let priceTextError = false;
        let participantsTextError = false;
        let selectedItemError = false;

        if (!this.state.priceText) {
            priceTextError = true;
        }

        if (!this.state.participantsText) {
            participantsTextError = true;
        }

        if (!this.state.selectedItem) {
            selectedItemError = true;
        }

        if (priceTextError || participantsTextError || selectedItemError) {
            this.setState({priceTextError, participantsTextError, selectedItemError});
            return false;
        }

        this.setState({priceTextError, participantsTextError, selectedItemError});
        return true;
    };

    submitLot = (e) => {
        try{
            e.preventDefault();
            const isValid = this.validate();
            if (!isValid) return;
            this.setState(prevState => {
                return {
                    creationNotificationItem: prevState.selectedItem,
                    isSending: true
                }
            }, async () => {
                const hasExtension = await this.props.checkExtension();
                if (hasExtension) {
                    this.sendLot();
                } else {
                    this.setState({
                        isSending: false,
                        creationNotification: {
                            type: 'error',
                            active: true,
                            title: 'У вас не установлено расширение',
                            text: 'Вам необходимо установить расширение для размещения лота'
                        }
                    })
                }
            })
        }
        catch (e) {
            console.log(e)
        }
    };

    sendLot = () => {
        try{
            fetch(`/api/inventory`, {
                signal: this.controller.signal,
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({
                    item: this.state.selectedItem,
                    price: this.state.priceText,
                    participants: this.state.participantsText,
                    lot_step: Math.ceil(this.state.priceText / this.state.participantsText),
                    cost_of_participation: Math.ceil(this.state.priceText / this.state.participantsText),
                    hotlot: false
                })
            })
                .then(response => response.json())
                .then(({lot}) => {
                    this.setState(prevState => {
                        const inventory = prevState.inventory.filter(inventoryItem => inventoryItem.assetid !== prevState.creationNotificationItem.assetid);
                        return {
                            inventory,
                            selectedItem: null,
                            isSending: false,
                            creationNotification: {
                                type: 'success',
                                active: true,
                                title: 'Вы разместили лот!',
                                text: 'Вы успешно разместили лот'
                            }
                        }
                    });
                })
                .catch(error => {
                    this.setState({
                        isSending: false,
                        creationNotification: {
                            type: 'error',
                            active: true,
                            title: 'Не удалось разместить лот!',
                            text: 'Попробуйте повторить попытку'
                        }
                    })
                });
        }
        catch (e) {
            console.log(e)
        }
    };

    resetAfterCreation = () => {
        this.setState({
            creationNotification: defaultNotification
        })
    };

    componentWillUnmount() {
        this.controller.abort();
    }

    render() {
        const {
            tabIndex, activeId, isOpen, inventory,
            filteredInventory, searchText, priceText, participantsText,
            isFetching, isSending, priceTextError, participantsTextError,
            selectedItemError, creationNotificationItem, creationNotification
        } = this.state;
        return (
            <React.Fragment>
                <Button
                    text='Создать лот'
                    type='button_big button_add button_transparent'
                    onClick={this.openModal}
                    name={'createLot'}
                />
                <Popup
                    open={isOpen}
                    closeOnDocumentClick={false}
                    onClose={this.closeModal}
                    overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                    contentStyle={{
                        borderRadius: '15px',
                        background: 'rgb(16, 26, 43)', border: 'none',
                        color: '#3c4b63', fontSize: '12px', padding: '0',
                        width: '60%', maxHeight: '90%', minHeight: '646px'
                    }}>
                    <div className='create-lot-wrapper'>
                        <div className='modal-close' onClick={this.closeModal}/>
                        <Tabs selectedIndex={tabIndex} onSelect={(index, lastIndex) => {
                            if (!isSending) {
                                this.setState({tabIndex: index},
                                    () => this.getInventory(index, lastIndex))
                            }
                        }}>
                            <div className='create-lot-top'>
                                <div className="create-lot__heading">Создание лота</div>
                                <TabList className='tab-list_create'>
                                    <Tab
                                        className='tab-pill tab-pill_create'>
                                        <TabPillCreate
                                            img={require('../assets/img/cs-go_create.png')}
                                            text='CS:GO'/>
                                    </Tab>
                                    <Tab
                                        className='tab-pill tab-pill_create'>
                                        <TabPillCreate
                                            img={require('../assets/img/dota_create.png')}
                                            text='Dota 2'/>
                                    </Tab>
                                </TabList>
                            </div>
                            <TabPanel>
                                <CreateLotPanel
                                    tabIndex={tabIndex}
                                    inventory={searchText ? filteredInventory : inventory}
                                    activeId={activeId}
                                    searchText={searchText}
                                    priceText={priceText}
                                    participantsText={participantsText}
                                    priceTextError={priceTextError}
                                    participantsTextError={participantsTextError}
                                    isFetching={isFetching}
                                    isSending={isSending}
                                    selectedItemError={selectedItemError}
                                    creationNotificationItem={creationNotificationItem}
                                    creationNotification={creationNotification}
                                    selectItem={this.selectItem}
                                    handleSearchChange={this.handleSearchChange}
                                    handlePriceChange={this.handlePriceChange}
                                    handleParticipantsChange={this.handleParticipantsChange}
                                    submitLot={this.submitLot}
                                    resetAfterCreation={this.resetAfterCreation}
                                />
                            </TabPanel>
                            <TabPanel>
                                <CreateLotPanel
                                    tabIndex={tabIndex}
                                    inventory={searchText ? filteredInventory : inventory}
                                    activeId={activeId}
                                    searchText={searchText}
                                    priceText={priceText}
                                    participantsText={participantsText}
                                    priceTextError={priceTextError}
                                    participantsTextError={participantsTextError}
                                    isFetching={isFetching}
                                    isSending={isSending}
                                    selectedItemError={selectedItemError}
                                    creationNotificationItem={creationNotificationItem}
                                    creationNotification={creationNotification}
                                    selectItem={this.selectItem}
                                    handleSearchChange={this.handleSearchChange}
                                    handlePriceChange={this.handlePriceChange}
                                    handleParticipantsChange={this.handleParticipantsChange}
                                    submitLot={this.submitLot}
                                    resetAfterCreation={this.resetAfterCreation}
                                />
                            </TabPanel>
                        </Tabs>
                    </div>
                </Popup>
            </React.Fragment>
        )
    }
}

export default PopupCreateLot;
