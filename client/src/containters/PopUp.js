import React from 'react';
import SelectedLotItem from '../components/SelectedLotItem';
import Button from '../components/Button';
import ParticipantsList from "../components/ParticipantsList";
import HistoryTable from "../components/HistoryTable";
import LastBidsTable from "../components/LastBidsTable";
import GameControl from "../components/GameControl";
import Loader from "../components/Loader";
import {UserContext} from "../App";

class PopUp extends React.Component {
    render() {
        const {
            isOpen, lot, params, participants, translateTo, isFetching,
            lastBids, time, timer, makeBid, closePopup, similarLots, endedLot
        } = this.props;
        return (
            <UserContext.Consumer>
                {() => (
                    <div className='popup'
                         style={{top: isOpen ? `${translateTo}px` : `100%`}}>
                        <div className="popup-wrapper">
                            {isOpen ? (
                                <React.Fragment>
                                    <div className='popup-button'>
                                        <Button text='Закрыть'
                                                type='button_small button_transparent'
                                                onClick={() => closePopup(lot.id)}
                                        />
                                    </div>
                                    {isFetching ? (
                                        <div>
                                            <Loader
                                                height={300}
                                                width={300}
                                                top='20%'
                                            />
                                        </div>
                                    ) : (
                                        <div className="popup-container">
                                            <div className="popup-col">
                                                <ParticipantsList participants={participants}/>
                                            </div>
                                            <div className="popup-col">
                                                <div className="popup-lot">
                                                    <SelectedLotItem
                                                        img={lot.GameItem.icon_url}
                                                        name={lot.GameItem.name}
                                                        type={params.CsType?params.CsType.name_ru:params.DotaType.name_ru}
                                                        exterior={params.CsExterior?params.CsExterior.name_ru:params.DotaRarity.name_ru}
                                                        appid={lot.app_id}
                                                    />
                                                    <div className='popup-lot-cost'>
                                                        <div className='popup-lot-cost__text'>Рыночная стоимость лота:</div>
                                                        <div className='popup-lot-cost__val'>4700 &#8381;</div>
                                                    </div>
                                                </div>
                                                <div className="popup-history-lot">
                                                    <HistoryTable

                                                        title='История похожих лотов'
                                                        col1='Дата'
                                                        col2='Победитель'
                                                        col3='Цена'
                                                        list={similarLots}
                                                        lotId={lot.id}
                                                    />
                                                </div>
                                            </div>
                                            <div className="popup-col">
                                                <GameControl
                                                    lot={lot}
                                                    lastBid={lastBids[0]}
                                                    time={time}
                                                    timer={timer}
                                                    makeBid={makeBid}
                                                    endedLot={endedLot}
                                                />
                                                <LastBidsTable   name = {lot.GameItem.name} lastBids={lastBids} lotId={lot.id}/>
                                            </div>
                                        </div>
                                    )}
                                </React.Fragment>
                            ) : null}
                        </div>
                    </div>)}
            </UserContext.Consumer>

        )
    }
}

export default PopUp;
