import React from 'react';
import Burger from '../components/Burger';
import {Link} from 'react-router-dom';
import Label from '../components/Label';
import Can from '../components/Can';

class Sidebar extends React.Component {

    burgerClick = () => {
        this.props.onClick()
    };

    render() {
        const {isToggled, openModal, role, balance} = this.props;
        return (
            <aside className='sidebar-wrapper'>
                <div className='sidebar-header'>
                    <div className='sidebar__heading'>Основное меню</div>
                    <div className='sidebar__burger'>
                        <Burger onClick={this.burgerClick}
                                open={isToggled}/>
                    </div>
                </div>
                <nav className='sidebar-nav'>
                     <span className='sidebar-nav__item'>
                            <Link className='sidebar-nav__link' to='/faq'>FAQ</Link>
                        </span>
                    <div className="sidebar-nav__tablet">
                        <Can
                            role={role}
                            perform='profile:update'
                            yes={() => (
                                <div className='sidebar-nav__mobile'>
                                    <span className='sidebar-nav__item'>
                                         <Link className='sidebar-nav__link' to='/profile'>Профиль пользователя
                                            <Label color='green' text={`${balance} Р`} style={{marginTop: '6px'}}/>
                                         </Link>
                                    </span>
                                </div>
                            )}
                            no={() => null}
                        />
                        <Can
                            role={role}
                            perform='main-page:create'
                            yes={() => (
                                <div className='sidebar-nav__tablet_small'>
                                    <span className='sidebar-nav__item'>
                                        <span className='sidebar-nav__link' onClick={() => {
                                            openModal('createLot');
                                            this.burgerClick()
                                        }}>Создать лот
                                        </span>
                                    </span>
                                </div>
                            )}
                            no={() => null}
                        />
                        <span className='sidebar-nav__item'>
                            <span className='sidebar-nav__link' onClick={() => {
                                openModal('bots');
                                this.burgerClick()
                            }}>Боты</span>
                        </span>
                        <span className='sidebar-nav__item'>
                             <Link className='sidebar-nav__link' to='/sample'>Настройки</Link>
                         </span>
                    </div>
                    <span className='sidebar-nav__item'>
                        <Link className='sidebar-nav__link' to='/exchange'>Обмены</Link>
                    </span>
                    <span className='sidebar-nav__item'>
                        <Link className='sidebar-nav__link' to='/userlots'>Лоты</Link>
                    </span>
                    <span className='sidebar-nav__item'>
                        <Link className='sidebar-nav__link' to='/'>И кое-что еще</Link>
                    </span>
                </nav>
            </aside>
        )
    }
}

export default Sidebar;
