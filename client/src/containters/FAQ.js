import React from 'react';
import FAQTabs from '../components/FAQTabs';
import Video from '../components/Video';
import '../App.css';

class FAQ extends React.Component {

    state = {
        open: false,
        activeIndex: null,
        tabs: []
    };

    controller = new AbortController();

    componentDidMount() {
        this.getTabs()
    }

    getTabs = () => {
        try {
            fetch(`/api/faqTab/faqs`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(data => {
                    const sortByOrder = (a, b) => a.order - b.order;
                    this.setState({
                        isFetchingLots: false,
                        isFetchingTab: false,
                        tabs: data.map(tab => {
                            tab.Faqs = tab.Faqs.sort(sortByOrder);
                            return tab
                        }).sort(sortByOrder),
                        activeIndex: 0
                    })
                })
                .catch(error => console.log(`ОШИБКА!!!!!!!!! ${error}`));
        } catch (e) {
            console.log('Ошибка');
            console.log(e)
        }
    };

    openModal = (index) => {
        this.setState({
            open: true,
            activeIndex: index
        })
    };

    closeModal = () => {
        this.setState({open: false})
    };

    componentWillUnmount() {
        this.controller.abort();
    }

    render() {
        const {open, activeIndex} = this.state;
        return (
            <div className="page-content-wrapper">
                <div className='content content-faq'>
                    <div className="content-container content-container-faq">
                        <div className="content-container-header">
                            <div className='heading'>
                                <div className='heading__faq'>Частые вопросы</div>
                            </div>
                        </div>
                        <div className="container-body container-faq-body">
                            <FAQTabs tabs={this.state.tabs}/>
                            <div className='video-list-container'>
                                <Video title="Как играть?" videoId='2g811Eo7K8U' onPlay={this.openModal} open={open}
                                       onClose={this.closeModal} index={1} activeIndex={activeIndex}/>
                                <Video title="Как пополнить и вывести баланс?" videoId='01Q_f2VgnSI'
                                       onPlay={this.openModal} open={open} onClose={this.closeModal} index={2}
                                       activeIndex={activeIndex}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

export default FAQ;
