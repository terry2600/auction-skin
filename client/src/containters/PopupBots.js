import React from "react";
import Popup from "reactjs-popup";
import Label from "../components/Label";
import ReactPaginate from "react-paginate";
import 'react-perfect-scrollbar/dist/css/styles.css';
import '../App.css';
import {Scrollbars} from "react-custom-scrollbars";
import icons from "../assets/img/headesr-sheet.svg";
import Loader from "react-loader-spinner";
import {Link} from "react-router-dom";

const RANGE = 8;
const LINK = 'https://steamcommunity.com/profiles/';

const initialState = {
    bots: [],
    isOpen: false,
    isFetching: true
};

class PopupBots extends React.Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    openModal =()=> {
        this.setState({isOpen: true}, () => document.body.classList.add('modal-open'));
        this.getBots();
    };

    closeModal=()=> {
        this.setState(initialState, () => document.body.classList.remove('modal-open'));
    };

    getBots=(range = [0, 7])=> {
        try{
            if (this.state.isOpen) {
                this.setState({
                    isFetching: true
                })
            }
            fetch(`/api/bots?sort=["id","DESC"]&range=[${range}]&filter={}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(bots => {
                    for (let bot of bots.data) {
                        bot.link = LINK + bot.steamid64;
                    }
                    this.setState({
                        pageCount: Math.ceil(bots.total / RANGE),
                        bots: bots.data,
                        isFetching: false
                    })
                });
        }
        catch (e) {
            console.log(e)
        }
    };

    changePage=(page)=> {
        const start = page.selected * RANGE;
        this.getBots(start, start + RANGE - 1)
    }

    render() {
        const {isOpen, bots, isFetching, pageCount} = this.state;
        const {text, name} = this.props;
        return (
            <React.Fragment>
                <div className='bot-popup-trigger' onClick={this.openModal}>
                    <div className='icons-header-bots'>
                        <div className='icons-header_icon-bots'></div>
                        <span className='tab-text'>Боты</span>
                    </div>
                </div>
                <Popup open={isOpen}
                       onClose={this.closeModal}
                       overlayStyle={{background: 'rgba(0, 0, 0, .7)', cursor: 'default'}}
                       contentStyle={{
                           borderRadius: '15px', overflow: 'hidden',
                           background: 'rgb(16, 26, 43)', border: 'none',
                           color: '#3c4b63', fontSize: '12px', padding: '0',
                           width: '55%'
                       }}
                >
                    <div className='bot-list-wrapper'>
                        <div className='modal-close' onClick={this.closeModal}/>
                        <div className='modal-pagination-container'>
                            <div className="bot-list__heading">Список ботов</div>
                            <ReactPaginate
                                pageCount={pageCount}
                                pageRangeDisplayed={4}
                                marginPagesDisplayed={1}
                                nextLabel={''}
                                previousLabel={''}
                                previousClassName={'previous-label'}
                                nextClassName={'next-label'}
                                containerClassName={'pagination'}
                                pageClassName={'pagination-page'}
                                pageLinkClassName={'pagination-link'}
                                onPageChange={(page) => this.changePage(page)}
                            />
                        </div>
                        <Scrollbars
                            autoHide
                            style={{width: 'auto', height: 300}}
                            className='scrollbar-container_custom'
                            renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                            renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                            renderView={props => <div {...props} className="view"/>}
                        >
                            {isFetching ? (
                                <Loader
                                    type='BallTriangle'
                                    color='#00BFFF'
                                    height={300}
                                    width={300}
                                />
                            ) : (
                                <table className='bots-table table'>
                                    <thead>
                                    <tr className="table__row">
                                        <th className='table__heading'>Номер</th>
                                        <th className='table__heading'>Имя бота</th>
                                        <th className='table__heading'>Профиль</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {bots.map(bot => (
                                        <tr className='table__row' key={bot.id}>
                                            <td className='table__item'>{bot.id}</td>
                                            <td className='table__item'>{bot.steam_nick}</td>
                                            <td className='table__item'><Label text='Перейти в профиль бота'
                                                                               link={bot.link}/></td>
                                        </tr>
                                    ))}
                                    </tbody>
                                </table>
                            )}
                        </Scrollbars>
                    </div>
                </Popup>
            </React.Fragment>
        )
    }
}

export default PopupBots;

