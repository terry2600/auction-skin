import React from "react";
import Popup from "reactjs-popup";
import {Scrollbars} from "react-custom-scrollbars";
import '../App.css';
import DribbleButton from "../components/DribbleButton";

class PopupTradeURL extends React.Component {
    constructor(props) {
        super(props);

        this.state = this.props.hasTradeUrl ? {isOpen: false} : {isOpen: true, tradeUrl: ''}
    }

    openModal=()=> {
        this.setState({isOpen: true}, () => document.body.classList.add('modal-open'));
        this.getSimilarLots(this.props.lotId);
    };

    closeModal=()=> {
        this.setState({isOpen: false}, () => document.body.classList.remove('modal-open'));
    };

    handleInputChange = e => {
        this.setState({tradeUrl: e.target.value})
    }

    submitTradeUrl = e => {
        try{
            e.preventDefault();
            fetch(`/api/users/setTradeUrl`, {
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({tradeUrl: this.state.tradeUrl})
            })
                .then(response => response.json())
                .then(tradeUrl => this.props.changeTradeUrl(tradeUrl))
                .catch(error => {
                    console.log(error);
                });
        }
       catch (e) {
           console.log(e)
       }
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(prevProps)
        console.log(this.props)
    }

    render() {
        const {isOpen, tradeUrl} = this.state;
        return (
            <React.Fragment>
                <Popup open={isOpen}
                       onClose={this.closeModal}
                       overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                       contentStyle={{
                           borderRadius: '15px', overflow: 'hidden',
                           background: 'rgb(16, 26, 43)', border: 'none',
                           color: '#3c4b63', fontSize: '12px', padding: '0',
                           width: '55%'
                       }}
                >
                    <div className='modal-wrapper'>
                        <div className="modal-text">
                            <div className="modal__heading">Введите ваш tradeUrl</div>
                        </div>
                        <Scrollbars
                            autoHide
                            hideTracksWhenNotNeeded={true}
                            style={{width: 'auto', height: 300}}
                            className='scrollbar-container_custom'
                            renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                            renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                            renderView={props => <div {...props} className="view"/>}
                        >
                            <div className="modal-content">
                                <form onSubmit={this.submitTradeUrl}>
                                    <input
                                        value={tradeUrl}
                                        onChange={this.handleInputChange}
                                        placeholder='Начните вводить trade url'
                                        type='text'
                                        className='filter__search filter__search_create'/>
                                    <DribbleButton type='submit' text='Отправить'/>
                                </form>
                            </div>
                        </Scrollbars>
                    </div>
                </Popup>
            </React.Fragment>
        )
    }
}

export default PopupTradeURL;
