import React from "react";
import Popup from "reactjs-popup";
import LastBidTime from '../components/LastBidTime';
import LastBidItem from '../components/LastBidItem';
import 'react-perfect-scrollbar/dist/css/styles.css';
import {Scrollbars} from "react-custom-scrollbars";
import ButtonMore from "../components/ButtonMore";
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const initialState = {
    historyBids: [],
    isOpen: false,
    isFetching: true
};

class PopupHistoryBid extends React.Component {
    constructor(props) {
        super(props);
        this.state = initialState;
    }

    openModal = () => {
        this.setState({isOpen: true}, () => document.body.classList.add('modal-open'));
        this.getHistoryBids();
    };

    closeModal = () => {
        this.setState(initialState, () => document.body.classList.remove('modal-open'));
    };

    getHistoryBids = () => {
        try{
            this.setState(prevState => {
                if (prevState.isOpen) return {isFetching: true};
            }, () => {
                fetch(`/api/lots/${this.props.lotId}/bids`, {
                    method: "GET",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*"
                    }
                })
                    .then(response => response.json())
                    .then(historyBids => {
                        this.setState({
                            historyBids: historyBids,
                            isFetching: false
                        })
                    });
            });
        }
        catch (e) {
            console.log(e)
        }
    };

    render() {
        const {isOpen, historyBids, isFetching} = this.state;
        return (
            <React.Fragment>
                <div className='popup__more'>
                    <ButtonMore text='Показать все' openModal={this.openModal}/>
                </div>
                <Popup open={isOpen}
                       onClose={this.closeModal}
                       overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                       contentStyle={{
                           borderRadius: '15px', overflow: 'hidden',
                           background: 'rgb(16, 26, 43)', border: 'none',
                           color: '#3c4b63', fontSize: '12px', padding: '0',
                           width: '55%'
                       }}
                >
                    <div className='modal-wrapper'>
                        <div className='modal-close' onClick={this.closeModal}/>
                        <div className="modal-text">
                            <div className="modal__heading">История ставок лота</div>
                            <div className="modal__subheading">{this.props.name}</div>
                        </div>
                        <Scrollbars
                            autoHide={true}
                            hideTracksWhenNotNeeded={true}
                            style={{width: 'auto', height: 300}}
                            className='scrollbar-container_custom'
                            renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                            renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                            renderView={props => <div {...props} className="view"/>}
                        >
                            <div className="modal-content ">
                                {isFetching ? (
                                    <Loader
                                        type='BallTriangle'
                                        color='#00BFFF'
                                        height={300}
                                        width={300}
                                    />
                                ) : (
                                    <div className='modal__item'>
                                        {historyBids.map((item, index) => (
                                            <div key={index} className='selected-lot__last-bid'>
                                                <LastBidTime time={new Date(item.date)}/>
                                                <LastBidItem user={item.User.steam_nick}
                                                             bid={item.bet_size}/>
                                            </div>
                                        ))}
                                    </div>
                                )}
                            </div>
                        </Scrollbars>
                    </div>
                </Popup>
            </React.Fragment>
        )
    }
}

export default PopupHistoryBid;



