import React from "react";
import UserLots from '../components/UserLots';
import Can from '../components/Can';
import PopUp from "./PopUp";
import {UserContext} from "../App";

let socket;

class MainUserLots extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            activeLotId: null,
            isFetchingLots: true,
            isFetchingLot: false,
            isPopupOpen: false,
            lots: [],
            endedLot: null,
            serverTime: new Date()
        };

        socket = props.socket;

        socket.emit('joinUserLots');
        socket.on('userLot', lot => {
            this.setState(state => {
                state.lots.push(lot);
                return state;
            })
        });
    }

    openPopup = () => {
        try{
            const height = document.querySelector('.lots-content-container').scrollHeight;

            this.setState({
                isPopupOpen: true,
                translateTo: height / 2,
                isFetchingLot: true
            });
        }
       catch (e) {
           console.log(e)
       }
    };

    closePopup = (id) => {
        try{
            socket.emit('leaveRoom', id);

            this.setState({
                isPopupOpen: false,
                activeLotId: null
            })
        }
       catch (e) {
        console.log(e)
       }
    };

    makeBid = () => {
        try {
            console.log('Делаем ставочку');
            socket.emit('makeBid', {
                lotId: this.state.selectedActiveLot.id,
                userId: this.props.steamId
            })
        }
        catch (e) {
            console.log(e)
        }
    };

    setActiveSelectedLot = (id) => {
        try {
            if (!this.state.isPopupOpen) this.openPopup();
            if (this.state.activeLotId !== id) {
                if (this.state.activeLotId !== null)
                    socket.emit('leaveRoom', this.state.activeLotId);

                socket.emit('joinRoom', id);

                this.getActiveSelectedLot(id)
                    .then(activeLot => {
                        this.setState({
                            selectedActiveLot: activeLot.item,
                            selectedActiveLotParams: activeLot.params,
                            selectedActiveLotParticipants: activeLot.participants,
                            selectedActiveLotLastBids: activeLot.lastBids ? activeLot.lastBids : 0,
                            similarLots: activeLot.similarLots,
                            isFetchingLot: false
                        });

                    })
                    .catch(error => console.log(`ОШИБКА!!!!!!!!! ${error}`));
            }
            this.setState({activeLotId: id});
        }
        catch (e) {
            console.log(e)
        }
    };

    getActiveSelectedLot = (id) => {
        try{
            return fetch(`/api/lots/active/${id}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(activeLot => activeLot.json())
        }
        catch (e) {
            console.log(e)
        }
    };

    getActiveLots = (page = 0) => {
        try{
            fetch(`/api/lots/active/user/${page}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(data => {
                    console.log(data);
                    this.setState({
                        isFetchingLots: false,
                        lots: data.lots,
                        userPerPage: data.perPage,
                        userTotal: data.total
                    })
                })
        }
        catch (e) {
            console.log(e)
        }
    };

    componentDidMount() {
        this.getActiveLots();

        socket.on('makeBid', lastBids => {
            this.setState({
                selectedActiveLotLastBids: lastBids,
            })
        });

        socket.on('time', serverTime => this.setState({serverTime}));

        socket.on('lotEnded', (lot) => {
            this.setState({endedLot: lot})
        });

        socket.on('closedLot', ({id, status}) => {
            if (this.state.selectedActiveLot.id === id) {
                this.setState({time: `Статус: ${status}`, timer: 'lot'});
            }

            this.setState(state => {
                const index = state.lots.findIndex(lot => lot.id === id);

                if (index >= 0) {
                    state.lots[index].status_id = status;
                }

                return state
            })
        });
    }

    componentWillUnmount() {
        socket.off('userLot');
        socket.off('makeBid');
        socket.off('time');
        socket.off('lotEnded');
        socket.off('closedLot');
    }

    render() {
        const {
            translateTo, isFetchingLots, isFetchingLot, lots,
            userTotal, isPopupOpen, selectedActiveLot, similarLots,
            selectedActiveLotParams, selectedActiveLotParticipants,
            selectedActiveLotLastBids, endedLot,
            serverTime
        } = this.state;

        let timer = 'lot';
        let time = selectedActiveLot ? new Date(selectedActiveLot.start_date) - new Date(serverTime) : 0;
        if (time < 0) {
            timer = 'bid';

            if (selectedActiveLotLastBids.length)
                time = new Date(selectedActiveLotLastBids[0].date) - new Date(serverTime) + 10000;
            else
                time += 10000;

            if (time < 0)
                time = 0
        }

        return (
            <div className={"lots-content-container"}>
                <UserContext.Consumer>
                    {({role}) => (
                        <React.Fragment>
                            <Can
                                role={role}
                                resource='lots'
                                action='read:own'
                                yes={() => (
                                    lots.length !== 0 &&
                                    <UserLots
                                        isFetching={isFetchingLots}
                                        lots={lots}
                                        total={userTotal}
                                        setSelectedLot={this.setActiveSelectedLot}
                                    />
                                )}
                                no={() => null}
                            >
                            </Can>
                            <PopUp
                                isOpen={isPopupOpen} lot={selectedActiveLot} params={selectedActiveLotParams}
                                participants={selectedActiveLotParticipants} translateTo={translateTo}
                                isFetching={isFetchingLot} similarLots={similarLots} endedLot={endedLot}
                                lastBids={selectedActiveLotLastBids} time={time} timer={timer} makeBid={this.makeBid}
                                closePopup={this.closePopup}
                            />
                        </React.Fragment>
                    )}
                </UserContext.Consumer>
            </div>
        )
    }
}

export default MainUserLots;
