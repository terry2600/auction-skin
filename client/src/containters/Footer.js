import React from 'react';
import StatisticsLogo from '../components/StatisticsLogo';
import StatisticsValue from '../components/StatisticsValue';
import {Link} from 'react-router-dom';
import '../App.css';

export default ({stats}) => (
    <footer className='footer'>
        <div className="footer-wrapper">
            <div className='footer__heading'>Статистика сервиса</div>
            <div className="footer-statistics-list statistics-list">
                <div className="statistics-list__item statistics-item">
                    <StatisticsLogo
                        img={'../img/statistics-total.png'}
                        color='violet'/>
                    <StatisticsValue
                        text='Всего аукционов'
                        value={stats.total}/>
                </div>
                <div className="statistics-list__item statistics-dash"/>
                <div className="statistics-list__item statistics-item">
                    <StatisticsLogo
                        img={'../img/statistics-cs.png'}
                        color='orange'/>
                    <StatisticsValue
                        text='Counter-Strike: GO'
                        value={stats.cs}/>
                </div>
                <div className="statistics-list__item statistics-dash"/>
                <div className="statistics-list__item statistics-item">
                    <StatisticsLogo
                        img={'../img/statistics-dota.png'}
                        color='red'/>
                    <StatisticsValue
                        text='Dota 2'
                        value={stats.dota}/>
                </div>
            </div>
            <div className='footer-rights'>
                <div className='footer__notification'>
                    <Link to='/static/contacts' className='footer__link'>Контакты</Link>
                </div>
                <div className='footer__notification'>
                    <Link to='/static/tos' className='footer__link'>Пользовательскогое соглашение</Link>
                </div>
                <div className='footer__notification'>
                    <Link to='/static/privacy' className='footer__link'>Политика конфиденциальности</Link>
                </div>
                <div className='footer__notification'>
                    <Link to='/faq' className='footer__link'>Помощь</Link>
                </div>
                <div className='footer-rights__reserved'>&#9400; Auction. 2019</div>
                <a href="//showstreams.tv/"><img src="//www.free-kassa.ru/img/fk_btn/16.png" title="Бесплатный видеохостинг"/></a>
                <a href="https://www.fkwallet.ru"><img src="https://www.fkwallet.ru/assets/2017/images/btns/iconsmall_wallet8.png" title="Криптовалютный кошелек" /></a>
            </div>
        </div>
    </footer>
)
