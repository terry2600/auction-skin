import React from "react";
import Popup from "reactjs-popup";
import LastBidDate from "../components/LastBidDate";
import LastBidItem from '../components/LastBidItem';
import ButtonMore from "../components/ButtonMore";
import {Scrollbars} from "react-custom-scrollbars";
import Loader from 'react-loader-spinner';
import 'react-perfect-scrollbar/dist/css/styles.css';
import '../App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

const initialState = {
    similarLots: [],
    isOpen: false,
    isFetching: true
};

class PopupSimilarLot extends React.Component {
    constructor(props) {
        super(props);

        this.state = initialState;
    }

    openModal=()=> {
        this.setState({isOpen: true}, () => document.body.classList.add('modal-open'));
        this.getSimilarLots(this.props.lotId);
    };

    closeModal=()=> {
        this.setState(initialState, () => document.body.classList.remove('modal-open'));
    };

    getSimilarLots=(id) =>{
        try{
            if (this.state.isOpen) {
                this.setState({
                    isFetching: true
                })
            }
            fetch(`/api/lots/${id}/similar`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(similarLots => {
                    this.setState({
                        similarLots: similarLots,
                        isFetching: false
                    })
                });
        }
        catch (e) {
            console.log(e)
        }
    };

    render() {
        const {isOpen, similarLots, isFetching} = this.state;
        return (
            <React.Fragment>
                <div className='popup__more'>
                    <ButtonMore text='Показать все' openModal={this.openModal}/>
                </div>
                <Popup open={isOpen}
                       onClose={this.closeModal}
                       overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                       contentStyle={{
                           borderRadius: '15px', overflow: 'hidden',
                           background: 'rgb(16, 26, 43)', border: 'none',
                           color: '#3c4b63', fontSize: '12px', padding: '0',
                           width: '55%'
                       }}
                >
                    <div className='modal-wrapper'>
                        <div className='modal-close' onClick={this.closeModal}/>
                        <div className="modal-text">
                            <div className="modal__heading">История похожих лотов</div>
                        </div>
                        <Scrollbars
                            autoHide={true}
                            hideTracksWhenNotNeeded={true}
                            style={{width: 'auto', height: 300}}
                            className='scrollbar-container_custom'
                            renderTrackVertical={props => <div {...props} className="track-vertical"/>}
                            renderThumbVertical={props => <div {...props} className="thumb-vertical"/>}
                            renderView={props => <div {...props} className="view"/>}
                        >
                            <div className="modal-content">
                                {isFetching ? (
                                    <Loader
                                        type='BallTriangle'
                                        color='#00BFFF'
                                        height={300}
                                        width={300}
                                    />
                                ) : (
                                    similarLots.map((lot, index) => (
                                        <div key={index} className='selected-lot__last-bid'>
                                            <LastBidDate time={new Date(lot.end_date)}/>
                                            <LastBidItem
                                                user={lot.LotResult.User.steam_nick}
                                                bid={lot.LotResult.purchase_price}/>
                                        </div>
                                    ))
                                )}
                            </div>
                        </Scrollbars>
                    </div>
                </Popup>
            </React.Fragment>
        )
    }
}

export default PopupSimilarLot;
