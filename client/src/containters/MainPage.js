import React from 'react';
import LotSlider from '../components/LotSlider';
import Lots from '../components/Lots';
import Filter from '../components/Filter';
import SelectedLot from '../components/SelectedLot';
import TradeUrlInput from '../components/TradeUrlInput';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import {UserContext} from "../App";
import validateTradeUrl from "../functions/validateTradeUrl";
import setTradeUrl from "../functions/setTradeUrl";
import {notification } from 'antd'

const openNotificationWithIcon = (type,title,text) => {
    notification[type]({
        message: title,
        description: text,
    });
};

let socket;
const defaultApplyNotification = {
    active: false,
    type: null,
    title: null,
    text: null
};

const lotFilters = [
    {sortType: 'create_date', sortValue: 'desc', label: 'По дате'},
    {sortType: 'create_date', sortValue: 'asc', label: 'По дате'},
    {sortType: 'participants', sortValue: 'desc', label: 'По количеству участников'},
    {sortType: 'participants', sortValue: 'asc', label: 'По количеству участников'},
    {sortType: 'price', sortValue: 'desc', label: 'По стоимости предмета'},
    {sortType: 'price', sortValue: 'asc', label: 'По стоимости предмета'},
    {sortType: 'cost_of_participation', sortValue: 'desc', label: 'По стоимости участия'},
    {sortType: 'cost_of_participation', sortValue: 'asc', label: 'По стоимости участия'}
];

const perPages = [
    {value: '8', label: '8'},
    {value: '25', label: '25'},
    {value: '50', label: '50'},
    {value: '100', label: '100'},
];

const tabs = [
    {value: 'open', label: 'Открытые'},
    {value: 'engage', label: 'Участвую'}
];

class MainPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isFetchingLots: true,
            isFetchingFilters: true,
            isNewLotsAvailable: true,
            isScrollable: true,
            isSelected: false,
            activeSortFilter: lotFilters[0],
            lotFiltersIndex: 0,
            filtersOpened: {
                isSortFilterOpened: false,
                isNumberFilterOpened: false,
                isTypeFilterOpened: false
            },
            filters: [],
            paramsToSend: [],
            tab: tabs[0],
            tabIndex: 0,
            page: 0,
            perPage: perPages[0],
            perPageIndex: 0,
            hotlots: [],
            heroes: [],
            selectedLot: {},
            isExtensionExist: false,
            applyNotification: defaultApplyNotification,
            newTradeUrl: null,
            tradeUrlError: null
        };

        this.validateTradeUrl = validateTradeUrl.bind(this);
        this.setTradeUrl = setTradeUrl.bind(this);

        socket = props.socket;
        this.controller = new AbortController();

        socket.on('updateParticipants', ({id, current_participants}) => {
            let lotIndex = this.state.lots.findIndex(lot => lot.id === id);
            if (lotIndex > -1)
                this.setState(prevState => {
                    let selectedLot = Object.assign({}, prevState.selectedLot);

                    let lots = prevState.lots.slice(0);
                    lots[lotIndex].current_participants = current_participants;

                    if (selectedLot && selectedLot.id === lots[lotIndex].id) {
                        selectedLot.current_participants = current_participants
                    }

                    return {
                        lots,
                        selectedLot
                    }
                })
        });

        socket.on('closedLot', ({id, status}) => {
            if (!this.state.lots)
                return false;

            let lotIndex = this.state.lots.findIndex(lot => lot.id === id);
            if (lotIndex > -1)
                this.setState(state => {
                    state.lots[lotIndex].status_id = status;

                    return state
                })
        });

        socket.on('createdHotLot', lot => {
            const lots = this.state.hotlots;
            lots.push(lot);

            this.setState({
                hotlots: lots
            })
        });
    }

    componentDidMount() {
        window.addEventListener('resize', this.resizing);
        this.getFilters(this.props.selectedGame.value);
        this.getHotLots();
        socket.emit('joinLots', this.props.selectedGame.value);
        socket.on('createdLot', () => {
            console.log('Создан проект');
            this.setState({isNewLotsAvailable: true})
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevState.page !== this.state.page ||
            prevState.tab.value !== this.state.tab.value ||
            prevState.perPage.value !== this.state.perPage.value ||
            prevState.activeSortFilter !== this.state.activeSortFilter
        ) {
            this.sendFilters();
        } else if (this.props.selectedGame.value !== prevProps.selectedGame.value) {
            socket.emit('leaveLots', prevProps.selectedGame.value);
            this.setState({isFetchingLots: true, isFetchingFilters: true}, () => {
                this.getFilters(this.props.selectedGame.value);
                this.getHotLots();
                socket.emit('joinLots', this.props.selectedGame.value);
            })
        }
    }

    componentWillUnmount() {
        this.controller.abort();
        if (this.state.selectedLot)
            socket.emit('leaveRoom', this.state.selectedLot.id);

        window.removeEventListener('resize', this.resizing);
        socket.emit('leaveAllLots');
        socket.off('closedLot');
        socket.off('createdHotLot');
        socket.off('createdLot');
        socket.off('updateParticipants')
    }

    getLots = (tab, page = 0) => {
        this.setState({
            tab,
            page
        })
    };

    setLots = (data) => {
        this.setState({
            isFetchingLots: false,
            isNewLotsAvailable: false,
            lots: data.lots,
            viewed: data.viewed,
            total: data.total,
            pages: data.page
        })
    };

    getHotLots() {
        try{
            fetch(`/api/lots/hot?appId=${this.props.selectedGame.value}`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        hotlots: data.lots
                    }, () => {
                        if (window.innerWidth * 2 >= this.state.hotlots.length * 2 * 175) {
                            this.setState({isScrollable: false})
                        }
                    });

                })
                .catch(error => console.log(`ОШИБКА!!!!!!!!!`, error));
        }
        catch (e) {
            console.log(e)
        }
    };

    getFilters = (selectedGameId) => {
            fetch(`/api/lots/filters?app_id=${selectedGameId}`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(data => {
                    if(!data.ok) throw new Error('err');

                   return  data.json()})
                .then(filters => {
                    let {filterList} = filters;
                    let options = [];
                    let params = {};
                    let strength = [];
                    let agility = [];
                    let intelligence = [];

                    if (filterList['Hero']) {
                        filterList.Hero.values.map(item => {
                            if (item.type === "strength") {
                                strength.push({...item, active: false});
                            }
                            if (item.type === "agility") {
                                agility.push({...item, active: false});
                            }
                            if (item.type === "intelligence") {
                                intelligence.push({...item, active: false});
                            }
                        });
                    }

                    for (let filter in filterList) {
                        filterList[filter].values = filterList[filter].values.reduce((prevOptions, option) => {
                            return [...prevOptions, {
                                value: [filter.replace(/\w(?=[A-Z])/g, '$&_').toLowerCase() + '_id', option.id],
                                label: option.name_ru
                            }]
                        }, []);
                        options = [...options, filterList[filter]];

                        const type = filterList[filter].values[0].value[0];
                        filterList[filter].type = type;
                        const id = filterList[filter].values[0].value[1];
                        const label = filterList[filter].values[0].label;

                        params[type] = {
                            id: id,
                            label: label
                        };
                    }

                    this.setState({
                        filters: options,
                        paramsToSend: params,
                        heroes: {
                            strength,
                            agility,
                            intelligence
                        },
                        rangePrice: [filters.minPrice, filters.maxPrice],
                        stepPrice: filters.step,
                        minPrice: filters.minPrice,
                        maxPrice: filters.maxPrice,
                        isFetchingFilters: false,
                    }, () => this.sendFilters());
                })
                .catch(err =>{
                    openNotificationWithIcon('error','Не удалось получить фильтры','К сожалению, в данный момент невозможно получить фильтры, повторите запрос позже!')

                })


    };
    sendFilters = () => {

            const params = {};
            for (let key in this.state.paramsToSend) {
                if (this.state.paramsToSend[key].id !== 0) {
                    params[key] = this.state.paramsToSend[key].id
                }
            }

            const filter = this.state.activeSortFilter;

            fetch(`/api/lots/${this.state.tab.value}/${this.state.page}/${this.state.perPage.value}`, {
                signal: this.controller.signal,
                method: "POST",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                },
                body: JSON.stringify({
                    filters: {
                        app_id: this.props.selectedGame.value,
                        price: {
                            min: this.state.rangePrice[0],
                            max: this.state.rangePrice[1]
                        },
                        params: params,
                        search: this.state.search,
                        sort: filter
                    }
                })
            })
                .then(res => {
                    if(!res.ok){
                        throw new Error('err');
                    }
                    return res.json()
                })
                .then(this.setLots)
                .catch((res)=>{
                        openNotificationWithIcon('error','Не удалось получить открытые лоты','К сожалению, в данный момент невозможно получить открытые лоты, повторите запрос позже!')

                })
    };
    changeRangePrice = (rangePrice) => {
        this.setState({rangePrice})
    };
    setActiveHeroes = (index, type) => {
        try {
            let buffer = this.state.heroes;
            switch (type) {
                case 'strength':
                    buffer.strength.map((item) => {
                        if (item.id === index) {
                            item.active = !item.active;
                        }
                    });
                    return this.setState({heroes: buffer});
                case 'agility':
                    buffer.agility.map((item) => {
                        if (item.id === index) {
                            item.active = !item.active;
                        }
                    });
                    return this.setState({heroes: buffer});
                case 'intelligence':
                    buffer.intelligence.map((item) => {
                        if (item.id === index) {
                            item.active = !item.active;
                        }
                    });
                    return this.setState({heroes: buffer});
                default:
                    return;
            }
        }
        catch (e) {
            console.log(e)
        }
    };

    resetHeroes = () => {
        try{
            let buffer = this.state.heroes;
            for (let property in buffer) {
                buffer[property].forEach(item => item.active = false)
            }
            this.setState({heroes: buffer})
        }
        catch (e) {
            console.log(e)
        }
    };

    submitHeroes = (closeHeroes) => {
        try{
            let hero_id = [];
            if (this.state.heroes !== undefined) {
                if (this.state.heroes.strength) {
                    this.state.heroes.strength.map((item) => {
                        if (item.active === true) hero_id.push(item.id);
                    });
                }
                if (this.state.heroes.agility) {
                    this.state.heroes.agility.map((item) => {
                        if (item.active === true) hero_id.push(item.id);
                    });
                }
                if (this.state.heroes.intelligence) {
                    this.state.heroes.intelligence.map((item) => {
                        if (item.active === true) hero_id.push(item.id);
                    });
                }

                this.setState(state => {
                    let params = Object.assign({}, state.paramsToSend);
                    params['hero_id'].id = hero_id;
                    if (params['hero_id'].id.length === 0) {
                        params['hero_id'].id = 0
                    }
                    return {paramsToSend: params}
                }, () =>  closeHeroes())
            }
        }
       catch (e) {
        console.log(e)
       }
    };

    onFilterSelect = (option) => {
        this.setState((state => {
            let params = Object.assign({}, state.paramsToSend);
            params[option.value[0]].id = option.value[1];
            params[option.value[0]].label = option.label;
            return {paramsToSend: params}
        }))
    };

    changeLotsPage = (page) => {
        this.setState({page: page.selected})
    };

    changeLotsTab = (tab) => {
        try {
            this.setState(prevState => {
                const filtersOpened = Object.fromEntries(
                    Object.entries(prevState.filtersOpened).map(([key, value]) => {
                        return [key, false]
                    })
                );
                let tabIndex = tabs.findIndex( tabItem => tab === tabItem);

                return {tab, tabIndex, filtersOpened, page: 0 }
            })
        }
      catch (e) {
          console.log(e)
      }
    };

    changePerPage = (perPage) => {
        try{
            this.setState(prevState => {
                const filtersOpened = Object.fromEntries(
                    Object.entries(prevState.filtersOpened).map(([key, value]) => {
                        return [key, false]
                    })
                );
                let perPageIndex = perPages.findIndex( perPageItem => perPage === perPageItem);

                return {perPage, perPageIndex, filtersOpened}
            })
        }
       catch (e) {
           console.log(e)
       }
    };

    toggleLotFilter = (filterToToggle) => {
        try{
            this.setState(prevState => {
                const filtersOpened = Object.fromEntries(
                    Object.entries(prevState.filtersOpened).map(([key, value]) => {
                        if (key === filterToToggle) {
                            return [key, !value]
                        } else {
                            return [key, false]
                        }
                    })
                );

                return {filtersOpened: filtersOpened}
            })
        }
        catch (e) {
            console.log(e)
        }


    };

    changeLotFilter = (activeSortFilter) => {
        try {
            this.setState(prevState => {
                const filtersOpened = Object.fromEntries(
                    Object.entries(prevState.filtersOpened).map(([key, value]) => {
                        return [key, false]
                    })
                );
                let lotFiltersIndex = lotFilters.findIndex( sortFilterItem => activeSortFilter === sortFilterItem);

                return {activeSortFilter, lotFiltersIndex, filtersOpened}
            })
        }
        catch (e) {
            console.log(e);
        }
    };

    changeFilterSearch = (e) => this.setState({search: e.target.value});

    refreshLots = () => {
        this.setState({isFetchingLots: true}, () => this.sendFilters())
    };

    getOpenSelectedLot = (id, current_participants) => {
            if (id === this.state.selectedLot.id) return;

            fetch(`/api/lots/open/${id}`, {
                signal: this.controller.signal,
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => {
                    if(!response.ok){
                        console.log(response.status)
                        throw new Error('oops');
                    }
                    response.json()})
                .then(data => {
                    if (this.state.selectedLot) socket.emit('leaveRoom', this.state.selectedLot.id);

                    const millisec = Date.parse(data.item.expired_date);

                    data.item.expired_date = new Date(millisec);

                    const startDate = new Date(millisec);
                    startDate.setDate(startDate.getDate() + 1);
                    data.item.start_date = startDate;

                    data.item.current_participants = current_participants;

                    socket.emit('joinRoom', id);
                    this.props.checkExtension()
                        .then(res => {
                            if (res === 'pong') {
                                this.setState({isExtensionExist: true});

                            }
                            if (res === undefined) {
                                this.setState({isExtensionExist: false});
                                this.props.addWarning('Не установлено расширение',
                                    'Пожалуйста установите расширение и не ебите нам голову,Пожалуйста установите расширение и не ебите нам голову,Пожалуйста установите расширение и не ебите нам голову',
                                    '#ff0059',
                                    require('../assets/img/red-warn.png')
                                );

                            }
                        });
                    this.props.checkTradeBan(true);

                    this.setState({
                        selectedLot: data.item,
                        steamId: data.steamid64,
                        selectedLotParams: data.itemParams,
                        similarLots: data.similarLots,
                        isParticipated: data.item.isParticipated,
                        isSelected: true,
                    });

                })
                .catch(error => openNotificationWithIcon('error','Не удалось получить открытые лоты','К сожалению, в данный момент невозможно получить открытые лоты, повторите запрос позже!')
                );
    };

    participate = (lotId, cost, balance,) => {
            if (balance >= cost) {
                fetch(`/api/lots/participate`, {
                    signal: this.controller.signal,
                    method: "POST",
                    headers: {
                        Accept: "application/json",
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*"
                    },
                    body: JSON.stringify({id: lotId})
                })
                    .then(response => {
                        if(!response.ok) throw new Error('err');
                        return response.json()})
                    .then(isApplySuccessful => {
                        if (isApplySuccessful) {
                            this.setState({
                                applyNotification: {
                                    type: 'success',
                                    title: 'Успешно',
                                    text: 'Вы успешно приняли участие в лоте'
                                },
                                isParticipated: true,
                            })
                        } else {
                            this.setState({
                                applyNotification: {
                                    type: 'error',
                                    title: 'Произошла ошибка',
                                    text: 'К сожалению произошла непредвиденная ошибка'
                                },
                                isParticipated: false,
                            })
                        }
                    })
                    .catch(error => openNotificationWithIcon('error','Не удалось принять участие в лоте','К сожалению, в данный момент не удалось принять участие в лоте повторите запрос позже!'))
            }

    };

    resizing = () => {
        if (window.innerWidth * 2 >= this.state.hotlots.length * 2 * 175) {
            this.setState({isScrollable: false})
        } else {
            this.setState({isScrollable: true})
        }
    };

    render() {
        const {
            isScrollable, selectedLot, isNewLotsAvailable, selectedLotParams, similarLots, isFetchingLots,
            isSelected, isParticipated, applyNotification,
            lots, viewed, total, page, isExtensionExist, filtersOpened, tradeUrlError
        } = this.state;
        const {warnings, tradeUrl, steamId} = this.props
        return (
            <UserContext.Consumer>
                {({role}) => (
                    <div className='main-page'>
                        <style>{isScrollable ? `
                        @keyframes scroll {
                             0% { transform: translateX(0); }
                             100% { transform: translateX(calc(-175px * ${this.state.hotlots.length})); }
                        }
                        .slide-track {
                            width: calc(175px * ${this.state.hotlots.length * 2})
                        }` :
                            `.slide-track {
                            width: calc(175px * ${this.state.hotlots.length})
                        }`
                        }
                        </style>
                        <div className="page-content-wrapper">
                            <div className='content content-main'>
                                <LotSlider
                                    slides={this.state.hotlots}
                                    isScrollable={isScrollable}
                                    getSelectedLot={this.getOpenSelectedLot}
                                    selectedGame={this.props.selectedGame.label.toLowerCase().replace(':', '-')}
                                />
                                {(!tradeUrl && steamId) && (
                                    <TradeUrlInput
                                        error={tradeUrlError}
                                        change={(value) => this.setState({newTradeUrl: value})}
                                        setTradeUrl={this.setTradeUrl}
                                    />
                                )}
                                <div className='main-content-container'>
                                    <Lots
                                        isFetching={isFetchingLots}
                                        isNewLotsAvailable={isNewLotsAvailable}
                                        filtersOpened={filtersOpened}
                                        lots={lots}
                                        viewed={viewed}
                                        total={total}
                                        role={role}
                                        lotFilters={lotFilters}
                                        lotFiltersIndex={this.state.lotFiltersIndex}
                                        tabs={tabs}
                                        perPages={perPages}
                                        currentTab={this.state.tab}
                                        tabIndex={this.state.tabIndex}
                                        page={page}
                                        currentPerPage={this.state.perPage}
                                        perPageIndex={this.state.perPageIndex}
                                        selectedGame={this.props.selectedGame.label.toLowerCase().replace(':', '-')}
                                        selectedLotId={selectedLot.id}
                                        toggleLotFilter={this.toggleLotFilter}
                                        changeLotFilter={this.changeLotFilter}
                                        refreshLots={this.refreshLots}
                                        changePage={this.changeLotsPage}
                                        getSelectedLot={this.getOpenSelectedLot}
                                        sendFilters={this.sendFilters}
                                        changeTab={this.changeLotsTab}
                                        changePerPage={this.changePerPage}
                                    />
                                    <Filter
                                        paramsToSend={this.state.paramsToSend}
                                        filters={this.state.filters}
                                        games={this.props.games}
                                        selectedGame={this.props.selectedGame.label.toLowerCase().replace(':', '-')}
                                        toggleGame={this.props.toggleGame}
                                        search={this.search}
                                        rangePrice={this.state.rangePrice}
                                        stepPrice={this.state.stepPrice}
                                        minPrice={this.state.minPrice}
                                        maxPrice={this.state.maxPrice}
                                        heroes={this.state.heroes}
                                        isFetching={this.state.isFetchingFilters}
                                        changeRangePrice={this.changeRangePrice}
                                        setActiveHeroes={this.setActiveHeroes}
                                        resetHeroes={this.resetHeroes}
                                        submitHeroes={this.submitHeroes}
                                        onFilterSelect={this.onFilterSelect}
                                        sendFilters={this.sendFilters}
                                        changeFilterSearch={this.changeFilterSearch}/>
                                    <SelectedLot
                                        lot={selectedLot}
                                        steamId={steamId}
                                        itemParams={selectedLotParams}
                                        similarLots={similarLots}
                                        isSelected={isSelected}
                                        isParticipated={isParticipated}
                                        applyNotification={applyNotification}
                                        participate={this.participate}
                                        isExtensionExist={isExtensionExist}
                                        warnings={warnings}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                )}
            </UserContext.Consumer>
        )
    };
}

export default MainPage;
