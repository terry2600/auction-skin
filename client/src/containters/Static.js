import React from 'react';
import '../App.css';

class Static extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            html: null,
            title: 'Static page'
        };
    }

    componentDidMount() {
        this.getHtml(this.props.match.params.page)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
       if (this.props.match.params.page !== prevProps.match.params.page) {
           this.getHtml(this.props.match.params.page)
       }
    }

    getHtml = (page, language = 'ru') => {
        try{
            fetch(`/api/static/${page}?language=${language}`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(data => {
                    document.title = data.title;
                    this.setState({
                        html: data.html,
                        title: data.title
                    })
                })
                .catch(error => console.log(`ОШИБКА!!!!!!!!! ${error}`));
        }
        catch (e) {
            console.log(e)
        }
    };

    render() {
        const {html, title} = this.state;
        return (
            <div className='sample-page'>
                <div className="page-content-wrapper">
                    <div className='content'>
                        <div className="content-container static">
                            <article>
                                <header><h1>{title}</h1></header>
                                <section dangerouslySetInnerHTML={{__html: html}} />
                            </article>
                        </div>
                    </div>
                </div>
            </div>
        )
    };
}

export default Static;
