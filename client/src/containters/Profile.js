import React from 'react';
import {Redirect} from 'react-router-dom';
import Popup from "reactjs-popup";
import ProfileInfo from '../components/ProfileInfo';
import CircleImg from '../components/CircleImg';
import Button from '../components/Button';
import Label from '../components/Label';
import ProfileTabs from '../components/ProfileTabs';
import Date from '../components/Date/Date'
import WithdrawPopup from "./withdraw/WithdrawPopup";
import ProfileInfoTradeUrl from '../components/ProfileInfoTradeUrl';
import Dropdown from "react-dropdown";
import {
    getAffiliate,
    onFormSubmit,
    sendDeposit,
    getDepositMethods,
    getWonLots,
    getParticipatedLots,
    loadInventory,
    getOwnLots,
    getLots,
    getTransactions,
    fetchTradeBanProfile,
    getDeals,
    getNotifications
} from '../functions/api/Profile/api'
import {payTest} from "../functions/api/Profile/constants";
import {
    setCurrency,
    handleInputChange,
    setPaymentActive,
    selectItem,
    setActiveNotification,
    onChangeInput,
    setDeposit,
    getPayMethods,
    onChangeInputAmount
} from "../functions/api/Profile/functions";
import validateTradeUrl from "../functions/validateTradeUrl";
import setTradeUrl from "../functions/setTradeUrl";

const CopyContext = React.createContext({});

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
            isWithdrawOpen: false,
            activeIndex: null,
            moneySum: '',
            inventory: [],
            steamId: this.props.steamId,
            lots: [],
            ownLots: [],
            newTradeUrl: null,
            inChangeMode: false,
            transactions: [],
            lotsWon: 0,
            lotsParticipated: 0,
            payMethods: [],
            payTest: payTest,
            notifications: [
                {
                    id: 222,
                    message: 'aaaasssssssssss',
                    date: '12:08,11.11.1999',
                    active: false
                },
                {
                    id: 223,
                    message: 'Вы успешно приняли участие. Вы успешно приняли участие. Вы успешно приняли участие. Вы успешно приняли участие. Вы успешно приняли участие.  И теперь. Вы успешно приняли участие.  Вы успешно приняли участие.',
                    date: '12:08,11.11.1999',
                    active: false
                }
            ],
            currentPayment: [],
            paymentFields: [],
            currencies: [],
            sumMin: 0,
            sumMax: 0,
            options: [],
            amount: '',
            id: 0,
            escrow: false,
            currentDeposit: '',
            deals: [],
            affiliateProgram: [],
            tradeUrlError: null
        };
        this.getAffiliate = getAffiliate.bind(this);
        this.getWonLots = getWonLots.bind(this);
        this.getParticipatedLots = getParticipatedLots.bind(this);
        this.loadInventory = loadInventory.bind(this);
        this.getOwnLots = getOwnLots.bind(this);
        this.getLots = getLots.bind(this);
        this.getTransactions = getTransactions.bind(this);
        this.fetchTradeBanProfile = fetchTradeBanProfile.bind(this);
        this.getDeals = getDeals.bind(this);
        this.getNotifications = getNotifications.bind(this);
        this.setPaymentActive = setPaymentActive.bind(this);
        this.getDepositMethods = getDepositMethods.bind(this);
        this.selectItem = selectItem.bind(this);
        this.sendDeposit = sendDeposit.bind(this);
        this.setActiveNotification = setActiveNotification.bind(this);
        this.onChangeInput = onChangeInput.bind(this);
        this.setDeposit = setDeposit.bind(this);
        this.getPayMethods = getPayMethods.bind(this);
        this.onFormSubmit = onFormSubmit.bind(this);
        this.onChangeInputAmount = onChangeInputAmount.bind(this);
        this.handleInputChange = handleInputChange.bind(this);
        this.setCurrency = setCurrency.bind(this);
        this.validateTradeUrl = validateTradeUrl.bind(this);
        this.setTradeUrl = setTradeUrl.bind(this);
    }

    openModal = () => {
        this.setState({open: true}, () => document.body.classList.add('modal-open'))
    };
    closeModal = () => {
        this.setState({open: false}, () => document.body.classList.remove('modal-open'))
    };
    openModalWithdraw = () => {
        this.setState({isWithdrawOpen: true}, () => document.body.classList.add('modal-open'));
    };
    closeModalWithdraw = () => {
        this.setState({isWithdrawOpen: false}, () => document.body.classList.remove('modal-open'));
    };

    componentDidMount() {
        this.loadInventory(730);
        this.getLots();
        this.getOwnLots();
        this.getTransactions();
        this.getWonLots();
        this.getParticipatedLots();
        this.getPayMethods();
        this.getNotifications();
        this.getDeals();
        this.getAffiliate();
        this.fetchTradeBanProfile()
            .then(res => {
                if (res.escrow !== 0) {
                    this.setState({escrow: false})
                } else this.setState({escrow: true})
            })
    }

    validateFields = (value, pattern) => {
        // if(!pattern.validate){
        //     console.log('Неверно');
        // }
    };

    copyToClipboard = () => {
        if (navigator.clipboard) {
            navigator.clipboard.writeText(document.getElementById('referal-link').textContent)
                .then(success => console.log(success))
        } else {
            let range = new Range();
            range.selectNodeContents(document.getElementById('referal-link'));

            document.getSelection().removeAllRanges();
            document.getSelection().addRange(range);
            document.execCommand('copy');
        }
    };

    render() {
        const {avatar, userName, steamId, balance, regDate, tradeUrl} = this.props;
        const {
            deals,
            currentDeposit,
            deposits,
            escrow,
            notifications,
            open,
            moneySum,
            lots,
            ownLots,
            transactions,
            lotsParticipated,
            lotsWon,
            isWithdrawOpen,
            payMethods,
            currentPayment,
            paymentFields,
            currencies,
            sumMin,
            sumMax,
            currency,
            affiliateProgram,
            newTradeUrl,
            inChangeMode,
            tradeUrlError
        } = this.state;
        return (
            userName ? (
                <div className="page-content-wrapper">
                    <div className='content content-profile'>
                        <div className="content-container content-container-profile">
                            <div className="content-container-header">
                                <div className='heading'>
                                    <div className='heading__profile'>Профиль пользователя</div>
                                </div>
                                <div className='container-profile__name'>{userName}</div>
                            </div>
                            <div className="container-body container-profile-body">
                                <div className="profile-row profile-row_top">
                                    <ProfileInfoTradeUrl
                                        heading='Trade URL'
                                        tradeUrl={tradeUrl}
                                        newTradeUrl={newTradeUrl}
                                        inChangeMode={inChangeMode}
                                        error={tradeUrlError}
                                        setTradeUrl={this.setTradeUrl}
                                        changeTradeUrl={() => this.setState({
                                            newTradeUrl: tradeUrl,
                                            inChangeMode: true
                                        })}
                                        change={(value) => this.setState({newTradeUrl: value})}
                                    />
                                    <ProfileInfo heading='Steam ID' value={steamId}/>
                                </div>
                                <div className="profile-row">
                                    <div className="profile-info">
                                        <div className="profile-info-user">
                                            <CircleImg height='88px' width='88px' avatar={avatar}/>
                                            <div className='user-text__name'>{userName}</div>
                                        </div>
                                        <div className="profile-info-money info-money">
                                            <div className='info-money__value'>{balance} Р</div>
                                            <Button
                                                text='Пополнить'
                                                type='button_small button_gradient'
                                                onClick={() => {
                                                    this.getDepositMethods('cypix');
                                                    this.openModal()
                                                }}
                                            />
                                            <Popup
                                                open={open}
                                                onClose={this.closeModalWithdraw}
                                                className='popup-replenish'
                                                overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                                                contentStyle={{
                                                    borderRadius: '15px', overflow: 'hidden',
                                                    background: 'rgb(16, 26, 43)', border: 'none',
                                                    color: '#3c4b63', fontSize: '12px', padding: '0',
                                                    width: '580px'
                                                }}
                                            >
                                                <div className="replenish-wrapper">
                                                    <div className='modal-close' onClick={this.closeModal}/>
                                                    <div className="replenish-content">
                                                        <div style={{display: 'flex', margin: 15}}>
                                                            <input placeholder='Введите сумму'
                                                                   type='text'
                                                                   className='insert-sum'
                                                                   value={moneySum}
                                                                   onChange={this.handleInputChange}
                                                                   style={{marginTop: '6px'}}
                                                            />
                                                            <Dropdown
                                                                options={deposits ? deposits : null}
                                                                value={currentDeposit ? currentDeposit : {
                                                                    label: "Выберите систему...",
                                                                    value: 'null'
                                                                }}
                                                                onChange={this.setDeposit}
                                                                controlClassName='filter__dropdown deposit dropdown'
                                                                menuClassName='dropdown_menu'
                                                                arrowClassName='dropdown__arrow'
                                                            />
                                                        </div>
                                                        <button
                                                            className='button button_big button_lightblue button_transparent'
                                                            onClick={() => this.sendDeposit('cypix')}>Пополнить
                                                        </button>
                                                        <div className='selected-lot__description'>Деньги приходят
                                                            мгновенно, но могут быть задержки до 5-10 минут
                                                        </div>
                                                    </div>
                                                </div>
                                            </Popup>
                                            <Button text='Вывести'
                                                    type='button_small button_transparent'
                                                    style={{marginLeft: '8px'}}
                                                    onClick={this.openModalWithdraw}
                                            />
                                            <Popup
                                                open={isWithdrawOpen}
                                                onClose={this.closeModalWithdraw}
                                                className='popup-replenish'
                                                overlayStyle={{background: 'rgba(0, 0, 0, .7)'}}
                                                contentStyle={{
                                                    borderRadius: '15px', overflow: 'hidden',
                                                    background: 'rgb(16, 26, 43)', border: 'none',
                                                    color: '#3c4b63', fontSize: '12px', padding: '0',
                                                    width: '700px'
                                                }}
                                            >
                                                <WithdrawPopup onChangeInputAmount={this.onChangeInputAmount}
                                                               onChangeInput={this.onChangeInput}
                                                               onFormSubmit={this.onFormSubmit} currency={currency}
                                                               setCurrency={this.setCurrency} payMethods={payMethods}
                                                               sums={{sumMin, sumMax}}
                                                               setPaymentActive={this.setPaymentActive}
                                                               currencies={currencies} validate={this.validateFields}
                                                               currentPayment={currentPayment}
                                                               paymentFields={paymentFields}/>
                                            </Popup>
                                        </div>
                                    </div>
                                    <div className="profile-info">
                                        <div className="profile-info-statistics info-statistics">
                                            <div className="text-column info-statistics-column">
                                                <div className='info-statistics-column__text'>
                                                    <div className="text-column__heading">Регистрация</div>
                                                    <div className="text-column__value"><Date date={regDate}
                                                                                              type='year'/></div>
                                                </div>
                                            </div>
                                            <div className="text-column info-statistics-column">
                                                <div className="info-statistics-column__text">
                                                    <div className="text-column__heading">Статус аккаунта</div>
                                                    <div className="text-column__value">VIP</div>
                                                </div>

                                            </div>

                                            <div className="text-column info-statistics-column">
                                                <div className="info-statistics-column__text">
                                                    <div className="text-column__heading">Бонусные очки</div>
                                                    <div className="text-column__value">200</div>
                                                </div>

                                            </div>

                                            <div className="text-column info-statistics-column">
                                                <div className="info-statistics-column__text">
                                                    <div className="text-column__heading">Steam Guard</div>
                                                    <Label color='green' text={escrow ? 'Включен' : 'Выключен'}
                                                           style={{cursor: 'pointer'}}/>
                                                </div>

                                            </div>
                                            <div className="text-column info-statistics-column">
                                                <div className="info-statistics-column__text">
                                                    <div className="text-column__heading">Участие в лотах</div>
                                                    <div className="text-column__value">{lotsParticipated}</div>
                                                </div>

                                            </div>
                                            <div className="text-column info-statistics-column">
                                                <div className="info-statistics-column__text">
                                                    <div className="text-column__heading">Выиграно лотов</div>
                                                    <div className="text-column__value">{lotsWon}</div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <CopyContext.Provider value={{copyToClipboard: this.copyToClipboard}}>
                                    <ProfileTabs
                                        lots={lots}
                                        ownLots={ownLots}
                                        deals={deals}
                                        affiliate={affiliateProgram}
                                        inventory={this.state.inventory}
                                        activeIndex={this.state.activeIndex}
                                        onClick={this.selectItem}
                                        transactions={transactions}
                                        loadInventory={this.loadInventory}
                                        notifications={notifications}
                                        setActiveNotifications={this.setActiveNotification}
                                        steamId={steamId}
                                    />
                                </CopyContext.Provider>
                            </div>
                        </div>
                    </div>
                </div>
            ) : (
                <Redirect to='/'/>
            )
        )
    };
}

export {CopyContext};
export default Profile;
