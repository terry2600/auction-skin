import React from "react";
import SkinsTransactions from "../components/SkinsTransactions";

class Exchange extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            exchanges: [],
            transactions: []
        };

        this.controller = new AbortController();

        this.getTransactions = this.getTransactions.bind(this);
    }

    componentDidMount() {
        this.getTransactions()
    }

    componentWillUnmount() {
        this.controller.abort();
    }

    getTransactions() {
        fetch(`/api/trades/ended`, {
            signal: this.controller.signal,
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(response => response.json())
            .then(transactions => {
                this.setState({transactions})
            })
            .catch(e => console.log(e))
    }


    render() {
        const {transactions} = this.state;
        return (
            <div className="profile-tab-wrapper">
                <div className="transactions-block">
                    <SkinsTransactions transactions={transactions}/>
                </div>
            </div>
        )
    }
}

export default Exchange;
