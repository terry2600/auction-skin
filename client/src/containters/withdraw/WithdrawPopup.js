import React from "react";
import Dropdown from 'react-dropdown';

export default ({payMethods,setPaymentActive,currentPayment,paymentFields,validate,onChangeInputAmount,currencies,sums,setCurrency,currency,onFormSubmit,onChangeInput})=>(

    <div className='withdraw-container'>
        <div className='withdraw-block_left'>
            <div className='withdraw-header'>
                <p>Выбор системы</p>
            </div>
            <div className='payment-container'>
                <ul className='payment-list'>
                {payMethods?payMethods.map((item,key) =>(
                    <li className={item.active ? 'payment-block payment-block_active' : 'payment-block'}
                        onClick={() => {
                            setPaymentActive(item.name);
                        }} key={key}>
                        <div className='payment-block_text'>{item.name}</div>
                        <div className='payment-block_image'><img src={item.image} alt=""/></div>
                    </li>
                )):''}
                </ul>
            </div>
        </div>
        <div className='withdraw-block_right'>
            <div className='withdraw-header'>
                <h4>Вывести средства</h4>
            </div>
            <div>
                <form onSubmit={onFormSubmit}>
                {currentPayment?
                    currentPayment.map(item =>(
                        <div className='withdraw-input_container'>
                            <p>Сумма вывода:</p>
                            <input onChange={(event)=>onChangeInputAmount(event)} name='amount' type="text" pattern="" className='withdraw-input' placeholder={`От:${sums.sumMin[currency]?sums.sumMin[currency]:'0'},  До:${sums.sumMax[currency]?sums.sumMax[currency]:'0'}`}/>
                            <Dropdown options={currencies}  controlClassName='filter__dropdown dropdown withdraw'
                                      onChange={(value)=>setCurrency(value.value)}
                                      menuClassName='dropdown_menu '
                                      arrowClassName='dropdown__arrow' value={currency?currency:'Выберите...'}/>
                        </div>
                    )):''}
                {paymentFields ? paymentFields.map(item => (
                    <div className='withdraw-input_container'>
                        <p>{item.name}:</p> <input onChange={(event)=>onChangeInput(event)}  className='withdraw-input' name={item.title} type="text"  placeholder={item.example} required={true}/>
                     </div>
                )) : ''}
                    {currentPayment? currentPayment.map(item =>(
                <input type="submit" value='Отправить' className='withdraw-submit'/>)):''}
                </form>

            </div>
        </div>
    </div>
)