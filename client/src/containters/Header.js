import React from 'react';
import Burger from '../components/Burger';
import Logo from '../components/Logo';
import HeaderTabs from '../components/HeaderTabs';
import HeaderProfile from '../components/HeaderProfile';
import Authorization from './Authorization';

const tooltipProfileOptions = [
    {label: 'Выйти', url: '/api/auth/logout'}
];

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isTooltipHeaderOpened: false
        }
    }

    toggleProfileTooltip = () => {
        this.setState(prevState => ({isTooltipHeaderOpened: !prevState.isTooltipHeaderOpened}))
    };

    burgerClick = () => {
        this.props.onClick()
    };

    render() {
        const {isToggled, games, selectedGame, toggleGame, checkExtension, checkTradeBan} = this.props;
        const {isTooltipHeaderOpened} = this.state;
        return (
            <header className='header'>
                <Burger onClick={this.burgerClick}
                        open={isToggled}/>
                <Logo
                    checkExtension = {checkExtension}
                    checkTradeBan={checkTradeBan}
                    games={games}
                    selectedGame={selectedGame}
                    toggleGame={toggleGame}/>
                <HeaderTabs/>
                <HeaderProfile
                    isTooltipOpened={isTooltipHeaderOpened}
                    options={tooltipProfileOptions}
                    toggleTooltip={this.toggleProfileTooltip}/>
                <Authorization/>
            </header>
        )
    };
}

export default Header;
