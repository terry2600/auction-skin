const defaults = {
    EXTENSION_ID: 'fmadghckfohbhafcdmigiddopfbpcope',
    BACKEND_URL: 'http://auction.ad.gtbrain.ru:9017/'
};

for(let key in defaults){
    if(process.env.hasOwnProperty(key))
        defaults[key] = process.env[key]
}

module.exports = defaults;
