export default {
    Admin: {
        routes: [],
        permissions: {
            lots: ['create:any'],
            profile: ['read:any', 'update:own'],
            admin: ['create:any', 'read:any', 'update:any', 'delete:any']
        }
    },
    User: {
        routes: [
            {
                url: '/profile',
                component: 'Profile'
            },
            {
                url:'/exchange',
                component: 'Exchange'
            },
            {
                url:'/userlots',
                component:'MainUserLots'
            }
        ],
        permissions: {
            lots: ['create:own', 'read:own'],
            profile: ['update:own', 'read:own'],
        }
    },
    Viewer: {
        routes: [
            {
                url: '/',
                component: 'MainPage'
            },
            {
                url: '/faq',
                component: 'FAQ'
            },
            {
                url: '/static/:page',
                component: 'Static'
            }
        ],
        permissions: {
            auth: ['read:own'],
            lots: ['read:any'],
            faq: ['read:any'],
            profile: ['read:any']
        }
    }
}
