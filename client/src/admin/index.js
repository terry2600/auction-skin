import React from 'react';
import {Admin, Resource} from 'react-admin';

import dataProvider from './DataProvider';
import Dashboard from './Dashboard';
import users from './users';
import bots from './bots';
import lots from './lots';
import inventory from './inventory';
import faq from './faq';
import faqTab from './faqTab';
import statics from './static'; // static is reserved word
import stats from './stats';
import MyLayout from './MyLayout'
import Routes from './Routes'

const AdminPage = () => (
        <Admin dashboard={Dashboard} dataProvider={dataProvider} customRoutes={Routes} layout={MyLayout}>
            <Resource name='admin/users' {...users}/>
            <Resource name='admin/bots' {...bots}/>
            <Resource name='admin/lots' {...lots}/>
            <Resource name='admin/inventory' {...inventory}/>
            <Resource name='admin/faq' {...faq}/>
            <Resource name='admin/faqTab' {...faqTab}/>
            <Resource name='admin/static' {...statics}/>
            <Resource name='admin/stats' {...stats}/>
        </Admin>
);

export default AdminPage;
