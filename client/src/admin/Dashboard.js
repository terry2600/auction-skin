import React, { useState, useEffect, useCallback } from 'react';
import { useDataProvider} from 'react-admin';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CountCard from './CountCard';
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import moment from "moment";
import MomentUtils from "@date-io/moment";
import "moment/locale/ru";

moment.locale("ru");


const styles = {
    flex: { display: 'flex' },
    flexColumn: { display: 'flex', flexDirection: 'column' },
    leftCol: { flex: 1, margin: '1em' },
    rightCol: { flex: 1, margin: '1em' },
    singleCol: { margin: '2em' },
    grid: {
        display: 'grid',
        gridTemplateColumns: 'repeat(auto-fill, minmax(15em, 1fr))'
    }
};

const Dashboard = () => {
    const startDateInit = new Date();
    startDateInit.setDate(startDateInit.getDate() - 1);

    const [state, setState] = useState({
        stats: [],
        allTimeStatsLoaded: false
    });

    const [startDate, setStartDateState] = useState(startDateInit)
    const [endDate, setEndDateState] = useState(new Date())

    //const version = useVersion();
    const dataProvider = useDataProvider();

    const fetchStats = useCallback(async (startDate, endDate, shouldLoadAllTimeStats) => {
        let allTimeStats = {}
        if(shouldLoadAllTimeStats)
            allTimeStats = (await dataProvider.getOne('stats', {
                id: 0
            })).data

        const { data: stats } = await dataProvider.getList('stats', {
            filter: {
                startDate,
                endDate
            },
            sort: {},
            pagination: {}
        });

        setState(state => ({
            ...state,
            ...allTimeStats,
            allTimeStatsLoaded: true,
            stats
            //stats: Object.entries(stats[0]).reduce((acc, [value, title]) => (acc.push({title, value})), [])
        }));
    }, [dataProvider]);

    useEffect(() => {
        fetchStats(startDate, endDate, !state.allTimeStatsLoaded);
    }, [startDate, endDate]);

    const {
        users,
        doneAuctions,
        money,
        itemsPrice,
        stats
    } = state;

    const setStartDate = (date) => setStartDateState(date.toDate())
    const setEndDate = (date) => setEndDateState(date.toDate())

    return (
    <div style={styles.flexColumn}>
        <div style={styles.leftCol}>
            <div style={styles.flex}>
                <CountCard title="Пользователи" value={users}  />
                <CountCard title="Аукционы" value={doneAuctions}  />
                <CountCard title="Оборот" value={money}  />
                <CountCard title="Цена лута" value={itemsPrice}  />
            </div>
        </div>

        <div style={styles.leftCol}>
            <Card style={styles.flexColumn}>
                <CardHeader title="Статистика" />
                <CardContent>
                    <MuiPickersUtilsProvider libInstance={moment} utils={MomentUtils} locale="ru">
                        <div style={styles.flex}>
                            <KeyboardDatePicker
                              style={styles.singleCol}
                              margin="normal"
                              label="Начало"
                              value={startDate}
                              onChange={setStartDate}
                              format="DD.MM.YYYY"
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                            />
                            <KeyboardDatePicker
                              style={styles.singleCol}
                              margin="normal"
                              label="Конец"
                              value={endDate}
                              onChange={setEndDate}
                              format="DD.MM.YYYY"
                              KeyboardButtonProps={{
                                'aria-label': 'change date',
                              }}
                            />
                        </div>
                     </MuiPickersUtilsProvider>
                    <List style={styles.grid}>
                        {stats.map((stat, i) =>
                            <ListItem key={i}>
                                <ListItemText primary={stat.title} secondary={stat.value} />
                            </ListItem>
                        )}
                    </List>
                </CardContent>
            </Card>
        </div>
    </div>
    )
}

export default Dashboard;

/*
    <TextField source="period"/>
    <TextField source="users"/>
    <TextField source="money"/>
    <TextField source="openAuctions"/>
    <TextField source="doneAuctions"/>
    <TextField source="bots"/>
    <TextField source="items"/>
    <TextField source="itemsPrice"/>*/
