import React from 'react';
import { Route } from 'react-router-dom';
import Dashboard from './Dashboard'

export default [
    <Route exact path='/admin' component={Dashboard}/>
]
