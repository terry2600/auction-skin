import FaqTabList from "./FaqTabList";
import FaqTabCreate from "./FaqTabCreate";
import FaqTabEdit from "./FaqTabEdit";

export default {
    list: FaqTabList,
    create: FaqTabCreate,
    edit: FaqTabEdit,
    options: {
        label: 'Faq Tab'
    }
}
