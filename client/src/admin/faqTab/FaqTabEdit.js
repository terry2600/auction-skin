import React from 'react';
import { Edit, SimpleForm, TextInput} from 'react-admin';


export default (props) => (
    <Edit {...props}>
      <SimpleForm>
          <TextInput disabled source="id"/>
          <TextInput source="title"/>
          <TextInput source="order"/>
          <TextInput source="language"/>
      </SimpleForm>
    </Edit>
);
