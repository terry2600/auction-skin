import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';
//import RichTextInput from 'ra-input-rich-text';

export default (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="title"/>
            <TextInput source="order"/>
            <TextInput source="language"/>
        </SimpleForm>
    </Create>
);
