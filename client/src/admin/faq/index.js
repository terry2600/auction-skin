import FaqList from "./FaqList";
import FaqCreate from "./FaqCreate";
import FaqEdit from "./FaqEdit";

export default {
    list: FaqList,
    create: FaqCreate,
    edit: FaqEdit,
    options: {
        label: 'Faq'
    }
}
