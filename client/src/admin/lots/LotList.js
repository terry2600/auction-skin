import React from 'react';
import {List, Datagrid, TextField, Pagination} from 'react-admin';

const PostPagination = props => <Pagination rowsPerPageOptions={[5, 10, 25, 50, 100]} {...props}/>;

export default (props) => (
    <List {...props} pagination={<PostPagination/>}>
        <Datagrid {...props} rowClick='edit'>
            <TextField source="id"/>
            <TextField source="GameItem.Game.name" label='Category'/>
            <TextField source="GameItem.name" label='Skin'/>
            <TextField source="participants"/>
            <TextField source="cost_of_participation"/>
            <TextField source="lot_step"/>
        </Datagrid>
    </List>
);
