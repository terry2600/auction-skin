import LotList from "./LotList";

export default {
    list: LotList,
    options: {
        label: 'Lots'
    }
}
