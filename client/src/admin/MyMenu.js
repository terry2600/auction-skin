import React from 'react';
import PropTypes from 'prop-types';
import { shallowEqual, useSelector } from 'react-redux';
import inflection from 'inflection';
import { makeStyles, useMediaQuery } from '@material-ui/core';
import DefaultIcon from '@material-ui/icons/ViewList';
import classnames from 'classnames';
import { getResources, useTranslate } from 'ra-core';

import {MenuItemLink} from 'react-admin';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SiteIcon from '@material-ui/icons/Accessible';
//import DashboardMenuItem from './myDashboardMenuItem'

const useStyles = makeStyles(
    {
        main: {
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-start',
        },
    },
    { name: 'RaMenu' }
);

const Menu = ({
    classes: classesOverride,
    className,
    dense,
    hasDashboard,
    onMenuClick,
    logout,
    ...rest
}) => {
    const classes = useStyles({ classes: classesOverride });
    const isXSmall = useMediaQuery(theme => theme.breakpoints.down('xs'));
    const open = useSelector(state => state.admin.ui.sidebarOpen);
    const resources = useSelector(getResources, shallowEqual);
    useSelector(state => state.router.location.pathname); // used to force redraw on navigation

    return (
        <div className={classnames(classes.main, className)} {...rest}>
            <MenuItemLink
                to="/"
                primaryText='Site'
                leftIcon={<SiteIcon />}
                exact
                onClick={e => window.location = '/'}
                dense={dense}
                sidebarIsOpen={open}
            />
            {hasDashboard && (
                <MenuItemLink
                    to="/admin"
                    primaryText='Dashboard'
                    leftIcon={<DashboardIcon />}
                    exact
                    onClick={onMenuClick}
                    dense={dense}
                    sidebarIsOpen={open}
                />
            )}
            {resources
                .filter(r => r.hasList)
                .map(resource => (
                    <MenuItemLink
                        key={resource.name}
                        to={`/${resource.name}`}
                        primaryText={resource.options.label}
                        leftIcon={
                            resource.icon ? <resource.icon /> : <DefaultIcon />
                        }
                        onClick={onMenuClick}
                        dense={dense}
                        sidebarIsOpen={open}
                    />
                ))}
            {isXSmall && logout}
        </div>
    );
};

Menu.propTypes = {
    classes: PropTypes.object,
    className: PropTypes.string,
    dense: PropTypes.bool,
    hasDashboard: PropTypes.bool,
    logout: PropTypes.element,
    onMenuClick: PropTypes.func,
};

Menu.defaultProps = {
    onMenuClick: () => null,
};

export default Menu;
