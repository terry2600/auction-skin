import React from 'react';
import { Create, SimpleForm, NumberInput, DateTimeInput, BooleanInput, required } from 'react-admin';

export default ({location, ...props}) => {
    return (
        <Create
            record={location.state}
            location={location}
            {...props}
        >
            <SimpleForm redirect='/lots'>
                <NumberInput source="item.id" label='id' disabled/>
                <NumberInput source='participants' validate={required()}/>
                <NumberInput source='cost_of_participation' validate={required()}/>
                <NumberInput source='lot_step' validate={required()}/>
                <NumberInput source='price' validate={required()}/>
                <DateTimeInput source='start_time' validate={required()}/>
                <BooleanInput label='Hotlot' source='hotlot' defaultValue={false}/>
            </SimpleForm>
        </Create>
    )
};
