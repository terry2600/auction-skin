import InventoryList from "./InventoryList";
import InventoryCreate from "./InventoryCreate";

export default {
    list: InventoryList,
    create: InventoryCreate,
    options: {
        label: 'Inventory'
    }
}
