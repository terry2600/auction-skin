import React from 'react';
import {
    Filter,
    List,
    Datagrid,
    TextField,
    Pagination,
    TextInput,
    SelectInput,
    ImageField,
} from 'react-admin';
import Button from '@material-ui/core/Button';
import { Link } from 'react-admin';

const PostPagination = props => <Pagination rowsPerPageOptions={[5, 10, 25, 50, 100]} {...props}/>;

const InventoryFilter = (props) => (
    <Filter {...props}>
        <TextInput label='Поиск' source='q' alwaysOn/>
        <SelectInput source="game" alwaysOn choices={[
            {id: '730', name: 'CS:GO'},
            {id: '570', name: 'Dota 2'},
        ]}/>
    </Filter>
);

const CreateLotButton = ({record}) => (
    <Button
        component={Link}
        to={{
            pathname: '/admin/inventory/create',
            state: {
                item: {
                    id: record.id,
                    appid: record.appid,
                    assetid: record.assetid,
                    icon_url: record.icon_url,
                    icon_url_large: record.icon_url_large,
                    description: record.descriptions,
                    name: record.name,
                    name_color: record.name_color,
                    market_name: record.market_name,
                    market_hash_name: record.market_hash_name,
                    marketable: record.marketable,
                    tags: record.tags,
                    actions: record.actions
                }
            }
        }}
    >
        Создать лот
    </Button>
);

export default (props) => (
    <React.Fragment>
        <List {...props}
              filters={<InventoryFilter/>}
              pagination={<PostPagination/>}
              filterDefaultValues={{game: '730'}}
              bulkActionButtons={false}
              actions={null}
        >
            <Datagrid {...props}>
                <ImageField source='icon_url' title='image'/>
                <TextField source="name"/>
                <TextField source="market_hash_name"/>
                <CreateLotButton/>
            </Datagrid>
        </List>
    </React.Fragment>
);
