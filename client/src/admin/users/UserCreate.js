import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';

export default (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name"/>
            <TextInput source="nickname"/>
            <TextInput source="email"/>
        </SimpleForm>
    </Create>
);