import React from 'react';
import { Edit, SimpleForm,TextInput} from 'react-admin';

export default (props) => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput disabled source="id"/>
            <TextInput disabled source="steamid64"/>
            <TextInput disabled source="steam_nick"/>
        </SimpleForm>
    </Edit>
);
