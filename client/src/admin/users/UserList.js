import React from 'react';
import { Filter, List, Datagrid, TextField, Pagination, TextInput } from 'react-admin';

const PostPagination = props => <Pagination rowsPerPageOptions={[5, 10, 25, 50, 100]} {...props}/>;

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label='Поиск' source='q' alwaysOn/>
    </Filter>
);

export default (props) => (
    <List {...props}  filters={<PostFilter/>} pagination={<PostPagination/>}>
        <Datagrid rowClick='edit'>
            <TextField source="id"/>
            <TextField source="steamid64"/>
            <TextField source="steam_nick"/>
            <TextField source="avatar"/>
            <TextField source="trade_url"/>
            <TextField source="steam_guard"/>
            <TextField source="registration_date"/>
            <TextField source="balance"/>
        </Datagrid>
    </List>
);