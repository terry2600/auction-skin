import StatsList from "./StatsList";

export default {
    list: StatsList,
    options: {
        label: 'Stats'
    }
}
