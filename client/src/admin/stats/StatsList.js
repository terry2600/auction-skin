import React from 'react';
import { Filter, List, Datagrid, TextField, Pagination, TextInput } from 'react-admin';

const PostPagination = props => <Pagination rowsPerPageOptions={[5, 10, 25, 50, 100]} {...props}/>;

const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label='Поиск' source='q' alwaysOn/>
    </Filter>
);

export default (props) => (
    <List {...props}  filters={<PostFilter/>} pagination={<PostPagination/>}>
        <Datagrid rowClick='edit'>
            <TextField source="period"/>
            <TextField source="users"/>
            <TextField source="money"/>
            <TextField source="openAuctions"/>
            <TextField source="doneAuctions"/>
            <TextField source="bots"/>
            <TextField source="items"/>
            <TextField source="itemsPrice"/>
        </Datagrid>
    </List>
);
