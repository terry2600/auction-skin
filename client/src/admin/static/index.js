import StaticList from "./StaticList";
import StaticCreate from "./StaticCreate";
import StaticEdit from "./StaticEdit";

export default {
    list: StaticList,
    create: StaticCreate,
    edit: StaticEdit,
    options: {
        label: 'Static'
    }
}
