import React from 'react';
import { Edit, SimpleForm, TextInput} from 'react-admin';
import RichTextInput from 'ra-input-rich-text';


export default (props) => (
    <Edit {...props}>
      <SimpleForm>
          <TextInput disabled source="id"/>
              <TextInput source="title"/>
              <TextInput source="page"/>
              <TextInput source="language"/>
              <RichTextInput source="html"  toolbar={[
                  ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                  ['blockquote', 'code-block'],

                  [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                  [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                  [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                  [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                  [{ 'direction': 'rtl' }],                         // text direction

                  [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                  [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

                  [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                  [{ 'font': [] }],
                  [{ 'align': [] }],

                  ['clean']                                         // remove formatting button
              ]}  />
      </SimpleForm>
    </Edit>
);
