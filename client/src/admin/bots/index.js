import BotList from "./BotList";
import BotCreate from "./BotCreate";

export default {
    list: BotList,
    create: BotCreate,
    options: {
        label: 'Bots'
    }
}
