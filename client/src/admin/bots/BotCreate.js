import React from 'react';
import { Create, SimpleForm, TextInput } from 'react-admin';

export default (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="steamid64"/>
        </SimpleForm>
    </Create>
);