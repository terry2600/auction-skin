import {stringify} from 'querystring';
import {GET_LIST, GET_ONE, CREATE, DELETE, DELETE_MANY, UPDATE} from 'react-admin';

export default (type, resource, params) => {
    resource = resource.replace('admin/', '')
    let url = '';
    let query;
    const options = {
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        })
    };
    switch (type) {
        case CREATE:
            url = `/api/${resource}`;
            options.method = 'POST';
            options.body = JSON.stringify(params.data);
            break;
        case GET_ONE:
            url = `/api/${resource}/${params.id}`;
            if(resource === 'static')
                url = `/api/static/id/${params.id}`
            break;
        case UPDATE:
            url = `/api/${resource}/${params.id}`;
            options.method = 'PUT';
            options.body = JSON.stringify(params.data);
            break;
        case DELETE:
            url = `/api/${resource}/${params.id}`;
            options.method = 'DELETE';
            break;
        case DELETE_MANY:
            query = JSON.stringify(params.ids);
            url = `/api/${resource}/${query}`;
            options.method = 'DELETE';
            break;
        case GET_LIST:
            const {page, perPage} = params.pagination;
            const { field, order } = params.sort;
            query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
                filter: JSON.stringify(params.filter)
            };
            url = `/api/${resource}?${stringify(query)}`;
            break
        default:
            break;
    }

    return fetch(url, options)
        .then(res => res.json())
        .then(json => {
            console.log('Сейчас будет ответ');
            console.log(type);
            console.log(json);
            switch(type){
                case GET_ONE:
                    return {
                        data: json
                    };
                case GET_LIST:
                    return {
                        data: json.data,
                        total: json.total
                    };
                case CREATE:
                    return {
                        data: {...params.data, id: json},
                    };
                case UPDATE:
                    return {
                        data: {...params.data},
                    };
                default:
                    return { data: json };
            }

        })
}
