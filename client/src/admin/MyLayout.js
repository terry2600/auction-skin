import React from 'react';
import { Sidebar, Layout, Menu} from 'react-admin';
import { makeStyles } from '@material-ui/core/styles';
import MyMenu from './MyMenu'

/*const MyMenu = props => {
    console.log(props)
    return (
        <Menu {...props}>
            text
        </Menu>
    )}*/

const MyLayout = props => <Layout {...props} menu={MyMenu} />

export default MyLayout
