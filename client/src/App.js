import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Loader from 'react-loader-spinner';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"
import MainPage from './containters/MainPage';
import Profile from './containters/Profile';
import FAQ from './containters/FAQ';
import Static from "./containters/Static";
import NotFound from './containters/NotFound';
import Can from './components/Can';
import AdminPage from './admin';
import Exchange from './containters/Exchange';
import MainUserLots from './containters/MainUserLots';
import PopUp from './containters/PopUp';
import rules from './config/rules';
import Sidebar from "./containters/Sidebar";
import Header from "./containters/Header";
import Footer from "./containters/Footer";
import NotificationList from "./components/NotificationList";
import {updateTradeUrl} from './functions/api/Profile/api';
import socketIOClient from 'socket.io-client';
import {EXTENSION_ID, BACKEND_URL} from './config/constants'
import {fetchTradeBanProfile} from "./functions/api/Profile/api";
/*global chrome*/


const UserContext = React.createContext({});
const BalanceContext = React.createContext({});

const routes = {MainPage, Profile, FAQ, Exchange, MainUserLots, PopUp, Static};

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggled: false,
            games: [{
                label: 'CS:GO',
                value: 730,
                color: '#f79308',
                img: 'img/statistics-cs.png'
            }, {
                label: 'DOTA',
                value: 570,
                color: '#b91b01',
                img: 'img/statistics-dota.png'
            }],
            selectedGame: {
                label: 'CS:GO',
                value: 730
            },
            stats: {
                total: 0,
                cs: 0,
                dota: 0
            },
            userName: null,
            avatar: null,
            steamId: null,
            regDate: null,
            role: 'Viewer',
            allowedRoutes: rules['Viewer'].routes,
            isFetching: true,
            warnings: [],
            socket: socketIOClient(BACKEND_URL),
        };

        this.state.socket.on('trade', tradeInfo => {
            return new Promise((res, rej) => {
                const cb = ({data}) => {
                    if (data.from !== 'extension')
                        return;

                    window.removeEventListener('message', cb);
                    this.state.socket.emit('trade', {tradeInfo, result: data.data})
                };
                window.addEventListener('message', cb);
                window.postMessage({
                    method: 'trade', data: tradeInfo
                }, '*')
            })
        });

        this.state.socket.on('apikey', e => {
            return new Promise((res, rej) => {
                const cb = ({data}) => {
                    if (data.from !== 'extension')
                        return;

                    window.removeEventListener('message', cb);
                    this.state.socket.emit('apikey', data.data)
                };
                window.addEventListener('message', cb);
                window.postMessage({
                    method: 'auth'
                }, '*')
            })
        });

        this.state.socket.on('updateBalance', balance => this.setState({balance}));
        this.fetchTradeBanProfile = fetchTradeBanProfile.bind(this);
    }

    componentDidMount() {
        fetch('/api/auth/steam/success', {
            method: 'GET',
            credentials: 'include',
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Credentials": true
            }
        })
            .then(response => {
                if (response.status === 200) return response.json();
                throw new Error("failed to authenticate user");
            })
            .then(responseJson => {
                if (responseJson.user) {
                    const {avatar, userName, id, steamid64, role, balance, registration_date, steam_guard, trade_url} = responseJson.user;

                    const roles = [role, 'Viewer'];
                    if (role === 'Admin')
                        roles.push('User')

                    const allowedRoutes = roles.reduce((cur, role) => {
                        return [...cur, ...rules[role].routes]
                    }, []);

                    this.setState({
                        avatar,
                        userName,
                        userId: id,
                        steamId: steamid64,
                        role,
                        balance,
                        allowedRoutes,
                        regDate: registration_date,
                        steamGuard: steam_guard,
                        tradeUrl: trade_url,
                        isFetching: false
                    })
                } else {
                    this.setState({
                        isFetching: false
                    });

                    const queryParams = new URLSearchParams(window.location.search);
                    const steamId = queryParams.get('invite');

                    if (steamId) {
                        window.history.replaceState(null, null, window.location.pathname);
                        fetch(`/api/users/referer/${steamId}`, {
                            method: 'GET',
                            headers: {
                                "Accept": "application/json",
                                "Content-Type": "application/json",
                                "Access-Control-Allow-Credentials": true
                            }
                        })
                    }
                }
            })
            .catch(err => console.log(err));
        this.getStats();
    }

    addWarning = (title, text, color, icon) => {
        this.setState({
            warnings: [...this.state.warnings, {
                title: title,
                text: text,
                color: color,
                icon: icon
            }]
        })
    };

    getStats = () => {
        try{
            fetch(`/api/lots/stats`, {
                method: "GET",
                headers: {
                    Accept: "application/json",
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            })
                .then(response => response.json())
                .then(stats => {
                    this.setState({stats})
                })
        }
       catch (e) {
           console.log(e)
       }
    };

    checkSteamGuard = async steamGuard => {
        try {
            if (!steamGuard) {
                this.setState({
                    warnings: [...this.state.warnings, {
                        title: 'Не установлен SteamGuard',
                        text: 'Пожалуйста установите расширение и не ебите нам голову,Пожалуйста установите расширение и не ебите нам голову,Пожалуйста установите расширение и не ебите нам голову',
                        color: '#ff8f00',
                        icon: require('./assets/img/orange-warn.png')
                    }]
                })
            }
        }
        catch (e) {
            console.log(e)
        }
    };

    toggleSidebar = () => {
        this.setState({isToggled: !this.state.isToggled})
    };

    toggleGame = (game) => {
        this.setState({selectedGame: game})
    };

    checkExtension = () => {
        try{
            this.setState({warnings: []});
            return new Promise((res, rej) => {
                const timeout = setTimeout(e => cb({data: {from: 'extension', data: false}}), 200)

                const cb = ({data}) => {
                    if (data.from !== 'extension')
                        return;

                    window.removeEventListener('message', cb)
                    clearTimeout(timeout)
                    res(data.data)
                }
                window.addEventListener('message', cb)
                window.postMessage({
                    method: 'ping'
                }, '*')
            })
        }
        catch (e) {
            console.log(e)
        }

    };

    checkTradeBan = async () => {
        try{
            await this.fetchTradeBanProfile().then(res => {
                    if (res.escrow !== 0) {
                        this.setState({
                            warnings: [...this.state.warnings, {
                                title: 'Вы заблокированы в системе обмена',
                                text: 'Вам необходимо установить расширение для участия',
                                color: '#ff0059',
                                icon: require('./assets/img/red-warn.png')
                            }]
                        })
                    }
                }
            );
        }
        catch (e) {
            console.log(e)
        }

    };

    render() {
        const {
            avatar, userName, steamId, userId, role, balance, allowedRoutes, isFetching, regDate, steamGuard, tradeUrl,
            isToggled, games, selectedGame, stats, socket
        } = this.state;
        return (
            isFetching ? (
                <Loader
                    type='BallTriangle'
                    color='#00BFFF'
                    height={300}
                    width={300}
                />
            ) : (
                <Can
                    role={role}
                    resource='admin'
                    action='read:any'
                    yes={(no) => {
                        if (!window.location.pathname.startsWith('/admin'))
                            return no();
                        return (<AdminPage/>)
                    }}
                    no={() => (
                        <UserContext.Provider
                            value={{
                                avatar,
                                userName,
                                role,
                                steamId,
                                regDate,
                                steamGuard,
                                tradeUrl,
                                updateTradeUrl: this.updateTradeUrl
                            }}>
                            <BalanceContext.Provider value={{balance}}>
                                <React.Fragment>
                                    <style>{`
                                    .wrapper::after {
                                        background: url("img/background-${selectedGame.value}.png") left top/70% no-repeat;
                                }`
                                    }
                                    </style>
                                    <div className={isToggled ? 'wrapper toggled' : 'wrapper'}>
                                        <Sidebar
                                            onClick={this.toggleSidebar}
                                            isToggled={isToggled}
                                            userName={userName}
                                            role={role}
                                            balance={balance}
                                        />
                                        <Header
                                            isToggled={isToggled}
                                            onClick={this.toggleSidebar}
                                            games={games}
                                            selectedGame={selectedGame}
                                            toggleGame={this.toggleGame}
                                            checkExtension={this.checkExtension}
                                            checkTradeBan={this.checkTradeBan}
                                        />
                                        <Switch>
                                            {allowedRoutes.map(({url, component}) => {
                                                let Component = routes[component];
                                                return (
                                                    <Route
                                                        key={url}
                                                        exact path={url}
                                                        render={({match}) =>
                                                           <Component
                                                               {...this.state}
                                                               toggleGame={this.toggleGame}
                                                               checkExtension={this.checkExtension}
                                                               checkSteamGuard={this.checkSteamGuard}
                                                               checkTradeBan={this.checkTradeBan}
                                                               addWarning={this.addWarning}
                                                               updateTradeUrl={this.updateTradeUrl}
                                                               match={match}/>
                                                        }
                                                    />
                                                )
                                            })}
                                            <Route component={NotFound}/>
                                        </Switch>
                                        <Footer stats={stats}/>
                                        <NotificationList userId={userId} socket={socket}/>
                                    </div>
                                </React.Fragment>
                            </BalanceContext.Provider>
                        </UserContext.Provider>
                    )}
                />
            )
        )
    };
}

export {UserContext, BalanceContext};
export default App;
