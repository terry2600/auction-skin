import ValidationTradeUrlError from '../erorrHandlers/ValidationTradeUrlError';

export default function(currentTradeUrl, newTradeUrl, tradeUrlError, returnState) {
    const regExp = 'https://steamcommunity.com/tradeoffer/new/\\?partner=.+&token=.{8}';
    if (!new RegExp(regExp).test(newTradeUrl)) {
        throw new ValidationTradeUrlError('Ваш trade URL не соответствует шаблону')
    } else if (currentTradeUrl === newTradeUrl) {
        throw new ValidationTradeUrlError('Вы ввели trade URL, который сейчас привязан к вашему профилю')
    }

    this.setState({...returnState, tradeUrlError: null});
}