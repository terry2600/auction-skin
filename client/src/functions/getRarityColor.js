export function getRarity(item) {
    if (item !== undefined) {
        switch (item.name) {
            case "Ширпотреб":
                return "#494a53";
            case "Запрещённое":
                return "#52188b";
            case "Засекреченное":
                return "#92102e";
            case "Армейское качество":
                return "#1683b2";
            case "Промышленное качество":
                return "#E1E0D7";
            default:
                return "#494a53";
        }
    } else return "#494a53";
}