export const payTest = {
    26808: {
        name: 'QIWI Кошелёк',
        id: "qiwi",
        image: 'https://payeer.com/upload/iblock/a5a/fe0f0469d68b6a57386e3d6aa8092596.png',
        currencies: ["RUB"],
        commission: {
            RUB: "1.5%"
        },
        r_fields: {
            ACCOUNT_NUMBER: {
                name: "Номер телефона",
                reg_expr: /^\\+\\d{9,15}$/,
                example: "+7953155XXXX"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 15000
        },
        commission_site_percent: "1.4"
    },
    268010: {
        name: 'Автоопределение телефона',
        id: "mobile",
        image: 'https://payeer.com/upload/iblock/a5a/fe0f0469d68b6a57386e3d6aa8092596.png',
        currencies: ["RUB"],
        commission: {
            RUB: "1.5%"
        },
        r_fields: {
            ACCOUNT_NUMBER: {
                name: "Номер телефона",
                reg_expr: /^\\+\\d{9,15}$/,
                example: "+7953155XXXX"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 15000
        },
        commission_site_percent: "1.4"
    },
    268056: {
        name: 'VISA/MASTERCARD UA',
        id: "card_ua",
        image: 'https://payeer.com/upload/iblock/a5a/fe0f0469d68b6a57386e3d6aa8092596.png',
        currencies: ["RUB"],
        commission: {
            RUB: "1.5%"
        },
        r_fields: {
            ACCOUNT_NUMBER: {
                "name": "Номер карты",
                "reg_expr": /^[45]{1}[\\d]{15}$/,
                "example": "512107XXXX785577"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 15000
        },
        commission_site_percent: "1.4"
    },
    510572988: {
        id: "card_mir_ru",
        image: 'https://payeer.com/upload/iblock/e74/mir_53.png',
        name: "МИР",
        gate_commission: {
            RUB: "2%+45"
        },
        gate_commission_min: [],
        gate_commission_max: [],
        currencies: [
            "RUB"
        ],
        commission_site_percent: "1.9",
        r_fields: {
            ACCOUNT_NUMBER: {
                "name": "Номер карты",
                "reg_expr": /^([245]{1}[\\d]{15}|[6]{1}[\\d]{17})$/,
                "example": "212107XXXX785577"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 75000
        }
    },
    244773909: {
        id: "card_ru",
        image: 'https://payeer.com/upload/iblock/60a/27cb184f9a6d71a01a7ad68635d12751.png',
        name: "VISA/MASTERCARD",
        gate_commission: {
            USD: "2.5%+5",
            EUR: "2.5%+5"
        },
        gate_commission_min: [],
        gate_commission_max: [],
        currencies: [
            "RUB"
        ],
        commission_site_percent: "2.4",
        r_fields: {
            ACCOUNT_NUMBER: {
                "name": "Номер карты",
                "reg_expr": /^[45]{1}[\\d]{15}$/,
                "example": "512107XXXX785577"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 15000
        }
    },
    244773123: {
        id: "yandex",
        image: 'https://www.realnyezarabotki.com/wp-content/uploads/ya-dengi-slider-rz-0-min.jpg',
        name: "YANDEX.Money",
        gate_commission: {
            USD: "2.5%+5",
            EUR: "2.5%+5"
        },
        gate_commission_min: [],
        gate_commission_max: [],
        currencies: [
            "RUB"
        ],
        commission_site_percent: "2.4",
        r_fields: {
            ACCOUNT_NUMBER: {
                "name": "Номер счёта",
                "reg_expr": /^[45]{1}[\\d]{15}$/,
                "example": "512107XXXX785577"
            }
        },
        sum_min: {
            RUB: 100
        },
        sum_max: {
            RUB: 15000
        }
    }
}