import React from "react";
import ValidationTradeUrlError from "../../../erorrHandlers/ValidationTradeUrlError";
import {notification } from 'antd'

const openNotificationWithIcon = (type,title,text) => {
    notification[type]({
        message: title,
        description: text,
    });
};


export  function getAffiliate(){
        fetch(`/api/users/referals`,{
            method:'GET'
        })
            .then(res => {
                if(!res.ok){
                    throw new Error('err');
                }
               return res.json()
            })
            .then(res => this.setState({affiliateProgram:res}))
            .catch(err =>{
                openNotificationWithIcon('error','Не удалось получить рефералов','К сожалению, в данный момент невозможно получить рефералов, повторите запрос позже!')

            })
}

export  function getWonLots(){
        fetch(`/api/users/lots/won`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                if(!res.ok){
                    throw new Error('err');
                }
               return  res.json()
            })
            .then(lotsWon => {
                this.setState({
                    lotsWon
                })
            })
            .catch(()=>{
                openNotificationWithIcon('error','Не удалось получить лоты','К сожалению, в данный момент невозможно получить ваши лоты, повторите запрос позже!')
            })
}
export function loadInventory (game){
        fetch(`/api/users/${this.state.steamId}/inventory/${game}`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                if(!res.ok){
                    throw new Error('err');
                }
                return  res.json()
            })
            .then(inventory => {
                this.setState({
                    inventory
                })
            })
            .catch(()=>{
                openNotificationWithIcon('error','Не удалось загрузить инвентарь','К сожалению, в данный момент невозможно получить ваш  инвентарь, повторите запрос позже!')

            })
}

export function getOwnLots() {
        fetch(`/api/lots/own/history`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                if(!res.ok){
                    throw new Error('err');
                }
               return res.json()
            })
            .then(ownLots => {
                this.setState({
                    ownLots
                })
            })
            .catch(()=>{
                openNotificationWithIcon('error','Не удалось ваши лоты!','К сожалению, в данный момент невозможно получить ваши лоты, повторите запрос позже!')

            })

}

export function getLots() {
        fetch(`/api/lots/history`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                if(!res.ok){
                    throw new Error('err');
                }
               return res.json()
            })
            .then(lots => {
                this.setState({
                    lots
                })
            })
            .catch(()=>{
                openNotificationWithIcon('error','Не удалось получить историю  лотов','К сожалению, в данный момент невозможно получить историю ваших лотов, повторите запрос позже!')

            })
}

export function getTransactions() {
        fetch(`/api/users/transactions`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => {
                if(!res.ok)throw new Error('err');
                return res.json()})
            .then(transactions => {
                    this.setState({
                        transactions
                    })
            })
            .catch(()=>{
                openNotificationWithIcon('error','Не удалось получить транзакции','К сожалению, в данный момент невозможно получить ваши транзакции, повторите запрос позже!')
            })

}

export async function fetchTradeBanProfile() {

        return await fetch(`/api/users/tradeable`, {
            method: 'GET',
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
            .then(res => res.json())

}

export function getDeals() {

        fetch(`/deals`, {
            method: 'GET'
        })
            .then(res =>{
                if (!res.ok){
                    throw new Error('err');
                }
                return res.json()
            })
            .then(res => {
                    this.setState({deals: res})
            })
            .catch(err=>{
                openNotificationWithIcon('error','Не удалось получить Ваши транзакции','К сожалению, в данный момент невозможно получить ваши транзакции, повторите запрос позже!')

            })

}

export function getNotifications() {

        fetch('/api/users/notifications')
            .then(res =>{
                if (!res.ok){
                    throw new Error('err');
                }
                return res.json();

            })
            .then(async res => {
                    let buffer = await res.map(item => {
                        return {...item, active: false}
                    });
                    return buffer;
                }
            )
            .then(res => {
                this.setState({notifications: res})
            })
            .catch(err =>{
                openNotificationWithIcon('error','Не удалось получить уведомления','К сожалению, в данный момент невозможно получить ваши уведомления, повторите запрос позже!')

            })
    };

export function getDepositMethods(system){

        fetch(`/api/deals/variants`, {
            method: 'GET'
        })
            .then(res => {
                if(!res.ok) throw new Error('err');
                return res.json()})
            .then(res => {
                let deposits = [];
                res[system].deposit.map(item => {
                    deposits.push({
                        value: item.system,
                        label: item.title
                    })
                });
                this.setState({deposits: deposits}, () => {
                    this.setState({currentDeposit: deposits[0]})
                })
            })
            .catch(err =>{
                openNotificationWithIcon('error','Не удалось получить варианты выплат','К сожалению, в данный момент невозможно получить варианты выплат, повторите запрос позже!')

            })


};

export function sendDeposit(service) {
    let summ = this.state.moneySum;
    let deposit = this.state.currentDeposit;
    try {
        fetch('/api/deals/deposit', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                service: service,
                system: deposit.value,
                currency: 'RUB',
                amount: summ
            })
        })
            .then(res => res.json())
            .then(res => {
                window.location = res.redirect
            })

    }
    catch (e) {
        console.log(e);
    }

};

export function getParticipatedLots() {
    try {
        fetch(`/api/users/lots`, {
            method: "GET",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
            }
        })
    }
    catch (e) {
        openNotificationWithIcon('error','Не удалось получить лоты','К сожалению, в данный момент невозможно получить лоты, повторите запрос позже!')

    }
}

export async function updateTradeUrl(tradeUrl) {
    let res = await fetch(`/api/users/setTradeUrl`, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({tradeUrl})
    });

    let updatedUser = await res.json();
    if (!updatedUser) {
        throw new ValidationTradeUrlError('Введенный вами trade URL не привязан к вашему аккаунту');
    }

    this.setState({
        tradeUrl: updatedUser.trade_url
    });

    return null
}

export function onFormSubmit(e) {
    e.preventDefault();
    const purse = this.state.options.reduce((prev, curr, i) => {
        prev[Object.keys(curr)[0]] = curr[Object.keys(curr)[0]];
        return prev;
    }, {});
    let arrayToSend = {
        system: this.state.id,
        amount: this.state.amount,
        purse: purse[Object.keys(purse)[0]],
        currency: this.state.currency.value
    };
    fetch('/api/deals/withdraw', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            service: 'cypix',
            system: arrayToSend.system,
            currency: arrayToSend.currency,
            amount: arrayToSend.amount,
            purse: arrayToSend.purse
        })
    })
        .then(res => {
            if(!res.ok) throw new Error('err');
            return res.json()})
        .catch(err =>{
            openNotificationWithIcon('error','Не удалось получить выплату','К сожалению, в данный момент невозможно получить выплату в данный момент, повторите запрос позже!')

        });
    console.log(arrayToSend)
};