export function setPaymentActive(name) {
    try{
        let buffer = this.state.payMethods;
        buffer.map(item => {
            if (item.name === name) {
                if (item.active === true) item.active = false;
                if (item.active === false) {
                    item.active = true;
                    this.setState({currentPayment: [item]}, () => {
                        this.setState({id: item.id});
                        let currencies = [];
                        this.setState({sumMin: item.sum_min, sumMax: item.sum_max});
                        for (let i in item.currencies) currencies.push({
                            value: item.currencies[i],
                            label: item.currencies[i]
                        });
                        this.setState({currencies: currencies}, () => {
                            this.setCurrency(currencies[0]);
                        });
                        let arr = [];
                        let fun = async () => {
                            for (let it in this.state.currentPayment) {
                                for (let value in this.state.currentPayment[it].r_fields) {
                                    arr.push({
                                        title: value,
                                        name: item.r_fields[value].name,
                                        reg_expr: item.r_fields[value].reg_expr,
                                        example: item.r_fields[value].example
                                    });
                                }
                            }
                        };
                        fun().then(() => {
                            this.setState({paymentFields: arr})
                        })
                    })
                }
            } else item.active = false;
        });
        this.setState({payMethods: buffer})
    }
    catch (e) {
        console.log(e)
    }

};
export function selectItem (index){
    try {
        let activeIndex = this.state.activeIndex === index ? null : index;
        this.setState({activeIndex: activeIndex});
    }
    catch (e) {
        console.log(e)
    }
};
export function setActiveNotification(id) {
    try{
        let buffer = this.state.notifications.map(item => {
            if (item.id === id) {
                if (item.active === false) {
                    item.active = true;
                } else {
                    item.active = false
                }
                return item;
            } else {
                item.active = false;
                return item
            }
        });
        console.log(buffer);
        this.setState({notifications: buffer})
    }
    catch (e) {
       console.log(e)
    }

};
export function onChangeInput (e, reg) {
    try {
        const name = e.target.name;
        let buff = this.state.options.filter((item) => {
            return Object.keys(item)[0] !== name
        });
        let str = e.target.value.match(reg);
        if (str !== null) {
            console.log(e.target.value.match(reg)[0]);
        } else {
            console.log("err");
        }
        buff.push({[name]: e.target.value});
        this.setState({options: buff})
    }
    catch (e) {
        console.log(e)
    }
};
export function setDeposit (value)  {
    this.setState({currentDeposit: value})
};
export function getPayMethods () {
    try{
        const object = [];
        for (let item in this.state.payTest) {
            this.state.payTest[item]['active'] = false;
            object.push(this.state.payTest[item]);
        }
        this.setState({payMethods: object})
    }
    catch (e) {
        console.log(e)
    }
};
export function onChangeInputAmount  (e, reg)  {
    try{
        if (e.target.value.match(reg)) {
            this.setState({amount: `${e.target.value}`})
        }
    }
    catch (e) {
        console.log(e)
    }

};
export function handleInputChange (e){
    try {
        let value = e.target.value;
        if (value === '' || /^(?!0+$)[0-9]{1,10}$/.test(value)) {
            this.setState({moneySum: value})
        }
    }
   catch (e) {
       console.log(e)
   }
};
export function setCurrency(value){
    this.setState({currency: value})
};

