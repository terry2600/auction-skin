export default function setTradeUrl() {
    const {newTradeUrl, tradeUrlError} = this.state;
    const {tradeUrl, updateTradeUrl} = this.props;
    const returnState = {
        inChangeMode: false
    };

    try {
        this.validateTradeUrl(tradeUrl, newTradeUrl, tradeUrlError, returnState);

        updateTradeUrl(newTradeUrl)
            .then((error => this.setState({...returnState, tradeUrlError: error})))
            .catch(error => this.setState({inChangeMode: false, tradeUrlError: error}));
    } catch (error) {
        this.setState({...returnState, tradeUrlError: error});
    }
}
