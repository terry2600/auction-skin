const path = require('path')
const fs = require('fs').promises;
const {
    Pool
} = require('pg');
const {
    DB_HOST,
    DB_PORT,
    DB_NAME,
    DB_USERNAME,
    DB_PASSWORD
} = require('../config/constants');

const createDb = () => new Promise((res, rej) => {
    const pool = new Pool({
        user: DB_USERNAME,
        password: DB_PASSWORD,
        database: 'postgres',
        host: DB_HOST,
        port: DB_PORT
    })

    pool.connect(async (err, client, done) => {
        if (err)
            rej(err)

        client.query('CREATE DATABASE ' + DB_NAME, (err) => {
            done()
            res()
        })
    })
})

const initDb = () => new Promise((res, rej) => {
    const pool = new Pool({
        user: DB_USERNAME,
        password: DB_PASSWORD,
        database: DB_NAME,
        host: DB_HOST,
        port: DB_PORT
    })

    pool.connect(async (err, client, done) => {
        if (err)
            rej(err)

        const fileHandle = await fs.open(path.join(__dirname, 'backup.sql'), 'r');
        const sql = await fileHandle.readFile('UTF8');
        await fileHandle.close()

        client.query(sql, (err, result) => {
            done();

            if (err)
                rej(err)

            res(result)
        })
    });
})

const run = async () => {
    try {
        await createDb()
    } catch (e) {
        console.error('Error creating DB: ', e)
        process.exit(1)
    }

    try {
        await initDb()
    } catch (e) {
        console.error('Error initting DB: ', e)
        process.exit(1)
    }

    process.exit(0)
}

run()
