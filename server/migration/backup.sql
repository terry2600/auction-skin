--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 12.0

-- Started on 2020-03-24 10:23:52

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

--
-- TOC entry 338 (class 1259 OID 28104)
-- Name: Bids; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Bids" (
    id integer NOT NULL,
    lot_id integer,
    user_id integer,
    date timestamp with time zone,
    bet_size integer
);


ALTER TABLE public."Bids" OWNER TO auc_admin;

--
-- TOC entry 337 (class 1259 OID 28102)
-- Name: Bids_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Bids_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Bids_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 337
-- Name: Bids_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Bids_id_seq" OWNED BY public."Bids".id;


--
-- TOC entry 307 (class 1259 OID 16608)
-- Name: CsExteriors; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsExteriors" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."CsExteriors" OWNER TO auc_admin;

--
-- TOC entry 306 (class 1259 OID 16606)
-- Name: CSGO_Exterior_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_Exterior_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_Exterior_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 306
-- Name: CSGO_Exterior_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_Exterior_id_seq" OWNED BY public."CsExteriors".id;


--
-- TOC entry 297 (class 1259 OID 16553)
-- Name: CsItemSets; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsItemSets" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."CsItemSets" OWNER TO auc_admin;

--
-- TOC entry 296 (class 1259 OID 16551)
-- Name: CSGO_ItemSet_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_ItemSet_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_ItemSet_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 296
-- Name: CSGO_ItemSet_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_ItemSet_id_seq" OWNED BY public."CsItemSets".id;


--
-- TOC entry 303 (class 1259 OID 16586)
-- Name: CsQualities; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsQualities" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    color character varying(255),
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."CsQualities" OWNER TO auc_admin;

--
-- TOC entry 302 (class 1259 OID 16584)
-- Name: CSGO_Quality_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_Quality_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_Quality_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 302
-- Name: CSGO_Quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_Quality_id_seq" OWNED BY public."CsQualities".id;


--
-- TOC entry 305 (class 1259 OID 16597)
-- Name: CsRarities; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsRarities" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    color character varying(255),
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."CsRarities" OWNER TO auc_admin;

--
-- TOC entry 304 (class 1259 OID 16595)
-- Name: CSGO_Rarity_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_Rarity_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_Rarity_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 304
-- Name: CSGO_Rarity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_Rarity_id_seq" OWNED BY public."CsRarities".id;


--
-- TOC entry 299 (class 1259 OID 16564)
-- Name: CsTypes; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsTypes" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255)
);


ALTER TABLE public."CsTypes" OWNER TO auc_admin;

--
-- TOC entry 298 (class 1259 OID 16562)
-- Name: CSGO_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_Type_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 298
-- Name: CSGO_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_Type_id_seq" OWNED BY public."CsTypes".id;


--
-- TOC entry 301 (class 1259 OID 16575)
-- Name: CsWeapons; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsWeapons" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."CsWeapons" OWNER TO auc_admin;

--
-- TOC entry 300 (class 1259 OID 16573)
-- Name: CSGO_Weapon_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CSGO_Weapon_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CSGO_Weapon_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 300
-- Name: CSGO_Weapon_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CSGO_Weapon_id_seq" OWNED BY public."CsWeapons".id;


--
-- TOC entry 346 (class 1259 OID 28347)
-- Name: CsParams; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."CsParams" (
    id integer NOT NULL,
    asset_id character varying(255),
    exterior_id integer,
    item_set_id integer,
    quality_id integer,
    rarity_id integer,
    type_id integer,
    weapon_id integer,
    "float" character varying(255)
);


ALTER TABLE public."CsParams" OWNER TO auc_admin;

--
-- TOC entry 345 (class 1259 OID 28345)
-- Name: CsParams_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."CsParams_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."CsParams_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 345
-- Name: CsParams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."CsParams_id_seq" OWNED BY public."CsParams".id;


--
-- TOC entry 315 (class 1259 OID 16664)
-- Name: DotaQualities; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaQualities" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    color character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE public."DotaQualities" OWNER TO auc_admin;

--
-- TOC entry 314 (class 1259 OID 16662)
-- Name: DOTA2_Quality_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DOTA2_Quality_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DOTA2_Quality_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 314
-- Name: DOTA2_Quality_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DOTA2_Quality_id_seq" OWNED BY public."DotaQualities".id;


--
-- TOC entry 313 (class 1259 OID 16653)
-- Name: DotaRarities; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaRarities" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    color character varying(255),
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."DotaRarities" OWNER TO auc_admin;

--
-- TOC entry 312 (class 1259 OID 16651)
-- Name: DOTA2_Rarity_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DOTA2_Rarity_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DOTA2_Rarity_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 312
-- Name: DOTA2_Rarity_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DOTA2_Rarity_id_seq" OWNED BY public."DotaRarities".id;


--
-- TOC entry 309 (class 1259 OID 16630)
-- Name: DotaSlots; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaSlots" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."DotaSlots" OWNER TO auc_admin;

--
-- TOC entry 308 (class 1259 OID 16628)
-- Name: DOTA2_Slot_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DOTA2_Slot_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DOTA2_Slot_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 308
-- Name: DOTA2_Slot_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DOTA2_Slot_id_seq" OWNED BY public."DotaSlots".id;


--
-- TOC entry 311 (class 1259 OID 16642)
-- Name: DotaTypes; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaTypes" (
    id bigint NOT NULL,
    internal_name character varying(255) NOT NULL,
    name_ru character varying(255) NOT NULL
);


ALTER TABLE public."DotaTypes" OWNER TO auc_admin;

--
-- TOC entry 310 (class 1259 OID 16640)
-- Name: DOTA2_Type_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DOTA2_Type_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DOTA2_Type_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 310
-- Name: DOTA2_Type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DOTA2_Type_id_seq" OWNED BY public."DotaTypes".id;


--
-- TOC entry 358 (class 1259 OID 42241)
-- Name: Deals; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Deals" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone,
    user_id integer,
    sign integer,
    currency character varying(255),
    amount numeric(10,2),
    service character varying(255),
    system character varying(255),
    status integer,
    purse json
);


ALTER TABLE public."Deals" OWNER TO auc_admin;

--
-- TOC entry 357 (class 1259 OID 42239)
-- Name: Deals_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Deals_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Deals_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 357
-- Name: Deals_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Deals_id_seq" OWNED BY public."Deals".id;


--
-- TOC entry 328 (class 1259 OID 19996)
-- Name: DotaHeros; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaHeros" (
    id integer NOT NULL,
    internal_name character varying(255),
    name character varying(255),
    img character varying(255),
    type character varying(255)
);


ALTER TABLE public."DotaHeros" OWNER TO auc_admin;

--
-- TOC entry 327 (class 1259 OID 19994)
-- Name: DotaHeros_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DotaHeros_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DotaHeros_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 327
-- Name: DotaHeros_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DotaHeros_id_seq" OWNED BY public."DotaHeros".id;


--
-- TOC entry 330 (class 1259 OID 20007)
-- Name: DotaParams; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."DotaParams" (
    id integer NOT NULL,
    asset_id character varying(255),
    hero_id integer,
    quality_id integer,
    rarity_id integer,
    slot_id integer,
    type_id integer
);


ALTER TABLE public."DotaParams" OWNER TO auc_admin;

--
-- TOC entry 329 (class 1259 OID 20005)
-- Name: DotaParams_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."DotaParams_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."DotaParams_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 329
-- Name: DotaParams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."DotaParams_id_seq" OWNED BY public."DotaParams".id;


--
-- TOC entry 322 (class 1259 OID 19959)
-- Name: FaqTabs; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."FaqTabs" (
    id integer NOT NULL,
    title character varying(255),
    "order" integer,
    language character varying(255)
);


ALTER TABLE public."FaqTabs" OWNER TO auc_admin;

--
-- TOC entry 321 (class 1259 OID 19957)
-- Name: FaqTabs_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."FaqTabs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."FaqTabs_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 321
-- Name: FaqTabs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."FaqTabs_id_seq" OWNED BY public."FaqTabs".id;


--
-- TOC entry 324 (class 1259 OID 19967)
-- Name: Faqs; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Faqs" (
    id integer NOT NULL,
    title character varying(255),
    text text,
    tab integer,
    "order" integer
);


ALTER TABLE public."Faqs" OWNER TO auc_admin;

--
-- TOC entry 323 (class 1259 OID 19965)
-- Name: Faqs_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Faqs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Faqs_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 323
-- Name: Faqs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Faqs_id_seq" OWNED BY public."Faqs".id;


--
-- TOC entry 334 (class 1259 OID 28070)
-- Name: GameItems; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."GameItems" (
    id integer NOT NULL,
    asset_id character varying(255),
    icon_url text,
    icon_url_large text,
    description text,
    name character varying(255),
    name_color character varying(255),
    market_name character varying(255),
    market_hash_name character varying(255),
    marketable boolean,
    app_id integer,
    stickers text
);


ALTER TABLE public."GameItems" OWNER TO auc_admin;

--
-- TOC entry 333 (class 1259 OID 28068)
-- Name: GameItems_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."GameItems_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."GameItems_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 333
-- Name: GameItems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."GameItems_id_seq" OWNED BY public."GameItems".id;


--
-- TOC entry 316 (class 1259 OID 17212)
-- Name: Games; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Games" (
    id integer NOT NULL,
    name character varying(255),
    game_filter character varying(255)
);


ALTER TABLE public."Games" OWNER TO auc_admin;

--
-- TOC entry 294 (class 1259 OID 16422)
-- Name: Grants; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Grants" (
    role_id integer NOT NULL,
    privilege_id integer NOT NULL
);


ALTER TABLE public."Grants" OWNER TO auc_admin;

--
-- TOC entry 340 (class 1259 OID 28122)
-- Name: LotResult; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."LotResult" (
    id integer NOT NULL,
    lot_id integer,
    winner_id integer,
    purchase_price character varying(255)
);


ALTER TABLE public."LotResult" OWNER TO auc_admin;

--
-- TOC entry 339 (class 1259 OID 28120)
-- Name: LotResult_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."LotResult_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."LotResult_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 339
-- Name: LotResult_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."LotResult_id_seq" OWNED BY public."LotResult".id;


--
-- TOC entry 320 (class 1259 OID 19828)
-- Name: LotStatuses; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."LotStatuses" (
    id integer NOT NULL,
    status character varying(255),
    status_description character varying(255)
);


ALTER TABLE public."LotStatuses" OWNER TO auc_admin;

--
-- TOC entry 319 (class 1259 OID 19826)
-- Name: LotStatuses_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."LotStatuses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."LotStatuses_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 319
-- Name: LotStatuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."LotStatuses_id_seq" OWNED BY public."LotStatuses".id;


--
-- TOC entry 336 (class 1259 OID 28086)
-- Name: Lots; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Lots" (
    id integer NOT NULL,
    game_item_id integer,
    status_id integer,
    create_date timestamp with time zone,
    expired_date timestamp with time zone,
    start_date timestamp with time zone,
    end_date timestamp with time zone,
    owner_id integer,
    hotlot boolean,
    price numeric(10,2),
    cost_of_participation numeric(10,2),
    lot_step integer,
    participants integer,
    color character varying(255),
    app_id integer,
    trade_ended_date timestamp with time zone
);


ALTER TABLE public."Lots" OWNER TO auc_admin;

--
-- TOC entry 335 (class 1259 OID 28084)
-- Name: Lots_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Lots_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Lots_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 335
-- Name: Lots_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Lots_id_seq" OWNED BY public."Lots".id;


--
-- TOC entry 354 (class 1259 OID 40961)
-- Name: NotificationTypes; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."NotificationTypes" (
    id integer NOT NULL,
    description character varying(255)
);


ALTER TABLE public."NotificationTypes" OWNER TO auc_admin;

--
-- TOC entry 353 (class 1259 OID 40959)
-- Name: NotificationTypes_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."NotificationTypes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."NotificationTypes_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 353
-- Name: NotificationTypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."NotificationTypes_id_seq" OWNED BY public."NotificationTypes".id;


--
-- TOC entry 356 (class 1259 OID 40990)
-- Name: Notifications; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Notifications" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone,
    user_id integer,
    type_id integer,
    body json
);


ALTER TABLE public."Notifications" OWNER TO auc_admin;

--
-- TOC entry 355 (class 1259 OID 40988)
-- Name: Notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Notifications_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Notifications_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 355
-- Name: Notifications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Notifications_id_seq" OWNED BY public."Notifications".id;


--
-- TOC entry 342 (class 1259 OID 28140)
-- Name: Participants; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Participants" (
    id integer NOT NULL,
    lot_id integer,
    user_id integer
);


ALTER TABLE public."Participants" OWNER TO auc_admin;

--
-- TOC entry 341 (class 1259 OID 28138)
-- Name: Participants_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Participants_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Participants_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 341
-- Name: Participants_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Participants_id_seq" OWNED BY public."Participants".id;


--
-- TOC entry 293 (class 1259 OID 16413)
-- Name: Privileges; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Privileges" (
    id integer NOT NULL,
    resource character varying(255),
    action character varying(255),
    attribute character varying(255)
);


ALTER TABLE public."Privileges" OWNER TO auc_admin;

--
-- TOC entry 291 (class 1259 OID 16405)
-- Name: Roles; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Roles" (
    id integer NOT NULL,
    type character varying(255)
);


ALTER TABLE public."Roles" OWNER TO auc_admin;

--
-- TOC entry 348 (class 1259 OID 40892)
-- Name: RustCategories; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."RustCategories" (
    id integer NOT NULL,
    internal_name character varying(255),
    name_ru character varying(255)
);


ALTER TABLE public."RustCategories" OWNER TO auc_admin;

--
-- TOC entry 347 (class 1259 OID 40890)
-- Name: RustCategories_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."RustCategories_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RustCategories_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 347
-- Name: RustCategories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."RustCategories_id_seq" OWNED BY public."RustCategories".id;


--
-- TOC entry 352 (class 1259 OID 40914)
-- Name: RustParams; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."RustParams" (
    id integer NOT NULL,
    asset_id character varying(255),
    category_id integer,
    type_id integer
);


ALTER TABLE public."RustParams" OWNER TO auc_admin;

--
-- TOC entry 351 (class 1259 OID 40912)
-- Name: RustParams_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."RustParams_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RustParams_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 351
-- Name: RustParams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."RustParams_id_seq" OWNED BY public."RustParams".id;


--
-- TOC entry 350 (class 1259 OID 40903)
-- Name: RustTypes; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."RustTypes" (
    id integer NOT NULL,
    internal_name character varying(255),
    name_ru character varying(255)
);


ALTER TABLE public."RustTypes" OWNER TO auc_admin;

--
-- TOC entry 349 (class 1259 OID 40901)
-- Name: RustTypes_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."RustTypes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RustTypes_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 349
-- Name: RustTypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."RustTypes_id_seq" OWNED BY public."RustTypes".id;


--
-- TOC entry 295 (class 1259 OID 16507)
-- Name: Sessions; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Sessions" (
    sid character varying(36) NOT NULL,
    expires timestamp with time zone,
    data text,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


ALTER TABLE public."Sessions" OWNER TO auc_admin;

--
-- TOC entry 326 (class 1259 OID 19985)
-- Name: Statics; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Statics" (
    id integer NOT NULL,
    title character varying(255),
    html text,
    page character varying(255),
    language character varying(255)
);


ALTER TABLE public."Statics" OWNER TO auc_admin;

--
-- TOC entry 325 (class 1259 OID 19983)
-- Name: Statics_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Statics_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Statics_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 325
-- Name: Statics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Statics_id_seq" OWNED BY public."Statics".id;


--
-- TOC entry 332 (class 1259 OID 20945)
-- Name: TransactionTypes; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."TransactionTypes" (
    id integer NOT NULL,
    description character varying(255)
);


ALTER TABLE public."TransactionTypes" OWNER TO auc_admin;

--
-- TOC entry 331 (class 1259 OID 20943)
-- Name: TransactionTypes_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."TransactionTypes_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."TransactionTypes_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 331
-- Name: TransactionTypes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."TransactionTypes_id_seq" OWNED BY public."TransactionTypes".id;


--
-- TOC entry 344 (class 1259 OID 28160)
-- Name: Transactions; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public."Transactions" (
    id integer NOT NULL,
    "createdAt" timestamp with time zone,
    user_id integer,
    lot_id integer,
    lot_name character varying(255),
    type_id integer,
    amount numeric(10,2)
);


ALTER TABLE public."Transactions" OWNER TO auc_admin;

--
-- TOC entry 343 (class 1259 OID 28158)
-- Name: Transactions_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public."Transactions_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Transactions_id_seq" OWNER TO auc_admin;

--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 343
-- Name: Transactions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public."Transactions_id_seq" OWNED BY public."Transactions".id;


--
-- TOC entry 292 (class 1259 OID 16411)
-- Name: privileges_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public.privileges_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.privileges_id_seq OWNER TO auc_admin;

--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 292
-- Name: privileges_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public.privileges_id_seq OWNED BY public."Privileges".id;


--
-- TOC entry 290 (class 1259 OID 16403)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public.roles_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO auc_admin;

--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 290
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public."Roles".id;


--
-- TOC entry 318 (class 1259 OID 19198)
-- Name: users; Type: TABLE; Schema: public; Owner: auc_admin
--

CREATE TABLE public.users (
    id integer NOT NULL,
    steamid64 character varying(255),
    steam_nick character varying(255),
    steam_guard boolean,
    trade_url character varying(255),
    avatar character varying(255),
    balance numeric,
    registration_date timestamp with time zone NOT NULL,
    role_id integer,
    referer integer,
    api_key character varying
);


ALTER TABLE public.users OWNER TO auc_admin;

--
-- TOC entry 317 (class 1259 OID 19196)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: auc_admin
--

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO auc_admin;

--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 317
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: auc_admin
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 3030 (class 2604 OID 28107)
-- Name: Bids id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Bids" ALTER COLUMN id SET DEFAULT nextval('public."Bids_id_seq"'::regclass);


--
-- TOC entry 3015 (class 2604 OID 16611)
-- Name: CsExteriors id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsExteriors" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_Exterior_id_seq"'::regclass);


--
-- TOC entry 3010 (class 2604 OID 16556)
-- Name: CsItemSets id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsItemSets" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_ItemSet_id_seq"'::regclass);


--
-- TOC entry 3034 (class 2604 OID 28350)
-- Name: CsParams id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams" ALTER COLUMN id SET DEFAULT nextval('public."CsParams_id_seq"'::regclass);


--
-- TOC entry 3013 (class 2604 OID 16589)
-- Name: CsQualities id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsQualities" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_Quality_id_seq"'::regclass);


--
-- TOC entry 3014 (class 2604 OID 16600)
-- Name: CsRarities id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsRarities" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_Rarity_id_seq"'::regclass);


--
-- TOC entry 3011 (class 2604 OID 16567)
-- Name: CsTypes id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsTypes" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_Type_id_seq"'::regclass);


--
-- TOC entry 3012 (class 2604 OID 16578)
-- Name: CsWeapons id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsWeapons" ALTER COLUMN id SET DEFAULT nextval('public."CSGO_Weapon_id_seq"'::regclass);


--
-- TOC entry 3040 (class 2604 OID 42244)
-- Name: Deals id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Deals" ALTER COLUMN id SET DEFAULT nextval('public."Deals_id_seq"'::regclass);


--
-- TOC entry 3025 (class 2604 OID 19999)
-- Name: DotaHeros id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaHeros" ALTER COLUMN id SET DEFAULT nextval('public."DotaHeros_id_seq"'::regclass);


--
-- TOC entry 3026 (class 2604 OID 20010)
-- Name: DotaParams id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams" ALTER COLUMN id SET DEFAULT nextval('public."DotaParams_id_seq"'::regclass);


--
-- TOC entry 3019 (class 2604 OID 16667)
-- Name: DotaQualities id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaQualities" ALTER COLUMN id SET DEFAULT nextval('public."DOTA2_Quality_id_seq"'::regclass);


--
-- TOC entry 3018 (class 2604 OID 16656)
-- Name: DotaRarities id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaRarities" ALTER COLUMN id SET DEFAULT nextval('public."DOTA2_Rarity_id_seq"'::regclass);


--
-- TOC entry 3016 (class 2604 OID 16633)
-- Name: DotaSlots id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaSlots" ALTER COLUMN id SET DEFAULT nextval('public."DOTA2_Slot_id_seq"'::regclass);


--
-- TOC entry 3017 (class 2604 OID 16645)
-- Name: DotaTypes id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaTypes" ALTER COLUMN id SET DEFAULT nextval('public."DOTA2_Type_id_seq"'::regclass);


--
-- TOC entry 3022 (class 2604 OID 19962)
-- Name: FaqTabs id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."FaqTabs" ALTER COLUMN id SET DEFAULT nextval('public."FaqTabs_id_seq"'::regclass);


--
-- TOC entry 3023 (class 2604 OID 19970)
-- Name: Faqs id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Faqs" ALTER COLUMN id SET DEFAULT nextval('public."Faqs_id_seq"'::regclass);


--
-- TOC entry 3028 (class 2604 OID 28073)
-- Name: GameItems id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."GameItems" ALTER COLUMN id SET DEFAULT nextval('public."GameItems_id_seq"'::regclass);


--
-- TOC entry 3031 (class 2604 OID 28125)
-- Name: LotResult id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotResult" ALTER COLUMN id SET DEFAULT nextval('public."LotResult_id_seq"'::regclass);


--
-- TOC entry 3021 (class 2604 OID 19831)
-- Name: LotStatuses id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotStatuses" ALTER COLUMN id SET DEFAULT nextval('public."LotStatuses_id_seq"'::regclass);


--
-- TOC entry 3029 (class 2604 OID 28089)
-- Name: Lots id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Lots" ALTER COLUMN id SET DEFAULT nextval('public."Lots_id_seq"'::regclass);


--
-- TOC entry 3038 (class 2604 OID 40964)
-- Name: NotificationTypes id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."NotificationTypes" ALTER COLUMN id SET DEFAULT nextval('public."NotificationTypes_id_seq"'::regclass);


--
-- TOC entry 3039 (class 2604 OID 40993)
-- Name: Notifications id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Notifications" ALTER COLUMN id SET DEFAULT nextval('public."Notifications_id_seq"'::regclass);


--
-- TOC entry 3032 (class 2604 OID 28143)
-- Name: Participants id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Participants" ALTER COLUMN id SET DEFAULT nextval('public."Participants_id_seq"'::regclass);


--
-- TOC entry 3009 (class 2604 OID 16416)
-- Name: Privileges id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Privileges" ALTER COLUMN id SET DEFAULT nextval('public.privileges_id_seq'::regclass);


--
-- TOC entry 3008 (class 2604 OID 16408)
-- Name: Roles id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Roles" ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 3035 (class 2604 OID 40895)
-- Name: RustCategories id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustCategories" ALTER COLUMN id SET DEFAULT nextval('public."RustCategories_id_seq"'::regclass);


--
-- TOC entry 3037 (class 2604 OID 40917)
-- Name: RustParams id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustParams" ALTER COLUMN id SET DEFAULT nextval('public."RustParams_id_seq"'::regclass);


--
-- TOC entry 3036 (class 2604 OID 40906)
-- Name: RustTypes id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustTypes" ALTER COLUMN id SET DEFAULT nextval('public."RustTypes_id_seq"'::regclass);


--
-- TOC entry 3024 (class 2604 OID 19988)
-- Name: Statics id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Statics" ALTER COLUMN id SET DEFAULT nextval('public."Statics_id_seq"'::regclass);


--
-- TOC entry 3027 (class 2604 OID 20948)
-- Name: TransactionTypes id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."TransactionTypes" ALTER COLUMN id SET DEFAULT nextval('public."TransactionTypes_id_seq"'::regclass);


--
-- TOC entry 3033 (class 2604 OID 28163)
-- Name: Transactions id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Transactions" ALTER COLUMN id SET DEFAULT nextval('public."Transactions_id_seq"'::regclass);


--
-- TOC entry 3020 (class 2604 OID 19201)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 3284 (class 0 OID 16608)
-- Dependencies: 307
-- Data for Name: CsExteriors; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (1, 'WearCategory2', 'После полевых испытаний');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (2, 'WearCategory1', 'Немного поношенное');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (3, 'WearCategory4', 'Закаленное в боях');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (4, 'WearCategory3', 'Поношенное');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (5, 'WearCategory0', 'Прямо с завода');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (6, 'WearCategoryNA', 'Не покрашено');
INSERT INTO public."CsExteriors" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3274 (class 0 OID 16553)
-- Dependencies: 297
-- Data for Name: CsItemSets; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (1, 'set_community_5', 'Коллекция «Авангард»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (2, 'set_bravo_ii', 'Коллекция «Альфа»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (3, 'set_gods_and_monsters', 'Коллекция «Боги и чудовища»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (4, 'set_bravo_i', 'Коллекция «Браво»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (5, 'set_gamma_2', 'Коллекция «Гамма 2»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (6, 'set_community_13', 'Коллекция «Гамма»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (7, 'set_community_20', 'Коллекция «Горизонт»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (8, 'set_community_11', 'Коллекция «Дикое пламя»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (9, 'set_community_21', 'Коллекция «Запретная зона»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (10, 'set_community_22', 'Коллекция «Призма»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (11, 'set_community_4', 'Коллекция «Прорыв»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (12, 'set_kimono', 'Коллекция «Рассвет»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (13, 'set_community_19', 'Коллекция «Решающий момент»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (14, 'set_community_18', 'Коллекция «Спектр 2»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (15, 'set_community_16', 'Коллекция «Спектр»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (16, 'set_community_8', 'Коллекция «Фальшион»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (17, 'set_community_2', 'Коллекция «Феникс»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (18, 'set_chopshop', 'Коллекция «Чик-чик»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (19, 'set_weapons_ii', 'Коллекция «Arms Deal 2»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (20, 'set_weapons_iii', 'Коллекция «Arms Deal 3»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (21, 'set_weapons_i', 'Коллекция «Arms Deal»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (22, 'set_assault', 'Коллекция «Assault»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (23, 'set_aztec', 'Коллекция «Aztec»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (24, 'set_baggage', 'Коллекция «Baggage»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (25, 'set_bank', 'Коллекция «Bank»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (26, 'set_blacksite', 'Коллекция «Blacksite»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (27, 'set_cache', 'Коллекция «Cache»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (28, 'set_cobblestone', 'Коллекция «Cobblestone»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (29, 'set_dust_2', 'Коллекция «Dust 2»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (30, 'set_dust', 'Коллекция «Dust»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (31, 'set_esports', 'Коллекция «eSports 2013»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (32, 'set_esports_ii', 'Коллекция «eSports Winter 2013»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (33, 'set_inferno_2', 'Коллекция «Inferno 2018»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (34, 'set_inferno', 'Коллекция «Inferno»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (35, 'set_italy', 'Коллекция «Italy»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (36, 'set_lake', 'Коллекция «Lake»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (37, 'set_militia', 'Коллекция «Militia»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (38, 'set_mirage', 'Коллекция «Mirage»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (39, 'set_nuke_2', 'Коллекция «Nuke 2018»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (40, 'set_nuke', 'Коллекция «Nuke»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (41, 'set_office', 'Коллекция «Office»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (42, 'set_overpass', 'Коллекция «Overpass»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (43, 'set_safehouse', 'Коллекция «Safehouse»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (44, 'set_train', 'Коллекция «Train»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (45, 'set_vertigo', 'Коллекция «Vertigo»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (46, 'set_community_1', 'Коллекция «Winter Offensive»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (47, 'set_community_9', 'Коллекция из тёмного кейса');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (48, 'set_community_6', 'Коллекция из хромированного кейса');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (49, 'set_community_7', 'Коллекция из хромированного кейса #2');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (50, 'set_community_12', 'Коллекция из хромированного кейса #3');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (51, 'set_community_17', 'Коллекция операции «Гидра»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (52, 'set_esports_iii', 'Коллекция eSports 2014 Summer');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (53, 'set_community_3', 'Охотничья коллекция');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (54, 'set_community_15', 'Перчаточная коллекция');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (55, 'set_community_10', 'Револьверная коллекция');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (56, 'set_op9_characters', 'Агенты «Расколотой сети»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (57, 'set_community_23', 'Коллекция «Расколотая сеть»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (58, 'set_xraymachine', 'Коллекция «Рентген»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (59, 'set_canals', 'Коллекция «Canals»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (60, 'set_community_24', 'Коллекция «CS20»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (61, 'set_stmarc', 'Коллекция «St. Marc»');
INSERT INTO public."CsItemSets" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3280 (class 0 OID 16586)
-- Dependencies: 303
-- Data for Name: CsQualities; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (1, 'normal', NULL, 'Обычный');
INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (2, 'strange', '#cf6a32', 'StatTrak™');
INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (3, 'tournament', '#ffd700', 'Сувенирный');
INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (4, 'unusual', '#8650ac', '★');
INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (5, 'unusual_strange', '#8650ac', '★ StatTrak™');
INSERT INTO public."CsQualities" (id, internal_name, color, name_ru) VALUES (0, 'any', NULL, 'Любое');


--
-- TOC entry 3282 (class 0 OID 16597)
-- Dependencies: 305
-- Data for Name: CsRarities; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (1, 'Rarity_Common_Weapon', '#b0c3d9', 'Ширпотреб');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (2, 'Rarity_Rare_Weapon', '#4b69ff', 'Армейское качество');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (3, 'Rarity_Uncommon_Weapon', '#5e98d9', 'Промышленное качество');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (4, 'Rarity_Mythical_Weapon', '#8847ff', 'Запрещённое');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (5, 'Rarity_Legendary_Weapon', '#d32ce6', 'Засекреченное');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (6, 'Rarity_Ancient_Weapon', '#eb4b4b', 'Тайное');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (7, 'Rarity_Common', '#b0c3d9', 'базового класса');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (8, 'Rare_Weapon', '#4b69ff', 'высшего класса');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (9, 'Rarity_Ancient', '#eb4b4b', 'экстраординарного типа');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (10, 'Rarity_Mythical', '#8847ff', 'примечательного типа');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (11, 'Rarity_Legendary', '#d32ce6', 'экзотичного вида');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (12, 'Rarity_Contraband', '#e4ae39', 'Контрабанда');
INSERT INTO public."CsRarities" (id, internal_name, color, name_ru) VALUES (0, 'any', NULL, 'Любое');


--
-- TOC entry 3276 (class 0 OID 16564)
-- Dependencies: 299
-- Data for Name: CsTypes; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (1, 'CSGO_Type_Pistol', 'Пистолет');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (2, 'CSGO_Type_SMG', 'Пистолет-пулемёт');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (3, 'CSGO_Type_Rifle', 'Винтовка');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (4, 'CSGO_Type_Shotgun', 'Дробовик');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (5, 'CSGO_Type_SniperRifle', 'Снайперская винтовка');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (6, 'CSGO_Type_Machinegun', 'Пулемёт');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (7, 'CSGO_Type_WeaponCase', 'Контейнер');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (8, 'CSGO_Type_Knife', 'Нож');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (9, 'CSGO_Tool_Sticker', 'Наклейка');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (10, 'Type_Hands', 'Перчатки');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (11, 'CSGO_Type_Spray', 'Граффити');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (12, 'CSGO_Type_MusicKit', 'Набор музыки');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (13, 'CSGO_Type_Collectible', 'Сувенир');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (14, 'CSGO_Tool_WeaponCase_KeyTag', 'Ключ');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (15, 'CSGO_Type_Ticket', 'Пропуск');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (16, 'CSGO_Tool_GiftTag', 'Подарок');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (17, 'CSGO_Tool_Name_TagTag', 'Ярлык');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (18, 'CSGO_Type_Tool', 'Инструмент');
INSERT INTO public."CsTypes" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3278 (class 0 OID 16575)
-- Dependencies: 301
-- Data for Name: CsWeapons; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (1, 'weapon_knife_karambi', 'Керамбит');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (2, 'weapon_knife_widowmaker', 'Коготь');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (3, 'weapon_knife_ursus', 'Медвежий нож');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (4, 'weapon_knife_gypsy_jackknife', 'Наваха');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (5, 'weapon_knife_survival_bowie', 'Нож Боуи');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (6, 'weapon_knife_gut', 'Нож с лезвием-крюком');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (7, 'weapon_knife_butterfly', 'Нож-бабочка');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (8, 'weapon_knife_tactical', 'Охотничий нож');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (9, 'weapon_bizon', 'ПП-19 Бизон');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (10, 'weapon_revolver', 'Револьвер R8');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (11, 'weapon_knife_flip', 'Складной нож');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (12, 'weapon_knife_stiletto', 'Стилет');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (13, 'weapon_knife_push', 'Тычковые ножи');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (14, 'weapon_knife_falchion', 'Фальшион');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (15, 'weapon_bayonet', 'Штык-нож');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (16, 'weapon_knife_m9_bayonet', 'Штык-нож M9');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (17, 'weapon_ak47', 'AK-47');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (18, 'weapon_aug', 'AUG');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (19, 'weapon_awp', 'AWP');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (20, 'weapon_cz75a', 'CZ75-Auto');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (21, 'weapon_deagle', 'Desert Eagle');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (22, 'weapon_elite', 'Dual Berettas');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (23, 'weapon_famas', 'FAMAS');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (24, 'weapon_fiveseven', 'Five-SeveN');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (25, 'weapon_g3sg1', 'G3SG1');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (26, 'weapon_galilar', 'Galil AR');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (27, 'weapon_glock', 'Glock-18');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (28, 'weapon_m249', 'M249');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (29, 'weapon_m4a1_silencer', 'M4A1-S');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (30, 'weapon_m4a1', 'M4A4');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (31, 'weapon_mac10', 'MAC-10');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (32, 'weapon_mag7', 'MAG-7');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (33, 'weapon_mp5sd', 'MP5-SD');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (34, 'weapon_mp7', 'MP7');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (35, 'weapon_mp9', 'MP9');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (36, 'weapon_negev', 'Negev');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (37, 'weapon_nova', 'Nova');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (38, 'weapon_hkp2000', 'P2000');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (39, 'weapon_p250', 'P250');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (40, 'weapon_p90', 'P90');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (41, 'weapon_sawedoff', 'Sawed-Off');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (42, 'weapon_scar20', 'SCAR-20');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (43, 'weapon_sg556', 'SG 553');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (44, 'weapon_ssg08', 'SSG 08');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (45, 'weapon_tec9', 'Tec-9');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (46, 'weapon_ump45', 'UMP-45');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (47, 'weapon_usp_silencer', 'USP-S');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (48, 'weapon_xm1014', 'XM1014');
INSERT INTO public."CsWeapons" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3305 (class 0 OID 19996)
-- Dependencies: 328
-- Data for Name: DotaHeros; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (52, 'npc_dota_hero_mars', 'Mars', 'http://cdn.dota2.com/apps/dota2/images/heroes/mars_hphover.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (6, 'npc_dota_hero_axe', 'Axe', 'http://cdn.dota2.com/apps/dota2/images/heroes/axe_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (7, 'npc_dota_hero_bane', 'Bane', 'http://cdn.dota2.com/apps/dota2/images/heroes/bane_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (8, 'npc_dota_hero_batrider', 'Batrider', 'http://cdn.dota2.com/apps/dota2/images/heroes/batrider_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (9, 'npc_dota_hero_beastmaster', 'Beastmaster', 'http://cdn.dota2.com/apps/dota2/images/heroes/beastmaster_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (10, 'npc_dota_hero_bloodseeker', 'Bloodseeker', 'http://cdn.dota2.com/apps/dota2/images/heroes/bloodseeker_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (11, 'npc_dota_hero_bounty_hunter', 'Bounty Hunter', 'http://cdn.dota2.com/apps/dota2/images/heroes/bounty_hunter_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (12, 'npc_dota_hero_brewmaster', 'Brewmaster', 'http://cdn.dota2.com/apps/dota2/images/heroes/brewmaster_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (13, 'npc_dota_hero_bristleback', 'Bristleback', 'http://cdn.dota2.com/apps/dota2/images/heroes/bristleback_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (14, 'npc_dota_hero_broodmother', 'Broodmother', 'http://cdn.dota2.com/apps/dota2/images/heroes/broodmother_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (15, 'npc_dota_hero_centaur', 'Centaur Warrunner', 'http://cdn.dota2.com/apps/dota2/images/heroes/centaur_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (16, 'npc_dota_hero_chaos_knight', 'Chaos Knight', 'http://cdn.dota2.com/apps/dota2/images/heroes/chaos_knight_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (18, 'npc_dota_hero_clinkz', 'Clinkz', 'http://cdn.dota2.com/apps/dota2/images/heroes/clinkz_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (21, 'npc_dota_hero_dark_seer', 'Dark Seer', 'http://cdn.dota2.com/apps/dota2/images/heroes/dark_seer_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (23, 'npc_dota_hero_dazzle', 'Dazzle', 'http://cdn.dota2.com/apps/dota2/images/heroes/dazzle_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (25, 'npc_dota_hero_doom_bringer', 'Doom', 'http://cdn.dota2.com/apps/dota2/images/heroes/doom_bringer_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (27, 'npc_dota_hero_drow_ranger', 'Drow Ranger', 'http://cdn.dota2.com/apps/dota2/images/heroes/drow_ranger_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (29, 'npc_dota_hero_earthshaker', 'Earthshaker', 'http://cdn.dota2.com/apps/dota2/images/heroes/earthshaker_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (31, 'npc_dota_hero_ember_spirit', 'Ember Spirit', 'http://cdn.dota2.com/apps/dota2/images/heroes/ember_spirit_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (33, 'npc_dota_hero_enigma', 'Enigma', 'http://cdn.dota2.com/apps/dota2/images/heroes/enigma_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (35, 'npc_dota_hero_gyrocopter', 'Gyrocopter', 'http://cdn.dota2.com/apps/dota2/images/heroes/gyrocopter_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (38, 'npc_dota_hero_jakiro', 'Jakiro', 'http://cdn.dota2.com/apps/dota2/images/heroes/jakiro_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (40, 'npc_dota_hero_keeper_of_the_light', 'Keeper of the Light', 'http://cdn.dota2.com/apps/dota2/images/heroes/keeper_of_the_light_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (43, 'npc_dota_hero_leshrac', 'Leshrac', 'http://cdn.dota2.com/apps/dota2/images/heroes/leshrac_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (45, 'npc_dota_hero_life_stealer', 'Lifestealer', 'http://cdn.dota2.com/apps/dota2/images/heroes/life_stealer_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (46, 'npc_dota_hero_lina', 'Lina', 'http://cdn.dota2.com/apps/dota2/images/heroes/lina_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (49, 'npc_dota_hero_luna', 'Luna', 'http://cdn.dota2.com/apps/dota2/images/heroes/luna_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (53, 'npc_dota_hero_medusa', 'Medusa', 'http://cdn.dota2.com/apps/dota2/images/heroes/medusa_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (54, 'npc_dota_hero_meepo', 'Meepo', 'http://cdn.dota2.com/apps/dota2/images/heroes/meepo_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (58, 'npc_dota_hero_naga_siren', 'Naga Siren', 'http://cdn.dota2.com/apps/dota2/images/heroes/naga_siren_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (59, 'npc_dota_hero_furion', 'Nature''s Prophet', 'http://cdn.dota2.com/apps/dota2/images/heroes/furion_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (61, 'npc_dota_hero_night_stalker', 'Night Stalker', 'http://cdn.dota2.com/apps/dota2/images/heroes/night_stalker_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (66, 'npc_dota_hero_obsidian_destroyer', 'Outworld Devourer', 'http://cdn.dota2.com/apps/dota2/images/heroes/obsidian_destroyer_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (65, 'npc_dota_hero_oracle', 'Oracle', 'http://cdn.dota2.com/apps/dota2/images/heroes/oracle_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (68, 'npc_dota_hero_phantom_assassin', 'Phantom Assassin', 'http://cdn.dota2.com/apps/dota2/images/heroes/phantom_assassin_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (70, 'npc_dota_hero_phoenix', 'Phoenix', 'http://cdn.dota2.com/apps/dota2/images/heroes/phoenix_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (72, 'npc_dota_hero_pudge', 'Pudge', 'http://cdn.dota2.com/apps/dota2/images/heroes/pudge_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (75, 'npc_dota_hero_razor', 'Razor', 'http://cdn.dota2.com/apps/dota2/images/heroes/razor_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (76, 'npc_dota_hero_riki', 'Riki', 'http://cdn.dota2.com/apps/dota2/images/heroes/riki_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (79, 'npc_dota_hero_shadow_demon', 'Shadow Demon', 'http://cdn.dota2.com/apps/dota2/images/heroes/shadow_demon_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (81, 'npc_dota_hero_shadow_shaman', 'Shadow Shaman', 'http://cdn.dota2.com/apps/dota2/images/heroes/shadow_shaman_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (83, 'npc_dota_hero_skywrath_mage', 'Skywrath Mage', 'http://cdn.dota2.com/apps/dota2/images/heroes/skywrath_mage_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (85, 'npc_dota_hero_slark', 'Slark', 'http://cdn.dota2.com/apps/dota2/images/heroes/slark_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (88, 'npc_dota_hero_spirit_breaker', 'Spirit Breaker', 'http://cdn.dota2.com/apps/dota2/images/heroes/spirit_breaker_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (90, 'npc_dota_hero_sven', 'Sven', 'http://cdn.dota2.com/apps/dota2/images/heroes/sven_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (92, 'npc_dota_hero_templar_assassin', 'Templar Assassin', 'http://cdn.dota2.com/apps/dota2/images/heroes/templar_assassin_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (94, 'npc_dota_hero_tidehunter', 'Tidehunter', 'http://cdn.dota2.com/apps/dota2/images/heroes/tidehunter_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (97, 'npc_dota_hero_tiny', 'Tiny', 'http://cdn.dota2.com/apps/dota2/images/heroes/tiny_hphover.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (99, 'npc_dota_hero_troll_warlord', 'Troll Warlord', 'http://cdn.dota2.com/apps/dota2/images/heroes/troll_warlord_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (101, 'npc_dota_hero_abyssal_underlord', 'Underlord', 'http://cdn.dota2.com/apps/dota2/images/heroes/abyssal_underlord_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (103, 'npc_dota_hero_ursa', 'Ursa', 'http://cdn.dota2.com/apps/dota2/images/heroes/ursa_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (115, 'DOTA_OtherType', 'Другие', NULL, NULL);
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (1, 'npc_dota_hero_abaddon', 'Abaddon', 'http://cdn.dota2.com/apps/dota2/images/heroes/abaddon_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (2, 'npc_dota_hero_alchemist', 'Alchemist', 'http://cdn.dota2.com/apps/dota2/images/heroes/alchemist_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (3, 'npc_dota_hero_ancient_apparition', 'Ancient Apparition', 'http://cdn.dota2.com/apps/dota2/images/heroes/ancient_apparition_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (4, 'npc_dota_hero_antimage', 'Anti-Mage', 'http://cdn.dota2.com/apps/dota2/images/heroes/antimage_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (5, 'npc_dota_hero_arc_warden', 'Arc Warden', 'http://cdn.dota2.com/apps/dota2/images/heroes/arc_warden_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (17, 'npc_dota_hero_chen', 'Chen', 'http://cdn.dota2.com/apps/dota2/images/heroes/chen_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (19, 'npc_dota_hero_rattletrap', 'Clockwerk', 'http://cdn.dota2.com/apps/dota2/images/heroes/rattletrap_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (20, 'npc_dota_hero_crystal_maiden', 'Crystal Maiden', 'http://cdn.dota2.com/apps/dota2/images/heroes/crystal_maiden_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (22, 'npc_dota_hero_dark_willow', 'Dark Willow', 'http://cdn.dota2.com/apps/dota2/images/heroes/dark_willow_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (24, 'npc_dota_hero_death_prophet', 'Death Prophet', 'http://cdn.dota2.com/apps/dota2/images/heroes/death_prophet_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (117, 'npc_dota_hero_disruptor', 'Disruptor', 'http://cdn.dota2.com/apps/dota2/images/heroes/disruptor_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (26, 'npc_dota_hero_dragon_knight', 'Dragon Knight', 'http://cdn.dota2.com/apps/dota2/images/heroes/dragon_knight_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (0, 'any', 'Любой', NULL, NULL);
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (28, 'npc_dota_hero_earth_spirit', 'Earth Spirit', 'http://cdn.dota2.com/apps/dota2/images/heroes/earth_spirit_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (30, 'npc_dota_hero_elder_titan', 'Elder Titan', 'http://cdn.dota2.com/apps/dota2/images/heroes/elder_titan_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (32, 'npc_dota_hero_enchantress', 'Enchantress', 'http://cdn.dota2.com/apps/dota2/images/heroes/enchantress_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (34, 'npc_dota_hero_faceless_void', 'Faceless Void', 'http://cdn.dota2.com/apps/dota2/images/heroes/faceless_void_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (36, 'npc_dota_hero_huskar', 'Huskar', 'http://cdn.dota2.com/apps/dota2/images/heroes/huskar_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (37, 'npc_dota_hero_invoker', 'Invoker', 'http://cdn.dota2.com/apps/dota2/images/heroes/invoker_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (39, 'npc_dota_hero_juggernaut', 'Juggernaut', 'http://cdn.dota2.com/apps/dota2/images/heroes/juggernaut_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (41, 'npc_dota_hero_kunkka', 'Kunkka', 'http://cdn.dota2.com/apps/dota2/images/heroes/kunkka_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (42, 'npc_dota_hero_legion_commander', 'Legion Commander', 'http://cdn.dota2.com/apps/dota2/images/heroes/legion_commander_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (44, 'npc_dota_hero_lich', 'Lich', 'http://cdn.dota2.com/apps/dota2/images/heroes/lich_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (47, 'npc_dota_hero_lion', 'Lion', 'http://cdn.dota2.com/apps/dota2/images/heroes/lion_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (48, 'npc_dota_hero_lone_druid', 'Lone Druid', 'http://cdn.dota2.com/apps/dota2/images/heroes/lone_druid_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (50, 'npc_dota_hero_lycan', 'Lycan', 'http://cdn.dota2.com/apps/dota2/images/heroes/lycan_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (51, 'npc_dota_hero_magnataur', 'Magnus', 'http://cdn.dota2.com/apps/dota2/images/heroes/magnataur_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (55, 'npc_dota_hero_mirana', 'Mirana', 'http://cdn.dota2.com/apps/dota2/images/heroes/mirana_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (56, 'npc_dota_hero_monkey_king', 'Monkey King', 'http://cdn.dota2.com/apps/dota2/images/heroes/monkey_king_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (57, 'npc_dota_hero_morphling', 'Morphling', 'http://cdn.dota2.com/apps/dota2/images/heroes/morphling_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (60, 'npc_dota_hero_necrolyte', 'Necrophos', 'http://cdn.dota2.com/apps/dota2/images/heroes/necrolyte_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (62, 'npc_dota_hero_nyx_assassin', 'Nyx Assassin', 'http://cdn.dota2.com/apps/dota2/images/heroes/nyx_assassin_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (63, 'npc_dota_hero_ogre_magi', 'Ogre Magi', 'http://cdn.dota2.com/apps/dota2/images/heroes/ogre_magi_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (64, 'npc_dota_hero_omniknight', 'Omniknight', 'http://cdn.dota2.com/apps/dota2/images/heroes/omniknight_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (67, 'npc_dota_hero_pangolier', 'Pangolier', 'http://cdn.dota2.com/apps/dota2/images/heroes/pangolier_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (69, 'npc_dota_hero_phantom_lancer', 'Phantom Lancer', 'http://cdn.dota2.com/apps/dota2/images/heroes/phantom_lancer_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (71, 'npc_dota_hero_puck', 'Puck', 'http://cdn.dota2.com/apps/dota2/images/heroes/puck_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (73, 'npc_dota_hero_pugna', 'Pugna', 'http://cdn.dota2.com/apps/dota2/images/heroes/pugna_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (74, 'npc_dota_hero_queenofpain', 'Queen of Pain', 'http://cdn.dota2.com/apps/dota2/images/heroes/queenofpain_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (77, 'npc_dota_hero_rubick', 'Rubick', 'http://cdn.dota2.com/apps/dota2/images/heroes/rubick_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (78, 'npc_dota_hero_sand_king', 'Sand King', 'http://cdn.dota2.com/apps/dota2/images/heroes/sand_king_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (80, 'npc_dota_hero_nevermore', 'Shadow Fiend', 'http://cdn.dota2.com/apps/dota2/images/heroes/nevermore_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (82, 'npc_dota_hero_silencer', 'Silencer', 'http://cdn.dota2.com/apps/dota2/images/heroes/silencer_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (84, 'npc_dota_hero_slardar', 'Slardar', 'http://cdn.dota2.com/apps/dota2/images/heroes/slardar_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (86, 'npc_dota_hero_sniper', 'Sniper', 'http://cdn.dota2.com/apps/dota2/images/heroes/sniper_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (87, 'npc_dota_hero_spectre', 'Spectre', 'http://cdn.dota2.com/apps/dota2/images/heroes/spectre_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (89, 'npc_dota_hero_storm_spirit', 'Storm Spirit', 'http://cdn.dota2.com/apps/dota2/images/heroes/storm_spirit_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (91, 'npc_dota_hero_techies', 'Techies', 'http://cdn.dota2.com/apps/dota2/images/heroes/techies_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (93, 'npc_dota_hero_terrorblade', 'Terrorblade', 'http://cdn.dota2.com/apps/dota2/images/heroes/terrorblade_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (95, 'npc_dota_hero_shredder', 'Timbersaw', 'http://cdn.dota2.com/apps/dota2/images/heroes/shredder_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (96, 'npc_dota_hero_tinker', 'Tinker', 'http://cdn.dota2.com/apps/dota2/images/heroes/tinker_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (98, 'npc_dota_hero_treant', 'Treant Protector', 'http://cdn.dota2.com/apps/dota2/images/heroes/treant_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (100, 'npc_dota_hero_tusk', 'Tusk', 'http://cdn.dota2.com/apps/dota2/images/heroes/tusk_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (102, 'npc_dota_hero_undying', 'Undying', 'http://cdn.dota2.com/apps/dota2/images/heroes/undying_lg.png', 'strength');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (114, 'npc_dota_hero_zuus', 'Zeus', 'http://cdn.dota2.com/apps/dota2/images/heroes/zuus_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (104, 'npc_dota_hero_vengefulspirit', 'Vengeful Spirit', 'http://cdn.dota2.com/apps/dota2/images/heroes/vengefulspirit_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (105, 'npc_dota_hero_venomancer', 'Venomancer', 'http://cdn.dota2.com/apps/dota2/images/heroes/venomancer_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (106, 'npc_dota_hero_viper', 'Viper', 'http://cdn.dota2.com/apps/dota2/images/heroes/viper_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (107, 'npc_dota_hero_visage', 'Visage', 'http://cdn.dota2.com/apps/dota2/images/heroes/visage_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (108, 'npc_dota_hero_warlock', 'Warlock', 'http://cdn.dota2.com/apps/dota2/images/heroes/warlock_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (109, 'npc_dota_hero_weaver', 'Weaver', 'http://cdn.dota2.com/apps/dota2/images/heroes/weaver_lg.png', 'agility');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (110, 'npc_dota_hero_windrunner', 'Windranger', 'http://cdn.dota2.com/apps/dota2/images/heroes/windrunner_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (111, 'npc_dota_hero_winter_wyvern', 'Winter Wyvern', 'http://cdn.dota2.com/apps/dota2/images/heroes/winter_wyvern_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (112, 'npc_dota_hero_witch_doctor', 'Witch Doctor', 'http://cdn.dota2.com/apps/dota2/images/heroes/witch_doctor_lg.png', 'intelligence');
INSERT INTO public."DotaHeros" (id, internal_name, name, img, type) VALUES (113, 'npc_dota_hero_skeleton_king', 'Wraith King', 'http://cdn.dota2.com/apps/dota2/images/heroes/skeleton_king_lg.png', 'strength');


--
-- TOC entry 3292 (class 0 OID 16664)
-- Dependencies: 315
-- Data for Name: DotaQualities; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (1, 'unique', '#d2d2d2', 'Standard');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (2, 'strange', '#cf6a32', 'Inscribed');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (3, 'lucky', '#32cd32', 'Auspicious');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (4, 'tournament', '#8650ac', 'Heroic');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (5, 'genuine', '#4d7455', 'Genuine');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (6, 'autographed', '#ade55c', 'Autographed');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (7, 'frozen', '#4682b4', 'Frozen');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (8, 'haunted', '#8650ac', 'Cursed');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (9, 'base', '#b2b2b2', 'Base');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (10, 'corrupted', '#a52a2a', 'Corrupted');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (11, 'infused', '#8847ff', 'Infused');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (12, 'unusual', '#8650ac', 'Unusual');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (13, 'exalted', '#cccccc', 'Exalted');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (14, 'vintage', '#476291', 'Elder');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (15, 'legacy', '#ffffff', 'Legacy');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (16, 'ascendant', '#eb4b4b', 'Ascendant');
INSERT INTO public."DotaQualities" (id, internal_name, color, name) VALUES (0, 'any', NULL, 'Любое');


--
-- TOC entry 3290 (class 0 OID 16653)
-- Dependencies: 313
-- Data for Name: DotaRarities; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (1, 'Rarity_Common', '#b0c3d9', 'Common');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (2, 'Rarity_Uncommon', '#5e98d9', 'Uncommon');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (3, 'Rarity_Rare', '#4b69ff', 'Rare');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (4, 'Rarity_Mythical', '#8847ff', 'Mythical');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (5, 'Rarity_Immortal', '#e4ae39', 'Immortal');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (6, 'Rarity_Legendary', '#d32ce6', 'Legendary');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (7, 'Rarity_Arcana', '#ade55c', 'Arcana');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (8, 'Rarity_Seasonal', '#fff34f', 'Сезонный предмет');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (9, 'Rarity_Ancient', '#eb4b4b', 'Ancient');
INSERT INTO public."DotaRarities" (id, internal_name, color, name_ru) VALUES (0, 'any', NULL, 'Любое');


--
-- TOC entry 3286 (class 0 OID 16630)
-- Dependencies: 309
-- Data for Name: DotaSlots; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (1, 'ability1', '1-я способность');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (2, 'ability2', '2-я способность');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (3, 'ability3', '3-я способность');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (4, 'ability4', '4-я способность');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (5, 'armor', 'Броня');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (6, 'ward', 'Вард');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (7, 'heroic_statue', 'Геройская статуя');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (8, 'head', 'Голова');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (9, 'voice', 'Голос');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (10, 'offhand_weapon', 'Доп. оружие');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (11, 'mount', 'Животное');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (12, 'loading_screen', 'Загрузочный экран');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (13, 'announcer', 'Комментатор');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (14, 'courier', 'Курьер');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (15, 'terrain', 'Ландшафт');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (16, 'mega_kills', 'Мега-убийства');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (17, 'music', 'Музыка');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (18, 'none', 'Н/Д');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (19, 'cursor_pack', 'Набор курсоров');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (20, 'taunt', 'Насмешка');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (21, 'legs', 'Ноги');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (22, 'shapeshift', 'Облик волка');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (23, 'weapon', 'Оружие');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (24, 'gloves', 'Перчатки');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (25, 'shoulder', 'Плечи');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (26, 'belt', 'Пояс');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (27, 'action_item', 'Предмет действия');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (28, 'summon', 'Призванное существо');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (29, 'misc', 'Разное');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (30, 'arms', 'Руки');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (31, 'multikill_banner', 'Сообщения о серии убийств');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (32, 'back', 'Спина');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (33, 'hud_skin', 'Стиль интерфейса');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (34, 'body_head', 'Тело/голова');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (35, 'ability_ultimate', 'Ульт');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (36, 'tail', 'Хвост');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (37, 'neck', 'Шея');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (38, 'emblem', 'Эмблема');
INSERT INTO public."DotaSlots" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3288 (class 0 OID 16642)
-- Dependencies: 311
-- Data for Name: DotaTypes; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (1, 'league', 'Билет');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (2, 'ward', 'Вард');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (3, 'pennant', 'Вымпел');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (4, 'loading_screen', 'Загрузочный экран');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (5, 'tool', 'Инструмент');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (6, 'player_card', 'Карточка игрока');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (7, 'key', 'Ключ от сокровищницы');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (8, 'announcer', 'Комментатор');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (9, 'courier', 'Курьер');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (10, 'terrain', 'Ландшафт');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (11, 'music', 'Музыка');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (12, 'bundle', 'Набор');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (13, 'cursor_pack', 'Набор курсоров');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (14, 'taunt', 'Насмешка');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (15, 'misc', 'Разное');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (16, 'socket_gem', 'Самоцвет/руна');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (17, 'emoticon_tool', 'Смайлик');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (18, 'treasure_chest', 'Сокровищница');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (19, 'hud_skin', 'Стиль интерфейса');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (20, 'wearable', 'Украшение');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (21, 'retired_treasure_chest', 'Устаревшая сокровищница');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (22, 'emblem', 'Эмблема');
INSERT INTO public."DotaTypes" (id, internal_name, name_ru) VALUES (0, 'any', 'Любое');


--
-- TOC entry 3299 (class 0 OID 19959)
-- Dependencies: 322
-- Data for Name: FaqTabs; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."FaqTabs" (id, title, "order", language) VALUES (1, 'Популярные', 0, 'ru');


--
-- TOC entry 3301 (class 0 OID 19967)
-- Dependencies: 324
-- Data for Name: Faqs; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (2, 'Как зарегистрироваться?', '<ul><li>Зарегистрируйтесь, нажав кнопку "Войти через Steam".</li><li>Добавьте вашу торговую ссылку Steam.</li></ul><p><br></p>', 1, 0);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (5, 'Что такое лот?', '<p><span style="background-color: transparent;">Лот - это товар, который вы выставляете или хотите купить на аукционе.</span></p>', 1, 3);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (6, 'Как принять участие в лотах?', '<p>Для принятия участия выберите интересующий Вас скин. В правой стороне вы можете увидеть полную информацию по предмету. Пополните баланс, Вам должно хватить денег на участие и на выкуп предмета.</p>', 1, 4);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (12, 'У меня есть деньги на Steam аккаунте, а тут они не отображаются. Почему?', '<p>Деньги Steam не действуют на этом сайте. Пополнять аккаунт необходимо отдельно.</p>', 1, 10);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (9, 'Как вывести деньги?', '<p><span style="background-color: transparent;">Для вывода денег Вам необходимо нажать на соответствующую кнопку в верхней части сайта. Далее надо выбрать удобный для вас способ получения денег и сумму. На страницу пополнения следуйте инструкциям</span></p><p><span style="background-color: transparent;"><span class="ql-cursor">﻿</span></span></p>', 1, 7);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (4, 'Что такое аукцион?', '<p>Публичная продажа цифровых или иных товаров. Общим для всех аукционов принципом является принцип состязательности между потенциальными покупателями.</p>', 1, 2);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (13, 'Я не передал скин после продажи, что будет?', '<p><span style="background-color: rgba(0, 0, 0, 0.04);">Необходимо передать предмет как указано в правилах сервиса. В случае отказа от передачи предмета, денег за проведение аукциона Вы не получите. Участникам будут возвращены их средства. Ваш аккаунт будет заблокирован.<span class="ql-cursor">﻿</span></span></p>', 1, 11);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (10, 'Сколько денег я могу получить за свои скины?', '<p>Цену устанавливаете только вы. Мы настоятельно рекомендуем не превышать рыночную стоимость, это может привести к длительному сбору людей на Ваш лот или полному отказу от участия.</p>', 1, 8);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (8, 'Как пополнить баланс?', '<p>Для пополнения баланса Вам необходимо нажать на соответствующую кнопку в верхней части сайта. Далее надо выбрать удобный для вас способ пополнения и сумму. На страницу пополнения следуйте инструкциям.</p>', 1, 6);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (7, 'Почему есть плата за вход?', '<p>Наш сервис дает возможность не только покупать предметы по очень выгодной цене, но и позволяет продать свой предмет за рыночную стоимость. Для этого мы используем уникальную систему аукциона, с платой за вход. Плата является очень маленький, но за счет количества участников полностью формирует цену, которую хочет получить продавец.</p>', 1, 5);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (3, 'Как узнать свою торговую ссылку Steam?', '<ul><li><span style="background-color: transparent;">Нажмите на свой ник в Steam и выберите пункт “Инвентарь”;</span></li><li><span style="background-color: transparent;">Справа вверху вы увидите кнопку “Еще”, нажмите на неё и выберите пункт “Настройки приватности”:</span></li></ul><p><span style="color: rgb(86, 88, 103); background-color: transparent;"><img src="https://lh4.googleusercontent.com/crDeMKaufQqgYTeKcihUDhdh8nu-V497RFfAvOiRmbhhkJacN9KqS96OLaqbmL6yuPnCBfThtmw7yJTsgTjmiusPJZ9r5ookTgZ1KzeJ8tS06WO0_bErEWBth9AaybLcExVZ0iVm" height="176" width="495"></span></p><p><span style="color: rgb(86, 88, 103); background-color: transparent;">В последнем разделе на странице “Инвентарь” выберите пункт “Открытый”:&nbsp;</span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);"><img src="https://lh5.googleusercontent.com/Kv2AYEu7WJ9y8NNiYEitO8tgK2LPp5fP_IzWNc1fd1m9s0Jf6ZRtcCwHiNbi8notyH6lKeUvLVpqcWOCS60vYCANOQdZEfwxcJo4Eewr5orfNJcOk2fFeacPOVKLe_6lQ_5zRUmy" height="242" width="600"></span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);">Не забудьте нажать кнопку “Сохранить изменения”!</span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);">Снова нажмите на свой ник в Steam. Далее:</span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);">“Инвентарь” (как в первом пункте), но теперь вам нужно выбрать пункт “Предложения обмена” там же справа сверху:</span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);"><img src="https://lh3.googleusercontent.com/3dAxhiI_ozaBuhzk2bQ8QccEh67MoUBg4NPiuA8w6XUC-uMHoIcB9tI0GnZ006mQ76jzUlwqXa1itT5wNK9555NtIAViT9-kGp3nNFuwWem7cHE8HfssyO3CpkboebldEu_lqvm7" height="105" width="348"></span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);">Справа в списке разделов нажмите “Кто может отправлять мне предложения об обмене?” — откроется страница, в нижнем разделе которой вы найдёте нужную ссылку:</span></p><p><span style="background-color: transparent; color: rgb(86, 88, 103);"><img src="https://lh4.googleusercontent.com/EFEk7AWLmqZAH5xbsZICaIH8ZcObrWn_h5_EgJ8_pmysM2OH50YE9u59rEHZP5kNo_wsk3yg6pF4PddewOrigYwzty6_F4XOvwI9PNRZwB6LedRyO3S_Lqq-D1Vi_SJiT6d-4f3_" height="228" width="600"></span></p>', 1, 1);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (14, 'Мне не отдали скин после аукциона, что делать?', '<p><span style="background-color: rgba(0, 0, 0, 0.04);">Если вам не передали предмет после совершения аукциона, напишите об этом на соответствующей странице. Приложите скриншоты. Не переживайте, в случае обмана со стороны продавца, все деньги будут возвращены, а продавец получит бан аккаунта.</span></p><p><span style="background-color: rgba(0, 0, 0, 0.04);"><span class="ql-cursor">﻿</span></span></p>', 1, 12);
INSERT INTO public."Faqs" (id, title, text, tab, "order") VALUES (11, 'Не отображаются все предметы в инвентаре и Ошибка загрузки данных?', '<p>Заходим в стим инвентарь, находим предмет, который сайт "не видит" и читаем всю информацию о нем. Там будет написано, можно ли обменивать и продавать предмет или нет. Если предмет был куплен на ТП стима, то его нельзя продавать в течении 7 дней, учтите, что иногда это время затягивается на несколько часов, так что не переживайте. Предмет, который нельзя обменять, соответственно, продать у нас нельзя, поэтому они не отображаются.</p>', 1, 9);


--
-- TOC entry 3293 (class 0 OID 17212)
-- Dependencies: 316
-- Data for Name: Games; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Games" (id, name, game_filter) VALUES (730, 'CS:GO', 'CsParams');
INSERT INTO public."Games" (id, name, game_filter) VALUES (570, 'Dota2', 'DotaParams');
INSERT INTO public."Games" (id, name, game_filter) VALUES (252490, 'Rust', 'RustParams');


--
-- TOC entry 3271 (class 0 OID 16422)
-- Dependencies: 294
-- Data for Name: Grants; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 1);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 2);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 3);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 4);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 5);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 7);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 8);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (4, 2);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 9);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 10);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 2);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (2, 6);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 11);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (3, 11);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 5);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 7);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 8);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 9);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 10);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 13);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 14);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 15);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 16);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 17);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 18);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 19);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 20);
INSERT INTO public."Grants" (role_id, privilege_id) VALUES (1, 21);


--
-- TOC entry 3297 (class 0 OID 19828)
-- Dependencies: 320
-- Data for Name: LotStatuses; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (1, 'open
', 'Открытый, сбор средств');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (2, 'active', 'Активный, ожидается проведение');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (3, 'active', 'Активный, проводится');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (4, 'close', 'Завершен, ожидается обмен');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (5, 'close', 'Завершен, обмен не произошел');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (6, 'close', 'Завершен, обмен произошел');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (7, 'cancel', 'Отменен');
INSERT INTO public."LotStatuses" (id, status, status_description) VALUES (8, 'close', 'Завершен, предмет находится у бота');


--
-- TOC entry 3331 (class 0 OID 40961)
-- Dependencies: 354
-- Data for Name: NotificationTypes; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."NotificationTypes" (id, description) VALUES (1, 'Лот завершен успешно');
INSERT INTO public."NotificationTypes" (id, description) VALUES (2, 'Лот отменен');
INSERT INTO public."NotificationTypes" (id, description) VALUES (3, 'Успешное пополнение');
INSERT INTO public."NotificationTypes" (id, description) VALUES (4, 'Неуспешное пополнение');
INSERT INTO public."NotificationTypes" (id, description) VALUES (5, 'Успешный вывод');
INSERT INTO public."NotificationTypes" (id, description) VALUES (6, 'Неуспешный вывод');
INSERT INTO public."NotificationTypes" (id, description) VALUES (7, 'Лот продан');
INSERT INTO public."NotificationTypes" (id, description) VALUES (8, 'Успешный обмен');
INSERT INTO public."NotificationTypes" (id, description) VALUES (9, 'Неуспешный обмен');


--
-- TOC entry 3270 (class 0 OID 16413)
-- Dependencies: 293
-- Data for Name: Privileges; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (5, 'main', 'read:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (1, 'bots', 'create:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (2, 'bots
', 'read:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (3, 'bots', 'update:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (4, 'bots', 'delete:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (7, 'lots
', 'read:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (8, 'lots', 'read:own', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (9, 'lots
', 'update:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (10, 'users', 'read:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (6, 'lots', 'create:own', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (11, 'lots', 'create:any', '*
');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (12, 'bots', 'create:own', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (13, 'faq', 'create:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (14, 'faq', 'update:any', '*
');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (15, 'faq', 'delete:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (16, 'faqTab', 'create:any', '*
');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (17, 'faqTab', 'update:any
', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (18, 'faqTab', 'delete:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (19, 'static', 'create:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (20, 'static', 'update:any', '*');
INSERT INTO public."Privileges" (id, resource, action, attribute) VALUES (21, 'static', 'delete:any', '*');


--
-- TOC entry 3268 (class 0 OID 16405)
-- Dependencies: 291
-- Data for Name: Roles; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Roles" (id, type) VALUES (1, 'Admin');
INSERT INTO public."Roles" (id, type) VALUES (2, 'User');
INSERT INTO public."Roles" (id, type) VALUES (3, 'Bot');
INSERT INTO public."Roles" (id, type) VALUES (4, 'Viewer');


--
-- TOC entry 3325 (class 0 OID 40892)
-- Dependencies: 348
-- Data for Name: RustCategories; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."RustCategories" (id, internal_name, name_ru) VALUES (1, 'steamcat.weapon', 'Оружие');
INSERT INTO public."RustCategories" (id, internal_name, name_ru) VALUES (2, 'steamcat.misc', 'Разное');
INSERT INTO public."RustCategories" (id, internal_name, name_ru) VALUES (3, 'steamcat.clothing', 'Одежда');
INSERT INTO public."RustCategories" (id, internal_name, name_ru) VALUES (4, 'steamcat.armor', 'Броня');
INSERT INTO public."RustCategories" (id, internal_name, name_ru) VALUES (5, 'steamcat.resource', 'Ресурс');


--
-- TOC entry 3329 (class 0 OID 40914)
-- Dependencies: 352
-- Data for Name: RustParams; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."RustParams" (id, asset_id, category_id, type_id) VALUES (3, '266071715255649554', 3, 5);


--
-- TOC entry 3327 (class 0 OID 40903)
-- Dependencies: 350
-- Data for Name: RustTypes; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (0, 'any', 'Любые');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (1, 'rifle.ak', 'AK47u');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (2, 'door.hinged.toptier', 'Armored Metal Door');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (3, 'mask.balaclava', 'Balaclava');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (4, 'mask.bandana', 'Bandana');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (5, 'hat.beenie', 'Beenie');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (6, 'rifle.bolt', 'Bolt Rifle');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (7, 'bone.club', 'Bone Club');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (8, 'knife.bone', 'Bone Knife');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (9, 'hat.boonie', 'Boonie');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (10, 'shoes.boots', 'Boots');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (11, 'attire.hide.boots', 'Boots');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (12, 'bucket.helmet', 'Bucket Helmet');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (13, 'burlap.gloves', 'Burlap Gloves');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (14, 'burlap.headwrap', 'Burlap Headwrap');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (15, 'burlap.shirt', 'Burlap Shirt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (16, 'burlap.shoes', 'Burlap Shoes');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (17, 'burlap.trousers', 'Burlap Trousers');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (18, 'hat.cap', 'Cap');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (19, 'coffeecan.helmet', 'Coffeecan Helmet');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (20, 'shirt.collared', 'Collared Shirt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (21, 'barricade.concrete', 'Concrete Barricade');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (22, 'crossbow', 'Crossbow');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (23, 'deer.skull.mask', 'Deer Skull Mask');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (24, 'shotgun.double', 'Double Barrel Shotgun');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (25, 'grenade.f1', 'Grenade');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (26, 'fun.guitar', 'Guitar');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (27, 'hammer', 'Hammer');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (28, 'hatchet', 'Hatchet');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (29, 'attire.hide.helterneck', 'Hide Halterneck');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (30, 'attire.hide.pants', 'Hide Pants');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (31, 'attire.hide.poncho', 'Hide Poncho');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (32, 'attire.hide.skirt', 'Hide Skirt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (33, 'hoodie', 'Hoodie');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (34, 'jacket', 'Jacket');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (35, 'box.wooden.large', 'Large Wooden Box');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (36, 'tshirt.long', 'Long TShirt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (37, 'longsword', 'Longsword');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (38, 'metal.facemask', 'Metal Facemask');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (39, 'metal.plate.torso', 'Metal Torso Plate');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (40, 'hat.miner', 'Miner''s Hat');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (41, 'smg.mp5', 'Mp5');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (42, 'pants', 'Pants');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (43, 'shotgun.pump', 'Pump Shotgun');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (44, 'target.reactive', 'Reactive Sign');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (45, 'pistol.revolver', 'Revolver');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (46, 'riot.helmet', 'Rifle Helmet');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (47, 'roadsign.jacket', 'Roadsign Jacket');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (48, 'roadsign.kilt', 'Roadsign Kilt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (49, 'rock', 'Rock');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (50, 'rocket.launcher', 'Rocket Launcher');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (51, 'icepick.salvaged', 'Salvaged Icepick');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (52, 'salvaged.sword', 'Salvaged Sword');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (53, 'barricade.sandbags', 'Sandbag Barricade');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (54, 'explosive.satchel', 'Satchel Explosives');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (55, 'pistol.semiauto', 'Semi Auto Pistol');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (56, 'rifle.semiauto', 'Semi Auto Rifle');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (57, 'door.hinged.metal', 'Sheet Metal Door');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (58, 'pants.shorts', 'Shorts');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (59, 'sleepingbag', 'Sleeping Bag');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (60, 'smg.2', 'SMG');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (61, 'jacket.snow', 'Snow Jacket');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (62, 'stonehatchet', 'Stone Hatchet');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (63, 'stone.pickaxe', 'Stone Pickaxe');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (64, 'shirt.tanktop', 'Tank Top');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (65, 'smg.thompson', 'Thompson');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (66, 'tshirt', 'TShirt');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (67, 'shotgun.waterpipe', 'Waterpipe Shotgun');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (68, 'box.wooden', 'Wooden Box');
INSERT INTO public."RustTypes" (id, internal_name, name_ru) VALUES (69, 'door.hinged.wood', 'Wooden Door');


--
-- TOC entry 3303 (class 0 OID 19985)
-- Dependencies: 326
-- Data for Name: Statics; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."Statics" (id, title, html, page, language) VALUES (1, 'test tit;e', '<h1><span style="color: rgb(230, 0, 0); background-color: rgb(255, 255, 0);">FUCK YOU</span></h1>', 'none', 'ru');
INSERT INTO public."Statics" (id, title, html, page, language) VALUES (2, 'shitty title', '<h1>Что такое Lorem Ipsum?</h1><h1 class="ql-align-justify"><strong>Lorem Ipsum</strong>&nbsp;- это текст-"рыба", часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной "рыбой" для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.</h1><h1><br></h1>', 'shit', 'ru');
INSERT INTO public."Statics" (id, title, html, page, language) VALUES (3, 'Политика конфиденциальности', '<h2>Сервис</h2><p>Данная Политика конфиденциальности распространяется на сайт auction-skin.com и предоставляемый Сервис по продаже, покупке или обмену игровыми предметами (далее, Сервис), включая маркетинговые мероприятия на сторонних сайтах.</p><p>Компания занимается обработкой ваших персональных данных, предоставленных и собранных посредством или в процессе предоставления Сервиса. При этом вы принимаете Условия использования Сервиса auction-skin.com.</p><h2>Обрабатываемые данные (включая категории данных)</h2><h3>Данные получаемые auction-skin.com от пользователей Сервиса</h3><p>Для получения доступа к некоторым функциям Сервиса вам будет предложено добавить фамилию и имя, данные и сканированную копию документа удостоверяющего личность (паспорт, ID, водительское удостоверение), адрес местожительства, данные платежной карты, номер телефона и e-mail. Вы не обязаны указывать в профиле дополнительную информацию. Эта информация не будет общедоступной.</p><h3>Данные получаемые компанией от сторонних организаций</h3><p>Для использования всех функций Сервиса auction-skin.com вам необходимо пройти авторизацию через Steam. Во время первой авторизации от Valve Corporation мы получаем ваши Steam ID, аватар и Profile Name. Steam ID используется для создания Аккаунта на сайте auction-skin.com и предоставления пользователю информации о имеющихся у него игровых предметов в Steam.</p><p>В процессе создания Аккаунта на сайте auction-skin.com вам будет присвоен Seller ID - уникальный цифровой номер сайта auction-skin.com.</p><h3>Транзакции и платежные данные</h3><p>Через Сервис невозможно совершать платежи для пополнения виртуального счета. Однако, это можно сделать через другие платежные системы, которые будут предложены в зависимости от вашего местонахождения. Компания может передавать выбранному вами оператору платежной системы контактную информацию, чтобы обеспечить возможность проведения транзакции и выполнения проверок для борьбы с мошенничеством. Точно так же компания будет получать данные от вашего оператора платежной системы по тем же причинам.</p><p>Также мы собираем, храним и обрабатываем информацию о наличии и обменах ваших игровых предметов, о ваших предпочтениях в выборе игровых предметов.</p><h3>Файлы cookie</h3><p>Как описано в Политике использования файлов cookie, компания использует файлы cookie и аналогичные технологии (например, веб-маяки, рекламные теги и идентификаторы устройств), чтобы распознать вас иваше устройство в рамках и вне пределов Сервиса. Кроме того, компания разрешает некоторым другим лицам использовать файлы cookie, как описано в Политике использования файлов cookie. Вы можете управлять файлами cookie в настройках браузера и с помощью других средств. Кроме того, вы можете отказаться от использования файлов cookie и аналогичных технологий с целью отслеживания вашей активности на сторонних веб-сайтах для сторонних рекламодателей.</p><h3>Информация о вашем устройстве и местоположении</h3><p>Когда вы посещаете наш сайт компания получает адрес сайта, с которого вы вошли. Кроме того, компания получает информацию о вашем IP-адресе, прокси-сервере, веб-браузере, выбранной цветовой настройке сайта, разрешении экрана, версия операционной системы и другие данные, позволяющие идентифицировать вас и ваше устройство.</p><h3>Сообщения</h3><p>Отправленная вами информация в виде сообщений или комментариев на форуме на нашем сайте или в чате с техподдержкой. Размещая информацию на форуме, вы делаете ее общедоступной. Информация, переданная в чате с техподдержкой, является конфиденциальной и обеспечивается ее защита.</p><h3>Google Analytics</h3><p>Наш сайт использует сервис веб-аналитики компании Google, Inc. («Google») Google Analytics. Google Analytics преимущественно использует собственные файлы cookie, чтобы отслеживать ваше поведение. Вы можете полностью отключить файлы cookie или удалить отдельные файлы cookie. Более подробную информацию вы можете посмотреть здесь https://policies.google.com/technologies/cookies. Чтобы полностью отключить возможность сбора данных о посещенных сайтах Google разработал Google Analytics Opt-out Browser Add-on https://tools.google.com/dlpage/gaoptout?hl=en.</p><h2>Цели обработки</h2><h3>Сервис</h3><p>Компания использует ваши данные для предоставления доступа к Сервису по покупке, продаже, обмену игровых предметов. В зависимости от вашего местонахождения мы предоставляем список платежных систем, работающих в вашем регионе. Также мы предоставляем вам возможность вывести свои деньги с личного счета Сервиса через платежную систему. Используя файлы cookie, мы сохраняем выбранное вами оформление сайта, выбранный язык и другие настройки для комфортного использования Сервиса.</p><h3>Маркетинг</h3><p>Компания показывает рекламу посетителям сайта как в рамках Сервиса так и за его пределами напрямую или через партнеров, используя следующие данные:</p><ul><li>данные, полученные от маркетинговых технологий, в том числе рекламные теги, файлы cookie и идентификаторы устройств;</li><li>контактные данные, предоставленные пользователями Сервиса;</li><li>данные, полученные в результате использования Сервиса (история поиска, посещенные разделы сайта, просмотренные товары, выбор цветовой схемы оформления сайта и т.д.);</li><li>данные, сформированные на основе полученных выше данных.</li></ul><h3>Служба поддержки</h3><p>Компания использует ваши данные для ответа на вопросы и претензии по работе Сервиса для рассмотрения и принятия решений.</p><h3>Безопасность</h3><p>Компания использует ваши данные для обеспечения безопасности и расследования предполагаемых или реальных мошеннических действий, либо других противоправных действий.</p><h3>Статистика</h3><p>Компания использует ваши данные для получения и распространения агрегированной статистики, которая не позволяет вас идентифицировать. Например, для расчета количества показов рекламы и перехода по ней, посещения и использования Сервиса в различных регионах и странах.</p><h2>Сроки обработки</h2><p>Если вы используете право на возражение против обработки ваших персональных данных, мы рассмотрим ваше возражение и удалим данные, обрабатываемые с целью, против которой вы возражаете, если нет иной правовой основы для обработки и хранения этих данных или сохранение этих данных не требуется в соответствии с действующим законодательством.</p><p>Если вы отзываете свое согласие на обработку своих персональных данных или данных вашего ребенка, то мы удалим данные в том объеме, в котором сбор и обработка данных основана на согласии.</p><h2>Права пользователей</h2><p>Законы о защите данных, действующие на различных территориях, предоставляют права в отношении ваших персональных данных.</p><p>Для реализации своих прав вы можете обратиться к нам используя контактную информацию, указанную на сайте, либо через личный кабинет.</p><h3>Право на доступ к информации</h3><p>Вы можете обратиться к нам с просьбой предоставить копию ваших персональных данных, отправив запрос любым доступным способом.</p><h3>Право на исправление</h3><p>Вы можете изменять некоторые свои персональные данные в личном кабинете, либо обратиться к нам с просьбой изменить или исправить ваши данные, если они являются неточными.</p><h3>Право на удаление («забвение»)</h3><p>Вы можете обратиться к нам с просьбой удалить все или некоторые ваши персональные данные, например, если:</p><ul><li>они больше не нужны для целей, для которых они изначально обрабатывались;</li><li>использовалось согласие в качестве законного основания для их обработки;</li><li>отсутствуют легитимные интересы для дальнейшей обработки персональных данных;</li><li>они использовались для целей маркетинга.</li></ul><h3>Право на ограничение обработки</h3><p>Вы можете обратиться к нам с просьбой приостановить обработку всех или некоторых ваших персональных данных, например, если ваши персональные данные являются неточными или вы считаете, что они обрабатываются незаконно.</p><h3>Право на переносимость данных</h3><p>Вы можете обратиться к нам с просьбой предоставить вам копию персональных данных, предоставленных вами ранее, в машиночитаемом формате (например, xml, csv).</p><h3>Право на возражение</h3><p>Вы можете обратиться к нам с просьбой о прекращении использования ваших персональных данных в целях прямого маркетинга.</p><h2>Дети</h2><p>Так как основную информацию, необходимую для работы нашего Сервиса, мы получаем от Valve Corporation, мы придерживаемся аналогичной политики в отношении персональных данных детей (раздел 7 https://store.steampowered.com/privacy_agreement). Мы призываем родителей объяснить своим детям, когда можно, и когда нельзя открывать свои персональные данные в сети Интернет.</p><h2>Трансграничная передача данных</h2><p>Компания обрабатывает персональные данные как на территории Европейского союза, так и за их пределами, в соответствии с предусмотренными законом механизмами трансграничной передачи данных. Законодательство стран, на территории которых обрабатываются данные, может отличаться от законодательства стран Европейского союза и обеспечивать меньшую эффективность защиты данных.</p><p>В некоторых случаях мы используем Standard contractual clauses (SCC), принятые Европейской Комиссией, в качестве механизма для передачи данных из стран Европейского союза. Организации, занимающиеся обработкой персональных данных по поручению DTS, приняли соответствующим образом SCC, с целью обеспечения конфиденциальности, безопасности и законности обработки ваших персональных данных.</p><p>Персональные данные пользователей Сервиса в настоящее время хранятся на территории Европейского союза.</p><h2>Законные основания обработки.</h2><p>Компания обрабатывает персональные данные при наличии таких законных оснований:</p><ul><li>согласие (которое мы получили от вас);</li><li>договор (договорные отношения с вами для предоставления доступа ко всем функциям нашего Сервиса, или отношения предшествующие заключению договора);</li><li>легитимные интересы;</li><li>юридические обязательства, возложенные на нас по выполнению действующего законодательства.</li></ul><p>Обработка персональных данных в соответствии с легитимными интересами осуществляется для следующих целей:</p><ul><li>защитить вас и себя от угроз нарушения конфиденциальности, безопасности и мошеннических действий со стороны третьих лиц;</li><li>обеспечить выполнение требований законодательства;</li><li>анализировать и улучшать качество Сервиса для вас;</li><li>обеспечить условия для ведения нашего бизнеса.</li></ul><p>В случае, когда обработка персональных данных осуществляется в соответствии с вашим согласием, вы имеете право отозвать его в любой момент.</p><p>В случае, когда обработка персональных данных осуществляется в соответствии с легитимными интересами, вы имеете право опротестовать эти интересы.</p><p>Если у вас возникли вопросы относительно законных оснований для обработки ваших персональных данных, вы можете обратиться к Data Protection Officer по контактным данным указанным ниже.</p>', 'privacy', 'ru');
INSERT INTO public."Statics" (id, title, html, page, language) VALUES (4, 'Пользовательское соглашение', '<h3>ОБЩАЯ ИНФОРМАЦИЯ</h3><p>Данное Соглашение об условиях использования сайта Пользователем <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> (далее — «Пользователь») является юридическим документом, устанавливающим Ваши права и обязанности в качестве Пользователя сервиса auction-skin.com. Пожалуйста, внимательно прочитайте эти условия перед использованием данного сайта. Продолжая использовать сайт, Вы подтверждаете, что условия приемлемы для Вас, и Вы соглашаетесь с ними. Если Вы не согласны со всеми условиями, изложенными здесь, пожалуйста, не используйте данный сайт. <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> предлагает поиск и обмен виртуальных предметов, являющихся собственностью Valve Corporation, между пользователями Steam, зарегистрированными в <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com </span>этом сервисе в соответствии с условиями пользовательского соглашения, предложенными <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>. Пользователи могут выбирать только виртуальные предметы, поддерживаемые Steam. Обмен виртуальных предметов в Steam осуществляется в соответствии с условиями использования торговой площадки Steam. <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> никоим образом не связан с Valve и ее филиалами. При входе на <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, пользователи Steam, имеющие виртуальные предметы, соглашаются с тем, что <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> выступает в качестве агента по отношению к ним и другим пользователям Steam, которые также используют услуги <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, принимают условия установленной на сайте комиссии, и предполагают возможность совершения обмена и выбора предметов пользователей.</p><p><span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> не имеет никакого отношения к азартным играм. Сайт не содержит никаких розыгрышей, лотерей и виртуальных казино. Деньги на вашем балансе на сайте не могут быть использованы для внесения ставок и не могут быть перемещены на другой сайт.</p><h3>ИСПОЛЬЗОВАНИЕ САЙТА&nbsp;</h3><p>Чтобы использовать <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, вам должно быть не менее 18 лет. Вы подтверждаете, что вам больше 18 лет. Если Вам не исполнилось 18 лет, пожалуйста, не пользуйтесь услугами <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span></p><p>Вы можете попытаться улучшить веб-сайт, разработав и используя специальные решения или ботов для автоматизации работы веб-сайта, однако вы не можете вмешиваться или компрометировать внутренние алгоритмы работы веб-сайта.</p><h3>ОБМЕНЫ</h3><p>Все сделки, совершаемые с помощью наших ботов, не зависят друг от друга. Дополнительные деньги от одной сделки будут сохранены на вашем балансе и могут быть использованы позже. Тем не менее, нет никакой гарантии, что будут какие-либо доступные предметы по этой цене.</p><h3>БЕЗОПАСНОСТЬ</h3><p>Пользователь самостоятельно проверяет обмены. Пользователь несет полную ответственность за свою учетную запись и пароль, а также за сохранение конфиденциальности пароля. Пользователь также несет исключительную ответственность за ограничение доступа к своей учетной записи, которую он использует на этом веб-сайте.</p><p>Члены команды <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> никогда не отправляют пустые обмены, запрашивая их подтверждение. Не добавляйте сотрудников сайта в Steam для технических консультаций.</p><h3>ПРАВА <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span></h3><p>Мы сохраняем за собой право отказаться от любого предмета.</p><p>Мы сохраняем за собой право отказать в обслуживании по любой причине.</p><p>Мы не осуществляем возврат средств по любой причине. Все обмены являются окончательными.</p><h3>ИЗМЕНЕНИЕ УСЛОВИЙ ИСПОЛЬЗОВАНИЯ</h3><p>Сайт <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> может время от времени вносить изменения в настоящие условия. Измененные условия использования данного сайта вступают в силу с момента их публикации на этом сайте. Просматривайте эту страницу на регулярной основе, чтобы оставаться в курсе текущей версии.</p><p>Настоящие Условия обслуживания могут быть изменены только со стороны <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>.</p><h3>ПЕРЕДАЧА ПРАВ И ОБЯЗАТЕЛЬСТВ</h3><p>Администратор сайта может переуступать, передавать в субподряд или иным образом передавать свои права и / или обязательства по настоящему Соглашению, не уведомив вас об этом или не получив вашего согласия. Вы, однако, не можете переуступать, передавать в субподряд или иным образом передавать свои права и/или обязательства по настоящему Соглашению.</p><h3>ВОЗВРАТ, ВОЗМЕЩЕНИЯ, ПОЛИТИКА ОТМЕНЫ СДЕЛОК</h3><p>Все сделки являются окончательными и не могут быть отменены.</p><h3>ИНФОРМАЦИЯ О ПРОФИЛЕ ПОЛЬЗОВАТЕЛЯ</h3><p>Когда Вы заходите на сайт <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> через Steam, мы собираем информацию о Вашем общедоступном профиле Steam в нашей базе данных. Мы не можем видеть имя пользователя, адрес электронной почты или пароль Вашей учетной записи Steam. Мы будем хранить обезличенные журналы использования сайта, которые будут использоваться для предотвращения злоупотреблений на нашем веб-сайте и будут автоматически удалены через 1 месяц.</p><h3>АВТОРИЗАЦИЯ ЧЕРЕЗ STEAM</h3><p>Когда Вы входите на сайт через Steam, нам не предоставляются Ваши учетные данные для входа, вместо этого нам присваивается ваш Steam64ID, который является уникальным значением для вашего аккаунта Steam.</p><h3>СОБСТВЕННОСТЬ И ЮРИДИЧЕСКОЕ СООТВЕТСТВИЕ</h3><p>Пользователь заявляет и гарантирует, что он или она владеет любыми Предметами, перечисленными пользователем для обмена на Сайте, и соблюдает все применимые законы в отношении обмена таких Предметов.</p><h3>ТРЕТЬИ СТОРОНЫ</h3><p>Ваше имя, SteamID, Профиль Steam, URL Сделки не будут переданы третьим лицам; Все данные о ваших сделках будут храниться анонимно и в безопасности.</p><h3>ОТКАЗ ОТ КОЛЛЕКТИВНЫХ ИСКОВ</h3><p>Пользователь соглашается решать все споры и претензии в порядке индивидуального арбитража, имеющего обязательную силу для сторон. Сюда относятся, среди прочего, любые претензии, возникающие в связи с или связанные с: (а) любыми аспектами взаимодействия, (б) настоящим соглашением или (в) использованием аккаунта, устройств или контента и услуг. Также согласие Пользователя распространяется на все претензии, поданные от имени третьей стороны.</p><h3>АРБИТРАЖ</h3><p>Любые претензии или споры между пользователем и <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, возникающие полностью или частично из-за использования вами Веб-сайта или Сервисов, разрешаются исключительно арбитром, расположенным в г. Челябинск, Россия.</p><h3>ЦЕЛОСТНОСТЬ</h3><p>Настоящие Условия предоставления услуг представляют собой полное и окончательное выражение полного и единственного понимания между Вами и <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> в отношении предмета настоящего соглашения и заменяют любые предыдущие письменные или устные заявления.</p><h3>ОТСУТСТВИЕ ОТКАЗА ОТ ПРАВА ТРЕБОВАТЬ ИСПОЛНЕНИЯ УСЛОВИЙ</h3><p>Отказ <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> от претензии по поводу любого нарушения Вами условий настоящего Соглашения не будет отказом от любого другого предшествующего или последующего нарушения.</p><h3>ДЕЛИМОСТЬ УСЛОВИЙ</h3><p>Если какое-либо положение настоящих Условий обслуживания считается недействительным или не имеющим законной силы в соответствии с каким-либо актом, постановлением, или решением компетентной юрисдикции, то такое положение считается измененным или исключенным, но только в той степени, которая необходима для его соблюдения c таким постановлением, или решением суда, при этом, остальные положения остаются в полной силе и действии.</p><h3>ГАРАНТИЯ И ВОЗМЕЩЕНИЕ УЩЕРБА</h3><p>Пользователь обязуется выступать в защиту, освобождать от ответственности и ограждать от возможного ущерба <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, его лицензиаров, лицензиатов, дистрибьюторов, агентов, представителей и других авторизованных пользователей, а также всех соответствующих должностных лиц, директоров, владельцев, сотрудников, агентов вышеупомянутых организаций, представителей компании от и против любых возможных претензий, убытков, обязательств, издержек, гонораров адвокатов и расходов, связанных с (i) использованием вами Веб-сайта или Сервисов, (ii) нарушением Вами настоящих Условий обслуживания, (iii) продажей, покупкой или использованием любых Предметов, (iv) нарушением Вами каких-либо прав третьих лиц, включая, помимо прочего, авторское право, товарный знак или право на неприкосновенность частной жизни и (v) любые Ваши сообщения, которые наносят ущерб третьей стороне.</p><p>Пользователь обязуется сотрудничать настолько полно, насколько это требуется в разумных пределах для защиты любой компании от любой претензии. <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> оставляет за собой право на защиту и контроль всех вопросов, связанных с любыми возмещениями Пользователям. Пользователи не должны заключать какие-либо мировые соглашения, затрагивающие права <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> без предварительного письменного согласия <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>.</p><h3>ОГРАНИЧЕНИЕ ОТВЕТСТВЕННОСТИ</h3><p>В МАКСИМАЛЬНО ДОПУСТИМОЙ СООТВЕТСТВУЮЩИМ ЗАКОНОМ СТЕПЕНИ НИ <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, НИ ИХ АФФИЛИРОВАННЫЕ ЛИЦА, НИ ПОСТАВЩИКИ УСЛУГ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ЗА УБЫТКИ, СТАВШИЕ СЛЕДСТВИЕМ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ СЕРВИСА <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span>, ВАШЕГО АККАУНТА, ВАШИХ ПОДПИСОК И УСЛУГ, В ТОМ ЧИСЛЕ ПОТЕРЮ НЕМАТЕРИАЛЬНЫХ АКТИВОВ, ОСТАНОВКУ РАБОЧЕГО ПРОЦЕССА, ВЫХОД ИЗ СТРОЯ ИЛИ НЕИСПРАВНОЕ ФУНКЦИОНИРОВАНИЕ КОМПЬЮТЕРОВ, А ТАКЖЕ ЛЮБЫЕ ДРУГИЕ КОММЕРЧЕСКИЕ УБЫТКИ. НИ К КАКОМ СЛУЧАЕ <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com </span>НЕ НЕСЕТ ОТВЕТСТВЕННОСТИ ЗА ПРЯМЫЕ, КОСВЕННЫЕ, ВТОРОСТЕПЕННЫЕ, ОСОБЕННЫЕ, ШТРАФНЫЕ И ИНЫЕ УБЫТКИ, СТАВШИЕ СЛЕДСТВИЕМ ПОЛЬЗОВАНИЯ СЛУЖБОЙ STEAM, ИСПОЛЬЗОВАНИЯ КОНТЕНТА И УСЛУГ, ПОДПИСОК И ЛЮБОЙ ИНФОРМАЦИИ, СТАВШЕЙ ДОСТУПНОЙ В РЕЗУЛЬТАТЕ ПОЛЬЗОВАНИЯ УСЛУГАМИ СЛУЖБЫ STEAM, А ТАКЖЕ ЗА ЗАДЕРЖКУ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТЬ ИСПОЛЬЗОВАНИЯ КОНТЕНТА И УСЛУГ, ПОДПИСОК ИЛИ ЛЮБОЙ ИНФОРМАЦИИ ДАЖЕ В ТОМ СЛУЧАЕ, ЕСЛИ ЭТО СТАЛО СЛЕДСТВИЕМ ВИНОВНОГО ДЕЯНИЯ, ДЕЛИКТА (В ТОМ ЧИСЛЕ ГРУБОЙ НЕОСТОРОЖНОСТИ), ДЕЯНИЙ, ВЛЕКУЩИХ ПРЯМУЮ ОТВЕТСТВЕННОСТЬ, НАРУШЕНИЙ ПОЛОЖЕНИЙ СОГЛАШЕНИЯ ИЛИ ГАРАНТИЙНЫХ ОБЯЗАТЕЛЬСТВ <span style="background-color: rgba(0, 0, 0, 0.04);">auction-skin.com</span> ИЛИ ЕЕ АФФИЛИРОВАННЫХ ЛИЦ. ДАННЫЕ ОГРАНИЧЕНИЯ И ИСКЛЮЧЕНИЯ ОТВЕТСТВЕННОСТИ ОСТАЮТСЯ В СИЛЕ ДАЖЕ В ТОМ СЛУЧАЕ, КОГДА ИНЫЕ СРЕДСТВА ПРАВОВОЙ ЗАЩИТЫ НЕ ДЕЙСТВУЮТ.</p><p>В той степени, в какой любая юрисдикция не допускает исключения или ограничения побочных или косвенных убытков, вышеуказанное ограничение применяется в той мере, в которой это допускается действующим законодательством.</p>', 'tos', 'ru');
INSERT INTO public."Statics" (id, title, html, page, language) VALUES (5, 'Контакты', '<p>email: info@auction-skin.com</p><p>Данный юр.лица:
454085, ЧЕЛЯБИНСКАЯ ОБЛАСТЬ, ГОРОД ЧЕЛЯБИНСК, УЛИЦА ЧОППА, 7, 53, ОГРН: 1147452000410, Дата присвоения ОГРН: 29.01.2014, ИНН: 7452114246, КПП: 745201001, ДИРЕКТОР: Кузнецов Александр Сергеевич</p>', 'contacts', 'ru');


--
-- TOC entry 3309 (class 0 OID 20945)
-- Dependencies: 332
-- Data for Name: TransactionTypes; Type: TABLE DATA; Schema: public; Owner: auc_admin
--

INSERT INTO public."TransactionTypes" (id, description) VALUES (1, 'Списание за участие в лоте');
INSERT INTO public."TransactionTypes" (id, description) VALUES (2, 'Возврат - лот не состоялся');
INSERT INTO public."TransactionTypes" (id, description) VALUES (3, 'Возврат - передача лота не состоялась');
INSERT INTO public."TransactionTypes" (id, description) VALUES (4, 'Начисление за продажу лота');
INSERT INTO public."TransactionTypes" (id, description) VALUES (5, 'Списание за Покупку лота');
INSERT INTO public."TransactionTypes" (id, description) VALUES (6, 'Кэшбек');


--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 337
-- Name: Bids_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Bids_id_seq"', 63, true);


--
-- TOC entry 3376 (class 0 OID 0)
-- Dependencies: 306
-- Name: CSGO_Exterior_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_Exterior_id_seq"', 6, true);


--
-- TOC entry 3377 (class 0 OID 0)
-- Dependencies: 296
-- Name: CSGO_ItemSet_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_ItemSet_id_seq"', 55, true);


--
-- TOC entry 3378 (class 0 OID 0)
-- Dependencies: 302
-- Name: CSGO_Quality_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_Quality_id_seq"', 5, true);


--
-- TOC entry 3379 (class 0 OID 0)
-- Dependencies: 304
-- Name: CSGO_Rarity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_Rarity_id_seq"', 12, true);


--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 298
-- Name: CSGO_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_Type_id_seq"', 18, true);


--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 300
-- Name: CSGO_Weapon_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CSGO_Weapon_id_seq"', 48, true);


--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 345
-- Name: CsParams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."CsParams_id_seq"', 33, true);


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 314
-- Name: DOTA2_Quality_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DOTA2_Quality_id_seq"', 16, true);


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 312
-- Name: DOTA2_Rarity_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DOTA2_Rarity_id_seq"', 9, true);


--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 308
-- Name: DOTA2_Slot_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DOTA2_Slot_id_seq"', 38, true);


--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 310
-- Name: DOTA2_Type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DOTA2_Type_id_seq"', 22, true);


--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 357
-- Name: Deals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Deals_id_seq"', 59, true);


--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 327
-- Name: DotaHeros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DotaHeros_id_seq"', 115, true);


--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 329
-- Name: DotaParams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."DotaParams_id_seq"', 52, true);


--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 321
-- Name: FaqTabs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."FaqTabs_id_seq"', 1, true);


--
-- TOC entry 3391 (class 0 OID 0)
-- Dependencies: 323
-- Name: Faqs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Faqs_id_seq"', 14, true);


--
-- TOC entry 3392 (class 0 OID 0)
-- Dependencies: 333
-- Name: GameItems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."GameItems_id_seq"', 98, true);


--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 339
-- Name: LotResult_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."LotResult_id_seq"', 24, true);


--
-- TOC entry 3394 (class 0 OID 0)
-- Dependencies: 319
-- Name: LotStatuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."LotStatuses_id_seq"', 1, false);


--
-- TOC entry 3395 (class 0 OID 0)
-- Dependencies: 335
-- Name: Lots_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Lots_id_seq"', 253, true);


--
-- TOC entry 3396 (class 0 OID 0)
-- Dependencies: 353
-- Name: NotificationTypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."NotificationTypes_id_seq"', 1, false);


--
-- TOC entry 3397 (class 0 OID 0)
-- Dependencies: 355
-- Name: Notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Notifications_id_seq"', 57, true);


--
-- TOC entry 3398 (class 0 OID 0)
-- Dependencies: 341
-- Name: Participants_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Participants_id_seq"', 110, true);


--
-- TOC entry 3399 (class 0 OID 0)
-- Dependencies: 347
-- Name: RustCategories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."RustCategories_id_seq"', 1, false);


--
-- TOC entry 3400 (class 0 OID 0)
-- Dependencies: 351
-- Name: RustParams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."RustParams_id_seq"', 3, true);


--
-- TOC entry 3401 (class 0 OID 0)
-- Dependencies: 349
-- Name: RustTypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."RustTypes_id_seq"', 1, false);


--
-- TOC entry 3402 (class 0 OID 0)
-- Dependencies: 325
-- Name: Statics_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Statics_id_seq"', 5, true);


--
-- TOC entry 3403 (class 0 OID 0)
-- Dependencies: 331
-- Name: TransactionTypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."TransactionTypes_id_seq"', 1, false);


--
-- TOC entry 3404 (class 0 OID 0)
-- Dependencies: 343
-- Name: Transactions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public."Transactions_id_seq"', 193, true);


--
-- TOC entry 3405 (class 0 OID 0)
-- Dependencies: 292
-- Name: privileges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public.privileges_id_seq', 2, true);


--
-- TOC entry 3406 (class 0 OID 0)
-- Dependencies: 290
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 317
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: auc_admin
--

SELECT pg_catalog.setval('public.users_id_seq', 20, true);


--
-- TOC entry 3092 (class 2606 OID 28109)
-- Name: Bids Bids_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Bids"
    ADD CONSTRAINT "Bids_pkey" PRIMARY KEY (id);


--
-- TOC entry 3060 (class 2606 OID 16616)
-- Name: CsExteriors CSGO_Exterior_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsExteriors"
    ADD CONSTRAINT "CSGO_Exterior_pkey" PRIMARY KEY (id);


--
-- TOC entry 3050 (class 2606 OID 16561)
-- Name: CsItemSets CSGO_ItemSet_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsItemSets"
    ADD CONSTRAINT "CSGO_ItemSet_pkey" PRIMARY KEY (id);


--
-- TOC entry 3056 (class 2606 OID 16594)
-- Name: CsQualities CSGO_Quality_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsQualities"
    ADD CONSTRAINT "CSGO_Quality_pkey" PRIMARY KEY (id);


--
-- TOC entry 3058 (class 2606 OID 16605)
-- Name: CsRarities CSGO_Rarity_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsRarities"
    ADD CONSTRAINT "CSGO_Rarity_pkey" PRIMARY KEY (id);


--
-- TOC entry 3052 (class 2606 OID 16572)
-- Name: CsTypes CSGO_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsTypes"
    ADD CONSTRAINT "CSGO_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3054 (class 2606 OID 16583)
-- Name: CsWeapons CSGO_Weapon_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsWeapons"
    ADD CONSTRAINT "CSGO_Weapon_pkey" PRIMARY KEY (id);


--
-- TOC entry 3102 (class 2606 OID 28355)
-- Name: CsParams CsParams_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_pkey" PRIMARY KEY (id);


--
-- TOC entry 3068 (class 2606 OID 16672)
-- Name: DotaQualities DOTA2_Quality_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaQualities"
    ADD CONSTRAINT "DOTA2_Quality_pkey" PRIMARY KEY (id);


--
-- TOC entry 3066 (class 2606 OID 16661)
-- Name: DotaRarities DOTA2_Rarity_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaRarities"
    ADD CONSTRAINT "DOTA2_Rarity_pkey" PRIMARY KEY (id);


--
-- TOC entry 3062 (class 2606 OID 16638)
-- Name: DotaSlots DOTA2_Slot_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaSlots"
    ADD CONSTRAINT "DOTA2_Slot_pkey" PRIMARY KEY (id);


--
-- TOC entry 3064 (class 2606 OID 16650)
-- Name: DotaTypes DOTA2_Type_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaTypes"
    ADD CONSTRAINT "DOTA2_Type_pkey" PRIMARY KEY (id);


--
-- TOC entry 3114 (class 2606 OID 42249)
-- Name: Deals Deals_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Deals"
    ADD CONSTRAINT "Deals_pkey" PRIMARY KEY (id);


--
-- TOC entry 3082 (class 2606 OID 20004)
-- Name: DotaHeros DotaHeros_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaHeros"
    ADD CONSTRAINT "DotaHeros_pkey" PRIMARY KEY (id);


--
-- TOC entry 3084 (class 2606 OID 20012)
-- Name: DotaParams DotaParams_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_pkey" PRIMARY KEY (id);


--
-- TOC entry 3076 (class 2606 OID 19964)
-- Name: FaqTabs FaqTabs_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."FaqTabs"
    ADD CONSTRAINT "FaqTabs_pkey" PRIMARY KEY (id);


--
-- TOC entry 3078 (class 2606 OID 19975)
-- Name: Faqs Faqs_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Faqs"
    ADD CONSTRAINT "Faqs_pkey" PRIMARY KEY (id);


--
-- TOC entry 3088 (class 2606 OID 28078)
-- Name: GameItems GameItems_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."GameItems"
    ADD CONSTRAINT "GameItems_pkey" PRIMARY KEY (id);


--
-- TOC entry 3070 (class 2606 OID 17219)
-- Name: Games Games_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Games"
    ADD CONSTRAINT "Games_pkey" PRIMARY KEY (id);


--
-- TOC entry 3094 (class 2606 OID 28127)
-- Name: LotResult LotResult_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotResult"
    ADD CONSTRAINT "LotResult_pkey" PRIMARY KEY (id);


--
-- TOC entry 3074 (class 2606 OID 19836)
-- Name: LotStatuses LotStatuses_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotStatuses"
    ADD CONSTRAINT "LotStatuses_pkey" PRIMARY KEY (id);


--
-- TOC entry 3090 (class 2606 OID 28091)
-- Name: Lots Lots_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Lots"
    ADD CONSTRAINT "Lots_pkey" PRIMARY KEY (id);


--
-- TOC entry 3110 (class 2606 OID 40966)
-- Name: NotificationTypes NotificationTypes_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."NotificationTypes"
    ADD CONSTRAINT "NotificationTypes_pkey" PRIMARY KEY (id);


--
-- TOC entry 3112 (class 2606 OID 40998)
-- Name: Notifications Notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Notifications"
    ADD CONSTRAINT "Notifications_pkey" PRIMARY KEY (id);


--
-- TOC entry 3096 (class 2606 OID 28147)
-- Name: Participants Participants_lot_id_user_id_key; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Participants"
    ADD CONSTRAINT "Participants_lot_id_user_id_key" UNIQUE (lot_id, user_id);


--
-- TOC entry 3098 (class 2606 OID 28145)
-- Name: Participants Participants_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Participants"
    ADD CONSTRAINT "Participants_pkey" PRIMARY KEY (id);


--
-- TOC entry 3104 (class 2606 OID 40900)
-- Name: RustCategories RustCategories_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustCategories"
    ADD CONSTRAINT "RustCategories_pkey" PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 40919)
-- Name: RustParams RustParams_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustParams"
    ADD CONSTRAINT "RustParams_pkey" PRIMARY KEY (id);


--
-- TOC entry 3106 (class 2606 OID 40911)
-- Name: RustTypes RustTypes_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustTypes"
    ADD CONSTRAINT "RustTypes_pkey" PRIMARY KEY (id);


--
-- TOC entry 3048 (class 2606 OID 16514)
-- Name: Sessions Sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Sessions"
    ADD CONSTRAINT "Sessions_pkey" PRIMARY KEY (sid);


--
-- TOC entry 3080 (class 2606 OID 19993)
-- Name: Statics Statics_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Statics"
    ADD CONSTRAINT "Statics_pkey" PRIMARY KEY (id);


--
-- TOC entry 3086 (class 2606 OID 20950)
-- Name: TransactionTypes TransactionTypes_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."TransactionTypes"
    ADD CONSTRAINT "TransactionTypes_pkey" PRIMARY KEY (id);


--
-- TOC entry 3100 (class 2606 OID 28165)
-- Name: Transactions Transactions_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Transactions"
    ADD CONSTRAINT "Transactions_pkey" PRIMARY KEY (id);


--
-- TOC entry 3046 (class 2606 OID 16426)
-- Name: Grants grants_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Grants"
    ADD CONSTRAINT grants_pkey PRIMARY KEY (role_id, privilege_id);


--
-- TOC entry 3044 (class 2606 OID 16421)
-- Name: Privileges privileges_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Privileges"
    ADD CONSTRAINT privileges_pkey PRIMARY KEY (id);


--
-- TOC entry 3042 (class 2606 OID 16410)
-- Name: Roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 3072 (class 2606 OID 19206)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 3127 (class 2606 OID 28110)
-- Name: Bids Bids_lot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Bids"
    ADD CONSTRAINT "Bids_lot_id_fkey" FOREIGN KEY (lot_id) REFERENCES public."Lots"(id) ON UPDATE CASCADE;


--
-- TOC entry 3128 (class 2606 OID 28115)
-- Name: Bids Bids_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Bids"
    ADD CONSTRAINT "Bids_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- TOC entry 3136 (class 2606 OID 28356)
-- Name: CsParams CsParams_exterior_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_exterior_id_fkey" FOREIGN KEY (exterior_id) REFERENCES public."CsExteriors"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3137 (class 2606 OID 28361)
-- Name: CsParams CsParams_item_set_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_item_set_id_fkey" FOREIGN KEY (item_set_id) REFERENCES public."CsItemSets"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3138 (class 2606 OID 28366)
-- Name: CsParams CsParams_quality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_quality_id_fkey" FOREIGN KEY (quality_id) REFERENCES public."CsQualities"(id) ON UPDATE CASCADE;


--
-- TOC entry 3139 (class 2606 OID 28371)
-- Name: CsParams CsParams_rarity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_rarity_id_fkey" FOREIGN KEY (rarity_id) REFERENCES public."CsRarities"(id) ON UPDATE CASCADE;


--
-- TOC entry 3140 (class 2606 OID 28376)
-- Name: CsParams CsParams_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_type_id_fkey" FOREIGN KEY (type_id) REFERENCES public."CsTypes"(id) ON UPDATE CASCADE;


--
-- TOC entry 3141 (class 2606 OID 28381)
-- Name: CsParams CsParams_weapon_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."CsParams"
    ADD CONSTRAINT "CsParams_weapon_id_fkey" FOREIGN KEY (weapon_id) REFERENCES public."CsWeapons"(id) ON UPDATE CASCADE;


--
-- TOC entry 3119 (class 2606 OID 20013)
-- Name: DotaParams DotaParams_hero_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_hero_id_fkey" FOREIGN KEY (hero_id) REFERENCES public."DotaHeros"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3120 (class 2606 OID 20018)
-- Name: DotaParams DotaParams_quality_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_quality_id_fkey" FOREIGN KEY (quality_id) REFERENCES public."DotaQualities"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3121 (class 2606 OID 20023)
-- Name: DotaParams DotaParams_rarity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_rarity_id_fkey" FOREIGN KEY (rarity_id) REFERENCES public."DotaRarities"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3122 (class 2606 OID 20028)
-- Name: DotaParams DotaParams_slot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_slot_id_fkey" FOREIGN KEY (slot_id) REFERENCES public."DotaSlots"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3123 (class 2606 OID 20033)
-- Name: DotaParams DotaParams_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."DotaParams"
    ADD CONSTRAINT "DotaParams_type_id_fkey" FOREIGN KEY (type_id) REFERENCES public."DotaTypes"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3118 (class 2606 OID 19976)
-- Name: Faqs Faqs_tab_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Faqs"
    ADD CONSTRAINT "Faqs_tab_fkey" FOREIGN KEY (tab) REFERENCES public."FaqTabs"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3124 (class 2606 OID 28079)
-- Name: GameItems GameItems_app_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."GameItems"
    ADD CONSTRAINT "GameItems_app_id_fkey" FOREIGN KEY (app_id) REFERENCES public."Games"(id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3129 (class 2606 OID 28128)
-- Name: LotResult LotResult_lot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotResult"
    ADD CONSTRAINT "LotResult_lot_id_fkey" FOREIGN KEY (lot_id) REFERENCES public."Lots"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3130 (class 2606 OID 28133)
-- Name: LotResult LotResult_winner_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."LotResult"
    ADD CONSTRAINT "LotResult_winner_id_fkey" FOREIGN KEY (winner_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- TOC entry 3125 (class 2606 OID 28092)
-- Name: Lots Lots_game_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Lots"
    ADD CONSTRAINT "Lots_game_item_id_fkey" FOREIGN KEY (game_item_id) REFERENCES public."GameItems"(id) ON UPDATE CASCADE;


--
-- TOC entry 3126 (class 2606 OID 28097)
-- Name: Lots Lots_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Lots"
    ADD CONSTRAINT "Lots_status_id_fkey" FOREIGN KEY (status_id) REFERENCES public."LotStatuses"(id) ON UPDATE CASCADE;


--
-- TOC entry 3145 (class 2606 OID 41004)
-- Name: Notifications Notifications_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Notifications"
    ADD CONSTRAINT "Notifications_type_id_fkey" FOREIGN KEY (type_id) REFERENCES public."NotificationTypes"(id) ON UPDATE CASCADE;


--
-- TOC entry 3144 (class 2606 OID 40999)
-- Name: Notifications Notifications_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Notifications"
    ADD CONSTRAINT "Notifications_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- TOC entry 3131 (class 2606 OID 28148)
-- Name: Participants Participants_lot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Participants"
    ADD CONSTRAINT "Participants_lot_id_fkey" FOREIGN KEY (lot_id) REFERENCES public."Lots"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3132 (class 2606 OID 28153)
-- Name: Participants Participants_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Participants"
    ADD CONSTRAINT "Participants_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE;


--
-- TOC entry 3142 (class 2606 OID 40920)
-- Name: RustParams RustParams_category_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustParams"
    ADD CONSTRAINT "RustParams_category_id_fkey" FOREIGN KEY (category_id) REFERENCES public."RustCategories"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3143 (class 2606 OID 40925)
-- Name: RustParams RustParams_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."RustParams"
    ADD CONSTRAINT "RustParams_type_id_fkey" FOREIGN KEY (type_id) REFERENCES public."RustTypes"(id) ON UPDATE CASCADE;


--
-- TOC entry 3134 (class 2606 OID 28171)
-- Name: Transactions Transactions_lot_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Transactions"
    ADD CONSTRAINT "Transactions_lot_id_fkey" FOREIGN KEY (lot_id) REFERENCES public."Lots"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3135 (class 2606 OID 28176)
-- Name: Transactions Transactions_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Transactions"
    ADD CONSTRAINT "Transactions_type_id_fkey" FOREIGN KEY (type_id) REFERENCES public."TransactionTypes"(id) ON UPDATE CASCADE;


--
-- TOC entry 3133 (class 2606 OID 28166)
-- Name: Transactions Transactions_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Transactions"
    ADD CONSTRAINT "Transactions_user_id_fkey" FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3116 (class 2606 OID 16432)
-- Name: Grants grants_privilege_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Grants"
    ADD CONSTRAINT grants_privilege_id_fkey FOREIGN KEY (privilege_id) REFERENCES public."Privileges"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3115 (class 2606 OID 16427)
-- Name: Grants grants_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public."Grants"
    ADD CONSTRAINT grants_role_id_fkey FOREIGN KEY (role_id) REFERENCES public."Roles"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3117 (class 2606 OID 19207)
-- Name: users users_role_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: auc_admin
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_role_id_fkey FOREIGN KEY (role_id) REFERENCES public."Roles"(id) ON UPDATE CASCADE ON DELETE SET NULL;


-- Completed on 2020-03-24 10:23:58

--
-- PostgreSQL database dump complete
--
