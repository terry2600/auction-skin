const asyncDecorator = (func) => {
    return (req, res, next) => {
        func(req, res, next).catch(next);
    }
};

exports.asyncDecorator = asyncDecorator;