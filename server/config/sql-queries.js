const query = {
    queryOpenLots(params) {
        const { canReadOwn, engage, active, open, history, userLots, lotId, userId, appId, hotlot, sortType, sortValue } = params;
        return `SELECT DISTINCT lot.id, create_date, hotlot, price,
            cost_of_participation, lot_step, color, participants,
           COUNT(lot_id) OVER (PARTITION BY lot.id) AS current_participants,
           icon_url, "GameItems"."name", "GameItems"."stickers", name_color,
            "Games"."name" AS game_name,
            game_item_id, status_id, start_date
    FROM (
        SELECT id, game_item_id, create_date, hotlot,
                price, cost_of_participation, lot_step, color,
                participants, status_id, start_date
        FROM "Lots"
        WHERE status_id ${active ? '= 2' : history ? 'IN (4, 5, 6, 7)' : '= 1'}  ${appId ? `AND app_id = ${appId}` : ''}
        ${hotlot ? 'AND hotlot = true' : ''} ${lotId !== undefined ? `AND id = ${lotId}` : ''}
    ) lot
    LEFT JOIN "Participants" ON lot_id = "lot".id
    INNER JOIN "GameItems" ON lot.game_item_id = "GameItems"."id"
    INNER JOIN "Games" ON app_id = "Games"."id"
    ${canReadOwn ? (
            `WHERE lot.id ${open ? 'NOT' : ''} IN (
            SELECT DISTINCT lot_id
            FROM "Participants"
            WHERE user_id = ${userId}
        )`
        ) : ``}
        ${!hotlot && !userLots ? (
            `ORDER BY ${sortType} ${sortValue}`
        ): ''}
    `;
    },

    queryLots(id) {
        return `SELECT
                       "Lots"."id",
                       "Lots"."create_date",
                       "Lots"."hotlot",
                       "Lots"."price",
                       "Lots"."cost_of_participation",
                       "Lots"."lot_step",
                       "Lots"."participants",
                       "GameItems"."stickers",
                        lot.current_participants,
                        "GameItems"."icon_url",
                         "GameItems"."name",
                         "GameItems"."name_color",
                         "Games"."name" as game_name
                    FROM (
                           SELECT "Participants"."lot_id", COUNT("Participants"."lot_id") as current_participants
                           FROM "Participants"
                    GROUP BY "Participants"."lot_id"
                    ) lot INNER JOIN "Participants" ON "Participants"."user_id" = ${id} AND lot.lot_id = "Participants"."lot_id"
                         INNER JOIN "Lots" ON  lot.lot_id = "Lots"."id"
                         INNER JOIN "GameItems" ON "Lots"."game_item_id" = "GameItems"."id"
                         INNER JOIN "Games" ON "GameItems"."app_id" = "Games"."id"
                     ORDER BY "Lots"."create_date" DESC`
    },
    queryUser(steamId) {
        return `SELECT steam_nick, steam_guard, trade_url, avatar,
                       registration_date,
                       COUNT("Participants".lot_id) AS participations, SUM(CASE WHEN "LotResult".winner_id = users.id THEN 1 ELSE 0 END) AS winnings
                    FROM users
                         INNER JOIN "Participants"
                         ON id = user_id
                         INNER JOIN "Lots" AS lot
                         ON lot.id = lot_id
                            AND lot.status_id IN (4, 5, 6)
                         INNER JOIN "LotResult"
                         ON "Participants".lot_id = "LotResult".lot_id
                   WHERE steamid64 = '${steamId}'
                   GROUP BY steam_nick, steam_guard, trade_url, avatar, registration_date`
    }
};

module.exports = query;
