const Bot = require('./bots');
const models = require('../models');

const Op = models.Sequelize.Op;

const getAvailableInventory = async (forCreation, botInventory, appId, userId, steamId = null, assetId = null) => {
    try {
        let inventory =  !botInventory ? await Bot.getUserInventory(appId, steamId) : await Bot.getBotInventory(appId);

        inventory = inventory.filter(item => {
            switch (appId) {
                case '730':
                    return ['CSGO_Type_Pistol', "CSGO_Type_SMG", "CSGO_Type_Rifle", "CSGO_Type_Shotgun", "CSGO_Type_SniperRifle", "CSGO_Type_Machinegun", "CSGO_Type_Knife", 'Type_Hands', 'CSGO_Tool_Sticker'].includes(item.tags[0].internal_name)
                    break;
                default:
                    return true;
            }
        })

        const activeUserLots = await models.Lot.findAll({
            attributes: [],
            where: {
                owner_id: userId,
                status_id: {
                    [Op.between]: [1, 5]
                }
            },
            include: {
                model: models.GameItem,
                attributes: ['asset_id']
            },
            raw: true
        }).map(item => item['GameItem.asset_id']);

        if (forCreation) {
            const item = inventory.find(item => item.assetid === assetId);
            if (!item || activeUserLots.find(activeAssetId => activeAssetId === item.assetid))
                return undefined;
            return item
        } else {
            return inventory.filter(item => {
                return !activeUserLots.includes(item.assetid)
            });
        }
    } catch (e) {
        console.log(e);
        console.log('Данный предмет либо отсутствует в инвентаре пользователя, либо с ним уже существует лот')
    }
};

module.exports = {
    getAvailableInventory
};

