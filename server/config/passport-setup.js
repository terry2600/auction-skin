const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const models = require('../models');
const {PASSPORT_API_KEY, SERVER_URL} = require('../config/constants');

passport.serializeUser((id, done) => {
    done(null, id);
});

passport.deserializeUser(function (id, done) {
    models.User.findOne({
        where: {id: id},
        include: [{
            model: models.Role,
        }]
    })
        .then(user => {
            done(null, {
                id: user.dataValues.id,
                steamid64: user.steamid64,
                trade_url: user.trade_url,
                userName: user.steam_nick,
                avatar: user.avatar,
                role: user.Role.type,
                balance: user.balance,
                registration_date: user.registration_date,
                steam_guard: user.steam_guard,
                api_key: user.api_key
            })
        })
});

passport.use(new SteamStrategy({
    returnURL: `${SERVER_URL}/api/auth/steam/return`,
    apiKey: PASSPORT_API_KEY
}, async (identifier, profile, done) => {
    let [user, isCreated] = await models.User.findOrCreate({
        where: {
            steamid64: profile.id,
        },
        defaults: {
            steamid64: profile.id,
            steam_nick: profile.displayName,
            avatar: profile.photos[2].value,
            role_id: 2,
            balance: 0
        }
    });

    if (isCreated)
        return done(null, user.dataValues.id, {new: true, user});

    const {steamid64, nick, avatar} = user.dataValues;

    if (!user.trade_url)
        return done(null, user.dataValues.id);

    //user.steam_guard = await bots.getUserEscrow(user.trade_url) < 1
    //await user.save()
    done(null, user.id)
    /*
        .then(([user, isCreated]) => {
            if (!isCreated) {
                const {steamid64, nick, avatar} = user.dataValues;
                bots.getUserEscrow(user.trade_url).then(escrowDays => console.log(escrowDays))
                //console.log

                if(!user.trade_url)
                    return done(null, user.dataValues.id)

                models.User.update({
                    steam_guard: bots.getUserEscrow(user.trade_url) < 1
                }, {
                    where: {
                        steamid64: steamid64,
                    },
                    returning: true
                }).then(([rows, currentUser]) => done(null, currentUser.id))
            } else {
                done(null, user.dataValues.id, {new: true, user})
            }
        })*/
}));
