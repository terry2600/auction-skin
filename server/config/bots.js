const SteamUser = require('steam-user');
const GlobalOffensive = require('globaloffensive');
const SteamCommunity = require('steamcommunity');
const SteamID = SteamCommunity.SteamID;
const TradeOfferManager = require('steam-tradeoffer-manager');
const SteamTotp = require('steam-totp');
const SteamWebAPI = require('@doctormckay/steam-webapi');
const config = require('./bots-config');
const models = require('../models');
const {
    TRADE_SERVICE_URL,
    TRADE_SERVICE_SECRET
} = require('./constants')

const axios = require('axios');

class SteamBot {
    constructor(logOnOptions, config) {
        this.client = new SteamUser();
        this.csgo = new GlobalOffensive(this.client);
        this.community = new SteamCommunity();
        this.manager = new TradeOfferManager({
            steam: this.client,
            community: this.community,
            language: 'ru',
            cancelTime: 1000 * 60 * 60
        });
        let res;
        this.started = new Promise(r => {res = r})
        this.started.resolve = res

        this.logOn(logOnOptions);
    }

    logOn(logOnOptions) {
        this.client.logOn(logOnOptions);
        this.client.on('loggedOn', () => {
            console.log('Logged into Steam');
            this.client.setPersona(SteamUser.EPersonaState.Online);
        });

        this.client.on('webSession', (sessionid, cookies) => {
            console.log('Time for cookies');
            this.manager.setCookies(cookies, (err, key) => {
                if (err) {
                    console.log(err)
                } else {
                    this.started.resolve()
                    console.log('successful connection', key)
                }
            });
            this.community.setCookies(cookies);
        });

        this.csgo.on('connectedToGC', e => {
            if (e) {
                console.log(e)
            } else {
                this.csgo.inspectItem('76561197980878029', '9711974855', '5371187558936318624', item => {
                    console.log('ВСЕ НА МАЗИ!')
                    //this.getUserTrades('084DEDE823F9C699C69D9653CD6345B3')
                    //console.log('Тут должен ьыть предмет')
                    //console.log(item)
                })
            }
        });
    }

    getUserEscrow(tradeUrl){
        return new Promise((resolve, reject) => {
            try {
                const offer = this.manager.createOffer(tradeUrl);
                offer.getUserDetails((err, me, them) => {
                    console.log(err)
                	if(err)
                        resolve({escrow: false, err: err.message})
                    else
                        resolve({escrow: them.escrowDays, err: null})
                })
            } catch (err) {
                resolve({escrow: false, err: err.message})
            }
        })
    }

    getSteamId64FromTradeUrl(tradeUrl){
        const offer = this.manager.createOffer(tradeUrl);
        return offer.partner.getSteamID64()
    }

    async sendItemToWinner({lotId, tradeUrl, assetId, appId, contextId}) {
        let status = false
        while (status !== 'OK') {
            let backendRes = await axios.post(`${TRADE_SERVICE_URL}/sendItem`, {lotId, tradeUrl, assetId, appId, contextId, TRADE_SERVICE_SECRET})
            status = backendRes.data
            await new Promise(res => setTimeout(res, 1000))
        }
        return status
    }

    async getUserItem({lotId, steamId, tradeUrl, assetId, appId}) {
        let status = false
        while (status !== 'OK') {
            let backendRes = await axios.post(`${TRADE_SERVICE_URL}/getItem`, {lotId, steamId, tradeUrl, assetId, appId, TRADE_SERVICE_SECRET})
            status = backendRes.data
            await new Promise(res => setTimeout(res, 1000))
        }
        return status
    }

    getWear(steamId, item) {
        return new Promise((resolve, reject) => {
            try {
                let showUrl = item.actions.find(action => action.name === 'Осмотреть в игре…').link
                let d = showUrl.substr(showUrl.indexOf('assetid%D') + 9)

                console.log('inspectingItem: ', steamId, item.assetid, d)

                this.csgo.inspectItem(steamId, item.assetid, d, item => {
                    resolve(item.paintwear.toString())
                })

                /*
                this.csgo.inspectItem(showUrl, (data) => {
                    console.log('resolved wear with: ', data)
                    resolve(data)
                })
                */
            } catch (e) {
                console.log(e)
                reject(e)
            }
        });
    }

    async getFloat(steamId, item) {
        let showUrl = item.actions.find(action => action.name === 'Осмотреть в игре…').link
            .replace('%owner_steamid%', steamId)
            .replace('%assetid%', item.assetid)

        const floatApi = await axios.get(`https://api.csgofloat.com/?url=${showUrl}`)
        return floatApi.data.iteminfo.floatvalue.toString()
    }

    getBotInventory(gameId) {
        return new Promise((resolve, reject) => {
            this.manager.getInventoryContents(gameId, 2, true, (err, inventory) => {
                if (err) {
                    console.log(err)
                } else {
                    resolve(inventory)
                }
            })
        })
    }

    getBotTradeUrl() {
        return new Promise((resolve, reject) => {
            this.client.getTradeURL((err, {token, url}) => {
                if (err) {
                    console.log(err)
                } else {
                    resolve(url)
                }
            })
        })
    }

    async getBotInventoryPrice(gameId) {
        let items = await this.getBotInventory(gameId)
        let marketItems = await Promise.all(items.map(item => {
            return this.getMarketItem(gameId, item.market_hash_name)
        }))
        return marketItems.reduce((acc, item) => acc + item.lowestPrice, 0)
    }

    getUserInventory(gameId, steamId) {
        return new Promise((resolve, reject) => {
            try {
                this.manager.getUserInventoryContents(steamId, gameId, 2, false, (err, inventory) => {
                    if (err) {
                        console.log(err)
                    } else {
                        resolve(inventory)
                    }
                })
            } catch (e) {
                console.log(e)
            }
        });
    }

    async findUserItem(steamId, gameId, assetId) {
        const inventory = await this.getUserInventory(gameId, steamId);
        return inventory.find(item => item.assetid === assetId);
    }

    getUserTrades(apiKey, since = Date.now() / 1000){
        let api = new SteamWebAPI(apiKey);
        api.get('IEconService', 'GetTradeOffers', 1, {
            get_sent_offers: true,
            time_historical_cutoff: since,
            get_descriptions: true,
            language: 'ru',
            active_only: true
        }, (err, res) => {
            if(err)
                console.log(err)

            console.log(res.trade_offers_sent)
            console.log(res)
            for(let tradeOffer of res.trade_offers_sent){
                console.log(tradeOffer.items_to_give)
            }
        })
    }

    getUserInfo(steamId) {
        return new Promise(resolve => {
            this.community.getSteamUser(new SteamID(steamId), (err, user) => {
                if (err) {
                    console.log(err)
                } else {
                    resolve(user.tradeBanState)
                }
            })
        })
    }

    getMarketItem(appid, hashName) {
        return new Promise(resolve => {
            this.community.getMarketItem(appid, hashName, (err, cMarketItem) => {
                if (err) {
                    console.log(err)
                } else {
                    resolve(cMarketItem)
                }
            })
        })
    }
}

(function initBot() {
    const code = SteamTotp.generateAuthCode(config.Bots[config.selectedBot].shasec)
    console.log('init bot code: ', code)
    let steamBot = new SteamBot({
        accountName: config.Bots[config.selectedBot].username,
        password: config.Bots[config.selectedBot].password,
        twoFactorCode: code,
        logonID: Math.floor(Math.random() * 1000000)
    }, config);

    module.exports = steamBot;
})();
