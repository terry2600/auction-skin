const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Bot = require('../config/bots');
const models = require('../models');
const axios = require('axios');
const {asyncDecorator} = require('../asyncDecorator');

const {
    TIME_SERVICE_URL,
    BID_TIME,
    TIME_SERVICE_SECRET
} = require('../config/constants.js');

router.post('/timer', asyncDecorator(async (req, res) => {
    if(req.body.TIME_SERVICE_SECRET !== TIME_SERVICE_SECRET)
        return res.sendStatus(500);

    const {
        lotId,
        type,
        lastBidId,
        time,
    } = req.body;

    const io = req.app.get('io');

    if (type === 'lot') {
        models.Lot.update({
            status_id: 3
        }, {
            where: {
                id: lotId
            }
        });

        const tsResult = await axios.post(TIME_SERVICE_URL, {
            lotId,
            type: 'bid',
            lastBidId: null,
            time: BID_TIME,
            TIME_SERVICE_SECRET
        })
    } else if (type === 'bid'){
        const lastBid = await models.Bid.findOne({
            where: {
                lot_id: lotId
            },
            order: [['date', 'DESC']],
            raw: true,
        });

        if(lastBid && lastBid.id !== lastBidId)
            return res.sendStatus(200)

        const {owner_id, game_item_id, price, cost_of_participation} = await models.Lot.findByPk(lotId, {
            attributes: ['owner_id', 'game_item_id', 'price', 'cost_of_participation'],
            raw: true
        });

        const item = await models.GameItem.findByPk(game_item_id, {
            raw: true
        });

        const owner = await models.User.findOne({
            where: {
                id: owner_id
            },
            raw: true
        });

        const participants = await models.Participants.findAll({
            where: {
                lot_id: lotId
            }
        });

        let status_id = 7;

        let transaction = await models.sequelize.transaction();
        try {
            /*if (!lastBid && owner.role_id === 1) {
                console.log('Созданный админом лот отменен');
                const [numberOfUpdatedLots] = await models.Lot.update({
                    status_id,
                    end_date: Date.now()
                }, {
                    where: {id: lotId},
                    transaction
                });

                await models.Transaction.bulkCreate(participants.map(participant => {
                        return {
                            user_id: participant.user_id,
                            lot_id: lotId,
                            lot_name: item.name,
                            type: 2,
                            amount: cost_of_participation
                        }
                    }),
                    {transaction});

                await transaction.commit();
            } else*/ if (!lastBid/* && owner.role_id !== 1*/) {
                console.log('Созданный лот отменен');
                const [numberOfUpdatedLots] = await models.Lot.update({
                    status_id,
                    end_date: Date.now()
                }, {
                    where: {id: lotId},
                    transaction
                });


                const {asset_id, app_id} = await models.GameItem.findByPk(game_item_id, {
                    attributes: ['asset_id', 'app_id'],
                    raw: true
                });

                await models.Transaction.bulkCreate(participants.map(participant => {
                        return {
                            user_id: participant.user_id,
                            lot_id: lotId,
                            lot_name: item.name,
                            type: 2,
                            amount: cost_of_participation
                        }
                    }),
                    {transaction});

                for(let participant of participants){
                    const notification = await models.Notification.create({
                        user_id: participant.user_id,
                        type_id: 2,
                        body: {
                            item_icon: item.icon_url,
                            item_name: item.name
                        }
                    }, {transaction});

                    io.remoteEmitToUser(notification.user_id, 'notification', notification)
                }

                await transaction.commit();

                //Bot.getUserItem(owner.steamid64, owner.trade_url, asset_id, app_id);
            } else {
                status_id = 4;
                console.log('созданный лот идет на передачу епт');
                const [numberOfUpdatedLots] = await models.Lot.update({
                    status_id,
                    end_date: Date.now()
                }, {
                    attributes: ['role_id'],
                    where: {id: lotId},
                    transaction
                });

                const result = await models.LotResult.create({
                    lot_id: lotId,
                    winner_id: lastBid.user_id,
                    purchase_price: lastBid.bet_size,
                }, {transaction});

                const winner = await models.User.findByPk(lastBid.user_id, {
                    attributes: ['avatar', 'steam_nick', 'trade_url', 'id'],
                    raw: true,
                    transaction
                });

                io.to(`lot${lotId}`).emit('lotEnded', {
                    winner_avatar: winner.avatar,
                    winner_nick: winner.steam_nick,
                    item_icon: item.icon_url,
                    item_name: item.name
                });

                const ownerNotification = await models.Notification.create({
                    user_id: owner.id,
                    type_id: 1,
                    body: {
                        winner_avatar: winner.avatar,
                        winner_nick: winner.steam_nick,
                        item_icon: item.icon_url,
                        item_name: item.name,
                        purchase_price: result.purchase_price
                    }
                }, {transaction});

                io.remoteEmitToUser(ownerNotification.user_id, 'notification', ownerNotification);

                for(let participant of participants){
                    const notification = await models.Notification.create({
                        user_id: participant.user_id,
                        type_id: 1,
                        body: {
                            winner_avatar: winner.avatar,
                            winner_nick: winner.steam_nick,
                            item_icon: item.icon_url,
                            item_name: item.name,
                            purchase_price: result.purchase_price
                        }
                    }, {transaction});

                    io.remoteEmitToUser(notification.user_id, 'notification', notification)
                }

                await transaction.commit();
                //Bot.sendItemToWinner(lotId, winner.trade_url, item.asset_id, item.app_id, 2, 1);
            }

            io.to(`lot${lotId}`).emit('closedLot', {id: lotId, status: status_id})
        } catch (err) {
            console.log('говно жопа ошибка передачи пидор ', err);
            res.sendStatus(500)
            return
        }
    }

    res.sendStatus(200)
}));

module.exports = router;
