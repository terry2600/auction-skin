const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const path = require('path');
const Bot = require('../config/bots');
const Op = Sequelize.Op;
const timerList = require('../timers');
let accessControl = require('../models');
const query = require('../config/sql-queries');
const {asyncDecorator} = require('../asyncDecorator');
const {
    TIME_SERVICE_URL,
    TIME_SERVICE_SECRET
} = require('../config/constants.js');
const axios = require('axios');

const filters = {
    dota: [
        {model: "Hero", title: "Герой", pro: false},
        {model: "Type", title: "Тип лота", pro: false},
        {model: "Quality", title: "Качество", pro: true},
        {model: "Rarity", title: "Редкость", pro: true},
        {model: "Slot", title: "Ячейка", pro: true}
    ],
    cs: [
        {model: "Exterior", title: "Качество лота", pro: false},
        {model: "Type", title: "Тип лота", pro: false},
        {model: "ItemSet", title: "Набор", pro: true},
        {model: "Quality", title: "Редкость", pro: true},
        {model: "Rarity", title: "Категория", pro: true},
        {model: "Weapon", title: "Оружие", pro: true}
    ]
};

const userLotsPerPage = 30;

router.get('/', asyncDecorator(async (req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);

    models.Lot.findAll({
        include: [{
            model: models.GameItem,
            include: [{
                model: models.Games
            }]
        }],
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(lots => {
            res.json({data: lots.slice(range[0], range[1] + 1), total: lots.length})
        })
}));

router.post('/:type/:page/:perPage', asyncDecorator(async (req, res, next) => {
    const type = req.params.type;
    const page = parseInt(req.params.page);
    const perPage = parseInt(req.params.perPage);

    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('lots');

    let queryParams = {canReadOwn: permission.granted};

    //Если юзер - гость, получаем все лоты, если юзер уже залогиненен получаем только те лоты, в которых он не участвует
    if (permission.granted) {
        queryParams.userId = req.user.id;
    }

    if (permission.granted || (!permission.granted && type !== 'engage')) {
        queryParams[type] = true;
        queryParams.appId = req.body.filters.app_id;
    } else {
        res.sendStatus(403);
        return
    }

    queryParams = {...queryParams, ...req.body.filters.sort};

    let lots = await models.sequelize.query(query.queryOpenLots(queryParams), {type: models.sequelize.QueryTypes.SELECT});
    lots = lotsFilter(lots, req.body.filters, await getItemsFromFilters(req.body.filters.app_id, req.body.filters));

    let viewedLots = lots.slice(page * perPage, (page + 1) * perPage);

    res.json({
        lots: viewedLots,
        perPage: perPage,
        viewed: (page + 1) * perPage - (perPage - viewedLots.length),
        total: lots.length,
        totalPage: Math.ceil(lots.length / perPage)
    });
}));

router.get('/hot', asyncDecorator(async (req, res, next) => {
    let lots = await models.sequelize.query(query.queryOpenLots({
        hotlot: true,
        appId: req.query.appId
    }), {type: models.sequelize.QueryTypes.SELECT});

    res.json({
        lots: lots,
        total: lots.length
    })
}));

function lotsFilter(lots, filter, itemFilter = false) {
    return lots.filter(lot => {
        let ok = true;
        if (itemFilter !== false)
            ok = ok && itemFilter.includes(lot.game_item_id);
        ok = ok && lot.price <= filter.price.max && lot.price >= filter.price.min;

        return ok;
    })
}

async function getItemsFromFilters(app_id, filters) {
    let assets = [],
        params = null;
    switch (app_id) {
        case 730:
            params = await models.CsParams.findAll({where: filters.params});
            assets = params.map(param => param.asset_id);
            break;
        case 570:
            params = await models.DotaParams.findAll({where: filters.params});
            assets = params.map(param => param.asset_id);
            break;
        case 252490:
            params = await models.RustParams.findAll({where: filters.params});
            assets = params.map(param => param.asset_id);
            break;
    }

    if (assets.length < 1)
        return [];

    const gameItemFilter = {
        asset_id: {
            [Op.in]: assets
        }
    };

    if (filters.search)
        gameItemFilter.name = {
            [Op.iLike]: `%${filters.search}%`
        };

    let gameItems = await models.GameItem.findAll({where: gameItemFilter});

    return gameItems.map(gameItem => gameItem.id)
}

// Если пользователь авторизован, возвращаем лот с информацией о том, участвует ли пользователь или нет.
// В противном случае только лот
router.get('/open/:id', asyncDecorator(async (req, res, next) => {
    const id = req.params.id;
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('lots');
    let lotParams;

    try {
        let lot = await models.Lot.findByPk(id, {
            include: [{
                model: models.GameItem,
                include: [{
                    model: models.Games
                }]
            }]
        });

        switch (lot.GameItem.app_id) {
            case 730:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.CsType,
                        models.CsExterior,
                        models.CsItemSet,
                        models.CsQuality,
                        models.CsRarity,
                        models.CsWeapon
                    ]
                });
                break;
            case 570:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.DotaHero,
                        models.DotaType,
                        models.DotaQuality,
                        models.DotaRarity,
                        models.DotaSlot
                    ]
                });
                break;
            case 252490:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.RustCategory,
                        models.RustType
                    ]
                });
                break;
        }

        if (permission.granted) {
            const users = await lot.getUsers({attributes: ['steamid64'], joinTableAttributes: [], raw: true});
            const isParticipated = users.some(user => user.steamid64 === req.user.steamid64);
            lot = lot.get({plain: true});
            lot.isParticipated = isParticipated;
        }

        const similarLots = await models.Lot.findAll({
            attributes: ['end_date'],
            where: {
                game_item_id: lot.game_item_id,
                status_id: {
                    [Op.in]: [4, 5, 6]
                }
            },
            include: [{
                attributes: ['purchase_price'],
                model: models.LotResult,
                include: [{
                    attributes: ['steam_nick'],
                    model: models.User,
                }]
            }],
            order: [['end_date', 'DESC']],
            limit: 3
        });

        const {steamid64} = await models.User.findByPk(lot.owner_id);

        res.json({item: lot, itemParams: lotParams, similarLots, steamid64})
    } catch (e) {
        console.log(e)
    }
}));

//Получить разыгрываемый лот
router.get('/active/:id', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('lots');

    try {
        if (!permission.granted) {
            res.sendStatus(403);
            return
        }

        const id = req.params.id;
        let lotParams;
        const lot = await models.Lot.findByPk(id, {
            include: [{
                model: models.GameItem,
                include: [{
                    model: models.Games
                }]
            }],
        });

        const participants = await lot.getUsers({
            attributes: ['id', 'steam_nick', 'avatar'],
            joinTableAttributes: [],
            raw: true
        });

        switch (lot.GameItem.app_id) {
            case 730:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.CsType,
                        models.CsExterior,
                        models.CsItemSet,
                        models.CsQuality,
                        models.CsRarity,
                        models.CsWeapon
                    ]
                });
                break;
            case 570:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.DotaHero,
                        models.DotaType,
                        models.DotaQuality,
                        models.DotaRarity,
                        models.DotaSlot
                    ]
                });
                break;
            case 252490:
                lotParams = await models[lot.GameItem.Game.game_filter].findOne({
                    attributes: [],
                    where: {asset_id: lot.GameItem.asset_id},
                    include: [
                        models.RustCategory,
                        models.RustType,
                    ]
                });
        }

        const lastBids = await models.Bid.findAll({
            limit: 5,
            attributes: ['User.steam_nick', 'User.avatar', 'date', 'bet_size', 'id'],
            where: {
                lot_id: id
            },
            include: [{
                attributes: [],
                model: models.User
            }],
            order: [['date', 'DESC']],
            raw: true
        });

        const similarLots = await models.Lot.findAll({
            attributes: ['end_date'],
            where: {
                game_item_id: lot.game_item_id,
                status_id: {
                    [Op.in]: [4, 5, 6]
                }
            },
            include: [{
                attributes: ['purchase_price'],
                model: models.LotResult,
                include: [{
                    attributes: ['steam_nick'],
                    model: models.User
                }]
            }],
            limit: 5,
        });

        res.json({
            item: lot,
            params: lotParams,
            participants: participants,
            lastBids: lastBids,
            similarLots: similarLots
        })
    } catch (e) {
        console.log(e)
    }

}));

router.get('/active/game/:id', asyncDecorator( async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }
    const id = await parseInt(req.params.id);

    const Lot = await models.Lot.findByPk(id, {
        include: [{
            model: models.GameItem,
            include: [{
                model: models.Games
            }]
        }],
    });

    const participants = await Lot.getUsers({
        attributes: ['id', 'steam_nick', 'avatar'],
        joinTableAttributes: [],
        raw: true
    });

    const LotParams = await models[Lot.GameItem.Game.game_filter].findOne({
        attributes: [],
        where: {asset_id: Lot.GameItem.asset_id},
        include: [
            models.CsType,
            models.CsExterior,
            models.CsItemSet,
            models.CsQuality,
            models.CsRarity,
            models.CsWeapon
        ]
    });

    const lastBidBy = await models.Bid.findAll({
        limit: 5,
        attributes: ['User.steam_nick', 'User.avatar', 'date', 'bet_size', 'id'],
        where: {
            lot_id: id
        },
        include: [{
            attributes: [],
            model: models.User
        }],
        order: [['date', 'DESC']],
        raw: true
    });

    const similarLots = await models.Lot.findAll({
        attributes: ['end_date'],
        where: {
            game_item_id: Lot.game_item_id,
            status_id: {
                [Op.in]: [4, 5, 6]
            }
        },
        include: [{
            attributes: ['purchase_price'],
            model: models.LotResult,
            include: [{
                attributes: ['steam_nick'],
                model: models.User
            }]
        }],
        limit: 5,
    });

    res.json({
        item: Lot,
        params: LotParams,
        participants: participants,
        lastBidBy: lastBidBy,
        similarLots: similarLots
    })
}));


router.get('/active/user/:page', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const page = await parseInt(req.params.page);

    const userLots = await models.sequelize.query(query.queryOpenLots({
        canReadOwn: permission.granted,
        active: true,
        engage: true,
        userId: req.user.id,
        userLots: true
    }), {type: models.sequelize.QueryTypes.SELECT});

    res.json({
        lots: userLots.slice(page * userLotsPerPage, (page + 1) * userLotsPerPage),
        perPage: userLotsPerPage,
        total: userLots.length,
    })
}));


router.post('/active/user/:page', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const page = await parseInt(req.params.page);

    let userLots = await models.sequelize.query(query.queryOpenLots({
        canReadOwn: permission.granted,
        active: true,
        engage: true,
        userId: req.user.id
    }), {type: models.sequelize.QueryTypes.SELECT});
    userLots = lotsFilter(userLots, req.body.filters, await getItemsFromFilters(req.body.filters.app_id, req.body.filters));
    const viewedLots = await userLots.slice(page * userLotsPerPage, (page + 1) * userLotsPerPage);

    res.json({
        lots: viewedLots,
        perPage: userLotsPerPage,
        viewed: (page + 1) * perPage - (perPage - viewedLots.length),
        total: userLots.length,
        totalPage: Math.ceil(userLots.length / perPage)
    });
}));

router.get('/:id/bids', asyncDecorator(async (req, res, next) => {
    const lotId = parseInt(req.params.id);

    const bids = await models.Bid.findAll({
        where: {lot_id: lotId},
        include: [{
            attributes: ['steam_nick'],
            model: models.User
        }]
    });

    res.json(bids)
}));

router.get('/:id/participants', asyncDecorator(async (req, res, next) => {
    const lotId = parseInt(req.params.id);

    const participants = await models.Lot.findAll({
        where: {id: lotId},
        include: [{
            attributes: ['steam_nick', 'avatar'],
            model: models.User
        }],
        raw: true
    });

    res.json(participants)
}));

router.get('/:id/winner', asyncDecorator(async (req, res, next) => {
    const lotId = parseInt(req.params.id);

    const winner = await models.LotResult.findOne({
        attributes: ['purchase_price'],
        where: {lot_id: lotId},
        include: [{
            attributes: ['steam_nick', 'avatar'],
            model: models.User
        }]
    });

    res.json(winner)
}));

router.get('/:id/similar', asyncDecorator(async (req, res, next) => {
    const lotId = parseInt(req.params.id);

    const lot = await models.Lot.findByPk(lotId, {
        attributes: ['game_item_id'],
        raw: true
    });

    const similarLots = await models.Lot.findAll({
        attributes: ['end_date'],
        where: {
            game_item_id: lot.game_item_id,
            status_id: {
                [Op.in]: [4, 5, 6]
            }
        },
        include: [{
            attributes: ['purchase_price'],
            model: models.LotResult,
            include: [{
                attributes: ['steam_nick'],
                model: models.User,
            }]
        }],
    });

    res.json(similarLots)
}));

router.get('/own/history', asyncDecorator(async (req, res) => {
    const ownLots = await models.Lot.findAll({
        where: {
            owner_id: req.user.id
        },
        include: [{
            model: models.GameItem,
            include: ['Game']
        }, {
            model: models.LotResult,
            include: ['User']
        }, 'LotStatus']
    });
    res.json(ownLots)
}));

router.get('/history', asyncDecorator(async (req, res) => {
    const lots = await models.Lot.findAll({
        include: [{
            model: models.GameItem,
            include: ['Game']
        }, {
            model: models.LotResult,
            include: ['User'],
            required: true
        }, {
            model: models.User,
            where: {
                id: req.user.id
            },
            required: true
        }, 'LotStatus']
    });

    res.json(lots)
}));

router.post('/participate', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    //Получение информации о том, может ли учавствовать пользователь в аукционе(минимальный баланс и трейдбан)
    let transaction;
    const lotId = await parseInt(req.body.id);
    const io = req.app.get('io');
    // const tradeBan = await Bot.getUserInfo(steamid64);

    //Получения лотов, в которых участвует пользователь и вычитание суммы участия
    try {
        transaction = await models.sequelize.transaction();

        console.log('Перед блокировкой');

        let lot = await models.Lot.findOne({
            where: {id: lotId},
            lock: true
        });

        console.log('После снятия блокировки');

        const currentParticipants = await models.User.count({
            include: [{
                model: models.Lot,
                where: {id: lotId}
            }],
        });

        console.log(lot.participants);
        console.log(currentParticipants);

        if (lot.participants > currentParticipants) {
            const [[[user]]] = await models.User.decrement({
                balance: lot.cost_of_participation,
            }, {
                where: {id: req.user.id},
                raw: true
            }, transaction);

            let gameItem = await models.GameItem.findByPk(lot.game_item_id);

            await models.Transaction.create({
                user_id: req.user.id,
                lot_id: lot.id,
                lot_name: gameItem.name,
                type_id: 1,
                amount: lot.cost_of_participation
            }, {transaction});

            const participants = await lot.addUser(user.id, {transaction});

            const userLots = await models.sequelize.query(query.queryLots(user.id), {
                type: models.sequelize.QueryTypes.SELECT,
                transaction
            });

            console.log(lot.current_participants === currentParticipants);

            if (lot.participants === currentParticipants + 1) {
                console.log('Народ набрался');
                const [numberOfUpdatedLots, [updatedLot]] = await models.Lot.update({
                    status_id: 2,
                    collection_end_date: Date.now(),
                    // start_date: Date.now() + 86400000
                    start_date: Date.now() + 10000
                }, {
                    where: {id: lotId},
                    returning: true,
                    transaction
                });

                await transaction.commit();

                const [userLot] = await models.sequelize.query(query.queryOpenLots({
                    canReadOwn: permission.granted,
                    active: true,
                    engage: true,
                    userId: req.user.id,
                    lotId: lot.id,
                    userLots: true
                }), {type: models.sequelize.QueryTypes.SELECT});

                console.log('Да будет сетинтервал!');

                const tsResult = await axios.post(TIME_SERVICE_URL, {
                    lotId,
                    type: 'lot',
                    lastBidId: null,
                    time: userLot.start_date - Date.now(),
                    TIME_SERVICE_SECRET
                });

                io.to(`lots`).emit('closedLot', {id: lotId, status: 2});
                io.to('userLots').emit('userLot', userLot)
            } else {
                console.log('Народ еще не небрался');
                await transaction.commit();
            }

            io.remoteEmitToUser(user.id, 'updateBalance', user.balance);

            io.to(`lot${lotId}`).emit('updateParticipants', {
                id: lotId,
                current_participants: currentParticipants + 1
            });
            res.json(true);
        } else {
            console.log('МЕСТА КОНЧИЛИСЬ!');
            const answer = await transaction.rollback()
        }
    } catch (err) {
        if (transaction) await transaction.rollback()
    }
}));

router.get('/filters', asyncDecorator(async (req, res, next) => {
    let filterList = {};
    switch (req.query.app_id) {
        case '570':
            filterList = await filters.dota.reduce(reducer.bind(this, 'Dota'), {});
            break;
        case '730':
            filterList = await filters.cs.reduce(reducer.bind(this, 'Cs'), {});
            break;
        case '252490':
            filterList = await filters.rust.reduce(reducer.bind(this, 'Rust'), {});
            break;
        default:
            res.status(400).end();
            break;
    }

    const minPrice = await models.Lot.min('price', {
        where: {
            app_id: req.query.app_id,
        }
    });

    const maxPrice = await models.Lot.max('price', {
        where: {
            app_id: req.query.app_id,
        }
    });
    res.send({filterList, maxPrice, minPrice, step: 10});

    async function reducer(game, accP, {model, title, pro}) {
        const acc = await accP;
        let values = await models[game + model].findAll({
            order: [['id', 'ASC']],
            raw: true
        });
        acc[model] = {
            title,
            values: values.map(value => {
                if (!value.name_ru)
                    value.name_ru = value.name;

                return value
            }),
            pro
        };
        return acc
    }
}));

router.get('/stats', asyncDecorator(async (req, res) => {
    let cs = await models.Lot.count({where: {app_id: 730}});
    let dota = await models.Lot.count({where: {app_id: 570}});
    res.send({
        total: cs + dota,
        cs,
        dota
    })
}));

router.post('/cancel/:id', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const id = parseInt(req.params.id);
    const lot = await models.Lot.findByPk(id);

    if (!lot) {
        res.json(false);
        return
    }

    if (lot.owner_id !== req.user.id) {
        res.sendStatus(403);
        return
    }

    lot.status_id = 7;
    await lot.save();
    res.json(true)
}));


module.exports = router;
