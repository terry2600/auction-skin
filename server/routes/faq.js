const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const {asyncDecorator} = require('../asyncDecorator');

const Op = Sequelize.Op;
let accessControl = require('../models');

router.get('/', asyncDecorator( async(req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);

    const rangeCon = Object.entries(filter).length !== 0 ? {
        [Op.or]: [
            {title: {[Op.like]: `%${filter}%`}},
            {text: {[Op.like]: `%${filter}%`}}
        ]
    } : {};

    models.Faq.findAll({
        where: rangeCon,
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(faqs => res.json({data: faqs.slice(range[0], range[1] + 1), total: faqs.length}))
}));

router.get('/:id', asyncDecorator(async (req, res, next) => {
    const id = req.params.id;
    models.Faq.findOne({
        where: {id: id}
    })
        .then(faq => res.json(faq))
}));

router.post('/', asyncDecorator( async(req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createAny('faq');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {title, text, order, tab} = req.body;

    let newFaq = await models.Faq.create({
        title,
        text,
        order,
        tab
    });

    res.json(newFaq.id)
}));

router.put('/:data', asyncDecorator( async(req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('faq');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {id, title, text, order, tab} = req.body;

    let newFaq = await models.Faq.update({
        title,
        text,
        order,
        tab
    }, {
        where: {
            id
        }
    });

    res.json(newFaq)
}));

router.delete('/:ids', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).deleteAny('faq');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let r = await models.Faq.destroy({
        where: {
            id: {
                [Op.in]: JSON.parse(req.params.ids)
            }
        }
    });

    res.sendStatus(200)
}));

module.exports = router;
