const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Bot = require('../config/bots');
const models = require('../models');
const fetch = require('node-fetch');
let accessControl = require('../models');
const {asyncDecorator} = require('../asyncDecorator');
const {
    TRADE_SERVICE_SECRET
} = require('../config/constants');

const checks = {
    float: async (items, steamId, marketName, float) => {
        const floats = await Promise.all(items
            .filter(item => item.market_hash_name === marketName)
            .map(async item => await Bot.getFloat(steamId, item))
        );

        return floats.includes(float)
    },
    default: (items, marketName) => {
        items.filter(item => item.market_hash_name === marketName);

        return items.length > 0
    },
    advanced: async ({apiKeyWinner, apiKeyOwner, assetId, lotId}) => {
        const message = lotId.toString();
        const tradesWinner = await checks.getTrades({apiKey: apiKeyWinner});
        const tradesOwner = await checks.getTrades({apiKey: apiKeyOwner});

        const ownerSentTrade = checks.getTrade({assetId, get: false, message, trades: tradesOwner.sent});
        console.log({ownerSentTrade, p: {assetId, get: false, message, trades: tradesOwner}});
        if (ownerSentTrade) {
            if (ownerSentTrade.trade_offer_state === 9)
                return 0;

            if (ownerSentTrade.trade_offer_state === 2)
                return 2;

            if (ownerSentTrade.trade_offer_state === 3)
                return 1
        }

        const winnerSentTrade = checks.getTrade({assetId, get: true, message, trades: tradesOwner.sent});
        if (winnerSentTrade) {
            if (winnerSentTrade.trade_offer_state === 9)
                return 0;

            if (winnerSentTrade.trade_offer_state === 3)
                return 1
        }

        return 0
    },
    getTrades: async function ({apiKey}) {
        if (!apiKey)
            return false;

        const {response} = await fetch(`https://api.steampowered.com/IEconService/GetTradeOffers/v1?key=${apiKey}&time_historical_cutoff=${Math.floor(Date.now() / 1000 - 60 * 60 * 24 * 30)}&get_received_offers=true&get_sent_offers=true`)
            .then(res => res.status === 403 ? {response: undefined} : res.json());

        if (!response)
            return {
                received: [],
                sent: []
            };

        return {
            received: response.trade_offers_received,
            sent: response.trade_offers_sent
        }
    },
    getTrade: function ({assetId, get, message, trades}) {
        if (!trades)
            return;

        return get ?
            trades.find(trade => trade.message === message && trade.items_to_receive && trade.items_to_receive.some(recvItem => recvItem.assetid === assetId)) :
            trades.find(trade => trade.message === message && trade.items_to_give && trade.items_to_give.some(sentItem => sentItem.assetid === assetId))
    },
    stopTrade: async function ({apiKey, tradeofferid, their}) {
        if (!apiKey || !tradeofferid)
            return;

        const {response} = await fetch(`https://api.steampowered.com/IEconService/${their ? 'DeclineTradeOffer' : 'CancelTradeOffer'}/v1/?key=${apiKey}&tradeofferid=${tradeofferid}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json;'
            },
            body: JSON.stringify({key: apiKey, tradeofferid})
        })
            .then(res => res.status === 403 ? {response: undefined} : res.json());

        return response
    }
};

const actions = {
    tradeSuccess: async ({lot, status_id = 6}) => {
        let transaction = await models.sequelize.transaction();

        const [numberOfUpdatedLots] = await models.Lot.update({
            status_id,
            trade_ended_date: Date.now()
        }, {
            where: {id: lot.id},
            transaction
        });

        const lastBid = await models.Bid.findOne({
            where: {
                lot_id: lot.id
            },
            order: [['date', 'DESC']],
            raw: true,
            transaction
        });

        await models.Transaction.create({
            user_id: lot.owner_id,
            lot_id: lot.id,
            lot_name: lot.GameItem.name,
            type_id: 4,
            amount: lot.price
        }, {transaction});

        await models.Transaction.create({
            user_id: lot.LotResult.User.id,
            lot_id: lot.id,
            lot_name: lot.GameItem.name,
            type_id: 5,
            amount: lastBid.bet_size
        }, {transaction});

        const betSize = money.floatToAmount(lastBid.bet_size);
        const cashBack = await models.User.getCashback(lot.LotResult.User.id);

        if (cashBack !== '0.00')
            await models.Transaction.create({
                user_id: lot.LotResult.User.id,
                lot_id: lot.id,
                lot_name: lot.GameItem.name,
                type_id: 6,
                amount: money.mul(betSize, cashBack)
            }, {transaction});

        const [[[owner], count]] = await models.User.increment('balance', {
            by: lot.price,
            where: {id: lot.owner_id},
            transaction
        });

        io.remoteEmitToUser(owner.id, 'updateBalance', owner.balance);

        const winnerNotification = await models.Notification.create({
            user_id: lot.owner_id,
            type_id: 8,
            body: {
                lot_id: 5
            }
        }, {transaction});

        io.remoteEmitToUser(winnerNotification.user_id, 'notification', winnerNotification);

        const ownerNotification = await models.Notification.create({
            user_id: lot.LotResult.User.id,
            type_id: 8,
            body: {
                lot_id: 5
            }
        }, {transaction});

        io.remoteEmitToUser(ownerNotification.user_id, 'notification', ownerNotification);

        await transaction.commit();
    },
    tradeFail: async ({lot}) => {
        let transaction = await models.sequelize.transaction();

        const owner = await models.User.findByPk(lot.owner_id);
        await actions.removeOwnerTrade({lot, owner});

        const lastBid = await models.Bid.findOne({
            where: {
                lot_id: lot.id
            },
            attributes: ['user_id', 'bet_size'],
            order: [['date', 'DESC']],
            raw: true,
            transaction
        });

        const [[[winner], count]] = await models.User.increment('balance', {
            by: lastBid.bet_size,
            where: {id: lastBid.user_id},
            transaction
        });

        io.remoteEmitToUser(winner.id, 'updateBalance', winner.balance);

        const [numberOfUpdatedLots] = await models.Lot.update({
            status_id: 7,
            trade_ended_date: Date.now()
        }, {
            where: {id: lot.id},
            transaction
        });

        const participants = await models.Participants.findAll({
            where: {
                lot_id: lot.id
            }
        });

        await models.Transaction.bulkCreate(participants.map(participant => {
                return {
                    user_id: participant.user_id,
                    lot_id: lot.id,
                    lot_name: lot.GameItem.name,
                    type: 2,
                    amount: lot.cost_of_participation
                }
            }),
            {transaction});

        for (let participant of participants) {
            const [[[participantUser], count]] = await models.User.increment('balance', {
                by: lot.cost_of_participation,
                where: {id: participant.user_id},
                transaction
            });

            io.remoteEmitToUser(participantUser.id, 'updateBalance', participantUser.balance);

            const notification = await models.Notification.create({
                user_id: participant.user_id,
                type_id: 9,
                body: {
                    item_icon: lot.GameItem.icon_url,
                    item_name: lot.GameItem.name
                }
            }, {transaction});

            io.remoteEmitToUser(notification.user_id, 'notification', notification)
        }

        await transaction.commit();

        // вернуть всем деньги поставить статус 5
    },
    tradeToBot: async ({lot}) => {
        const owner = await models.User.findByPk(lot.owner_id);
        await actions.removeOwnerTrade({lot, owner});
        const trade = await Bot.getUserItem({
            lotId: lot.id,
            steamId: owner.steamid64,
            tradeUrl: owner.trade_url,
            assetId: lot.GameItem.asset_id,
            appId: lot.GameItem.app_id
        });

        const [numberOfUpdatedLots] = await models.Lot.update({
            status_id: 4,
            trade_ended_date: Date.now()
        }, {
            where: {id: lot.id}
        });

        console.log(trade);
        return trade

        // кинуть обмен от лица бота владельцу предмета
        // если обмен успешный поставить статус 8 "Завершен, предмет находится у бота"
        // если нет this.tradeFail
    },
    removeOwnerTrade: async ({lot, owner}) => {
        const message = lot.id.toString();
        const trades = await checks.getTrades({apiKey: owner.api_key});

        const sentTrade = await checks.getTrade({
            assetId: lot.GameItem.asset_id,
            get: false,
            message,
            trades: trades.sent
        });
        if (sentTrade) {
            const cancelTrade = await checks.stopTrade({
                apiKey: owner.api_key,
                tradeofferid: sentTrade.tradeofferid,
                their: false
            });
            console.log({cancelTrade})
        }

        const recvTrade = await checks.getTrade({
            assetId: lot.GameItem.asset_id,
            get: false,
            message,
            trades: trades.received
        });
        if (recvTrade) {
            const declineTrade = await checks.stopTrade({
                apiKey: owner.api_key,
                tradeofferid: sentTrade.tradeofferid,
                their: true
            });
            console.log({declineTrade})
        }
    }
};

let io;

const checkOvertimeTrades = async () => {
    if (!io) {
        sockets = require('../sockets');
        io = sockets.io
    }

    const lots = await models.Lot.findAll({
        where: {
            status_id: 4,
            [Op.or]: [{
                end_date: {
                    [Op.lte]: new Date(Date.now() - 24 * 60 * 60 * 1000)
                },
                trade_ended_date: null
            }, {
                end_date: {
                    [Op.lte]: new Date(Date.now() - 24 * 60 * 60 * 1000)
                },
                trade_ended_date: {
                    [Op.lte]: new Date(Date.now() - 24 * 60 * 60 * 1000)
                }
            }]
        },
        include: ['GameItem', {
            model: models.LotResult,
            include: ['User']
        }]
    });

    console.log('checkOvertimeTrades count: ', lots.length);

    for (const lot of lots) {

        if (lot.status_id === 6)
            continue;

        const items = await Bot.getUserInventory(lot.GameItem.app_id, lot.LotResult.User.steamid64);
        let checkRes = checks.default(items, lot.GameItem.market_hash_name);

        if (checkRes) {
            switch (lot.app_id) {
                case 730:
                    let {float} = await models.CsParams.findOne({where: {asset_id: lot.GameItem.asset_id}});
                    checkRes = await checks.float(items, lot.LotResult.User.steamid64, lot.GameItem.market_hash_name, float);
                    break;
            }
        }

        console.log({id: lot.id, checkRes});

        if (checkRes) {
            await actions.tradeSuccess({lot})
        } else {
            const owner = await models.User.findByPk(lot.owner_id);
            checkRes = await checks.advanced({
                apiKeyOwner: owner.api_key,
                apiKeyWinner: lot.LotResult.User.api_key,
                assetId: lot.GameItem.asset_id,
                lotId: lot.id
            });

            console.log({id: lot.id, checkRes});

            if (checkRes === 0) {
                await actions.tradeFail({lot})
            } else if (checkRes === 1) {
                await actions.tradeSuccess({lot})
            } else if (checkRes === 2) {
                await actions.tradeToBot({lot})
            }
        }
    }
};

setTimeout(checkOvertimeTrades, 5000);

router.get('/ended', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const lots = await models.Lot.findAll({
        where: {
            [Op.or]: [{
                status_id: {
                    [Op.in]: [5, 6]
                },
                owner_id: req.user.id
            }, {
                status_id: {
                    [Op.in]: [5, 6]
                },
                '$LotResult.winner_id$': req.user.id
            }]
        },
        include: ['GameItem', {
            model: models.LotResult,
            required: true,
            include: ['User']
        }]
    });

    res.send(lots.map(lot => {
        return {
            id: lot.id,
            name: lot.GameItem.market_name,
            date: lot.create_date,
            got: lot.LotResult.User.id === req.user.id,
            status: lot.status_id
        }
    }))
}));

router.get('/waiting', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    };

    const lots = await models.Lot.findAll({
        where: {
            status_id: 8,
            '$LotResult.winner_id$': req.user.id,
            trade_ended_date: {
                [Op.lte]: new Date(Date.now() - 7 * 24 * 60 * 60 * 1000)
            }
        },
        include: ['GameItem', {
            model: models.LotResult,
            required: true
        }]
    });

    res.send(lots.map(lot => {
        return {
            id: lot.id,
            name: lot.GameItem.market_name,
            date: lot.create_date
        }
    }))
}));

router.get('/requstFromBot/:lotId', asyncDecorator(async (req, res) => {
    if (!req.user)
        return res.sendStatus(403);

    const lot = await models.Lot.findByPk(parseInt(req.params.lotId), {
        include: ['GameItem', {
            model: models.LotResult,
            required: true
        }]
    });

    if (!lot || lot.LotResult.winner_id !== req.user.id || lot.status_id !== 8)
        return res.sendStatus(403);


    await Bot.sendItemToWinner({
        lotId: lot.id,
        tradeUrl: req.user.trade_url,
        assetId: lot.GameItem.asset_id,
        appId: lot.GameItem.app_id,
        contextId: 2
    });

    res.json(true)
}));

router.post('/sentOfferChanged', asyncDecorator(async (req, res) => {
    const {
        offer,
        lotInfo
    } = req.body;

    if (req.body.TRADE_SERVICE_SECRET !== TRADE_SERVICE_SECRET)
        return res.sendStatus(403);

    if (!lotInfo)
        return res.sendStatus(403);

    const lot = await models.Lot.findByPk(lotInfo.id);

    if (lotInfo.type === 'get') {
        if (offer.state === 3)
            await actions.tradeSuccess({lot, status_id: 8});
        else if ([4, 5, 6, 7, 8, 10].includes(offer.state))
            await actions.tradeFail({lot})
    } else if (lot.type === 'send') {
        if ([3, 4, 5, 6, 7, 8, 10].includes(offer.state))
            await models.Lot.update({
                status_id: 6,
                trade_ended_date: Date.now()
            }, {
                where: {id: lot.id},
                row: true
            })
    }

    res.sendStatus(200)
}));

module.exports = router;
