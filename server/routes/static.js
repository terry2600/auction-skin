const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');

const Op = Sequelize.Op;
let accessControl = require('../models');
const {asyncDecorator} = require('../asyncDecorator');

router.get('/', asyncDecorator(async (req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);

    const rangeCon = Object.entries(filter).length !== 0 ? {
        [Op.or]: [
            {title: {[Op.like]: `%${filter}%`}},
            {html: {[Op.like]: `%${filter}%`}},
            {page: {[Op.like]: `%${filter}%`}}
        ]
    } : {};

    models.Static.findAll({
        where: rangeCon,
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(statics => res.json({data: statics.slice(range[0], range[1] + 1), total: statics.length}))
}));

router.get('/id/:id', asyncDecorator(async (req, res, next) => {
    const id = req.params.id;
    models.Static.findOne({
        where: {id}
    })
        .then(statics => res.json(statics))
}));

router.get('/:page', asyncDecorator(async (req, res, next) => {
    const page = req.params.page;
    const language = req.query.language ? req.query.language : 'en';

    models.Static.findOne({
        where: {
            page,
            language
        }
    })
        .then(statics => res.json(statics))
}));

router.post('/', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createAny('static');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {title, html, page, language} = req.body;

    let newStatic = await models.Static.create({
        title,
        html,
        page,
        language
    });

    res.json(newStatic.id)
}));

router.put('/:data', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('static');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {id, title, html, page, language} = req.body;
    let newStatic = await models.Static.update({
        title,
        html,
        page,
        language
    }, {
        where: {
            id
        }
    });

    res.json(newStatic)
}));

router.delete('/:ids', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).deleteAny('static');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let r = await models.Static.destroy({
        where: {
            id: {
                [Op.in]: JSON.parse(req.params.ids)
            }
        }
    });

    res.sendStatus(200)
}));

module.exports = router;
