const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const Bot = require('../config/bots');
const {asyncDecorator} = require('../asyncDecorator');

let accessControl = require('../models');
const Op = Sequelize.Op;

router.get('/:id', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('stats');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const users = await models.User.count();
    const doneAuctions = await models.Lot.count({
        where: {
            status_id: 6
        }
    });

    const money = await models.Transaction.sum('amount');
    let itemsPrice = await Bot.getBotInventoryPrice(730);
    itemsPrice += await Bot.getBotInventoryPrice(570);

    res.json({
        users,
        money,
        doneAuctions,
        itemsPrice
    })
}));

router.get('/', asyncDecorator(async (req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('bots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const today = (column) =>
        models.sequelize.where(models.sequelize.fn('DATE', models.sequelize.col(column)), models.sequelize.literal('CURRENT_DATE'));

    const {startDate, endDate} = filter;

    const inDate = {
        [Op.between]: [startDate, endDate]
    };

    const users = await models.User.count({
        where: {
            registration_date: inDate
            //[Op.and]: today('registration_date')
        }
    });

    const doneAuctionsCs = await models.Lot.count({
        where: {
            app_id: 730,
            status_id: 6,
            end_date: inDate
        }
    });

    const doneAuctionsDota = await models.Lot.count({
        where: {
            app_id: 570,
            status_id: 6,
            end_date: inDate
        }
    });

    const doneAuctions = await models.Lot.count({
        where: {
            status_id: 6,
            end_date: inDate
        }
    });

    const openAuctionsCs = await models.Lot.count({
        where: {
            app_id: 730,
            status_id: 1,
            create_date: inDate
        }
    });

    const openAuctionsDota = await models.Lot.count({
        where: {
            app_id: 570,
            status_id: 1,
            create_date: inDate
        }
    });

    const openAuctions = await models.Lot.count({
        where: {
            status_id: 1,
            create_date: inDate
        }
    });

    const allLots = await models.Lot.count({
        where: {
            create_date: inDate
        }
    });

    const allLotsCs = await models.Lot.count({
        where: {
            app_id: 730,
            create_date: inDate
        }
    });

    const allLotsDota = await models.Lot.count({
        where: {
            app_id: 570,
            create_date: inDate
        }
    });

    res.json({
        data: [{
            title: 'Новых пользоватей',
            value: users
        }, {
            title: 'Проведенных лотов (cs)',
            value: doneAuctionsCs
        }, {
            title: 'Проведенных лотов (dota)',
            value: doneAuctionsDota
        }, {
            title: 'Проведенных лотов (общ.)',
            value: doneAuctions
        }, {
            title: 'Открытых лотов (cs)',
            value: openAuctionsCs
        }, {
            title: 'Открытых лотов (dota)',
            value: openAuctionsDota
        }, {
            title: 'Открытых лотов (общ.)',
            value: openAuctions
        }, {
            title: 'Всего лотов (cs)',
            value: allLotsCs
        }, {
            title: 'Всего лотов (dota)',
            value: allLotsDota
        }, {
            title: 'Всего лотов (общ.)',
            value: allLots
        }],
        total: 4
    })

    /*
    models.Static.findAll({
        where: {},
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(statics => res.json({data: statics.slice(range[0], range[1] + 1), total: statics.length}))*/
}));

module.exports = router;

/*

    <TextField source="period"/>
    <TextField source="users"/>
    <TextField source="money"/>
    <TextField source="auctions"/>
    <TextField source="bots"/>
    <TextField source="items"/>
    <TextField source="itemsPrice"/>

*/
