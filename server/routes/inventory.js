const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const Bot = require('../config/bots');
const query = require('../config/sql-queries');
const {STEAM_IMAGE_URL} = require('../config/constants');
const {getAvailableInventory} = require('../config/getAvailableInventory');
const {asyncDecorator} = require('../asyncDecorator');

const Op = Sequelize.Op;
let accessControl = require('../models');

router.get('/', asyncDecorator(async (req, res, next) => {
    const permission = accessControl.ac.can(req.user.role).readAny('bots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);

    const availableInventory = await getAvailableInventory(false, true, filter.game, req.user.id);

    const result = {
        items: availableInventory.slice(range[0], range[1] + 1),
        total: availableInventory.length
    };

    result.items.forEach(item => {
        item.icon_url = STEAM_IMAGE_URL + item.icon_url;
    });

    res.json({data: result.items, total: result.total})
}));

// Создание лота с сохранением параметров предмета
router.post('/', asyncDecorator(async (req, res, next) => {
    const io = req.app.get('io');
    const permission = accessControl.ac.can(req.user.role).createOwn('lots');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let lotInfo = req.body;

    lotInfo.item = await getAvailableInventory(true, accessControl.ac.can(req.user.role).createAny('lots').granted, lotInfo.item.appid, req.user.id, req.user.steamid64, lotInfo.item.assetid);

    if (!lotInfo.item) {
        return res.sendStatus(412)
    }

    let transaction;
    try {
        let filters = {};
        let itemParams;
        transaction = await models.sequelize.transaction();

        switch (lotInfo.item.appid) {
            case 730:
                //Пробегаемся по всем параметрам предмета и ищем их в соовтествующих таблицах
                for (const tag of lotInfo.item.tags) {
                    const model = models['Cs' + tag.category];
                    if (!model)
                        continue;

                    filters[tag.category] = await model.findOne({
                        where: {internal_name: tag.internal_name},
                        transaction

                    });
                }

                const float = await Bot.getFloat(req.user.steamid64, lotInfo.item);
                /*
                wear = float
                does not work
                let wear = await Bot.getWear(req.user.steamid64, item.assetid)
                console.log('WEAR: ', wear)
                */

                itemParams = await models.CsParams.findOrCreate({
                        where: {
                            asset_id: lotInfo.item.assetid,
                        },
                        defaults: {
                            asset_id: lotInfo.item.assetid,
                            exterior_id: filters.Exterior.id,
                            item_set_id: filters.ItemSet.id,
                            quality_id: filters.Quality.id,
                            rarity_id: filters.Rarity.id,
                            type_id: filters.Type.id,
                            weapon_id: filters.Weapon.id,
                            float
                        }, transaction
                    }
                );
                break;
            case 570 :
                for (const tag of lotInfo.item.tags) {
                    filters[tag.category] = await models['Dota' + tag.category].findOne({
                        where: {internal_name: tag.internal_name},
                        transaction
                    });
                }

                itemParams = await models.DotaParams.findOrCreate({
                        where: {
                            asset_id: lotInfo.item.assetid,
                        },
                        defaults: {
                            slot_id: filters.Slot.id,
                            hero_id: filters.Hero.id,
                            quality_id: filters.Quality.id,
                            rarity_id: filters.Rarity.id,
                            type_id: filters.Type.id,
                        }, transaction
                    }
                );
                break;
            case 252490:
                for (const tag of lotInfo.item.tags) {
                    let category = tag.category;
                    switch (category) {
                        case 'itemclass':
                            category = 'Type'
                            break;
                        case 'steamcat':
                            category = 'Category'
                            break;
                    }

                    console.log('Rust' + category)

                    filters[category] = await models['Rust' + category].findOne({
                        where: {internal_name: tag.internal_name},
                        transaction
                    });
                }

                itemParams = await models.RustParams.findOrCreate({
                        where: {
                            asset_id: lotInfo.item.assetid,
                        },
                        defaults: {
                            category_id: filters.Category.id,
                            type_id: filters.Type.id,
                        }, transaction
                    }
                );
                break;
        }

        let stickers = lotInfo.item.descriptions.find(description => description.value.includes('stickers'));
        if (stickers)
            stickers = stickers.value;

        const [gameItem, created] = await models.GameItem.findOrCreate({
            where: {
                asset_id: lotInfo.item.assetid
            },
            defaults: {
                app_id: lotInfo.item.appid,
                asset_id: lotInfo.item.assetid,
                icon_url: STEAM_IMAGE_URL + lotInfo.item.icon_url,
                icon_url_large: STEAM_IMAGE_URL + lotInfo.item.icon_url_large,
                name: lotInfo.item.name,
                name_color: lotInfo.item.name_color,
                market_name: lotInfo.item.market_name,
                market_hash_name: lotInfo.item.market_hash_name,
                marketable: lotInfo.item.marketable,
                stickers
            },
            raw: true,
            transaction
        });

        let createdLot = await models.Lot.create({
            game_item_id: gameItem.id,
            status_id: 1,
            create_date: Date.now(),
            expired_date: Date.now() + 1000 * 60 * 60 * 24,
            owner_id: req.user.id,
            hotlot: lotInfo.hotlot,
            price: lotInfo.price,
            cost_of_participation: Math.ceil(lotInfo.price / lotInfo.participants), //lotInfo.cost_of_participation,
            lot_step: lotInfo.lot_step,
            participants: lotInfo.participants,
            color: filters.Rarity ? filters.Rarity.color : '#FF00FF',
            app_id: lotInfo.item.appid
        }, {
            transaction
        });

        await transaction.commit();

        res.json({
            lot: createdLot
        });

        if (createdLot.hotlot) {
            const openLot = await models.sequelize.query(query.queryOpenLots({
                lotId: createdLot.id,
                hotlot: true
            }), {type: models.sequelize.QueryTypes.SELECT});
            io.to('lots').emit('createdHotLot', openLot[0])
        }

        io.to(createdLot.app_id).emit('createdLot')

    } catch (err) {
        if (transaction) await transaction.rollback();
        throw err
    }
}));

module.exports = router;
