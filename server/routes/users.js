const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
let accessControl = require('../models');
const Bot = require('../config/bots');
const query = require('../config/sql-queries');
const {getAvailableInventory} = require('../config/getAvailableInventory');
const {asyncDecorator} = require('../asyncDecorator');

const Op = Sequelize.Op;

router.get('/', asyncDecorator(async (req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const rangeCon = Object.entries(filter).length !== 0 ? {
        role_id: 2,
        [Op.or]: [
            {id: {[Op.like]: `%${filter}%`}},
            {steamid64: {[Op.like]: `%${filter}%`}},
            {steam_nick: {[Op.like]: `%${filter}%`}},
        ]
    } : {role_id: 2};

    models.User.findAll({
        where: rangeCon,
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(users => res.json({data: users.slice(range[0], range[1] + 1), total: users.length}))
}));

router.get('/tradeable', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    res.send(await Bot.getUserEscrow(req.user.trade_url))
}));

router.get('/transactions', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const transactions = await models.Transaction.findAll({
        where: {
            user_id: req.user.id
        },
        include: [{
            model: models.TransactionType,
            required: true
        }]
    });

    res.json(transactions)
}));

router.get('/lots/won', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const won = await models.LotResult.count({
        where: {
            winner_id: req.user.id
        }
    });

    res.json(won)
}));

router.get('/lots', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const participated = await models.Participants.count({
        where: {
            user_id: req.user.id
        }
    });

    res.json(participated)
}));

router.post('/setTradeUrl', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateOwn('users');

    if (!permission.granted) {
        res.json(false);
        return
    }

    const {tradeUrl} = req.body;

    if (Bot.getSteamId64FromTradeUrl(tradeUrl) !== req.user.steamid64)
        return res.json(false);

    const {escrow} = await Bot.getUserEscrow(req.user.trade_url)
    if(escrow === false)
        return res.json(false)

    const [update, [updatedUser]] = await models.User.update({

        trade_url: tradeUrl.toString()
    }, {
        where: {
            id: req.user.id
        },
        returning: true
    });

    if (update)
        res.json(updatedUser);
    else
        res.json(false)
}));

router.get('/referer/:steamId', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const steamid64 = req.params.steamId;
    const referer = await models.User.findOne({
        where: {
            steamid64
        }
    });

    if (referer)
        res.cookie('referer', steamid64, {
            maxAge: 1000 * 60 * 60 * 24
        });

    res.redirect('/')
}));

router.get('/referals/cashback', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    res.json(await models.User.getCashback(req.user.id))
}));

router.get('/referals', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const referals = await models.User.findAll({where: {referer: req.user.id}})
    res.send(referals.map(({registration_date, steam_nick}) => {
        return {
            registration_date,
            steam_nick
        }
    }))
}));


router.get('/notifications', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const notifications = await models.Notification.findAll({where: {user_id: req.user.id}})
    res.send(notifications)
}));

router.get('/:steamId', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }
    const steamId = req.params.steamId;

    const [[user], result] = await models.sequelize.query(query.queryUser(steamId), {raw: true});

    res.json(user)
}));

router.get('/:steamId/inventory/:appId', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('users');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const {steamId, appId} = req.params;

    const availableInventory = await getAvailableInventory(false, false, appId, req.user.id, steamId);

    res.json(availableInventory);
}));
// router.post('/', (req, res, next) => {
//     let {name, nickname, email} = req.body;
//     let sql = `INSERT INTO users (id, name, nickname, email)
//                 VALUES (DEFAULT, '${name}', '${nickname}', '${email}')`;
//     db.query(sql)
//         .then(() => {
//             sql = 'SELECT * FROM users ORDER BY id DESC LIMIT 1';
//             db.query(sql, {type: db.QueryTypes.SELECT})
//                 .then(users => {
//                     res.json(users)
//                 })
//         })
//
// });
//
// router.delete('/:id', (req, res) => {
//     let sql;
//     if (typeof JSON.parse(req.params.id) === 'number') {
//         sql = `DELETE FROM users WHERE id = ${req.params.id}`;
//         db.query(sql)
//             .then(result => res.json({id: req.params.id}))
//     } else {
//
//         let response = JSON.parse(req.params.id);
//         sql = `DELETE FROM users WHERE id IN (${response.ids})`;
//         db.query(sql)
//             .then(result => res.json(response.ids))
//     }
// });
//
// router.put('/:data', (req, res) => {
//     let {id, name, nickname, email} = req.body;
//     let sql = `UPDATE users SET name='${name}', nickname='${nickname}', email='${email}' WHERE id='${id}'`;
//     db.query(sql)
//         .then(() => {
//             sql = 'SELECT * FROM users ORDER BY id DESC LIMIT 1';
//             db.query(sql, {type: db.QueryTypes.SELECT})
//                 .then(users => {
//                     console.log(users);
//                     res.json(users)
//                 })
//         })
// });

module.exports = router;
