const express = require('express');
const passport = require('passport');
const router = express.Router();
const models = require('../models');
const {CLIENT_HOME_PAGE_URL, FAILURE_REDIRECT_URL} = require('../config/constants');
const {asyncDecorator} = require('../asyncDecorator');

router.get('/steam', passport.authenticate('steam'));

router.get('/steam/return',
    passport.authenticate('steam', {
        failureRedirect: FAILURE_REDIRECT_URL
    }), asyncDecorator(async (req, res) => {
        if(req.authInfo.new && req.cookies.referer){
            const referer = await models.User.findOne({where: {steamid64: req.cookies.referer}});
            if(referer && req.user)
                req.authInfo.user.referer = referer.id,
                await req.authInfo.user.save()
        }
        res.redirect(CLIENT_HOME_PAGE_URL)
    }
));

router.get('/steam/success', asyncDecorator( async(req, res) => {
    if (req.user) {
        res.json({
            user: req.user,
        })
    } else {
        res.json({
            userName: null,
        })
    }
}));

router.get('/logout', asyncDecorator(async (req, res) => {
    req.logout();
    req.session.destroy();
    res.redirect(CLIENT_HOME_PAGE_URL);
}));

module.exports = router;
