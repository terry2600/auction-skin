const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const Bot = require('../config/bots');
const {asyncDecorator} = require('../asyncDecorator');

const Op = Sequelize.Op;
let accessControl = require('../models');

router.get('/', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('bots');
    if (permission.granted) {
        const sort = JSON.parse(req.query.sort);
        const range = JSON.parse(req.query.range);
        const filter = JSON.parse(req.query.filter);
        const rangeCon = Object.entries(filter).length !== 0 ? {
            role_id: 3,
            [Op.or]: [
                {id: {[Op.like]: `%${filter}%`}},
                {steamid64: {[Op.like]: `%${filter}%`}},
                {steam_nick: {[Op.like]: `%${filter}%`}},
            ]
        } : {role_id: 3};


        models.User.findAll({
            where: rangeCon,
            order: [
                [sort[0], sort[1]]
            ]
        })
            .then(bots => res.json({data: bots.slice(range[0], range[1] + 1), total: bots.length}))
    } else {
        res.status(403).end()
    }
}));

router.get('/:id', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readAny('bots');
    if (permission.granted) {
        const id = req.params.id;

        console.log(id);

        models.User.findOne({
            where: {id: id}
        })
            .then(user => res.json(user))
    } else {
        res.status(403).end()
    }
}));

router.post('/', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createAny('bots');
    if (permission.granted) {
        const {steamid64} = req.body;

        const persona = await Bot.getUserInfo([steamid64]);

        const [user, created] = await models.User.findOrCreate({
            where: {
                steamid64: steamid64
            },
            defaults: {
                steamid64: steamid64,
                steam_nick: persona[steamid64].player_name,
                role_id: 3
            }
        });

        const {id} = user.dataValues;
        res.json(id)
    } else {
        res.status(403).end()
    }
}));

router.delete('/:ids', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).deleteAny('bots');
    if (permission.granted) {
        const ids = JSON.parse(req.params.ids);

        models.User.destroy({
            where: {
                id: {
                    [Op.in]: ids
                }
            }
        })
            .then(res.json(ids))
    } else {
        res.status(403).end()
    }
}));

module.exports = router;
