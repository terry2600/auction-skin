const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const Bot = require('../config/bots');
const models = require('../models');
let accessControl = require('../models');
const crypto = require('crypto');
const money = require('money-math');
const axios = require('axios');
const {asyncDecorator} = require('../asyncDecorator');
const {
    PAYMENT_SERVICE_URL,
    PAYMENT_SERVICE_SECRET
} = require('../config/constants');

const services = {
    cypix: {
        deposit: [
            {
                system: "mc",
                title: "Мобильная коммерция"
            },
            {
                system: "card",
                title: "Карты"
            },
            {
                system: "alfa_money",
                title: "Альфабанк"
            },
            {
                system: "psbank",
                title: "ПС Банк"
            },
            {
                system: "qiwi_wallet",
                title: "Киви"
            },
            {
                system: "sber_online",
                title: "Сбербанк"
            },
            {
                system: "yandex_wallet",
                title: "Яндекс"
            },
            {
                system: "webmoney",
                title: "Webmoney"
            }
        ],
        withdraw: [
            {
                system: "card_ru",
                title: "Пополнение VisaMastercard РФ"
            },
            {
                system: "card_mir_ru",
                title: "Пополнение МИР РФ"
            },
            {
                system: "card_ua",
                title: "Пополнение VisaMastercard Украина"
            },/*
            {
                system: "card_world",
                title: "Пополнение VisaMastercard иностранных банков"
            },*/
            {
                system: "mobile",
                title: "Автоопределение оператора мобильной связи"
            },
            {
                system: "mobile_mts",
                title: "РФ МТС"
            },
            {
                system: "mobile_beeline",
                title: "РФ Билайн"
            },
            {
                system: "mobile_megafon",
                title: "РФ Мегафон"
            },
            {
                system: "mobile_tele2",
                title: "РФ Tele2"
            },/*
            {
                system: "wmr",
                title: "WebMoney R"
            },
            {
                system: "wmz",
                title: "WebMoney Z"
            },*/
            {
                system: "yandex",
                title: "Yandex.Money"
            },
            {
                system: "qiwi",
                title: "Qiwi кошелек"
            }
        ]
    }
};

const currencies = ['RUB'];

router.get('/:service/available', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return

    }

    const service = req.params.service;
    const amount = money.floatToAmount(req.query.amount);

    const balance = await axios.post(`${PAYMENT_SERVICE_URL}/${service}/balance`, {
        PAYMENT_SERVICE_SECRET
    }).then(res => money.floatToAmount(res.data.balance));

    const available = money.isNegative(money.subtract(amount, balance));

    res.send(available);
}));

router.get('/', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    const deals = await models.Deal.findAll({
        where: {
            user_id: req.user.id
        }
    });

    res.send(deals);
}));

router.get('/confirm', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!unitPayIps.includes(req.connection.remoteAddress) || !permission.granted) {
        res.sendStatus(403);
        return
    }

    const user = await models.User.findByPk(req.query.account);
    if(!user)
        res.json({
            error: {
                   message: "Пользователь не найден"
            }
        });

    const signature = req.query.params.signature;
    delete req.query.params.signature;
    delete req.query.params.sign;

    const paramValues = Object.entries(req.query.params).sort(([key], [key2]) => key.localeCompare(key2)).map(([key, value]) => value);

    const separator = '{up}';
    const checkSign = [req.query.method, ...paramValues, secretKey];
    const hash = crypto.createHash('sha256').update(checkSign.join(separator)).digest('hex');
    //res.json(hash)

    console.log(hash, signature);
    if(hash !== signature)
        res.json({
            error: {
        	       message: "Несовпадение подписи"
            }
        });

    user.balance += orderSum;
    await user.save();

    await models.Deal.create({
        user_id: user.id,
        amount: orderSum,
        status: 'success',
        type: 1
    });

    res.json({
        result: {
            message: "Запрос успешно обработан"
        }
    })
}));

/*router.post('/signature', async (req, res) => {
    if(!req.user)
        return res.sendStatus(403)

    let {
        amount,
        service
    } = req.body

    if(!Object.keys(services).includes(service))
        return res.send({status: false, err: 'Такого платежного сервиса не существует.'})

    amount = money.floatToAmount(amount)
    if(money.isNegative(money.subtract(amount, '1.00')))
        return res.send({status: false, err: 'Нельзя вывести менее 1 руб.'})

    const deal = await models.Deal.create({
        user_id: req.user.id,
        sign: -1,
        amount,
        service,
        system: null,
        purse: null,
        status: 1,
    });

    const psRes = await axios.get(`${PAYMENT_SERVICE_URL}/${system}/signature`, {user_id: deal.user_id, deal_id: deal.id, amount, service})
})*/

router.get('/variants', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).readOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    res.send(services)
}));

router.post('/deposit', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createOwn('deals');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {
        service,
        amount,
        currency,
        system,
    } = req.body;

    if(!currencies.includes(currency))
        return res.send({status: false, err: 'Данная валюта недоступна для ввода.'});

    if(!Object.keys(services).includes(service))
        return res.send({status: false, err: 'Такого платежного сервиса не существует.'});

    if(!services[service].deposit.find(el => el.system === system))
        return res.send({status: false, err: 'Такой платежной системы не существует.'});

    amount = money.floatToAmount(amount);
    if(money.isNegative(money.subtract(amount, '1.00')))
        return res.send({status: false, err: 'Нельзя ввести менее 1 руб.'});

    const deal = await models.Deal.create({
        user_id: req.user.id,
        sign: 1,
        currency,
        amount,
        service,
        system,
        purse: null,
        status: 1,
    });

    const depositRes = await axios.post(`${PAYMENT_SERVICE_URL}/${service}/deposit`, {
        amount,
        currency,
        deal_id: deal.id,
        user_id: deal.user_id,
        system,
        amount,
        PAYMENT_SERVICE_SECRET
    });

    console.log({data: depositRes.data});

    res.json({
        status: true,
        redirect: depositRes.data.redirect
    })
}));

router.post('/withdraw', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createOwn('deals');

    if(!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {
        purse,
        amount,
        currency,
        service,
        system
    } = req.body;

    if(!currencies.includes(currency))
        return res.send({status: false, err: 'Данная валюта недоступна для вывода.'});

    if(!Object.keys(services).includes(service))
        return res.send({status: false, err: 'Такого платежного сервиса не существует.'});

    if(!services[service].withdraw.find(el => el.system === system))
        return res.send({status: false, err: 'Такой платежной системы не существует.'});

    const user = await models.User.findByPk(req.user.id);
    const balance = money.floatToAmount(user.balance);

    amount = money.floatToAmount(amount);
    if(money.isNegative(money.subtract(balance, amount)))
        return res.send({status: false, err: 'Недостаточно средств для вывода'});

    if(money.isNegative(money.subtract(amount, '1.00')))
        return res.send({status: false, err: 'Нельзя вывести менее 1 руб.'});

    const deal = await models.Deal.create({
        user_id: req.user.id,
        sign: -1,
        currency,
        amount,
        service,
        system,
        purse,
        status: 1,
    });

    const withdrawRes = await axios.post(`${PAYMENT_SERVICE_URL}/${service}/withdraw`, {
        amount,
        currency,
        deal_id: deal.id,
        user_id: deal.user_id,
        system,
        purse,
        PAYMENT_SERVICE_SECRET
    });

    if (withdrawRes.status_id === 1)
        res.json({status: true});
    else
        res.json({status: false})
}));

router.post('/callback', asyncDecorator( async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createOwn('deals');

    if(!permission.granted) {
        res.sendStatus(403);
        return
    }

    const io = req.app.get('io');
    const {
        deal_id,
        status_id,
        purse,
        system
    } = req.body;

    const deal = await models.Deal.findByPk(deal_id);
    deal.status = status_id;
    deal.purse = purse;
    deal.system = system;

    const user = await models.User.findByPk(deal.user_id);

    const notificationData = {
        user_id: deal.user_id,
        type_id: null,
        body: {
            amount: deal.amount,
            currency: deal.currency
        }
    };

    if(status_id === 3){
        user.balance += deal.amount * deal.sign;
        await user.save();

        io.remoteEmitToUser(user.id, 'updateBalance', user.balance);

        notificationData.type_id = deal.sign > 0 ? 3 : 5
        //исполнен
    } else if(status_id === 2){


        notificationData.type_id = deal.sign > 0 ? 4 : 6
        //отклонен
    }

    const notification = await models.Notification.create(notificationData);
    io.remoteEmitToUser(notification.user_id, 'notification', notification);

    await deal.save();

    res.send(true)
}));

module.exports = router;
