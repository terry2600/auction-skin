const express = require('express');
const router = express.Router();
const Sequelize = require('sequelize');
const models = require('../models');
const {asyncDecorator} = require('../asyncDecorator');

const Op = Sequelize.Op;
let accessControl = require('../models');

router.get('/', asyncDecorator(async (req, res, next) => {
    const sort = JSON.parse(req.query.sort);
    const range = JSON.parse(req.query.range);
    const filter = JSON.parse(req.query.filter);

    const rangeCon = Object.entries(filter).length !== 0 ? {
        [Op.or]: [
            {title: {[Op.like]: `%${filter}%`}}
        ]
    } : {};

    models.FaqTab.findAll({
        where: rangeCon,
        order: [
            [sort[0], sort[1]]
        ]
    })
        .then(faqTabs => res.json({data: faqTabs.slice(range[0], range[1] + 1), total: faqTabs.length}))
}));

router.get('/faqs', asyncDecorator(async (req, res, next) => {
    res.send(await models.FaqTab.findAll({include: [{model: models.Faq}]}))
}));

router.get('/:id', asyncDecorator( async (req, res, next) => {
    const id = req.params.id;
    models.FaqTab.findOne({
        where: {id: id}
    })
        .then(faqTab => res.json(faqTab))
}));

router.post('/', asyncDecorator(async (req, res, next) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).createAny('faqTab');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {title, order, language} = req.body;

    let newFaqTab = await models.FaqTab.create({
        title,
        order,
        language
    });

    res.json(newFaqTab.id)
}));

router.put('/:data', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).updateAny('faqTab');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let {id, title, order, language} = req.body;

    let newFaqTab = await models.FaqTab.update({
        title,
        order,
        language
    }, {
        where: {
            id
        }
    });

    res.json(newFaqTab)
}));

router.delete('/:ids', asyncDecorator(async (req, res) => {
    const role = req.hasOwnProperty('user') ? req.user.role : 'Viewer';
    const permission = accessControl.ac.can(role).deleteAny('faqTab');

    if (!permission.granted) {
        res.sendStatus(403);
        return
    }

    let r = await models.FaqTab.destroy({
        where: {
            id: {
                [Op.in]: JSON.parse(req.params.ids)
            }
        }
    });

    res.sendStatus(200)
}));

module.exports = router;
