const request = require('supertest')
const app = require('../app.js')
const agent = request(app)
const sockets = require('../sockets')
const debug = require('debug')('server:server');
const http = require('http');
const models = require('../models')

const port = 9102
app.set('port', port);

const server = http.createServer(app);

const configureSocketConnection = (cookie) => ({
    autoConnect: false,
    transportOptions: {
        polling: {
            extraHeaders: {
                Cookie: cookie
            }
        }
    }
})

// auth cookie is for sliter account (6)
const authCookie = 'connect.sid=s:wOaVxqmlWCDwmQi6G0w1fhRvqjl6mrKo.ajKCQRIeIHLMaCEfK67fDpmghpYrswTyGihfZyGNYP4'
const socketClient = require('socket.io-client')(`http://localhost:${port}`, configureSocketConnection(authCookie));
// auth cookie is for Ozon account (1)
const authCookie2 = 'connect.sid=s:wjljypFTDvZeN_HZB23d1s-qq7c0clpQ.+MywjzcOLjFTJyF/+erdwP2jCC4MVOffYTkn9WjQ4Cg'
const socketClient2 = require('socket.io-client')(`http://localhost:${port}`, configureSocketConnection(authCookie2));
// auth cookie is for Ilnur account (12)
const authCookie3 = 'connect.sid=s:xJf0bBjLCSmic1giEti2JuYJpFZU5SYo.SWOZ2etyijQbvtKiNuWqwye1bXPbZLz4dTc1xvhXc3M'
const socketClient3 = require('socket.io-client')(`http://localhost:${port}`, configureSocketConnection(authCookie3));

beforeAll(async () => {
    server.listen(port);

    let appStarted = new Promise(res => app.on("appStarted", res))
    let socketsInited = sockets.init(server, app)

    appStarted.then(e => console.log('app started'))

    await Promise.all([appStarted])
})

beforeAll(async () => {
    const tables = ['Transaction', 'Bid', 'LotResult', 'Participants', 'Lot']
    for (let table of tables) {
        await models[table].destroy({
            where: {},
            truncate: true,
            cascade: true
        })
    }
})

describe('Sockets', () => {
    it('should connect all socket clients to socket server', () => {
        const allSocketClientsConnected = Promise.all([
            new Promise((res) => {
                socketClient.connect()
                socketClient.once('connect', () => res(true))
            }),
            new Promise((res) => {
                socketClient2.connect()
                socketClient2.once('connect', () => res(true))
            }),
            new Promise((res) => {
                socketClient3.connect()
                socketClient3.once('connect', () => res(true))
            })
        ])

        return expect(allSocketClientsConnected).resolves.toEqual(expect.arrayContaining([true, true, true]))
    })
})

let user1;
let user2;
let user3

describe('Auth endpoint', () => {
    it('should return two users info at /auth/steam/success', async () => {
        const user1Info = await agent.get('/api/auth/steam/success')
            .set('Cookie', [authCookie])
            .send()

        expect(user1Info.statusCode).toEqual(200)
        expect(user1Info.body).toHaveProperty('user')
        user1 = user1Info.body.user

        const user2Info = await agent.get('/api/auth/steam/success')
            .set('Cookie', [authCookie2])
            .send()

        expect(user2Info.statusCode).toEqual(200)
        expect(user2Info.body).toHaveProperty('user')
        user2 = user2Info.body.user

        const user3Info = await agent.get('/api/auth/steam/success')
            .set('Cookie', [authCookie3])
            .send()

        expect(user3Info.statusCode).toEqual(200)
        expect(user3Info.body).toHaveProperty('user')
        user3 = user3Info.body.user
    })
})

//let createdLotId;
//let createdLotCostOfParticipation;
//let createdLotStartDate;

describe('Lots Endpoint', () => {
    it('should return hotlots', async () => {
        const res = await agent.get('/api/lots/hot').send()

        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('lots')

        return res
    })
    let createdLotId;
    let createdLotCostOfParticipation;
    let createdLotStartDate;

    describe('Lot end-to-end with success', () => {

        const price = 1000
        const participants = 2;

        it('should create lot', async () => {
            const createLot = await agent.post('/api/inventory')
                .set('Cookie', [authCookie])
                .send({
                    item: {
                        appid: 730,
                        assetid: '17678833269'
                    },
                    price,
                    participants,
                    cost_of_participation: 100,
                    lot_step: 15,
                    hotlot: false
                })

            expect(createLot.statusCode).toEqual(200)
            expect(createLot.body).toHaveProperty('lot')
            expect(createLot.body.lot).toHaveProperty('id')
            expect(createLot.body.lot).toHaveProperty('game_item_id')
            expect(createLot.body.lot.cost_of_participation).toEqual(price / participants)


            createdLotCostOfParticipation = createLot.body.lot.cost_of_participation
            createdLotId = createLot.body.lot.id
            console.log('created lot id: ', createdLotId)
        })

        describe('Create lot', () => {
            it('should find this lot in open lots by id', async () => {
                expect(createdLotId).toBeDefined()

                const foundLot = await agent.get(`/api/lots/open/${createdLotId}`).send()

                expect(foundLot.statusCode).toEqual(200)
                expect(foundLot.body).toHaveProperty('item')
                expect(foundLot.body.item.id).toEqual(createdLotId)
            })

            it.skip('should find this lot in open lots by page', async () => {
                expect(createdLotId).toBeDefined()

                const openLots = await agent.post('/api/lots/open/0').send({
                    filters: {
                        app_id: 730,
                        price: {
                            min: 0,
                            max: 100000
                        }
                    }
                })

                expect(openLots.statusCode).toEqual(200)
                expect(openLots.body).toHaveProperty('lots')

                const foundOpenLot = openLots.body.lots.find(lot => lot.id === createdLotId)
                expect(foundOpenLot).toBeDefined()
            })
        })

        describe('Participate in lot', () => {
            const updateBalancePromise = new Promise(res => socketClient2.once('updateBalance', (balance) => res(balance)))

            it('should participate in created lot', async () => {
                expect(createdLotId).toBeDefined()
                expect(createdLotCostOfParticipation).toBeDefined()
                expect(user2).toBeDefined()


                const participate2 = await agent.post('/api/lots/participate')
                    .set('Cookie', [authCookie2])
                    .send({
                        id: createdLotId
                    })

                const participate3 = await agent.post('/api/lots/participate')
                    .set('Cookie', [authCookie3])
                    .send({
                        id: createdLotId
                    })

                expect(participate2.statusCode).toEqual(200)
                expect(participate2.body).toEqual(true)
                user3.balance -= createdLotCostOfParticipation

                expect(participate3.statusCode).toEqual(200)
                expect(participate3.body).toEqual(true)
            })


            it('should have balance drained by cost_of_participation', async () => {
                expect(createdLotCostOfParticipation).toBeDefined()

                expect(await updateBalancePromise).toEqual(user2.balance - createdLotCostOfParticipation)
            })

            it('should have moved lot to userlots', async () => {
                expect(createdLotId).toBeDefined()

                const userLots = await agent.get('/api/lots/active/user/0')
                    .set('Cookie', [authCookie2])
                    .send()

                expect(userLots.statusCode).toEqual(200)
                const userLot = userLots.body.lots.find(lot => lot.id === createdLotId)
                expect(userLot).toBeDefined()
                expect(userLot.id).toEqual(createdLotId)
                expect(userLot.start_date).toBeDefined()

                createdLotStartDate = userLot.start_date
            })
        })

        let user3UpdateBalancePromise;

        describe('Bid in lot', () => {
            it('should connect to userLots', () => {
                socketClient2.emit('joinUserLots')
                socketClient3.emit('joinUserLots')
            })

            it('should connect to lot socket room', () => {
                expect(createdLotId).toBeDefined()

                socketClient2.emit('joinRoom', createdLotId)
                socketClient3.emit('joinRoom', createdLotId)
            })

            it('should wait until lot is available for bidding', () => {
                expect(createdLotStartDate).toBeDefined()

                return new Promise(res => setTimeout(res, new Date(createdLotStartDate) - new Date() + 2000))
            })

            it('should be able to bid on lot', () => {
                expect(createdLotId).toBeDefined()

                const makeBidResultPromise = new Promise(res => {
                    socketClient3.once('makeBid', res)
                })

                socketClient3.emit('makeBid', {
                    lotId: createdLotId
                })

                return makeBidResultPromise
            })

            it('should be able to overbid on lot', () => {
                expect(createdLotId).toBeDefined()

                user3UpdateBalancePromise = new Promise(res => socketClient3.once('updateBalance', (balance) => res(balance)))

                const makeBidResultPromise = new Promise(res => {
                    socketClient2.once('makeBid', res)
                })

                socketClient2.emit('makeBid', {
                    lotId: createdLotId
                })

                return makeBidResultPromise
            })

            it('should get balance returned because of overbid', async () => {
                expect(user3UpdateBalancePromise).toBeDefined()

                expect(await user3UpdateBalancePromise).toEqual(user3.balance)
            })

            it('should get socket event that lot is closed with status waiting for trade', () => {
                expect(createdLotId).toBeDefined()

                return new Promise(res => {
                    socketClient2.once('closedLot', ({
                        id,
                        status
                    }) => {
                        expect(id).toEqual(createdLotId)
                        expect(status).toEqual(4)
                        res()
                    })
                })
            })
        })

        describe('Trade lot', () => {
            it.skip('should get lot in /trades/waiting', async () => {
                expect(createdLotId).toBeDefined()

                const waitingLots = await agent.get('/api/trades/waiting')
                    .set('Cookie', [authCookie2])
                    .send()

                expect(waitingLots.statusCode).toEqual(200)
                expect(waitingLots.body.length).toBeGreaterThan(0)
                expect(waitingLots.body[0]).toHaveProperty('id')
                expect(waitingLots.body[0].id).toEqual(createdLotId)
            })
        })
    })

    let createdLotThatWillBeRemovedId;
    describe('Lot overtime', () => {
        beforeAll(async () => {
            const createLot = await agent.post('/api/inventory')
                .set('Cookie', [authCookie])
                .send({
                    item: {
                        appid: 730,
                        assetid: '17678971328'
                    },
                    price: 100,
                    participants: 100,
                    cost_of_participation: 100,
                    lot_step: 15,
                    hotlot: false
                })

            createdLotThatWillBeRemovedId = createLot.body.lot.id
        })

        it('should create lot', () => {
            expect(createdLotThatWillBeRemovedId).toBeDefined()
        })

        it('should participate in created lot', async () => {
            expect(createdLotThatWillBeRemovedId).toBeDefined()

            const participate = await agent.post('/api/lots/participate')
                .set('Cookie', [authCookie3])
                .send({
                    id: createdLotThatWillBeRemovedId
                })

            expect(participate.statusCode).toEqual(200)
            expect(participate.body).toEqual(true)
        })

        it('should set lot to overtime and remove it', async () => {
            expect(createdLotThatWillBeRemovedId).toBeDefined()

            const createdLot = await models.Lot.findByPk(createdLotThatWillBeRemovedId)
            expect(createdLot).not.toBeNull()

            createdLot.expired_date = new Date(0)
            await createdLot.save()

            let removedLots = await sockets.checkOvertimeLots()
            expect(removedLots).toBeGreaterThan(0)
        })
    })


    describe('Transactions', () => {
        let userTransactions;
        let user2Transactions;
        let user3Transactions;

        beforeAll(async () => {
            const userTransactionsQuery = await agent.get('/api/users/transactions')
                .set('Cookie', [authCookie])
                .send()

            expect(userTransactionsQuery.statusCode).toEqual(200)
            userTransactions = userTransactionsQuery.body

            const user2TransactionsQuery = await agent.get('/api/users/transactions')
                .set('Cookie', [authCookie2])
                .send()

            expect(user2TransactionsQuery.statusCode).toEqual(200)
            user2Transactions = user2TransactionsQuery.body

            const user3TransactionsQuery = await agent.get('/api/users/transactions')
                .set('Cookie', [authCookie3])
                .send()

            expect(user3TransactionsQuery.statusCode).toEqual(200)
            user3Transactions = user3TransactionsQuery.body
        })

        it('should have transactions for participation', () => {
            expect(userTransactions).toBeDefined()
            expect(user2Transactions).toBeDefined()
            expect(user3Transactions).toBeDefined()

            let findParticipateTransaction = transaction => transaction.lot_id === createdLotId && transaction.type_id === 1

            let user2And3ParticipateTransactions = [
                user2Transactions.find(findParticipateTransaction),
                user3Transactions.find(findParticipateTransaction)
            ]

            for (let userNParticipateTransaction of user2And3ParticipateTransactions) {
                expect(userNParticipateTransaction).toBeDefined()
                expect(userNParticipateTransaction).toHaveProperty('amount')
                expect(userNParticipateTransaction.amount).toEqual(createdLotCostOfParticipation)
            }
        })

        it('should have transaction for overtime lot', () => {
            expect(user3Transactions).toBeDefined()
            expect(createdLotThatWillBeRemovedId).toBeDefined()

            let transaction = user3Transactions.find(transaction => transaction.lot_id === createdLotThatWillBeRemovedId && transaction.type_id === 2)

            expect(transaction).toBeDefined()
            expect(transaction).toHaveProperty('amount')
        })
    })
})
