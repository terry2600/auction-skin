module.exports = (sequelize, Sequelize) => {
    let FaqTab = sequelize.define('FaqTab', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: Sequelize.STRING,
        order: Sequelize.INTEGER,
        language: Sequelize.STRING
    }, {
        timestamps: false
    });

    FaqTab.associate = (models) => {
        models.FaqTab.hasMany(models.Faq, {foreignKey: 'tab'});
    };

    return FaqTab;
};
