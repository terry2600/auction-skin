module.exports = (sequelize, Sequelize) => {
    let LotResult = sequelize.define('LotResult', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        lot_id: Sequelize.INTEGER,
        winner_id: Sequelize.INTEGER,
        purchase_price: Sequelize.STRING
    }, {
        timestamps: false,
        freezeTableName: true
    });

    LotResult.associate = (models) => {
        models.LotResult.belongsTo(models.User, {foreignKey: 'winner_id'});
        models.LotResult.belongsTo(models.Lot, {foreignKey: 'lot_id'});
    };

    return LotResult;
};

