module.exports = (sequelize, Sequelize) => {
    let TransactionType = sequelize.define('TransactionType', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: Sequelize.STRING
    }, {
        timestamps: false
    });

    TransactionType.associate = (models) => {
        models.TransactionType.hasMany(models.Transaction, {foreignKey: 'type_id'})
    };

    return TransactionType;
};
