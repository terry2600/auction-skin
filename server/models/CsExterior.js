module.exports = (sequelize, Sequelize) => {
    let CsExterior = sequelize.define('CsExterior', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsExterior.associate = (models) => {
        models.CsExterior.hasMany(models.CsParams, {foreignKey: 'exterior_id'});
    };

    return CsExterior;
};

