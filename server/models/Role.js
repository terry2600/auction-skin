module.exports = (sequelize, Sequelize) => {
    let Role =  sequelize.define('Role', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        type: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    Role.associate = (models) => {
        models.Role.hasMany(models.User);
        models.Role.belongsToMany(models.Privilege, {through: 'Grants', foreignKey: 'role_id', timestamps: false});
    };

    return Role;
};

