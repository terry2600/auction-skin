module.exports = (sequelize, Sequelize) => {
    let CsParams = sequelize.define('CsParams', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        asset_id: Sequelize.STRING,
        exterior_id: Sequelize.INTEGER,
        item_set_id: Sequelize.INTEGER,
        quality_id: Sequelize.INTEGER,
        rarity_id: Sequelize.INTEGER,
        type_id: Sequelize.INTEGER,
        weapon_id: Sequelize.INTEGER,
        float: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsParams.associate = (models) => {
        models.CsParams.belongsTo(models.CsItemSet, {foreignKey: 'item_set_id'});
        models.CsParams.belongsTo(models.CsQuality, {foreignKey: 'quality_id'});
        models.CsParams.belongsTo(models.CsExterior, {foreignKey: 'exterior_id'});
        models.CsParams.belongsTo(models.CsRarity, {foreignKey: 'rarity_id'});
        models.CsParams.belongsTo(models.CsType, {foreignKey: 'type_id'});
        models.CsParams.belongsTo(models.CsWeapon, {foreignKey: 'weapon_id'});
    };

    return CsParams;
};
