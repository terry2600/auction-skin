module.exports = (sequelize, Sequelize) => {
    let CsWeapon = sequelize.define('CsWeapon', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsWeapon.associate = (models) => {
        models.CsWeapon.hasMany(models.CsParams, {foreignKey: 'weapon_id'});
    };

    return CsWeapon;
};

