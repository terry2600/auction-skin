module.exports = (sequelize, Sequelize) => {
    let DotaQuality = sequelize.define('DotaQuality', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        color: Sequelize.STRING,
        name: Sequelize.STRING
    }, {
        timestamps: false
    });

    DotaQuality.associate = (models) => {
        models.DotaQuality.hasMany(models.DotaParams, {foreignKey: 'quality_id'});
    };

    return DotaQuality;
};

