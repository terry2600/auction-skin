module.exports = (sequelize, Sequelize) => {
    let CsType = sequelize.define('CsType', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsType.associate = (models) => {
        models.CsType.hasMany(models.CsParams, {foreignKey: 'type_id'});
    };

    return CsType;
};

