module.exports = (sequelize, Sequelize) => {
    let LotStatus = sequelize.define('LotStatus', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        status: Sequelize.STRING,
        status_description: Sequelize.STRING,
    }, {
        timestamps: false
    });

    LotStatus.associate = (models) => {
        models.LotStatus.hasMany(models.Lot, {foreignKey: 'status_id'})
    };

    return LotStatus;
};