module.exports = (sequelize, Sequelize) => {
    let Lot = sequelize.define('Lot', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        game_item_id: Sequelize.INTEGER,
        status_id: Sequelize.INTEGER,
        create_date: {
            type: Sequelize.DATE,
            isNull: false
        },
        expired_date: {
            type: Sequelize.DATE,
            isNull: false
        },
        start_date: Sequelize.DATE,
        end_date: Sequelize.DATE,
        trade_ended_date: Sequelize.DATE,
        owner_id: Sequelize.INTEGER,
        hotlot: Sequelize.BOOLEAN,
        price: Sequelize.DECIMAL(10,2),
        cost_of_participation: Sequelize.DECIMAL(10,2),
        lot_step: Sequelize.INTEGER,
        participants: Sequelize.INTEGER,
        color: Sequelize.STRING,
        app_id: Sequelize.INTEGER
    }, {
        timestamps: false
    });

    Lot.associate = (models) => {
        models.Lot.belongsToMany(models.User, {through: 'Participants', foreignKey: 'lot_id', timestamps: false});
        models.Lot.hasOne(models.LotResult, {foreignKey: 'lot_id'});
        models.Lot.hasMany(models.Transaction, {foreignKey: 'lot_id'});
        models.Lot.belongsTo(models.LotStatus, {foreignKey: 'status_id'});
        models.Lot.belongsTo(models.GameItem, {foreignKey: 'game_item_id'})
    };

    return Lot;
};
