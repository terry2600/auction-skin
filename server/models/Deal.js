module.exports = (sequelize, Sequelize) => {
    let Deal = sequelize.define('Deal', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: Sequelize.DATE,
        user_id: Sequelize.INTEGER,
        sign: Sequelize.INTEGER,
        currency: Sequelize.STRING,
        amount: Sequelize.DECIMAL(10,2),
        service: Sequelize.STRING,
        system: Sequelize.STRING,
        status: Sequelize.INTEGER,
        purse: Sequelize.JSON
    }, {
        timestamps: true,
        updatedAt: false
    });

    return Deal;
};
