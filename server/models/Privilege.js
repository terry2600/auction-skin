module.exports = (sequelize, Sequelize) => {
    let Privilege = sequelize.define('Privilege', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        resource: {
            type: Sequelize.STRING
        },
        action: {
            type: Sequelize.STRING
        },
        attribute: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    Privilege.associate = (models) => {
        models.Privilege.belongsToMany(models.Role, {through: 'Grants', foreignKey: 'privilege_id', timestamps: false});
    };

    return Privilege;
}
;
