module.exports = (sequelize, Sequelize) => {
    let Static = sequelize.define('Static', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: Sequelize.STRING,
        html: Sequelize.TEXT,
        page: Sequelize.STRING,
        language: Sequelize.STRING
    }, {
        timestamps: false
    });

    return Static;
};
