module.exports = (sequelize, Sequelize) => {
    let DotaParams = sequelize.define('DotaParams', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        asset_id: Sequelize.STRING,
        hero_id: Sequelize.INTEGER,
        rarity_id: Sequelize.INTEGER,
        slot_id: Sequelize.INTEGER,
        type_id: Sequelize.INTEGER
    }, {
        timestamps: false
    });

    DotaParams.associate = (models) => {
        models.DotaParams.belongsTo(models.DotaHero, {foreignKey: 'hero_id'});
        models.DotaParams.belongsTo(models.DotaQuality, {foreignKey: 'quality_id'});
        models.DotaParams.belongsTo(models.DotaRarity, {foreignKey: 'rarity_id'});
        models.DotaParams.belongsTo(models.DotaSlot, {foreignKey: 'slot_id'});
        models.DotaParams.belongsTo(models.DotaType, {foreignKey: 'type_id'});
    };

    return DotaParams;
};
