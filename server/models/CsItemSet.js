module.exports = (sequelize, Sequelize) => {
    let CsItemSet = sequelize.define('CsItemSet', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsItemSet.associate = (models) => {
        models.CsItemSet.hasMany(models.CsParams, {foreignKey: 'item_set_id'});
    };

    return CsItemSet;
};

