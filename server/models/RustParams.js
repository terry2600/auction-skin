module.exports = (sequelize, Sequelize) => {
    let RustParams = sequelize.define('RustParams', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        asset_id: Sequelize.STRING,
        category_id: Sequelize.INTEGER,
        type_id: Sequelize.INTEGER,
    }, {
        timestamps: false
    });

    RustParams.associate = (models) => {
        models.RustParams.belongsTo(models.RustCategory, {foreignKey: 'category_id'});
        models.RustParams.belongsTo(models.RustType, {foreignKey: 'type_id'});
    };

    return RustParams;
};
