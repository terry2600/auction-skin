module.exports = (sequelize, Sequelize) => {
    let Notification = sequelize.define('Notification', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: Sequelize.DATE,
        user_id: Sequelize.INTEGER,
        type_id: Sequelize.INTEGER,
        body: Sequelize.JSON
    }, {
        timestamps: true,
        updatedAt: false,
    });

    Notification.associate = (models) => {
        models.Notification.belongsTo(models.User, {foreignKey: 'user_id'});
        models.Notification.belongsTo(models.NotificationType, {foreignKey: 'type_id'});
    };

    return Notification;
};
