module.exports = (sequelize, Sequelize) => {
    let DotaHero = sequelize.define('DotaHero', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name: Sequelize.STRING,
        img: Sequelize.STRING,
        type: Sequelize.STRING
    }, {
        timestamps: false
    });

    DotaHero.associate = (models) => {
        models.DotaHero.hasMany(models.DotaParams, {foreignKey: 'hero_id'});
    };

    return DotaHero;
};

