module.exports = (sequelize, Sequelize) => {
    let DotaRarity = sequelize.define('DotaRarity', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        color: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    DotaRarity.associate = (models) => {
        models.DotaRarity.hasMany(models.DotaParams, {foreignKey: 'rarity_id'});
    };

    return DotaRarity;
};

