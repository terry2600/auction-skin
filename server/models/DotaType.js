module.exports = (sequelize, Sequelize) => {
    let DotaType = sequelize.define('DotaType', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    DotaType.associate = (models) => {
        models.DotaType.hasMany(models.DotaParams, {foreignKey: 'type_id'});
    };

    return DotaType;
};

