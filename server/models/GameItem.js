module.exports = (sequelize, Sequelize) => {
    let GameItem = sequelize.define('GameItem', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        asset_id: {
            type: Sequelize.STRING,
        },
        icon_url: {
            type: Sequelize.TEXT,
        },
        icon_url_large: {
            type: Sequelize.TEXT,
        },
        description: {
            type: Sequelize.TEXT
        },
        name: {
            type: Sequelize.STRING
        },
        name_color: {
            type: Sequelize.STRING
        },
        market_name: {
            type: Sequelize.STRING
        },
        market_hash_name: {
            type: Sequelize.STRING
        },
        marketable: {
            type: Sequelize.BOOLEAN
        },
        stickers: {
            type: Sequelize.TEXT
        }
    }, {
        timestamps: false
    });

    GameItem.associate = (models) => {
        models.GameItem.belongsTo(models.Games, {foreignKey: 'app_id'});
    };





    return GameItem;
};
