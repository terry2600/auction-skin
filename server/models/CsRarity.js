module.exports = (sequelize, Sequelize) => {
    let CsRarity = sequelize.define('CsRarity', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        color: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsRarity.associate = (models) => {
        models.CsRarity.hasMany(models.CsParams, {foreignKey: 'rarity_id'});
    };

    return CsRarity;
};

