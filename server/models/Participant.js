module.exports = (sequelize, Sequelize) => {
    let Participants = sequelize.define('Participants', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        lot_id: Sequelize.INTEGER,
        user_id: Sequelize.INTEGER
    }, {
        timestamps: false
    });

    Participants.associate = (models) => {
        models.Participants.belongsTo(models.User, {foreignKey: 'user_id'});
        models.Participants.belongsTo(models.Lot, {foreignKey: 'lot_id'});
    };

    return Participants;
};
