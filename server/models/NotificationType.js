module.exports = (sequelize, Sequelize) => {
    let NotificationType = sequelize.define('NotificationType', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        description: Sequelize.STRING
    }, {
        timestamps: false
    });

    NotificationType.associate = (models) => {
        models.NotificationType.hasMany(models.Notification, {foreignKey: 'type_id'})
    };

    return NotificationType;
};
