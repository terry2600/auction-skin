module.exports = (sequelize, Sequelize) => {
    let Transaction = sequelize.define('Transaction', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        createdAt: Sequelize.DATE,
        user_id: Sequelize.INTEGER,
        lot_id: Sequelize.INTEGER,
        lot_name: Sequelize.STRING,
        type_id: Sequelize.INTEGER,
        amount: Sequelize.DECIMAL(10,2),
    }, {
        timestamps: true,
        updatedAt: false
    });

    Transaction.associate = (models) => {
        models.Transaction.belongsTo(models.TransactionType, {foreignKey: 'type_id'});
    };

    return Transaction;
};
