module.exports = (sequelize, Sequelize) => {
    let CsQuality = sequelize.define('CsQuality', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        color: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    CsQuality.associate = (models) => {
        models.CsQuality.hasMany(models.CsParams, {foreignKey: 'quality_id'});
    };

    return CsQuality;
};

