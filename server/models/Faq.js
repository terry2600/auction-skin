module.exports = (sequelize, Sequelize) => {
    let Faq = sequelize.define('Faq', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: Sequelize.STRING,
        text: Sequelize.TEXT,
        tab: Sequelize.INTEGER,
        order: Sequelize.INTEGER,
    }, {
        timestamps: false
    });

    return Faq;
};
