module.exports = (sequelize, Sequelize) => {
    let Games = sequelize.define('Games', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        name: Sequelize.STRING,
        game_filter: Sequelize.STRING
    }, {
        timestamps: false
    });

    return Games;
};