module.exports = (sequelize, Sequelize) => {
    let RustType = sequelize.define('RustType', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    RustType.associate = (models) => {
        models.RustType.hasMany(models.RustParams, {foreignKey: 'type_id'});
    };

    return RustType;
};
