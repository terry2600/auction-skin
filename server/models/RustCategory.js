module.exports = (sequelize, Sequelize) => {
    let RustCategory = sequelize.define('RustCategory', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    RustCategory.associate = (models) => {
        models.RustCategory.hasMany(models.RustParams, {foreignKey: 'category_id'});
    };

    return RustCategory;
};
