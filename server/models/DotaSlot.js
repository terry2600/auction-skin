module.exports = (sequelize, Sequelize) => {
    let DotaSlot = sequelize.define('DotaSlot', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        internal_name: Sequelize.STRING,
        name_ru: Sequelize.STRING
    }, {
        timestamps: false
    });

    DotaSlot.associate = (models) => {
        models.DotaSlot.hasMany(models.DotaParams, {foreignKey: 'slot_id'});
    };

    return DotaSlot;
};

