module.exports = (sequelize, Sequelize) => {
    let Bid = sequelize.define('Bid', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        lot_id: Sequelize.INTEGER,
        user_id: Sequelize.INTEGER,
        date: Sequelize.DATE,
        bet_size: Sequelize.INTEGER
    }, {
        timestamps: false,
    });

    Bid.associate = (models) => {
        models.Bid.belongsTo(models.Lot, {foreignKey: 'lot_id'});
        models.Bid.belongsTo(models.User, {foreignKey: 'user_id'});
    };

    return Bid;
};

