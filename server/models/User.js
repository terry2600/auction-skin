module.exports = (sequelize, Sequelize) => {
    let User = sequelize.define('User', {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        steamid64: {
            type: Sequelize.STRING
        },
        steam_nick: {
            type: Sequelize.STRING
        },
        steam_guard: {
            type: Sequelize.BOOLEAN
        },
        trade_url: {
            type: Sequelize.STRING
        },
        api_key: {
            type: Sequelize.STRING
        },
        avatar: {
            type: Sequelize.STRING
        },
        referer: {
          type: Sequelize.INTEGER
        },
        balance: {
            type: Sequelize.DECIMAL,
            get () {
                return parseFloat(this.getDataValue('balance'))
            }
        },
        profile_url: {
            type: Sequelize.VIRTUAL,
            get () {
                return `https://steamcommunity.com/profiles/${this.getDataValue('steamid64')}`
            }
        }
    }, {
        underscored: true,
        timestamps: true,
        createdAt: 'registration_date',
        updatedAt: false,
    });

    User.associate = (models) => {
        models.User.belongsTo(models.Role, {foreignKey: 'role_id'});
        models.User.belongsToMany(models.Lot, {through: 'Participants', foreignKey: 'user_id', timestamps: false})
        models.User.hasMany(models.Transaction, {foreignKey: 'user_id'});
        models.User.hasMany(models.Notification, {foreignKey: 'user_id'});
    };

    User.countReferals = (uid) => {
        return User.count({where: {referer: uid}})
    };

    User.getCashback = async (uid) => {
        const referals = await User.countReferals(uid)

        let cashBack = '0.00'
        if(referals >= 1)
            cashBack = '0.02'
        if(referals >= 5)
            cashBack = '0.04'
        if(referals >= 10)
            cashBack = '0.08'
        if(referals >= 20)
            cashBack = '0.10'
        if(referals >= 50)
            cashBack = '0.15'

        return cashBack
    }

    return User;
};
