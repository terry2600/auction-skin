const Sequelize = require('sequelize');
const fs = require('fs');
const path = require('path');
const basename = path.basename(__filename);
const AccessControl = require('accesscontrol');
const session = require('express-session');
const SequelizeStore = require('connect-session-sequelize')(session.Store);
const {DB_NAME, DB_NAME_TEST, DB_USERNAME, DB_PASSWORD, DB_HOST, DB_PORT} = require('../config/constants');

let db = {};
let grantList = [];

Sequelize.postgres.DECIMAL.parse = function (value) {
    return parseFloat(value);
};

let dbName = DB_NAME;
let logging = console.log;

if(process.env.test === 'true')
    dbName = DB_NAME_TEST,
    logging = false;

const sequelize = new Sequelize(dbName, DB_USERNAME, DB_PASSWORD, {
    host: DB_HOST,
    port: DB_PORT,
    dialect: 'postgres',
    storage: './session.postgres',
    pool: {
        max: 13,
        min: 1,
        acquire: 20000,
        idle: 20000
    },
    logging
});

const sessionStore = new SequelizeStore({
    db: sequelize
});

fs
    .readdirSync(__dirname)
    .filter(file => {
        return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
    })
    .forEach(file => {
        let model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
    });

Object.keys(db).forEach(modelName => {
    if (db[modelName].associate) {
        db[modelName].associate(db);
    }
});

let dbReady;
sequelize.sync()
    .then(() => {
        // Role.findByPk(3)
        //     .then(role => role.addPrivilege(6))
        // // Role.addPrivilege(1).then(console.log(result))
        db.Role.findAll({
            raw: true,
            include: [{model: db.Privilege}]
        })
            .then(grants => {
                for (const item of grants) {
                    grantList = [...grantList, {
                        role: item.type,
                        resource: item['Privileges.resource'],
                        action: item['Privileges.action'],
                        attributes: item['Privileges.attribute'],
                    }]
                }
                module.exports.ac = new AccessControl(grantList);
                dbReady()
            });
    });

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.sessionStore = sessionStore;
db.ready = new Promise(r => dbReady = r);

module.exports = db;
