const createError = require('http-errors');
const express = require('express');
require('express-async-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const session = require('express-session');
const passport = require('passport');
const Bot = require('./config/bots');
const {sessionStore, ready} = require('./models');
require('./config/passport-setup');
require('pg').defaults.parseInt8 = true;
const {CLIENT_HOME_PAGE_URL, TIME_SERVICE_URL} = require('./config/constants');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const authRouter = require('./routes/auth');
const botsRouter = require('./routes/bots');
const lotsRouter = require('./routes/lots');
const inventoryRouter = require('./routes/inventory');
const faqRouter = require('./routes/faq');
const faqTabRouter = require('./routes/faqTab');
const staticRouter = require('./routes/static');
const tradesRouter = require('./routes/trades');
const statsRouter = require('./routes/stats');
const dealsRouter = require('./routes/deals');
const timersRouter = require('./routes/timers');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

const whiteList = [CLIENT_HOME_PAGE_URL, TIME_SERVICE_URL];

const corsOption = {
    origin: (origin, callback) => {
        if (!origin || whiteList.indexOf(origin) !== -1) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};

app.use(cors(corsOption));
app.use(express.static(path.join(__dirname, 'public')));
app.use(logger('dev'));
app.use(session({
    secret: 'tmnttmnt',
    resave: false,
    store: sessionStore,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/api/users', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/bots', botsRouter);
app.use('/api/lots', lotsRouter);
app.use('/api/inventory', inventoryRouter);
app.use('/api/faq', faqRouter);
app.use('/api/faqTab', faqTabRouter);
app.use('/api/static', staticRouter);
app.use('/api/trades', tradesRouter);
app.use('/api/stats', statsRouter);
app.use('/api/deals', dealsRouter);
app.use('/api/timers', timersRouter);

// error handler
app.use(function (err, req, res, next) {
    res.status(500).json({message: 'Не удалось выполнить ваш запрос'})
});

ready.then(async () => {
    await Bot.started;
    app.emit('appStarted')
});

module.exports = app;
