const models = require('../models');
const axios = require('axios');
const {
    TIME_SERVICE_URL,
    TIME_SERVICE_SECRET,
    BID_TIME
} = require('../config/constants.js');

let Lots = {
    joinRoom(io, socket, lotId) {
        socket.join(`lot${lotId}`, () => console.log(socket.rooms));
    },
    leaveRoom(io, socket, lotId) {
        socket.leave(`lot${lotId}`, () => console.log(socket.rooms))
    },
    joinLots(io, socket, appId) {
        socket.join(appId, () => console.log(socket.rooms))
    },
    leaveLots(io, socket, appId) {
        socket.leave(appId, () => console.log(socket.rooms))
    },
    joinUserLots(io, socket) {
        socket.join(`userLots`, () => console.log(socket.rooms))
    },
    leaveUserLots(io, socket) {
        socket.leave(`userLots`, () => console.log(socket.rooms))
    },
    async makeBid(io, socket, bid) {
        const transaction = await models.sequelize.transaction();

        console.log('makebid with bid: ', bid);

        const lastUser = await models.Bid.findOne({
            where: {
                lot_id: bid.lotId
            },
            attributes: ['user_id', 'bet_size'],
            order: [['date', 'DESC']],
            raw: true,
            transaction,
            lock: true
        });

        const lastUserAfterUpdate = await models.Bid.findOne({
            where: {
                lot_id: bid.lotId
            },
            attributes: ['user_id', 'bet_size'],
            order: [['date', 'DESC']],
            raw: true,
            transaction,
        });

        console.log(lastUser);
        console.log(lastUserAfterUpdate);

        const lot = await models.Lot.findOne({
            where: {id: bid.lotId},
            attributes: ['lot_step', 'status_id', 'owner_id'],
            raw: true,
            transaction
        });

        console.log(lot)

        if (lot.status_id !== 3) {
            console.log(`статус ${lot.status_id} !== 3 иди в пизду`);
            transaction.rollback()
        } else if (socket.request.user.id === lot.owner_id) {
            console.log('овнер пошел нах');
            transaction.rollback()
        } else if (lastUser === null) {
            console.log('уточуи')
            console.log(socket.request.user);
            this.createBid(null, 0, lot.lot_step, socket.request.user.balance, bid.lotId, socket.request.user.id, io, transaction)
        } else if (lastUser.user_id === socket.request.user.id) {
            console.log('Один и тот же юзер ебись в рот');
            transaction.rollback()
        } else if (lastUserAfterUpdate.bet_size !== lastUser.bet_size) {
            console.log('Увы, кто-то поставил раньше!');
            transaction.rollback()
        } else {
            this.createBid(lastUser.user_id, lastUser.bet_size, lot.lot_step, socket.request.user.balance, bid.lotId, socket.request.user.id, io, transaction)
        }
    },
    async createBid(prevUserId, prevUserBet, lotStep, currentBalance, lotId, userId, io, transaction) {
        if (currentBalance < prevUserBet + lotStep)
            return transaction.rollback();

        if (prevUserId) {
            const [[[prevUserUpdated], count]] = await models.User.increment('balance', {
                by: prevUserBet,
                where: {id: prevUserId},
                transaction
            });

            io.remoteEmitToUser(prevUserId, 'updateBalance', prevUserUpdated.balance)
        }

        const user = await models.User.decrement('balance', {
            by: lotStep + prevUserBet,
            where: {id: userId},
            raw: true,
            transaction
        });

        const createdBid = await models.Bid.create({
            lot_id: lotId,
            user_id: userId,
            date: Date.now(),
            bet_size: lotStep + prevUserBet,
        }, {raw: true, transaction});

        const lastBids = await models.Bid.findAll({
            limit: 5,
            attributes: ['User.steam_nick', 'User.avatar', 'date', 'bet_size', 'id'],
            where: {
                lot_id: lotId
            },
            include: [{
                attributes: [],
                model: models.User
            }],
            order: [['date', 'DESC']],
            raw: true,
            transaction
        });

        await transaction.commit();
        await io.remoteEmitToUser(user.id, 'updateBalance', user.balance)



        const tsResult = await axios.post(TIME_SERVICE_URL, {
            lotId,
            type: 'bid',
            lastBidId: createdBid.id,
            time: BID_TIME,
            TIME_SERVICE_SECRET
        });

        io.to(`lot${lotId}`).emit('makeBid', lastBids)
    }
};


module.exports = Lots;
