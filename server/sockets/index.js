const Lots = require('./Lots');
const passportSocketIo = require("passport.socketio");
const cookieParser = require('cookie-parser');
const models = require('../models');
const Op = models.Sequelize.Op;
const Bot = require('../config/bots');
const {SOCKET_SECRET_KEY, REDIS_URL} = require('../config/constants');
let sockets = {};

sockets.init = (server, app) => {
    const io = require('socket.io')(server);
    const redis = require('socket.io-redis');
    try {
        const adapter = redis(REDIS_URL)
        io.adapter(adapter);
    } catch (e) {
        console.error('Error connecting to redis adapter: ', e)
    }

    io.use(passportSocketIo.authorize({
        cookieParser: cookieParser,
        key: 'connect.sid',
        secret: SOCKET_SECRET_KEY,
        store: models.sessionStore,
        success: (data, accept) => {
            //console.log('passportSocketIo success => ', data)
            accept(null, true)
        },
        fail: (data, message, error, accept) => {
            console.log('passportSocketIo fail => ', message, error);
            accept()
        },
    }));

    io.of('/').adapter.customHook = ([method, data], cb) => {
        switch (method) {
            case 'remoteEmitToUser':
                cb(
                    passportSocketIo
                    .filterSocketsByUser(io, (user) => user.id === data.id)
                    .map(socket => socket.emit(...data.event))
                    .map(socket => socket.id)
                )
                break;
            default:
                cb(null)
        }
    }

    io.remoteEmitToUser = (id, event, data) => {
        return new Promise((res, rej) => {
            const query = ['remoteEmitToUser', {
                id,
                event: [event, data]
            }]

            if(!io.of('/').adapter.customRequest)
                return io.of('/').adapter.customHook(query, sockets => res([sockets]))

            io.of('/').adapter.customRequest(query, (err, responses) => {
                if(err)
                    rej(err)
                else
                    res(responses)
            })
        })
    }

    sockets.checkOvertimeLots = async e => {
        const lots = await models.Lot.findAll({
            where: {
                status_id: {
                    [models.Sequelize.Op.in]: [1, 9]
                },
                expired_date: {
                    [models.Sequelize.Op.lt]: new Date()
                }
            },
            include: ['Users', 'GameItem']
        });

        console.log('overtime lots length: ', lots.length);

        const transaction = await models.sequelize.transaction();
        for(let lot of lots){
            for(let user of lot.Users){
                await models.Transaction.create({
                    user_id: user.id,
                    lot_id: lot.id,
                    lot_name: lot.GameItem.name,
                    type_id: 2,
                    amount: lot.cost_of_participation
                }, transaction);

                user.balance += lot.cost_of_participation;
                io.remoteEmitToUser(user.id, 'updateBalance', user.balance);

                await user.save({transaction})
            }
            //if(lot.type_id === 2)
            lot.end_date = lot.expired_date;
            lot.status_id = 7;
            io.to(`lots`).emit('closedLot', {id: lot.id, status: 7});

            await lot.save({transaction})
        }

        await transaction.commit();

        return lots.length
    };

    setInterval(sockets.checkOvertimeLots, 60 * 1000);

    setInterval(e => {
        io.local.to('userLots').emit('time', Date.now())
    }, 1000)

    sockets.io = io
    app.set('io', io);

    io.on('connect', async socket => {
        console.log(`К сокету присоединился юзер: ${socket.request.user ? socket.request.user.id : 'Без авторизации'}`);
        if(socket.request.user)
            socket.emit('apikey')

        //console.log('юзер в сокете:', socket.request.user)

        const eventHandlers = {
            lots: Lots
        };

        for (let type in eventHandlers) {
            for (let handler of Object.getOwnPropertyNames(eventHandlers[type])) {
                socket.on(handler, value => eventHandlers[type][handler](io, socket, value))
            }
        }

        socket.on('apikey', async (api_key) => {
            if(!socket.request.user || !api_key)
                return false;

            await models.User.update({
                api_key
            }, {
                where: {id: socket.request.user.id}
            });
        })

        socket.on('trade', async res => {
            if(res.result !== 4 || !res.tradeInfo)
                return console.log(res)

            let lot = await models.Lot.findByPk(parseInt(res.tradeInfo.message), {
                include: ['GameItem', {
                    model: models.LotResult,
                    include: ['User']
                }]
            })

            if(lot.status_id === 6)
                return console.log('already 6')

            let {float} = await models.CsParams.findOne({where: {asset_id: lot.GameItem.asset_id}})

            const items = await Bot.getUserInventory(lot.GameItem.app_id, lot.LotResult.User.steamid64)
            const floats = await Promise.all(items
                .filter(item => item.market_hash_name === lot.GameItem.market_hash_name)
                .map(async item => await Bot.getFloat(lot.LotResult.User.steamid64, item))
            )

            if(!floats.includes(float))
                return console.log('no floats')

            lot.status_id = 6
            await lot.save()

            const lastBid = await models.Bid.findOne({
                where: {
                    lot_id: lot.id
                },
                order: [['date', 'DESC']],
                raw: true,
            });

            await models.Transaction.create({
                user_id: lot.owner_id,
                lot_id: lot.id,
                lot_name: lot.GameItem.name,
                type_id: 4,
                amount: lot.price
            });

            await models.Transaction.create({
                user_id: lot.LotResult.User.id,
                lot_id: lot.id,
                lot_name: lot.GameItem.name,
                type_id: 5,
                amount: lastBid.bet_size
            });

            return console.log('ok!')
        })

        const interval = setInterval(sendWaitingLots, 60 * 1000, socket)

        socket.on('disconnect', (reason) => {
            console.log(reason)
            clearInterval(interval)
        });
    });
};

async function sendWaitingLots(socket){
    if(!socket.request.user.id)
        return false

    const lots = await findWaitingLots(socket.request.user.id)

    if(lots.length){
        const {escrow} = await Bot.getUserEscrow(socket.request.user.trade_url)
        if(escrow === false){
            const [update, updatedUser] = await models.User.update({
                trade_url: null
            }, {
                where: {
                    id: req.user.id
                }
            });
        }
    }

    for(let lot of lots){
        let steamId = lot.won ? lot.owner.steamid64 : lot.winner.steamid64
        let tradeLink = lot.won ? lot.owner.trade_url : lot.winner.trade_url
        let tradeUrl = new URL(tradeLink)
        let partner = tradeUrl.searchParams.get('partner')
        let token = tradeUrl.searchParams.get('token')

        const trade = {
            partner,
            token,
            steamId,
            appid: lot.app_id,
            assetid: lot.asset_id,
            message: lot.id.toString(),
            get: lot.won,
            params: {
                trade_offer_access_token: token
            }
        }

        socket.emit('trade', trade)
    }
}

//Эту функцию надо оптимизировать
async function findWaitingLots(userId){
    if(!userId)
        return []

    const lots = await models.Lot.findAll({
        where: {
            status_id: 4,
            [Op.or]: [{
                owner_id: userId
            }, {
                '$LotResult.winner_id$': userId
            }]
        },
        include: [{
            model: models.GameItem,
            include: [{
                model: models.Games
            }]
        }, {
            model: models.LotResult,
            include: ['User']
        }]
    });

    const owners = await models.User.findAll({
        where: {
            id: {
                [Op.in]: lots.map(lot => lot.owner_id)
            }
        }
    })

    return lots.map(lot => {
        return {
            asset_id: lot.GameItem.asset_id,
            app_id: lot.app_id,
            id: lot.id,
            won: lot.LotResult.User.id === userId,
            winner: lot.LotResult.User,
            owner: owners.find(owner => owner.id === lot.owner_id),
            bot: lot.status_id === 8
        }
    })
}

module.exports = sockets;
