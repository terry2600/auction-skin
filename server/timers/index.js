const models = require('../models');
const Bot = require('../config/bots');

let timerList = {
    timers: {},

    addLotTimer(id, startDate, io) {
        const timerEnd = new Date(startDate);
        this.timers[id] = setInterval(() => {
            let timeDistance = timerEnd - Date.now();
            if (timeDistance > 0) {
                io.to(`lot${id}`).emit('timerLot', timeDistance)
            } else {
                clearInterval(this.timers[id]);
                models.Lot.update({
                    status_id: 3
                }, {
                    where: {id}
                });
                this.addBidTimer(id, io)
            }
        }, 1000)
    },

    addBidTimer(lotId, io) {
        let timeEnd = 10000;

        this.timers[lotId] = setInterval(async () => {
            if (timeEnd > 0) {
                io.to(`lot${lotId}`).emit('timerBid', timeEnd -= 1000)
            } else {
                console.log('Здесь');
                clearInterval(this.timers[lotId]);

                const {owner_id, game_item_id, price, cost_of_participation} = await models.Lot.findByPk(lotId, {
                    attributes: ['owner_id', 'game_item_id', 'price', 'cost_of_participation'],
                    raw: true
                });

                const item = await models.GameItem.findByPk(game_item_id, {
                    raw: true
                });

                const owner = await models.User.findOne({
                    where: {
                        id: owner_id
                    },
                    raw: true
                });

                const lastBid = await models.Bid.findOne({
                    where: {
                        lot_id: lotId
                    },
                    order: [['date', 'DESC']],
                    raw: true,
                });

                const participants = await models.Participants.findAll({
                    where: {
                        lot_id: lotId
                    }
                });

                let status_id = 7;

                let transaction = await models.sequelize.transaction();
                try {
                    if (!lastBid && owner.role_id === 1) {
                        console.log('Созданный админом лот отменен');
                        const [numberOfUpdatedLots] = await models.Lot.update({
                            status_id,
                            end_date: Date.now()
                        }, {
                            where: {id: lotId},
                            transaction
                        });

                        await models.Transaction.bulkCreate(participants.map(participant => {
                                return {
                                    user_id: participant.user_id,
                                    lot_id: lotId,
                                    lot_name: item.name,
                                    type: 2,
                                    amount: cost_of_participation
                                }
                            }),
                            {transaction});

                        await transaction.commit();
                    } else if (!lastBid && owner.role_id !== 1) {
                        console.log('Созданный пользователем лот отменен');
                        const [numberOfUpdatedLots] = await models.Lot.update({
                            status_id,
                            end_date: Date.now()
                        }, {
                            where: {id: lotId},
                            transaction
                        });


                        const {asset_id, app_id} = await models.GameItem.findByPk(game_item_id, {
                            attributes: ['asset_id', 'app_id'],
                            raw: true
                        });

                        await models.Transaction.bulkCreate(participants.map(participant => {
                                return {
                                    user_id: participant.user_id,
                                    lot_id: lotId,
                                    lot_name: item.name,
                                    type: 2,
                                    amount: cost_of_participation
                                }
                            }),
                            {transaction});

                        await transaction.commit();

                        //Bot.getUserItem(owner.steamid64, owner.trade_url, asset_id, app_id);
                    } else {
                        status_id = 4;
                        console.log('созданный лот идет на передачу епт');
                        const [numberOfUpdatedLots] = await models.Lot.update({
                            status_id,
                            end_date: Date.now()
                        }, {
                            attributes: ['role_id'],
                            where: {id: lotId},
                            transaction
                        });

                        const result = await models.LotResult.create({
                            lot_id: lotId,
                            winner_id: lastBid.user_id,
                            purchase_price: lastBid.bet_size,
                        }, transaction);

                        const winner = await models.User.findByPk(lastBid.user_id, {
                            attributes: ['avatar', 'steam_nick', 'trade_url', 'id'],
                            raw: true,
                            transaction
                        });

                        await transaction.commit();
                        io.to(`lot${lotId}`).emit('lotEnded', {
                            winner_avatar: winner.avatar,
                            winner_nick: winner.steam_nick,
                            item_icon: item.icon_url,
                            item_name: item.name
                        })
                        //Bot.sendItemToWinner(lotId, winner.trade_url, item.asset_id, item.app_id, 2, 1);
                    }

                    io.to(`lot${lotId}`).emit('closedLot', {id: lotId, status: status_id})
                } catch (err) {
                    console.log('говно жопа ошибка передачи пидор ', err)
                }
            }
        }, 1000)
    },
};


module.exports = timerList;
